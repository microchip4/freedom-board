
/*
;
;`-',
;.-.;""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""";, ,;
;`-';         SD Card MP3 Player         V2.0   /\    |\                    dsPic33FJ128GP802       ;`-';
;.-.;         Vector Mapping GPS               //\/''/''/                   MPLAB X IDE v4.05       ;.-.;
;`-';         Astronomical Clock              //''||' ||                   XC16 Compiler v1.33      ;`-';
;.-.;         Calendar and Game              |/   |/  |/2016-18           Brek Martin  12/02/18     ;.-.;
;`-';                                                                                               ;`-';
;"""'"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""'"""'
 */

//
#include <stdio.h>
#include <math.h>
#include "HardwareProfile.h"
#include "FSIO.h"
#include "ImageDecoder.h"
#include "Limits.h"
//
// serial output
#define SSERIAL _LATB4
// vs1003b pin defines
#define VSDREQ PORTBbits.RB12
#define VSXCS _LATB13
#define VSXDCS _LATB14
// LED pin define
#define LED_PIN _LATA4
// LCD shift register pin defines
#define SERDAT_PIN _LATB0
#define SRCLK_PIN _LATA1
// LCD module pin defines
#define LCD_CS2 _LATB2
#define LCD_CS1 _LATB3
#define LCD_DI _LATB5
#define LCD_EN _LATB15
// joystick shift register pin defines
#define JOYPAR_PIN _LATA0
#define JOYDAT_PIN PORTBbits.RB10
// manual spi defines
#define SPICLOCKLAT	_LATB8
#define SPIINPORT PORTBbits.RB6
#define SPIOUTLAT _LATB7
// gps module pins
#define GPSIN PORTBbits.RB11
#define GPSPWR_PIN _LATB1
// sine text frequency
#define sinfreq 2
// number of poi icons
#define numicons 13
//
// local astronomy constants
#define epoch 2444238.5
#define elonge 278.833540
#define elongp 282.596403
#define eccent 0.016718
#define sunsmax 1.495985e8
#define sunangsiz 0.533128
#define mmlong 64.975464
#define mmlongp 349.383063
#define mlnode 151.950429
#define minc 5.145396
#define mecc 0.054900
#define mangsiz 0.5181
#define msmax 384401.0
#define mparallax 0.9507
#define synmonth 29.53058868
#define lunatbase 2423436.0
#define earthrad 6378.16
#define sgn(x) (((x) < 0) ? -1 : ((x) > 0 ? 1 : 0))
#define abs(x) ((x) < 0 ? (-(x)) : (x))
#define fixangle(a) ((a) - 360.0 * (floor((a) / 360.0)))
#define torad(d) ((d) * d2r)
#define todeg(d) ((d) * (180.0 / PI))
#define dsin(x) (sin(torad((x))))
#define dcos(x) (cos(torad((x))))
#define EPSILON 1E-6
#define SunDia 0.53
#define AirRefr 34.0/60.0
//
// fixed width printed character and symbol definitions
// HD44780 character LCD compatible character glyphs,
// except "3", "B", and "s" characters are modified.
const unsigned char charmap[480] = {
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, // space
    0b00000000, 0b00000000, 0b01001111, 0b00000000, 0b00000000, // !
    0b00000000, 0b00000111, 0b00000000, 0b00000111, 0b00000000, // "
    0b00010100, 0b01111111, 0b00010100, 0b01111111, 0b00010100, // #
    0b00100100, 0b00101010, 0b01111111, 0b00101010, 0b00010010, // $
    0b00100011, 0b00010011, 0b00001000, 0b01100100, 0b01100010, // %
    0b00110110, 0b01001001, 0b01010101, 0b00100010, 0b01010000, // &
    0b00000000, 0b00000101, 0b00000011, 0b00000000, 0b00000000, // '
    0b00000000, 0b00011100, 0b00100010, 0b01000001, 0b00000000, // (
    0b00000000, 0b01000001, 0b00100010, 0b00011100, 0b00000000, // )
    0b00010100, 0b00001000, 0b00111110, 0b00001000, 0b00010100, // *
    0b00001000, 0b00001000, 0b00111110, 0b00001000, 0b00001000, // +
    0b00000000, 0b01010000, 0b00110000, 0b00000000, 0b00000000, // ,
    0b00001000, 0b00001000, 0b00001000, 0b00001000, 0b00001000, // -
    0b00000000, 0b01100000, 0b01100000, 0b00000000, 0b00000000, // .
    0b00100000, 0b00010000, 0b00001000, 0b00000100, 0b00000010, // /
    0b00111110, 0b01010001, 0b01001001, 0b01000101, 0b00111110, // 0
    0b00000000, 0b01000010, 0b01111111, 0b01000000, 0b00000000, // 1
    0b01000010, 0b01100001, 0b01010001, 0b01001001, 0b01000110, // 2
    0b00100010, 0b01000001, 0b01001001, 0b01001001, 0b00110110, // 3
    0b00011000, 0b00010100, 0b00010010, 0b01111111, 0b00010000, // 4
    0b00100111, 0b01000101, 0b01000101, 0b01000101, 0b00111001, // 5
    0b00111100, 0b01001010, 0b01001001, 0b01001001, 0b00110000, // 6
    0b00000001, 0b01110001, 0b00001001, 0b00000101, 0b00000011, // 7
    0b00110110, 0b01001001, 0b01001001, 0b01001001, 0b00110110, // 8
    0b00000110, 0b01001001, 0b01001001, 0b00101001, 0b00011110, // 9
    0b00000000, 0b00110110, 0b00110110, 0b00000000, 0b00000000, // :
    0b00000000, 0b01010110, 0b00110110, 0b00000000, 0b00000000, // ;
    0b00001000, 0b00010100, 0b00100010, 0b01000001, 0b00000000, // <
    0b00010100, 0b00010100, 0b00010100, 0b00010100, 0b00010100, // =
    0b00000000, 0b01000001, 0b00100010, 0b00010100, 0b00001000, // >
    0b00000010, 0b00000001, 0b01010001, 0b00001001, 0b00000110, // ?
    0b00110010, 0b01001001, 0b01111001, 0b01000001, 0b00111110, // @
    0b01111110, 0b00010001, 0b00010001, 0b00010001, 0b01111110, // A
    0b01111111, 0b01001001, 0b01001001, 0b01001001, 0b00110110, // B
    0b00111110, 0b01000001, 0b01000001, 0b01000001, 0b00100010, // C
    0b01111111, 0b01000001, 0b01000001, 0b00100010, 0b00011100, // D
    0b01111111, 0b01001001, 0b01001001, 0b01001001, 0b01000001, // E
    0b01111111, 0b00001001, 0b00001001, 0b00001001, 0b00000001, // F
    0b00111110, 0b01000001, 0b01001001, 0b01001001, 0b01111010, // G
    0b01111111, 0b00001000, 0b00001000, 0b00001000, 0b01111111, // H
    0b00000000, 0b01000001, 0b01111111, 0b01000001, 0b00000000, // I
    0b00100000, 0b01000000, 0b01000001, 0b00111111, 0b00000001, // J
    0b01111111, 0b00001000, 0b00010100, 0b00100010, 0b01000001, // K
    0b01111111, 0b01000000, 0b01000000, 0b01000000, 0b01000000, // L
    0b01111111, 0b00000010, 0b00001100, 0b00000010, 0b01111111, // M
    0b01111111, 0b00000100, 0b00001000, 0b00010000, 0b01111111, // N
    0b00111110, 0b01000001, 0b01000001, 0b01000001, 0b00111110, // O
    0b01111111, 0b00001001, 0b00001001, 0b00001001, 0b00000110, // P
    0b00111110, 0b01000001, 0b01010001, 0b00100001, 0b01011110, // Q
    0b01111111, 0b00001001, 0b00011001, 0b00101001, 0b01000110, // R
    0b01000110, 0b01001001, 0b01001001, 0b01001001, 0b00110001, // S
    0b00000001, 0b00000001, 0b01111111, 0b00000001, 0b00000001, // T
    0b00111111, 0b01000000, 0b01000000, 0b01000000, 0b00111111, // U
    0b00011111, 0b00100000, 0b01100000, 0b00100000, 0b00011111, // V
    0b00111111, 0b01000000, 0b00111000, 0b01000000, 0b00111111, // W
    0b01100011, 0b00010100, 0b00001000, 0b00010100, 0b01100011, // X
    0b00000111, 0b00001000, 0b01110000, 0b00001000, 0b00000111, // Y
    0b01100001, 0b01010001, 0b01001001, 0b01000101, 0b01000011, // Z
    0b00000000, 0b00000000, 0b01111111, 0b01000001, 0b01000001, // [
    0b00000010, 0b00000100, 0b00001000, 0b00010000, 0b00100000, // /
    0b00000000, 0b00000000, 0b01000001, 0b01000001, 0b01111111, // ]
    0b00000100, 0b00000010, 0b00000001, 0b00000010, 0b00000100, // ^
    0b01000000, 0b01000000, 0b01000000, 0b01000000, 0b01000000, // _
    0b00000000, 0b00000000, 0b00000001, 0b00000010, 0b00000000, // `
    0b00100000, 0b01010100, 0b01010100, 0b01010100, 0b01111000, // a
    0b01111111, 0b01001000, 0b01000100, 0b01000100, 0b00111000, // b
    0b00111000, 0b01000100, 0b01000100, 0b01000100, 0b00100000, // c
    0b00111000, 0b01000100, 0b01000100, 0b01001000, 0b01111111, // d
    0b00111000, 0b01010100, 0b01010100, 0b01010100, 0b00011000, // e
    0b00001000, 0b01111110, 0b00001001, 0b00000001, 0b00000010, // f
    0b00001100, 0b01010010, 0b01010010, 0b01010010, 0b00111110, // g
    0b01111111, 0b00001000, 0b00000100, 0b00000100, 0b01111000, // h
    0b00000000, 0b01000100, 0b01111101, 0b01000000, 0b00000000, // i
    0b00100000, 0b01000000, 0b01000100, 0b00111101, 0b00000000, // j
    0b01111111, 0b00010000, 0b00101000, 0b01000100, 0b00000000, // k
    0b00000000, 0b01000001, 0b01111111, 0b01000000, 0b00000000, // l
    0b01111100, 0b00000100, 0b00011000, 0b00000100, 0b01111000, // m
    0b01111100, 0b00001000, 0b00000100, 0b00000100, 0b01111000, // n
    0b00111000, 0b01000100, 0b01000100, 0b01000100, 0b00111000, // o
    0b01111100, 0b00010100, 0b00010100, 0b00010100, 0b00001000, // p
    0b00001000, 0b00010100, 0b00010100, 0b00011000, 0b01111100, // q
    0b01111100, 0b00001000, 0b00000100, 0b00000100, 0b00001000, // r
    0b00001000, 0b01010100, 0b01010100, 0b01010100, 0b00100000, // s
    0b00000100, 0b00111111, 0b01000100, 0b01000000, 0b00100000, // t
    0b00111100, 0b01000000, 0b01000000, 0b00100000, 0b01111100, // u
    0b00011100, 0b00100000, 0b01000000, 0b00100000, 0b00011100, // v
    0b00111100, 0b01000000, 0b00110000, 0b01000000, 0b00111100, // w
    0b01000100, 0b00101000, 0b00010000, 0b00101000, 0b01000100, // x
    0b00001100, 0b01010000, 0b01010000, 0b01010000, 0b00111100, // y
    0b01000100, 0b01100100, 0b01010100, 0b01001100, 0b01000100, // z
    0b00000000, 0b00001000, 0b00110110, 0b01000001, 0b00000000, // {
    0b00000000, 0b00000000, 0b01111111, 0b00000000, 0b00000000, // |
    0b00000000, 0b01000001, 0b00110110, 0b00001000, 0b00000000, // }
    0b00000000, 0b00000010, 0b00000001, 0b00000010, 0b00000001, // ~
    0b01010101, 0b00101010, 0b01010101, 0b00101010, 0b01010101 // cursor
};
//
// bold variable width character map by Anikki 2008
// based on IBM CGA character set with many tweaks
const unsigned char charmapb[672] = {
    0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, // space
    0b00000000, 0b00000000, 0b00000110, 0b01011111, 0b01011111, 0b00000110, 0b00000000, // !
    0b00000000, 0b00000011, 0b00000011, 0b00000000, 0b00000011, 0b00000011, 0b00000000, // "
    0b00010100, 0b01111111, 0b01111111, 0b00010100, 0b01111111, 0b01111111, 0b00010100, // #
    0b00100100, 0b00101110, 0b01101011, 0b01101011, 0b00111010, 0b00010010, 0b00000000, // $
    0b01000110, 0b01100110, 0b00110000, 0b00011000, 0b00001100, 0b01100110, 0b01100010, // %
    0b00110000, 0b01111010, 0b01001111, 0b01011101, 0b00110111, 0b01110100, 0b01001000, // &
    0b00000000, 0b00000100, 0b00000111, 0b00000011, 0b00000000, 0b00000000, 0b00000000, // '
    0b00000000, 0b00011100, 0b00111110, 0b01100011, 0b01000001, 0b00000000, 0b00000000, // (
    0b00000000, 0b01000001, 0b01100011, 0b00111110, 0b00011100, 0b00000000, 0b00000000, // )
    0b00001000, 0b01010100, 0b01111100, 0b00111000, 0b01111100, 0b01010100, 0b00001000, // *
    0b00000000, 0b00010000, 0b00010000, 0b01111100, 0b00010000, 0b00010000, 0b00000000, // +
    0b00000000, 0b01011000, 0b00111000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, // ,    
    0b00000000, 0b00001000, 0b00001000, 0b00001000, 0b00001000, 0b00000000, 0b00000000, // -
    0b00000000, 0b01100000, 0b01100000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, // .
    0b00000000, 0b00000000, 0b00110000, 0b00011000, 0b00001100, 0b00000000, 0b00000000, // /
    0b00111110, 0b01111111, 0b01000001, 0b01000001, 0b01111111, 0b00111110, 0b00000000, // 0
    0b01000000, 0b01000010, 0b01111111, 0b01111111, 0b01000000, 0b01000000, 0b00000000, // 1
    0b01100010, 0b01110011, 0b01011001, 0b01001001, 0b01101111, 0b01100110, 0b00000000, // 2
    0b00100010, 0b01100011, 0b01001001, 0b01001001, 0b01111111, 0b00110110, 0b00000000, // 3
    0b00011000, 0b00011100, 0b00010110, 0b01010011, 0b01111111, 0b01010000, 0b00000000, // 4
    0b00100111, 0b01100101, 0b01000101, 0b01000101, 0b01111101, 0b00111001, 0b00000000, // 5
    0b00111100, 0b01111110, 0b01001011, 0b01001001, 0b01111001, 0b00110000, 0b00000000, // 6
    0b00000011, 0b00000011, 0b01110001, 0b01111001, 0b00001111, 0b00000111, 0b00000000, // 7
    0b00110110, 0b01111111, 0b01001001, 0b01001001, 0b01111111, 0b00110110, 0b00000000, // 8
    0b00000110, 0b01001111, 0b01001001, 0b01101001, 0b00111111, 0b00011110, 0b00000000, // 9
    0b00000000, 0b00000000, 0b01100110, 0b01100110, 0b00000000, 0b00000000, 0b00000000, // :
    0b00000000, 0b01000000, 0b01110011, 0b00110011, 0b00000000, 0b00000000, 0b00000000, // ;
    0b00000000, 0b00001000, 0b00011100, 0b00110110, 0b01100011, 0b01000001, 0b00000000, // <
    0b01000110, 0b01100110, 0b00110000, 0b00011000, 0b00001100, 0b01100110, 0b01100010, // %
    0b00000000, 0b01000001, 0b01100011, 0b00110110, 0b00011100, 0b00001000, 0b00000000, // >
    0b00000010, 0b00000011, 0b01010001, 0b01011001, 0b00001111, 0b00000110, 0b00000000, // ?
    0b00000000, 0b00010100, 0b00010100, 0b00010100, 0b00010100, 0b00000000, 0b00000000, // =
    0b01111100, 0b01111110, 0b00010011, 0b00010011, 0b01111110, 0b01111100, 0b00000000, // A
    0b01000001, 0b01111111, 0b01111111, 0b01001001, 0b01001001, 0b01111111, 0b00110110, // B
    0b00011100, 0b00111110, 0b01100011, 0b01000001, 0b01000001, 0b01100011, 0b00100010, // C
    0b01000001, 0b01111111, 0b01111111, 0b01000001, 0b01100011, 0b00111110, 0b00011100, // D
    0b01000001, 0b01111111, 0b01111111, 0b01001001, 0b01011101, 0b01000001, 0b01100011, // E
    0b01000001, 0b01111111, 0b01111111, 0b01001001, 0b00011101, 0b00000001, 0b00000011, // F
    0b00011100, 0b00111110, 0b01100011, 0b01000001, 0b01010001, 0b01110011, 0b01110010, // G
    0b01111111, 0b01111111, 0b00001000, 0b00001000, 0b01111111, 0b01111111, 0b00000000, // H
    0b00000000, 0b01000001, 0b01111111, 0b01111111, 0b01000001, 0b00000000, 0b00000000, // I
    0b00110000, 0b01110000, 0b01000000, 0b01000001, 0b01111111, 0b00111111, 0b00000001, // J
    0b01000001, 0b01111111, 0b01111111, 0b00001000, 0b00011100, 0b01110111, 0b01100011, // K
    0b01000001, 0b01111111, 0b01111111, 0b01000001, 0b01000000, 0b01100000, 0b01110000, // L
    0b01111111, 0b01111111, 0b00001110, 0b00011100, 0b00001110, 0b01111111, 0b01111111, // M
    0b01111111, 0b01111111, 0b00000110, 0b00001100, 0b00011000, 0b01111111, 0b01111111, // N
    0b00011100, 0b00111110, 0b01100011, 0b01000001, 0b01100011, 0b00111110, 0b00011100, // O
    0b01000001, 0b01111111, 0b01111111, 0b01001001, 0b00001001, 0b00001111, 0b00000110, // P
    0b00011110, 0b00111111, 0b00100001, 0b01110001, 0b01111111, 0b01011110, 0b00000000, // Q
    0b01000001, 0b01111111, 0b01111111, 0b00001001, 0b00011001, 0b01111111, 0b01100110, // R
    0b00100010, 0b01100111, 0b01001101, 0b01011001, 0b01110011, 0b00100010, 0b00000000, // S
    0b00000011, 0b01000001, 0b01111111, 0b01111111, 0b01000001, 0b00000011, 0b00000000, // T
    0b00111111, 0b01111111, 0b01000000, 0b01000000, 0b01111111, 0b00111111, 0b00000000, // U
    0b00011111, 0b00111111, 0b01100000, 0b01100000, 0b00111111, 0b00011111, 0b00000000, // V
    0b01111111, 0b01111111, 0b00110000, 0b00011000, 0b00110000, 0b01111111, 0b01111111, // W
    0b01100011, 0b00110110, 0b00011100, 0b00011100, 0b00110110, 0b01100011, 0b00000000, // X
    0b00000111, 0b01001111, 0b01111000, 0b01111000, 0b01001111, 0b00000111, 0b00000000, // Y
    0b01000111, 0b01100011, 0b01110001, 0b01011001, 0b01001101, 0b01100111, 0b01110011, // Z
    0b00000000, 0b00000000, 0b01111111, 0b01111111, 0b01000001, 0b00000000, 0b00000000, // [
    0b00000000, 0b00110000, 0b00011000, 0b00001100, 0b00000110, 0b00000000, 0b00000000, // /
    0b00000000, 0b00000000, 0b01000001, 0b01111111, 0b01111111, 0b00000000, 0b00000000, // ]
    0b00000000, 0b00000100, 0b00000010, 0b00000001, 0b00000010, 0b00000100, 0b00000000, // ^
    0b00000000, 0b01000000, 0b01000000, 0b01000000, 0b01000000, 0b01000000, 0b00000000, // _
    0b00000000, 0b00000000, 0b00000011, 0b00000111, 0b00000100, 0b00000000, 0b00000000, // `
    0b00100000, 0b01110100, 0b01010100, 0b01010100, 0b00111100, 0b01111000, 0b01000000, // a
    0b01000001, 0b01111111, 0b00111111, 0b01001000, 0b01001000, 0b01111000, 0b00110000, // b
    0b00111000, 0b01111100, 0b01000100, 0b01000100, 0b01101100, 0b00101000, 0b00000000, // c
    0b00110000, 0b01111000, 0b01001000, 0b01001001, 0b00111111, 0b01111111, 0b01000000, // d
    0b00111000, 0b01111100, 0b01010100, 0b01010100, 0b01011100, 0b00011000, 0b00000000, // e
    0b01001000, 0b01111110, 0b01111111, 0b01001001, 0b00000011, 0b00000010, 0b00000000, // f
    0b10011000, 0b10111100, 0b10100100, 0b10100100, 0b11111000, 0b01111100, 0b00000100, // g
    0b01000001, 0b01111111, 0b01111111, 0b00001000, 0b00000100, 0b01111100, 0b01111000, // h
    0b00000000, 0b01000100, 0b01111101, 0b01111101, 0b01000000, 0b00000000, 0b00000000, // i
    0b01100000, 0b11100000, 0b10000000, 0b10000000, 0b11111101, 0b01111101, 0b00000000, // j
    0b01000001, 0b01111111, 0b01111111, 0b00010000, 0b00111000, 0b01101100, 0b01000100, // k
    0b00000000, 0b01000001, 0b01111111, 0b01111111, 0b01000000, 0b00000000, 0b00000000, // l
    0b01111100, 0b01111100, 0b00011000, 0b00111000, 0b00011100, 0b01111100, 0b01111000, // m
    0b01111100, 0b01111100, 0b00000100, 0b00000100, 0b01111100, 0b01111000, 0b00000000, // n
    0b00111000, 0b01111100, 0b01000100, 0b01000100, 0b01111100, 0b00111000, 0b00000000, // o
    0b10000100, 0b11111100, 0b11111000, 0b10100100, 0b00100100, 0b00111100, 0b00011000, // p
    0b00011000, 0b00111100, 0b00100100, 0b10100100, 0b11111000, 0b11111100, 0b10000100, // q
    0b01000100, 0b01111100, 0b01111000, 0b01001100, 0b00000100, 0b00011100, 0b00011000, // r
    0b01001000, 0b01011100, 0b01010100, 0b01010100, 0b01110100, 0b00100100, 0b00000000, // s
    0b00000000, 0b00000100, 0b00111110, 0b01111111, 0b01000100, 0b00100100, 0b00000000, // t
    0b00111100, 0b01111100, 0b01000000, 0b01000000, 0b00111100, 0b01111100, 0b01000000, // u
    0b00011100, 0b00111100, 0b01100000, 0b01100000, 0b00111100, 0b00011100, 0b00000000, // v
    0b00111100, 0b01111100, 0b01110000, 0b00111000, 0b01110000, 0b01111100, 0b00111100, // w
    0b01000100, 0b01101100, 0b00111000, 0b00010000, 0b00111000, 0b01101100, 0b01000100, // x
    0b10011100, 0b10111100, 0b10100000, 0b10100000, 0b11111100, 0b01111100, 0b00000000, // y
    0b01001100, 0b01100100, 0b01110100, 0b01011100, 0b01001100, 0b01100100, 0b00000000, // z
    0b00000000, 0b00001000, 0b00111110, 0b01110111, 0b01000001, 0b00000000, 0b00000000, // {
    0b00000000, 0b00000000, 0b00000000, 0b01111111, 0b00000000, 0b00000000, 0b00000000, // |
    0b00000000, 0b00000000, 0b01000001, 0b01110111, 0b00111110, 0b00001000, 0b00000000, // }
    0b00000010, 0b00000011, 0b00000001, 0b00000011, 0b00000010, 0b00000011, 0b00000001, // ~
    0b10101010, 0b01010101, 0b10101010, 0b01010101, 0b10101010, 0b01010101, 0b10101010 // cursor     
};
//
// Amiga styled mouse pointer bitmap
const char mpointer[22] = {
    0b11111100, 0b00000000, 0b10000010, 0b00000000, 0b11111010, 0b00000000,
    0b11110100, 0b00000000, 0b11111010, 0b00000000, 0b11111101, 0b00000000,
    0b01101110, 0b10000000, 0b00000111, 0b01000000, 0b00000011, 0b10100000,
    0b00000001, 0b11000000, 0b00000000, 0b10000000
};
//
// map point of interest icon bitmaps
const char poiicons[91] = {
    0b00111110, 0b01111111, 0b01000001, 0b01010111, 0b01000111, 0b01111111, 0b00111110, // car park
    0b00111110, 0b01111111, 0b01011111, 0b01000001, 0b01011111, 0b01111111, 0b00111110, // restroom
    0b00000000, 0b00001110, 0b00011010, 0b00101010, 0b00011010, 0b00001110, 0b00000000, // hut
    0b00000000, 0b01000011, 0b00111100, 0b00010011, 0b00001100, 0b00000011, 0b00000000, // waterfall
    0b00111110, 0b01000101, 0b01001001, 0b01010001, 0b01001001, 0b01000101, 0b00111110, // camp site
    0b01110110, 0b01010010, 0b01000110, 0b01010110, 0b01000110, 0b01010010, 0b01110110, // rest area
    0b00011000, 0b00100100, 0b00100100, 0b00011000, 0b00100100, 0b00100100, 0b00011000, // lookout 
    0b00111110, 0b01111111, 0b01000001, 0b01010111, 0b01011111, 0b01111111, 0b00111110, // fireplace
    0b00111110, 0b01000101, 0b01001001, 0b01001001, 0b01001001, 0b01000101, 0b00111110, // bridge
    0b00111110, 0b00010100, 0b00111110, 0b00010100, 0b00111110, 0b00010100, 0b00111110, // gate
    0b00111110, 0b01111111, 0b01111111, 0b01010001, 0b01111111, 0b01111111, 0b00111110, // information
    0b00111110, 0b01111111, 0b01111001, 0b01000111, 0b01111001, 0b01111111, 0b00111110, // radio tower
    0b01100011, 0b01110111, 0b00111110, 0b00011100, 0b00111110, 0b01110111, 0b01100011, // creek crossing
};
//
// Blop: Recorded by Mark DiAngelo
const unsigned char blop[240] = {
    0xFF, 0xF3, 0x20, 0xC4, 0x00, 0x09, 0xA0, 0x32, 0xBC, 0x5F, 0x43, 0x10, 0x02, 0x03, 0xE3, 0xB6,
    0x40, 0x00, 0xF7, 0x38, 0x80, 0x02, 0x28, 0x00, 0x82, 0x0F, 0x88, 0x0E, 0x02, 0x0E, 0xC4, 0x87,
    0x29, 0x10, 0x70, 0xF9, 0x77, 0x94, 0x39, 0xF2, 0x8E, 0xDC, 0x08, 0x56, 0x7F, 0xE1, 0xFE, 0x53,
    0xC3, 0x1F, 0xE1, 0xF0, 0xFF, 0xF3, 0x22, 0xC4, 0x00, 0x09, 0xC8, 0x8E, 0xD4, 0x01, 0x8F, 0xC0,
    0x00, 0x5D, 0x9B, 0x0F, 0x75, 0xF9, 0xEA, 0x24, 0xB6, 0xFF, 0x8C, 0x22, 0xD3, 0x48, 0xAC, 0x4E,
    0x9E, 0x00, 0x93, 0x84, 0x3D, 0xFE, 0x68, 0xE3, 0x65, 0xC6, 0x60, 0x42, 0xE4, 0x68, 0xFC, 0x11,
    0xF7, 0x9E, 0xD5, 0xEF, 0x8C, 0x16, 0xFF, 0xF5, 0x00, 0xFF, 0xF3, 0x20, 0xC4, 0x00, 0x08, 0xD8,
    0x7A, 0xCE, 0x59, 0xC8, 0x48, 0x02, 0x49, 0xC6, 0x48, 0x0E, 0xDA, 0x00, 0x8E, 0x3C, 0x61, 0xC6,
    0x69, 0x49, 0x45, 0x1F, 0x1C, 0x76, 0xB3, 0x13, 0x00, 0x2C, 0x04, 0xC4, 0xCD, 0x6E, 0xF2, 0xB6,
    0x1F, 0x68, 0x00, 0xC0, 0x00, 0xC0, 0x1E, 0x07, 0xA1, 0x49, 0x88, 0x29, 0xA0, 0xFF, 0xF3, 0x20,
    0xC4, 0x00, 0x08, 0xD8, 0x76, 0xC5, 0xB8, 0x00, 0xCA, 0x1B, 0x3F, 0xA4, 0x13, 0x96, 0xD0, 0x15,
    0x55, 0x7E, 0x33, 0x33, 0x7C, 0x6A, 0x5A, 0xFF, 0x85, 0x06, 0x9C, 0xF0, 0xA0, 0x90, 0x50, 0x50,
    0x50, 0x59, 0x72, 0x0A, 0x3B, 0xFF, 0xFF, 0x9F, 0xE0, 0xA2, 0xAF, 0xC0, 0xA1, 0xA9, 0x88, 0x29,
    0xA0, 0xFF, 0xF3, 0x20, 0xC4, 0x00, 0x00, 0x00, 0x03, 0x48, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
//
// Tiny Button: Recorded by Mike Koenig
const unsigned char tiny[368] = {
    0xFF, 0xF3, 0x22, 0xC4, 0x00, 0x09, 0x68, 0xC6, 0xFD, 0x91, 0x81, 0x38, 0x00, 0x3C, 0xF3, 0xCE,
    0x30, 0xC3, 0x00, 0x03, 0x05, 0x17, 0x21, 0x1E, 0x00, 0x04, 0x3C, 0x1F, 0x83, 0xF1, 0x2E, 0x69,
    0xBC, 0x00, 0x00, 0x00, 0x24, 0x48, 0x43, 0xE3, 0xC0, 0x30, 0x58, 0xC1, 0x10, 0xDC, 0x38, 0x5B,
    0xCF, 0xB8, 0xBA, 0x62, 0x08, 0xFF, 0xF3, 0x20, 0xC4, 0x00, 0x09, 0x90, 0xCE, 0xC6, 0xF9, 0xCB,
    0x30, 0x00, 0x56, 0x86, 0x76, 0xCD, 0x00, 0x30, 0x00, 0x05, 0x41, 0x34, 0xED, 0x7B, 0x9C, 0x93,
    0x5A, 0xD6, 0xFC, 0xB1, 0xD3, 0x70, 0xEF, 0x6D, 0x37, 0xE6, 0x56, 0xD5, 0x3F, 0xFD, 0xD1, 0x94,
    0x58, 0x90, 0x09, 0x16, 0x24, 0x49, 0x2D, 0xE5, 0x40, 0xFF, 0xF3, 0x20, 0xC4, 0x00, 0x09, 0x50,
    0xA2, 0x8D, 0x91, 0x4A, 0x10, 0x00, 0x05, 0x2A, 0x01, 0x28, 0x09, 0x02, 0x96, 0xD8, 0x87, 0x9E,
    0xBE, 0x6B, 0x4D, 0x35, 0x49, 0x6C, 0x69, 0xB9, 0xB2, 0x89, 0x50, 0xA1, 0x85, 0x1C, 0x20, 0xE0,
    0xE8, 0x19, 0xE0, 0xAF, 0xFF, 0xB9, 0x17, 0x27, 0x23, 0x92, 0x5B, 0x93, 0x00, 0xFF, 0xF3, 0x20,
    0xC4, 0x00, 0x09, 0x98, 0x4E, 0x80, 0x31, 0x8B, 0x00, 0x00, 0x00, 0x82, 0x10, 0x43, 0xFA, 0xE0,
    0xDA, 0x40, 0x50, 0x77, 0xDF, 0x12, 0x4C, 0x35, 0xBF, 0xD2, 0x54, 0x8E, 0x64, 0xC3, 0x21, 0x51,
    0x09, 0x85, 0x48, 0x03, 0x27, 0x7C, 0xA0, 0xE1, 0x6F, 0xE9, 0x00, 0x91, 0xFF, 0xAC, 0x51, 0xE2,
    0xC0, 0xFF, 0xF3, 0x22, 0xC4, 0x00, 0x09, 0x79, 0x42, 0xEC, 0x01, 0x81, 0x38, 0x01, 0xD5, 0x7F,
    0xD1, 0x7C, 0xE5, 0x33, 0xF6, 0x24, 0x5B, 0x4D, 0x98, 0x6A, 0x23, 0x0D, 0x41, 0x6E, 0x9B, 0x70,
    0x5C, 0x18, 0x02, 0x80, 0x06, 0x36, 0x34, 0x78, 0x78, 0xF1, 0x17, 0xF8, 0x17, 0x73, 0x0F, 0x7C,
    0x74, 0x6A, 0x35, 0x84, 0x98, 0x82, 0xFF, 0xF3, 0x20, 0xC4, 0x00, 0x00, 0x00, 0x03, 0x48, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xF3, 0x20, 0xC4, 0x00, 0x00,
    0x00, 0x03, 0x48, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
//
// Switch: Recorded by Adam_N
const unsigned char lswitch[166] = {
    0xFF, 0xF3, 0x30, 0xC4, 0x00, 0x09, 0xB8, 0x02, 0xFA, 0xFE, 0x00, 0x46, 0x00, 0xB8, 0x75, 0x47,
    0xD6, 0x48, 0x54, 0x49, 0xD0, 0x02, 0x8E, 0x14, 0x39, 0x28, 0x08, 0x5E, 0x1F, 0xCB, 0x9F, 0xCF,
    0x93, 0x75, 0x5C, 0xB8, 0x7F, 0x97, 0x07, 0xE0, 0x87, 0x39, 0xFF, 0x9F, 0xF0, 0x43, 0xFF, 0xE5,
    0x0C, 0xFC, 0xA0, 0x20, 0x98, 0x82, 0x9A, 0x8A, 0x66, 0x5C, 0x72, 0x72, 0x5C, 0x6A, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xF3,
    0x32, 0xC4, 0x00, 0x10, 0x48, 0xF2, 0xF2, 0xF6, 0x10, 0x86, 0x00, 0xCD, 0x65, 0x56, 0xB9, 0xD6,
    0xDD, 0x48, 0x00, 0x0C, 0x2B, 0x0C, 0x2B, 0x55, 0x66, 0x63, 0xAA, 0x24, 0x9B, 0x8C, 0x75, 0x4B,
    0x9A, 0xAA, 0xAE, 0xA5, 0xAA, 0x93, 0x1F, 0x4B, 0xD5, 0x54, 0xB6, 0x66, 0xFD, 0x40, 0x57, 0x58,
    0x2A, 0x0A, 0x9D, 0xF1, 0x31, 0xE2, 0x45, 0x81, 0xA8, 0x2B, 0xF5, 0x82, 0xA0, 0xD0, 0x34, 0x1D,
    0xF9, 0x50, 0xEA, 0x81, 0xA7, 0x96, 0x06, 0xA5, 0x83, 0xBC, 0x15, 0x0D, 0x80, 0xFF, 0xF3, 0x20,
    0xC4, 0x00, 0x00, 0x00, 0x03, 0x48
};
//
// Part of Dinging Interface Sounds: Recorded by Robinhood76
const unsigned char lock[160] = {
    0xFF, 0xF3, 0x20, 0xC4, 0x00, 0x09, 0xA8, 0x02, 0xEE, 0xFF, 0x40, 0x10, 0x00, 0x97, 0x76, 0x66,
    0xE8, 0x84, 0x2D, 0xF0, 0x70, 0x04, 0xE0, 0x20, 0x70, 0xA3, 0xA2, 0x7D, 0x78, 0x8C, 0xFC, 0xE0,
    0x60, 0xFC, 0xA1, 0xCE, 0x41, 0x60, 0x81, 0xCF, 0x28, 0x72, 0x08, 0x7E, 0x27, 0xFF, 0xF0, 0x7F,
    0xCB, 0xC9, 0xFF, 0x70, 0xFF, 0xF3, 0x22, 0xC4, 0x00, 0x08, 0xD0, 0xE3, 0x5A, 0x59, 0x81, 0x38,
    0x02, 0x18, 0x0C, 0x00, 0xDB, 0x6D, 0x06, 0xC2, 0xDD, 0x80, 0x00, 0x70, 0x7A, 0x7F, 0x98, 0x74,
    0x3C, 0xF7, 0xD4, 0xA0, 0xD8, 0xFC, 0x2F, 0xC9, 0x12, 0x35, 0x46, 0xA4, 0xDC, 0xCF, 0x41, 0x1C,
    0x8F, 0x1D, 0xC1, 0xAA, 0x93, 0x10, 0x53, 0x51, 0x40, 0xFF, 0xF3, 0x20, 0xC4, 0x00, 0x09, 0xA8,
    0x02, 0x69, 0x57, 0xC0, 0x18, 0x02, 0x1A, 0x46, 0xBD, 0xE0, 0x02, 0x83, 0xB8, 0x84, 0x34, 0x59,
    0xE0, 0xAA, 0xCE, 0xD6, 0x76, 0x59, 0xEB, 0x06, 0x83, 0xBC, 0x1A, 0x7C, 0x44, 0xA0, 0xED, 0xDE,
    0x25, 0xFF, 0x06, 0x86, 0x07, 0x7F, 0x12, 0xFF, 0xD4, 0x7B, 0xF0, 0x54, 0xF0, 0x54, 0x41, 0x47
};
//
// gpx track log file footer data
const unsigned char gpxfooter[33] = {
    0x20, 0x20, 0x20, 0x20, 0x3C, 0x2F, 0x74, 0x72, 0x6B, 0x73,
    0x65, 0x67, 0x3E, 0x0D, 0x0A, 0x20, 0x20, 0x3C, 0x2F, 0x74,
    0x72, 0x6B, 0x3E, 0x0D, 0x0A, 0x3C, 0x2F, 0x67, 0x70, 0x78,
    0x3E, 0x0D, 0x0A
};
//
// default id3 tag info for mp3 player
const char defaultinfo[21] = {// "NO ARTIST - NO TITLE"
    0x4E, 0x4F, 0x20, 0x41, 0x52, 0x54, 0x49, 0x53, 0x54, 0x20,
    0x2D, 0x20, 0x4E, 0x4F, 0x20, 0x54, 0x49, 0x54, 0x4C, 0x45, 0x00
};
//
// pre-calculated rotating cube points
const char cube[240] = {
    5, 5, 17, 17, 5, 5, 17, 17, 2, 14, 2, 14, 2, 14, 2, 14, 6, 5, 17, 16, 6, 5, 17, 16, 3, 13,
    4, 14, 2, 12, 3, 13, 7, 4, 18, 15, 7, 4, 18, 15, 3, 13, 6, 16, 0, 10, 3, 13, 8, 4, 18, 14,
    8, 4, 18, 14, 4, 12, 8, 16, 0, 8, 4, 12, 8, 4, 18, 14, 8, 4, 18, 14, 5, 13, 8, 16, 0, 8, 3,
    11, 9, 3, 19, 13, 9, 3, 19, 13, 5, 12, 9, 17, -1, 7, 4, 11, 10, 3, 19, 12, 10, 3, 19, 12,
    6, 12, 10, 17, -1, 6, 4, 10, 11, 3, 19, 11, 11, 3, 19, 11, 7, 12, 12, 17, -1, 4, 4, 9,
    11, 3, 19, 11, 11, 3, 19, 11, 8, 12, 12, 17, -1, 4, 4, 8, 12, 3, 19, 10, 12, 3, 19, 10,
    8, 12, 13, 17, -1, 3, 4, 8, 13, 3, 19, 9, 13, 3, 19, 9, 9, 12, 14, 17, -1, 2, 4, 7, 14,
    4, 18, 8, 14, 4, 18, 8, 10, 12, 14, 16, 0, 2, 4, 6, 14, 4, 18, 8, 14, 4, 18, 8, 11, 12, 14,
    15, 1, 2, 4, 5, 15, 4, 18, 7, 15, 4, 18, 7, 12, 13, 14, 15, 1, 2, 3, 4, 16, 5, 17, 6, 16, 5,
    17, 6, 13, 13, 14, 14, 2, 2, 3, 3
};
//
// wave table for spring physics main menu
// also used elsewhere for similar effects
const unsigned char mpspringtable[33] = {
    0, 1, 3, 5, 8, 11, 13, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 31, 32, 32, 32
};
//
// wave table for spring physics scrolling messages
const unsigned char spmessage[27] = {
    1, 1, 2, 4, 6, 9, 12, 16, 22, 30, 39, 48, 57, 66, 74, 82,
    90, 98, 106, 112, 116, 119, 122, 124, 126, 127, 127
};
//
// plasma and demo graphics sine table
const unsigned short sin_table[91] = {
    1, 285, 571, 857, 1142, 1427, 1712, 1996, 2280, 2563,
    2845, 3126, 3406, 3685, 3963, 4240, 4516, 4790, 5062, 5334,
    5603, 5871, 6137, 6401, 6663, 6924, 7182, 7438, 7691, 7943,
    8191, 8438, 8682, 8923, 9161, 9397, 9630, 9860, 10086, 10310,
    10531, 10748, 10963, 11173, 11381, 11585, 11785, 11982, 12175, 12365,
    12550, 12732, 12910, 13084, 13254, 13420, 13582, 13740, 13894, 14043,
    14188, 14329, 14466, 14598, 14725, 14848, 14967, 15081, 15190, 15295,
    15395, 15491, 15582, 15668, 15749, 15825, 15897, 15964, 16025, 16082,
    16135, 16182, 16224, 16261, 16294, 16321, 16344, 16361, 16374, 16381,
    16384
};
//
const char daynames[57] = {"Sunday:Monday:Tuesday:Wednesday:Thursday:Friday:Saturday:"};
const char unitrans[64] = {"AAAAAAECEEEEIIIIDNOOOOOXOUUUUYPBAAAAAASCEEEEIIIIOOOO-OUUUUYPY"};
const char programs[50] = {"ASTRO TIMEMP3 PLAYERVECTOR GPSTETRA BLOX CALENDAR "};
const char configsa[73] = {"TIMEZONE_OFFSETSERIAL_ENABLESOUND_ENABLECLOCK_VALUEFIRST_MENUSKIP_BROWSER"};
const char configsb[72] = {"AUTO_PLAYERMENU_DELAYLOG_INTERVALCOVERSHOWSHUFFLEGREYSCALEGPS_SIMULATION"};
const char configsc[69] = {"VIDEO_INTROSPI_SPEEDCARD_SPEED_UNITDATE_FORMATMIXED_CASEPITCH_SHIFTER"};
const char notfound[7] = {" FOLDER"};
//
const unsigned int vtimelensa[9] = {1306, 1306, 1306, 1254, 1672, 1672, 1628, 1202, 1672};
const unsigned int vtimelensb[10] = {1276, 1672, 1672, 1620, 1672, 1672, 2508, 2508, 1892, 2068};
const unsigned int vtimeposa[9] = {1472, 2778, 4084, 5390, 6644, 8316, 9988, 11616, 12818};
const unsigned int vtimeposb[10] = {14490, 15766, 17438, 19110, 20730, 22402, 24074, 26582, 29090, 30982};
const unsigned int samlens[10] = {1566, 7008, 4502, 3354, 1830, 926, 1216, 1564, 1540, 1716};
const unsigned int samadds[10] = {40174, 42158, 55374, 49264, 52618, 54448, 33050, 34266, 35830, 37370};
const unsigned int epochtable[13] = {1798, 1849, 1900, 1951, 1990, 2035, 2052, 2091, 2125, 2153, 2198, 2227, 2244};
const unsigned int pitches[12] = {11585, 12274, 13004, 13777, 14595, 15466, 16384, 17359, 18432, 19481, 20642, 21869};
const unsigned int tempos[8] = {27306, 20480, 18204, 16384, 14894, 13107, 10923, 8192};
const unsigned int clearbytes[12] = {896, 905, 1015, 1024, 947, 972, 917, 937, 981, 1001, 0, 128};
const unsigned int tabtab[11] = {0, 7, 14, 225, 430, 495, 754, 1013, 1220, 1295, 1307};
const unsigned int tetraspeech[5] = {240, 1546, 2852, 4158, 5412};
const unsigned int spaddr[7] = {0, 25, 240, 691, 715, 800, 929};
const unsigned char bitrates[9] = {160, 128, 112, 96, 80, 64, 56, 72, 48};
const unsigned char stdbitratesa[7] = {56, 64, 80, 96, 112, 128, 160};
const unsigned char xpixels[18] = {13, 114, 12, 115, 11, 116, 11, 116, 11, 116, 11, 116, 11, 116, 12, 115, 13, 114};
const unsigned char lpnts[24] = {0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 3, 7, 1, 5, 2, 6};
const unsigned char xrotp[8] = {60, 73, 79, 73, 60, 47, 41, 47};
const unsigned char yrotp[8] = {9, 15, 28, 41, 47, 41, 28, 15};
const unsigned char vlines[10] = {0, 9, 21, 41, 50, 76, 85, 105, 118, 127};
const unsigned char buttab[8] = {14, 14, 14, 17, 12, 14, 14, 14};
const unsigned char drectlines[4] = {65, 98, 66, 99};
const unsigned char jfif[4] = {0x4A, 0x46, 0x49, 0x46};
const unsigned char tlxtable[6] = {35, 48, 67, 80, 99, 112};
const unsigned char borderys[4] = {0, 17, 19, 36};
const unsigned char cubexpoint[12] = {1, 3, 1, 2, 1, 2, 3, 4, 5, 5, 7, 6};
const unsigned char cubeypoint[12] = {2, 4, 3, 4, 5, 6, 7, 8, 6, 7, 8, 8};
const unsigned char initbytes[3] = {0b00111111, 0b01000000, 0b10111000};
const unsigned int meterpos[4] = {32, 95, 2, 65};
const char silencedat[9] = {0xFF, 0xF3, 0x10, 0xC4, 0x00, 0x00, 0x00, 0x03, 0x48};
const char rtoffs[14] = {-3, -2, -5, -2, -5, 8, 1, 2, 1, -4, 3, -1, 1, 2};
const char lpositions[5] = {17, 49, 81, 113, -15};
const char menupositions[5] = {-31, 16, 63, 95, 127};
const char sphyphen[3] = {0x20, 0x1F, 0x20};
const char smhistory[3] = {0x41, 0x49, 0x54};
//
// global math constants
const double PI = 3.14159265358979323846;
const double d2r = 0.0174532925199433;
const double PIo2 = 3.14159265358979323846 / 2.0; // update for version 1.0.156
const double PIx2 = 3.14159265358979323846 * 2.0; // update for version 1.0.156
//
// mp3 player specific variables
unsigned char playing = 0;
unsigned char mpfileopen = 0;
unsigned char bufready = 0;
unsigned char saskin = 0;
unsigned char lastpressed = 1;
long int fileposition = 0;
char idtext[134];
unsigned char iseyedeethree = 0;
unsigned char eyedeethreelen = 0;
unsigned char idlooplimit = 0;
int eyedeethreepos = 0;
int eyedeethreedel = 0;
unsigned int eyedeejpgpos = 0;
int mpskip = 0;
int mpvolume = 0;
unsigned char mpvolumea = 0;
unsigned char mpvolumeb = 0;
unsigned char mpvol = 0;
unsigned char joystickcontrol = 0;
unsigned char mpvbr = 0;
unsigned char mppause = 0;
int mpvbrcount = 0;
int lastbitrate = 0;
unsigned char basscrank = 0;
unsigned short decodetime __attribute__((far)) = 0;
int olddtminutes __attribute__((far)) = 0;
int olddtseconds __attribute__((far)) = 0;
int oldtrminutes __attribute__((far)) = 0;
int oldtrseconds __attribute__((far)) = 0;
//
unsigned int vuneedles[2] = {0, 0}; // stereo meter positions
int lrPhase = 0; // root square variables
unsigned long lrSum[2] = {0, 0}; //
//
int sinindex = 0;
int sindir = 0;
int sinstart = 0;
int sinstartdir = 0;
int sinamp = 11;
//
// card and filesystem variables
int cardstat = 0;
int socketstate = 0;
long int drawing = 1;
int joytimer = 0;
char fileextension[5];
long int filesize = 0;
//
// gps vector map variables
int maptype = 0;
int polystat = 0;
int zooming = 0;
int zoomlevel = 0;
unsigned char maplevel[3];
float filelat = 0.0;
float filelon = 0.0;
float floatwork = 0.0;
char filelabel[25];
char foplabel[25];
unsigned char firstlinepoint = 0;
char receivebuffer[80];
unsigned char forcefirstdraw = 1;
unsigned int gpsunitsph = 0;
unsigned char maxlabels = 0;
unsigned char gpsskipdraw = 0;
int limitytop = 0;
int limitybottom = 0;
//
// gps hardware specific variables
unsigned char bailedgpsmode = 0;
unsigned char gpsbuttonpushed = 0;
unsigned char gpsmode = 0;
unsigned char gpsfix = 0;
unsigned char timewasacquired = 0;
unsigned char odometerslide = 0;
float gpslat = 0.0;
float gpslon = 0.0;
float oldgpslat = 0.0;
float oldgpslon = 0.0;
float mspointlat = 0.0;
float mspointlon = 0.0;
float msfoplat = 0.0;
float msfoplon = 0.0;
float foplat = 0.0;
float foplon = 0.0;
float fopdistance = 0.0;
float fopdistancekm = 0.0;
//
// image buffer select variable
// 0 = image a,  1024 = image b
int imageselect = 0;
int oldimageselect = 0;

// gps track logging variables
unsigned char simwasactive = 0;
unsigned char gpslog = 0;
int logindex = 0;
unsigned char oldlogmins __attribute__((far)) = 99;
unsigned char oldlogsecs __attribute__((far)) = 99;
unsigned char logflipflop __attribute__((far)) = 0;
float lastloglat __attribute__((far)) = 0.0;
float lastloglon __attribute__((far)) = 0.0;

// bearing and distance variables
float distance = 0.0;
int brngpoint = 0;
int brngdisplay = 0;
int closebearing = 0;
int fopbearing = 0;
int mapsniffer = 0;
int msshowlines = 0;
unsigned char msradius = 0;

// graphics variables for gps
float userlat;
float userlon;
float pointval;
float screentllat;
float screentllon;
float screenbrlat;
float screenbrlon;
float screenx = 0.0;
float screeny = 0.0;
float pointvaldiv = 0.0;
int drawmap = 0;
int lxa;
int lxb;
int lya;
int lyb;
int lxc;
int lyc;
//
// variables for float conversion
float afloat;
unsigned char bytes[sizeof (float)];
// general counter
int n;
// mouse pointer coordinates
int mx = 42;
int my = 16;
float jheld = 1.0;
float mmx = 42.0;
float mmy = 16.0;
unsigned char msine = 0;
// pixel coordinates
int x;
int y;
//
unsigned char blockpos = 0;
unsigned char firstgpsdata = 0;
//
// main graphics display buffer, and two extra image buffer to
// swap gps maps. also used as a buffer for the config file,
// mono image buffers for the covershow feature, id3 tag area
// for parsing mp3 file tags, temporary bitmap image buffer,
// forrest fire floodfill pattern buffer, among other uses.
unsigned char framebuffer[1024];
unsigned char imagebuffer[2048];
//
// variables for plasma demo
int sp1 = 4;
int sp2 = 2;
int sp3 = 4;
int sp4 = 2;
unsigned char pl1 = 0;
unsigned char pl2 = 0;
unsigned char pl3 = 0;
unsigned char pl4 = 0;
unsigned char pt[4];
int pdir = 0;
//
// variables for random generators
unsigned int wordrandom = 0x33;
long int masterlongrandom = 3339297;
//
// variables for click warble effect
unsigned char clickfreq = 0;
unsigned char clickfdir = 0;
//
// tetrabrick game exclusive variables
// now also borrowed for stereo meters
int tetx = 0;
int tety = 0;
int tetdir = 0;
int oldtetdir = 0;
int tetrashape = 0;
int tetrarot = 0;
int tetcollide = 0;
int tetrascore = 0;
int tetratimer = 0;
int tetgameover = 0;
int tetrepeat = 0;
int tetdelay = 0;
unsigned char tetrahistory[4];
unsigned char tetrottimer = 0;
unsigned char tetranext = 0;
//
// Spectrum Analyser and Pitch Changer code for vs1003b
// These are software programs that get loaded to the vs1003b.
// The vs1003b runs it, and provides the information for the
// dsPic program to draw the various graphic spectrum displays,
// and in case of the pitch changer, to change pitch without
// changing tempo, or change tempo without changing pitch.
// This code is provided by the chip manufacturer, VLSI Solutions,
// and explicit permission has been granted to use it for any
// project that uses at least one of their chips in hardware.
//
// From version 1.0.31 the data is read by the dsPic from a ROM
// file on SD card, and loaded to the vs1003b decoder chip to run.
//
FSFILE * pointer; // file pointer
SearchRec rec; // file search record structure
//
// variables for point rotation
int rotation;
long double theta;
int xx;
int yy;
int xnew;
int ynew;
int tdrotation = 0;
double tdtheta;
//
unsigned char cx = 0; //
unsigned char cy = 0; //
unsigned short sinvalue; //
unsigned char radius = 0; //
unsigned char pff = 0; // flip flop for dotted lines
//
// parallel shift register variables
// there is still an unused input pin
unsigned char srbyte = 0; // lcd byte to shift out
unsigned char joyup = 0;
unsigned char joydown = 0;
unsigned char joyleft = 0;
unsigned char joyright = 0;
unsigned char joyfire = 0;
unsigned char joyspareb = 0;
unsigned char joysparec = 0;
//
unsigned char txbyte = 0; // serial byte
int configindex; // configuration file index
//
int menuvars[8];
int springmenuvars[5];
char oldhoff = 0;
char thoff = 0;
int clrfb = 0; // counter
char text[134];
int tx = 0;
int ty = 0;
int trotation = 0;
float ox = 0;
float oy = 0;
int txtox = 64; // text rotation origin
int txtoy = 32; //
int framecount = 0;
int demo = 0;
int filecount = 0;
int fnumber = 0;
//
unsigned char msgcount = 0;
//
// variables for lcd backlight control
unsigned int backlighttimer = 0;
unsigned int backlightdelay = 2000;
//
unsigned char serialenabled = 0;
unsigned char soundenabled = 1;
unsigned char clockvalue = 3;
unsigned char clockbyte = 0xC4;
unsigned char firstmenu = 0;
unsigned char locale = 0;
unsigned char dateformat = 0;
unsigned char mixedcase = 0;
//
// fifo buffer variables
int queuein = 0;
int queueout = 0;
//
// variables for astronomy status
int risemin; //
int setmin; //
int mrisehr; //
int msethr; //
double mp = 0.0; //
int uyear = 0;
int umonth = 0;
int uday = 0;
int uhour = 0;
int umin = 0;
int usec = 0;
//
// multi purpose arrays
char line2[30]; // GPS time, day of week, date, moonset/sunset/illumination, peak array
char line3[22]; // moonrise/sunrise/moon percentage, spectrum analyser array
//
// variables for sun times functions
int hr; // sunrise sunset routine variables
int mn; //
double rads; //
double L; //
double g; //
//
// variables for moon times functions
BYTE spcond; //
BYTE istzneg; //
int rmth0; // digit buffers for moon rise - set
int rmth1; //
int rmtm0; //
int rmtm1; //
int smth0; // digit buffers for moon rise - set
int smth1; //
int smtm0; //
int smtm1; //
float userlat; // user defined latitude
float userlon; // user defined longitude
float mnbuff; //
float mnbuffb; //
float P2; //
float K1; //
float A; //
float A5; //
float B; //
float C; //
float C0; //
float D; //
float D0; //
float D1; //
float D2; //
float D5; //
float E; //
float F; //
float F0; //
float F1; //
float F2; //
float G; //
float H0; //
float H2; //
float H1; //
float P; //
float RR; //
float R5; //
float T; //
float T0; //
float T3; //
float V0; //
float V2; //
float U; //
float W; //
float N; //
float LL; //
float L0; //
float L2; //
float A0; //
float A2; //
float B5; //
float L5; //
float H; //
float M; //
float Z0; //
float M11; //
float M12; //
float M13; //
float M21; //
float M22; //
float M23; //
float M31; //
float M32; //
float M33; //
float M8; //
float W8; //
float J; //
float J3; //
float S; //
float V; //
float Z; //
float Z1; //
float V1; //
float H3; //
float M3; //
//
double formatbufa;
double formatbufb;
//
// variables for calendar code
unsigned char caldim[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; // calendar days in months
unsigned int calday; // calendar day
unsigned int calmonth; // calendar month
unsigned int calyear; // calendar year
unsigned char caldow; // calendar day of the week
unsigned char calleap; // current year leap status bit
unsigned char calbadentry; // bad calendar input flag
unsigned int cacnt; // counter for calendar
unsigned int cwork; // work buffer for calendar
unsigned int xwork; // work buffer for calendar
//
// last error value for temporary display or debug out
// not all errors are sent out through the serial port
int lasterror = 0; // last error is not usually displayed
// 0 = no errors
// 1 = joystick or sd card switch hardware error
// 2 = calendar range error
// 3 = calendar epoch year table accessed out of range
// 4 = "MAPS" directory was not found
// 5 = "MUSIC" directory was not found
// 6 = bad NMEA sentence checksum from GPS module
// 7 = SD card configuration file error
// 8 = no expected file types found in a media directory
// 9 = bad NMEA sentence checksum from GPS module
// 10 = graphics flood fill queue buffer was full
// 11 = media directory not found - reset to menu flag
// 11 = also used for an unrecognised map file format
// 12 = retrieved file name did not match image decoded name
// 13 = tetrabrick game or graphic calendar data did not load
// 14 = ID3 tag data exists but not decoded for tag type a
// 15 = ID3 tag data exists but not decoded for tag type b
// 16 = ID3 tag exists but no info extracted unknown reason
// 17 = error opening the GPX folder on SD card
// 18 = error opening the ROM file to read gpx header data
// 19 = card was write protected for gpx track log save
// 20 = gpx track log file write failed after retries
// 21 = failed to create gpx track log file for writing
// 22 = speed field was not present in NMEA sentence
// 23 = MP3 decoder chip was not ready at startup
// 24 = MP3 decoder chip was not detected at startup
// 25 = did not find a recent enough gpx file to re-open
// 26 = did not open gpx file because date was not acquired
// 27 = did not find closest road or trail on a map
// 28 = did not find the closest point of interest on a map
// 29 = audio noise sampler did not software reset the vs1003b
// 30 = audio noise sampler did not set vs1003b record mode
// 31 = locally adjusted calendar day of week was out of range
// 32 = spectrum analyser code checksum test failed for loading
// 33 = pitch changer code checksum test failed for loading
// 34 = vs1003b did not become ready for the noise sampler
// 35 = "FREEDOM.ROM" file was not found in SD card ROM folder
// 36 = program constant data arrays checksum was not correct
// 37 = no description found in map file, skipped title screen
// 38 = did not determine a valid moon phase expression message
// 39 = did not find map file that a gpx file was created with
//
int sdx = 0; // dynamic sd card graphic offsets
int sdy = 0; //
//
// time zone offset calculator variables
int timezone = 0; // timezone offset value
int gpsyear = 0;
int gpsmonth = 0;
int gpsday = 0;
int gpsdow = 0;
int gpshour = 0;
int gpsminute = 0;
int gpssecond = 0;
char tztime[7];
char tzdate[7];
//
unsigned char vtime[4]; // time of day buffer for voice announcement
unsigned char oldvtime[4]; // last time buffer
int vlen = 1850; // size of built in mp3 sample
int samlen = 0; // file size buffer for voice samples
int vbytecount = 0; //
unsigned char samplenum = 0; // sample select value
unsigned char vsame = 0; //
unsigned char gottime; //
unsigned char gotdate; //
unsigned char sdspeed = 5; // sd card hardware spi prescaler value
unsigned char configsign = 0; // flag to indicate positive or negative timezone offset
unsigned char configerror = 0; // error indicator value for configuration file function
unsigned char skipbrowser __attribute__((far)) = 1; // config value for skip file browser
unsigned char autoplayer __attribute__((far)) = 0; // config value for auto mp3 player
unsigned char menudelay __attribute__((far)) = 1; // config value for menu joystick delay
unsigned char loginterval __attribute__((far)) = 0; // config value for log interval
unsigned char covershow __attribute__((far)) = 0; // config value for cover show feature
unsigned char mpshuffle __attribute__((far)) = 1; // config value for mp3 player shuffle
unsigned char csgreyscale __attribute__((far)) = 1; // config value for covershow greyscale
unsigned char videointro __attribute__((far)) = 2; // config value for motion video intro
unsigned char pitchchanger = 0; // config value for pitch changer
unsigned char gpssimulation = 0; // config value for gps simulation mode
unsigned char spispeedconfig = 0; // configurable software spi write speed for mp3 player
unsigned char jpgdecstatus __attribute__((far)) = 0; // jpeg decoder status 1 = image decoded
unsigned char jpgbestmono __attribute__((far)) = 0; // best of four mono jpeg decodes
unsigned char mspoitype __attribute__((far)) = 0; // map sniffer nearest poi type
long int tagsize __attribute__((far)) = 0; // size of id3 tag to skip
int jpgpixcount[4] = {0, 0, 0, 0}; // pixel counts for mono jpegs
unsigned int delaycounter; // counter for delay routines
unsigned char dframecount = 0; // performance testing frame counter
//
// interrupt variables should not be used by programs
unsigned int tcount = 0;
unsigned int secondschanged = 0;
unsigned char rxbyte = 0;
unsigned char gotgprmc = 0;
unsigned char rxcount = 0;
//
// Speech and Sound buffer.
// The usage of this buffer includes caching of the speech announcements made by Astro Time.
// Individual sample files are loaded one after the other into this array before playing the
// entire time of day announcement as one single sample straight from this array.
// Single samples played in the menu, whether from file or other arrays are also loaded here.
// The initial startup sample loaded to this array is played once at startup, and is then
// overwritten by any other sound that plays. This is an old PD sample that cannot be credited.
// This array is used as memory for the shuffle list in mp3 player, the gps track log memory,
// and for the file browser cache where the first eight characters of the filenames are stored.
unsigned char voice[7536]__attribute__((far)); // only track logging accesses past 7140
//
void delay_gen(unsigned int n); // general delay is not in milliseconds
void delay_lcd(); // lcd communication delay function
void gpstracklogread(); // reads and draws the gps track log
void gpstracklogwrite(); // records the gps track log to memory
void gpstracklogpoint(int rindex); // read a single gps track log point
void dirnotfound(); // prints the directory that was missing
void setcal(); // software calendar by Brek Martin 2015-17.
void calgrid(); // monthly table format calendar display
void sdpixelx(); // specifically for setting pixels with offsets
void sdpixely(); // for the Amiga style insert sd card animation
void sdpixel(); // also used by other parts of the program
void startvoice(); // start vs1003b chip for voice
void playvoice(); // play macro assembled announcement (blocking)
void playtinydirect(); // play tiny sample directly from constant array
void playlockdirect(); // play lock sample directly from constant array
void playblopsound(); // play blop sample indirectly from voice array
void playswitchsound(); // play switch sample indirectly from voice array
void vfill_audio(); // play macro assembled announcement (non blocking)
void vsreadmode(); // read the vs1003b decoder chip mode register
void build_announcement(); // assemble time announcement from file samples
void readvoice(unsigned int pos); // read single voice sample file into buffer
void announcementdisplay(); // graphic display for time of day announcement
void load_sample(); // load sound sample from sd card to play buffer
void bassboost(); // enable or disable bass boost for mp3 player
void pitchtempocontrol(); // pitch and tempo control for pitch changer
void applytzoffset(); // apply local timezone offset to time and date
void read_config(); // read configuration file if present
void search_config(); // parse configuration file if it was present
void configreadram(int ia, int ib); // read config file chunk from file to ram
int configtrima(int cinx); // split out function for config file parser
int configtrimb(int cinx); // split out function for config file parser
unsigned char ctrmc(int a, int b); // split out function for config file parser
unsigned char ctrmb(int a, int b); // split out function for config file parser
unsigned char ctrma(int a, int b); // split out function for config file parser
unsigned char configtrimbig(); // split out function for config file parser
void runtime_config(); // run time configuration screen
void printcredit(); // please do not remove from distribution versions
void creditprivatea(); // private function for the print credit function
void creditprivateb(); // private function for the print credit function
void sdcardanimation(); // Amiga style "Insert SD Card" animation
void threedeerotation(); //
void threedeemultrotation(); //
void gpsconvertfloats(); // convert file floats to screen coordinates
//int gpsstaticclipping();            // static clipping box for rotating vector map
int gpsdynamicclippingcheck(); // dynamic clipping box for rotating vector map
void gpsdynamicclippingset(); // set dynamic clipping box bounds for an angle
void gpszoom(); // gps projection zoom function
void astrostartsample(); // start non-blocking sample playing
void astrocaseconvert(); // convert message strings to upper case
void clickframe(); // click sound for file browser scrolling
void silenceframes(); // silence the vs1003b when skipping tracks
void fillbufferwithzeros(); // fill vs1003b buffer with zero values
void shiftbit(int srbit); // lcd shift register functions
void shiftbyte(); //
void shiftbytein(); // joystick shift register function
void sbprivate(); // private clock function for the above
void count_files(); // private count and cache function
void get_file(); // retrieve a file full search method
void file_browserv2(); // file browser new fast cached version
void nofileserror(); // no files in folder error handler
void nofilesprivate(); // private funtion for nofileserror function
void mpfileshuffle(); // shuffle file indexes in an array
void mpresetdecodetime(); // reset decode time for vs1003b chip
void mptimecopy(int at, int bt); // copy time duration to text buffer
int retrieveshuffle(int value); // retrieve a shuffled file entry
void drawshufflegraphic(); // static shuffle graphic symbol
void reset_register_check(); // check for previous program cycle reset
void debugprivate(unsigned char p); // private function for reset debug display
void hardware_sanity_check(); // check joystick and card socket malfunctions
void software_sanity_check(); // check consolidated rom file on sd card
void nocardiniterror(); // first card initialise failed error message
void sanityprivatea(); // private function for sanity check functions
void sanityprivateb(); // private function for sanity check functions
void drawrectprivatea(); // private function a for draw rectangle function
void drawrectprivateb(); // private function b for draw rectangle function
void openromfile(); // open sd card rom file
void draw_folder(int fx, int fy); // folder graphic for file browser expansion
void drawsearchactivity(); // draw file search activity bar graphic
void drawgpsprogress(); // draw gps progress bar outline
void drawmpprogress(); // draw mp3 player progress bar outline
void drawroundedborder(); // draw rounded full screen border
void clearwaterfall(); // clear waterfall spectrum display data
void sendclrspectrum(); // send data to clear external spectrum display
void stereopeaklevels(); // stereo peak level meter calculator
void stereolevelsrs(); // stereo root square meter for analogue simulation
void drawanaloguemeters(); // draw the simulated analogue level meters
void monolevelsample(); // request mono level sample for time announcement
void gpsfillpolygon(); // seed the polygon floodfill for maps
void drawpoiicon(int poitype); // draw gps map point of interest icon
void gpscopygraphics(); // clear graphics function for gps program
void drawcountmpgraphic(int gfiles); // file search graphic animation
void drawmpscreen(int shuffnum); // display file entry numbers
void drawcube(int intx, int inty); // draw 3d rotating wire frame cube
void drawcubedisplay(); // draw a cube demo display
void cachefilename(int index); // cache a single filename
void recachefiles(int filenumber); // cache 881 filenames
int getstartofrange(); // get first value of current cache range
void retrievefilename(int index); // retrieve filename from the cache
int rounded(float unrounded); // since C30 has no round function
void floattobytes(void); // convert a float variable to four bytes
float Float(char unsigned *const p); // convert four bytes to a float value
int asciitoint(char msb, char lsb); // convert a pair of ascii bytes to integer value
void hardserialout(); // send byte out of hardware serial port 9600 baud 8N1
void serialeyedeethree(); // send the track artist and title from id3 tag serially
void handlegps(); // parse information from u-blox gps module
void badcheckstring(); // copy bad gps nmea checksum string to text buffer
int getbearing(float inlat, float inlon); // calculate bearing from a coordinate pair
float getdistance(float inlat, float inlon); // calculate distance from a coordinate pair
float footperpendiculardistance(); // calculate the foot of the perpendicular
void read_map_header(); // read in an mpi vector map file header
void gpxmessagescreen(int gmsg); // draw gpx message dialogue screen
void trackloggauge(); // calculation for track log gauge messages
void makegpx(); // build and save gpx track log file
void readgpx(); // open and read an existing gpx track log file
float parsegpxprivatea(); // private function for the readgpx function
void parsegpxprivateb(); // private function for the readgpx function
void copygpxfilename(); // copy the gpx track log file name to text buffer
void convert_meters(); // convert feet to meters
void multiplerotationf(); // rotate multiple points for the same angle
void WriteSPIManual(unsigned char data_out); // write one byte spi slow
void WriteSPIFast(unsigned char data_out); // write one byte spi fast
void vscommand(unsigned char bbb, unsigned char ccc, unsigned char ddd); // write command to vs1003b
unsigned char ReadSPIManual(); // read one byte spi
void loadspectrum(); // load the spectrum analyser program to VS1003b chip
void loadpitchchanger(); // load the pitch changer program to VS1003b chip
void eyedeemove(int width); // move the track information in scroller windows
void eyedeethree(); // extract information from mp3 file id3 tags
void eyedeephix(); // various id3 extractor fixes
void eyedeeveeone(); // id3 version one tag parser
int eyedeeunicodeartist(); // translate unicode id3 artist field to ascii
int eyedeeunicodetitle(int idsize); // translate unicode id3 title field to ascii
int idtextfilter(int subindex, int index, int nu, int su); // memory saving function
int idtexthyphenator(int subindex); // memory saving function
void printbearing(int pbearing); // copy bearing information to text string
void printdistance(); // copy distance information to text string
void mapsniffercheckvoid(); // check for null string for map sniffer function
void mapsniffercheckdefault(); // check for default labels meaning no item was found
void mapsnifferfinish(); // map sniffer calculations done after render
void mapsnifferscrpnt(float inlat, float inlon); // calculate screen coordinates for lines
int mapsniffercheckfound(); // check a place or road/trail was found in map
void stdimagetag(); // search for jpeg image in standard id3 tag
void clearartwork(); // clears previous images when no image is found
void clearwindowbackground(); // clears large display window background
void fillaudiobuffer(); // function to maintain the vs1003b buffer
void lcdmoduleinit(); // lcd module initialise function
void resettomenu(); // reset all variables ready to enter the main menu
void menutransition(int dir); // graphic menu transition animation in and out of menu
void astrotransition(int dir); // draw graphic transition for astro time program
void fbtransition(int block); // graphic transition out of file browser
void menuspringphysics(int mendir); // main menu movement spring physics
void checkmousebounds(); // check mouse pointer is in screen bounds
void gpsmodeexitdelay(); // exit delay for joystick left for gps mode
void drawplasma(); // draw plasma display
short plasmasin(int val); // plasma display sin table lookup function
void textrotation(); // rotated text
void analogueclockhand(); // calculate and draw analogue clock face hand
void drawdegreessymbol(); // draw graphic degrees symbol
void loadintro(); // load intro audio
void loadbitmap(); // load art bitmap
void printbitmap(); // print a 64x64 monochrome bitmap
void printbitmapflippedhoriz(); // print a 64x64 monochrome bitmap flipped horizontally
void runtimeconfigenable(); // runtime configuration override enable screen
void rtconfigtransitionout(); // transition out from runtim configuration enable
void rtconfigtransitionoutb(); // transition out from runtim configuration screen
void clrbmpchunk(); // private function to clear bitmap background
void printjpeg(); // load and print a jpeg compressed image
void full_motion_video(); // play full motion video from sd card
void joystickaccelerate(); // apply joystick input
int sdcardinit(); // sd card initialise with frequency settings
void plasmasaver(); // menu plasma screen saver
unsigned int noiseforrandom(); // sample background noise for random number seeds
void debugout(int derror); // serial debug output
void debugflash(); // flash the lcd backlight for debug
void framecounter(); // performance testing frame counter or error out
void tetrabrick(); // tetrabrick game functions
void tetrafloor(); //
void tetradraw(); //
void tetdrawcell(int px, int py); //
void tetracheckrows(); //
void tetrarotatewell(int linecnt); //
void tetraload(); //
int tetrarandom(); //
int tetraspawn(); //
void tetrotate(int shape, int rot); //
int tetcollision(); //
void tetdrawshape(); //
void tetralock(); //
void tetrastart(); //
void tetransition(); //
void tethistory(); //
void tetdelayrows(); //
int tetcheckhis(); //
void pixelworkprivatea(); // size optimisation pixel functions
void pixelworkprivateb(); //
void pixelworkprivatec(); //
void pixelworkprivated(); //
void pixelworkprivatee(); //
void pixelworkprivatef(); //
void pixelworkprivateg(); //
void pixelworkprivateh(); //
void pixelworkprivatei(); //
//
// clear the frame buffer and both image buffers
void clrgraphics();
// clear the graphics frame buffer
void clrframebuffer();
// clear first image buffer
void clrimagebuffer();
// set pixel at screen coordinates to black
void setpixel();
// clear pixel at screen coordinates
void clearpixel();
// set pixel at image coordinates to black
void setpixelimage();
// clear pixel at image coordinates
void clearpixelimage();
// toggle pixel value at screen coordinates
void flippixel();
// get pixel value at screen coordinates
int getpixel();
// get pixel value at image coordinates
int getpixelimage();
// sets pixel every second time the function is called
void togpixel();
// sets image pixel every second time the function is called
void togpixelimage();
// rotate screen coordinates at an angle
void pixelrotation();
// rotate multiple screen coordinates at the same angle
// after calling pixelrotation function once
void multiplerotation();
// draw a line between a pair of coordinates
void drawline(int bx1, int by1, int bx2, int by2);
// draw solid or dotted line between a pair of image coordinates
void drawlineimage(int bx1, int by1, int bx2, int by2, int dot);
// draw circle of radius, centered at input coordinates
void drawcircle();
// draw circle of radius, centered at input coordinates
void drawcircleimage();
// draw filled circle of radius, centered at input coordinates
void drawfilledcircle(int quality);
// draw small text in text buffer at screen coordinates
void drawtext();
// draw small text in text buffer to image buffer
void drawtextimage();
// draw small clear text in text buffer to image buffer
void drawtextimageclear();
// draw small text rotated at angle to image buffer
void drawtextimagerotated();
// draw large text in text buffer at screen coordinates
void drawtextlarge();
// draw large animated sin wave text to frame buffer
void drawtextlargesin();
// draw large sin text masked with existing graphics
void drawtextlargesinmasked();
// draw small horizontally centered string in text buffer
void drawtextcentered();
// draw variable width horizontally centered text string
void drawtextvarwidthcentered();
// draw variable width text string centered at input position
void drawtextvarwidthcenteredpos(int center);
// measure variable length text string in text buffer
int measuretextvarwidth();
// draw variable length text in buffer to framebuffer
void drawtextvarwidth();
// draw grey text in text buffer at screen coordinates
void drawtextgrey();
// draw large numeric digit in text buffer at screen coordinates
void drawtextbolddigit();
// read characters from an image buffer into text string
int readtextimage(int dmode);
// draw amiga style mouse pointer at screen coordinates
void drawpointer();
// draw rectangle with rounded corners of length and width at coordinates
// the rectangle should be at least 11 pixels long and 11 pixels wide
void drawroundedrectangle(int posx, int posy, int lenx, int leny);
// draw hollow rectangle using four lines
void drawrectangle(int posx, int posy, int lenx, int leny);
// draw rectangle outline filled clear
void drawrectanglefilledclear(int posx, int posy, int lenx, int leny);
// fill enclosed area of clear pixels in image
void floodfill(int ffx, int ffy, int pattern);
// refresh the display by drawing frame buffer to display
void drawscreen();
//
// Moon rise and set times BASIC routines appeared in
// Sky & Telescope magazine July 1989, on page 78.
// Presumably originally written for the Commodore C-64.
// ported from BASIC to C in January 2008 by Brek Martin.
// The original commented BASIC listing can be found here:
// http://www.schoepski.com/states/illinois/solunar/code/
//

void gosub495() {
    LL = 0.606434 + 0.03660110129 * T;
    M = 0.374897 + 0.03629164709 * T;
    F = 0.259091 + 0.03674819520 * T;
    D = 0.827362 + 0.03386319198 * T;
    N = 0.347343 - 0.00014709391 * T;
    G = 0.993126 + 0.00273777850 * T;
    LL = LL - floor(LL);
    M = M - floor(M);
    F = F - floor(F);
    D = D - floor(D);
    N = N - floor(N);
    G = G - floor(G);
    LL = LL*P2;
    M = M*P2;
    F = F*P2;
    D = D*P2;
    N = N*P2;
    G = G*P2;
    V = 0.39558 * sinf(F + N);
    V = V + 0.08200 * sinf(F);
    V = V + 0.03257 * sinf(M - F - N);
    V = V + 0.01092 * sinf(M + F + N);
    V = V + 0.00666 * sinf(M - F);
    V = V - 0.00644 * sinf(M + F - 2 * D + N);
    V = V - 0.00331 * sinf(F - 2 * D + N);
    V = V - 0.00304 * sinf(F - 2 * D);
    V = V - 0.00240 * sinf(M - F - 2 * D - N);
    V = V + 0.00226 * sinf(M + F);
    V = V - 0.00108 * sinf(M + F - 2 * D);
    V = V - 0.00079 * sinf(F - N);
    V = V + 0.00078 * sinf(F + 2 * D + N);
    U = 1 - 0.10828 * cosf(M);
    U = U - 0.01880 * cosf(M - 2 * D);
    U = U - 0.01479 * cosf(2 * D);
    U = U + 0.00181 * cosf(2 * M - 2 * D);
    U = U - 0.00147 * cosf(2 * M);
    U = U - 0.00105 * cosf(2 * D - G);
    U = U - 0.00075 * cosf(M - 2 * D + G);
    W = 0.10478 * sinf(M);
    W = W - 0.04105 * sinf(2 * F + 2 * N);
    W = W - 0.02130 * sinf(M - 2 * D);
    W = W - 0.01779 * sinf(2 * F + N);
    W = W + 0.01774 * sinf(N);
    W = W + 0.00987 * sinf(2 * D);
    W = W - 0.00338 * sinf(M - 2 * F - 2 * N);
    W = W - 0.00309 * sinf(G);
    W = W - 0.00190 * sinf(2 * F);
    W = W - 0.00144 * sinf(M + N);
    W = W - 0.00144 * sinf(M - 2 * F - N);
    W = W - 0.00113 * sinf(M + 2 * F + 2 * N);
    W = W - 0.00094 * sinf(M - 2 * D + G);
    W = W - 0.00092 * sinf(2 * M - 2 * D);
    S = W / sqrt(U - V * V);
    A5 = LL + atan(S / sqrt(1 - S * S));
    S = V / sqrt(U);
    D5 = atan(S / sqrt(1 - S * S));
    R5 = 60.40974 * sqrt(U);
    T = T + 0.5; // update for version 1.0.185
}

void gosub225() {
    A = F1 - F0;
    B = F2 - F1 - A;
    F = F0 + P * (2 * A + B * (2 * P - 1));
}

void gosub285() {
    int mnthour; // rise - set time buffers
    int mntminute; //
    int risetflag = 0; // tri state rise set flag
    L0 = T0 + C0*K1;
    L2 = L0 + K1;
    if (A2 < A0) {
        A2 = A2 + PIx2;
    } // update for version 1.0.156
    H0 = L0 - A0;
    H2 = L2 - A2;
    H1 = (H2 + H0) / 2;
    D1 = (D2 + D0) / 2;
    if (C0 > 0) {
        goto line325;
    }
    V0 = S * sinf(D0) + C * cosf(D0) * cosf(H0) - Z;
line325:
    V2 = S * sinf(D2) + C * cosf(D2) * cosf(H2) - Z;
    if (V0 < 0) {
        mnbuff = -1;
    } //
    if (V0 > 0) {
        mnbuff = 1;
    } //
    if (V2 < 0) {
        mnbuffb = -1;
    } //
    if (V2 > 0) {
        mnbuffb = 1;
    } //
    if (mnbuff == mnbuffb) {
        return;
    }
    V1 = S * sinf(D1) + C * cosf(D1) * cosf(H1) - Z;
    A = 2 * V2 - 4 * V1 + 2 * V0;
    B = 4 * V1 - 3 * V0 - V2;
    D = B * B - 4 * A*V0;
    if (D < 0) {
        return;
    }
    D = sqrt(D);
    if (V0 < 0) {
        if (V2 > 0) {
            M8 = 1;
            risetflag = 1; // set moonrise status
        }
    }
    if (V0 > 0) {
        if (V2 < 0) {
            W8 = 1;
            risetflag = 2; // set moonset status
        }
    }
    E = (-B + D) / (2 * A);
    if (E < 0 || E > 1) {
        E = (-B - D) / (2 * A);
    } // update for version 1.0.159
    T3 = C0 + E + 1 / 120;
    H3 = floor(T3);
    M3 = floor((T3 - H3)*60);
    mnthour = H3;
    mntminute = M3;
    if (risetflag == 1) { // moonrise status
        rmth0 = mnthour;
        rmtm0 = mntminute;
        rmth1 = rmth0 / 10;
        rmth0 = rmth0 - (rmth1 * 10);
        rmtm1 = rmtm0 / 10;
        rmtm0 = rmtm0 - (rmtm1 * 10);
        mrisehr = mnthour; // update for version 1.0.209
    } // moonrise
    if (risetflag == 2) { // moonset status
        smth0 = mnthour;
        smtm0 = mntminute;
        smth1 = smth0 / 10;
        smth0 = smth0 - (smth1 * 10);
        smtm1 = smtm0 / 10;
        smtm0 = smtm0 - (smtm1 * 10);
        msethr = mnthour; // update for version 1.0.209
    } // moonset
}

void gosub450() {
    spcond = 0;
    if (M8 == 0) {
        if (W8 == 0) {
            if (V2 < 0) {
                spcond = 3;
            }
            if (V2 > 0) {
                spcond = 4;
            }
            return;
        } // w8
        spcond = 1;
    } // m8
    if (W8 == 0) {
        spcond = 2;
    }
}

void moontimes() { // update for version 1.0.210
    long htz = timezone; // timezone value from configuration
    float xtz = htz; // copy to float for fabs operation
    float Y; //
    istzneg = 0; //
    if (htz < 0) {
        istzneg = 1;
    }
    if (istzneg == 0) { // is time zone a negative value?
        H = 0 - htz; // make time zone negative instead of positive (switch signs)
    } else { //
        H = 0 + fabs(xtz); // make time zone positive instead of negative (switch signs)
    } // the moon calc routine expects timezone expressed in hours West of Greenwich
    P2 = PIx2; // update for version 1.0.156
    RR = d2r; //
    K1 = 15 * RR * 1.0027379; //
    B5 = gpslat;
    L5 = gpslon; // input coordinates
    L5 = L5 / 360;
    Z0 = H / 24;
    Y = gpsyear; // uyear
    M = gpsmonth; // umonth
    D = gpsday; // uday
    G = 1;
    D1 = floor(D);
    F = D - D1 - 0.5;
    J = -floor(7 * (floor((M + 9) / 12) + Y) / 4);
    mnbuff = M - 9;
    if (mnbuff < 0) {
        mnbuff = -1;
    }
    if (mnbuff > 0) {
        mnbuff = 1;
    }
    S = mnbuff;
    A = fabs(M - 9);
    J3 = floor(Y + S * floor(A / 7));
    J3 = -floor((floor(J3 / 100) + 1)*3 / 4);
    J = J + floor(275 * M / 9) + D1 + G*J3;
    J = J + 1721027 + 2 * G + 367 * Y;
    if (F < 0) {
        F = F + 1;
        J = J - 1;
    }
    T = (J - 2451545) + F;
    T0 = T / 36525;
    S = 24110.5 + 8640184.813 * T0;
    S = S + 86636.6 * Z0 + 86400 * L5;
    S = S / 86400;
    S = S - floor(S);
    T0 = S * 360 * RR;
    T = T + Z0;
    gosub495();
    M11 = A5;
    M12 = D5;
    M13 = R5;
    gosub495();
    M21 = A5;
    M22 = D5;
    M23 = R5;
    gosub495();
    M31 = A5;
    M32 = D5;
    M33 = R5;
    if (M21 > M11) {
        goto line85;
    }
    M21 = M21 + P2;
line85:
    if (M31 > M21) {
        goto line95;
    }
    M31 = M31 + P2;
line95:
    Z1 = RR * (90.567 - 41.685 / M23);
    S = sinf(B5 * RR);
    C = cosf(B5 * RR);
    Z = cosf(Z1);
    M8 = 0;
    W8 = 0;
    A0 = M11;
    D0 = M12;
    for (C0 = 0; C0 < 23; C0++) {
        P = (C0 + 1) / 24;
        F0 = M11;
        F1 = M21;
        F2 = M31;
        gosub225();
        A2 = F;
        F0 = M12;
        F1 = M22;
        F2 = M32;
        gosub225();
        D2 = F;
        gosub285();
        A0 = A2;
        D0 = D2;
        V0 = V2;
    }
    gosub450();
    char printflaga = 0;
    char printflagb = 0;
    if (spcond == 0) {
        if (mrisehr < msethr) { // print in order of occurrence
            printflaga = 1;
            printflagb = 1;
        } else {
            sprintf(line2, "Moonrise:  %d%d:%d%d", rmth1, rmth0, rmtm1, rmtm0);
            sprintf(line3, "Moonset:   %d%d:%d%d", smth1, smth0, smtm1, smtm0);
        }
    }
    if (spcond == 1) {
        sprintf(line3, "No Moonrise Today"); // update for version 1.0.214
        printflagb = 1;
    }
    if (spcond == 2) {
        printflaga = 1;
        sprintf(line2, "No Moonset Today"); // update for version 1.0.214
    }
    if (spcond == 3 || spcond == 4) { // update for version 1.0.212
        sprintf(line3, "Moon is ");
        if (spcond == 3) {
            sprintf(line3 + 8, "Down");
        }
        if (spcond == 4) {
            sprintf(line3 + 8, "Up");
        }
        sprintf(line2, "All Day");
    } // spcond
    if (printflaga == 1) {
        sprintf(line3, "Moonrise:  %d%d:%d%d", rmth1, rmth0, rmtm1, rmtm0);
    }
    if (printflagb == 1) {
        sprintf(line2, "Moonset:   %d%d:%d%d", smth1, smth0, smtm1, smtm0);
    }
    astrocaseconvert(); // update for version 1.0.212
}
//
// Sun rise and set times calculation functions are
// a condensed version of those found on this page:
// http://www.sci.fi/~benefon/rscalc.c
// Written by Jarmo Lammi, and accurate until 2099.
// The original provides more Sun information that
// has been removed for this source.
//

double FNrange(double x) {
    double b = 0.5 * x / PI;
    double a = PIx2 * (b - (long) (b)); // update for version 1.0.156
    if (a < 0) a = PIx2 + a; // update for version 1.0.156
    return a;
};

void showhrmn(double dhr) {
    hr = (int) dhr;
    mn = (dhr - (double) hr)*60;
};

void sunset() { // update for version 1.0.210
    BYTE xrisehour; //
    BYTE xriseminute; //
    BYTE xsethour; //
    BYTE xsetminute; //
    int fny = gpsyear; //
    int fnm = gpsmonth; //
    int fnd = gpsday; //
    double settm; //
    double riset; //
    double latit, longit;
    double tzone, d, lambda;
    double obliq, alpha, delta, LL, equation, ha;
    rads = d2r;
    latit = (double) gpslat;
    longit = (double) gpslon;
    tzone = (double) timezone;
    long int luku = -7 * (fny + (fnm + 9) / 12) / 4 + 275 * fnm / 9 + fnd;
    luku += (long int) fny * 367;
    d = (double) luku - 730531.5 + 0.5;
    L = FNrange(280.461 * rads + .9856474 * rads * d);
    g = FNrange(357.528 * rads + .9856003 * rads * d);
    lambda = FNrange(L + 1.915 * rads * sin(g) + .02 * rads * sin(2 * g));
    obliq = 23.439 * rads - .0000004 * rads * d;
    alpha = atan2(cos(obliq) * sin(lambda), cos(lambda));
    delta = asin(sin(obliq) * sin(lambda));
    LL = L - alpha;
    if (L < PI) LL += PIx2; // update for version 1.0.156
    equation = 1440.0 * (1.0 - LL / PI / 2.0) / 60.0; // update for version 1.0.156
    double fflat = latit; // update for version 1.0.212
    double ffdeclin = delta;
    double fffo, ffdfo;
    ffdfo = rads * (0.5 * SunDia + AirRefr);
    if (fflat < 0.0) ffdfo = -ffdfo;
    fffo = tan(ffdeclin + ffdfo) * tan(fflat * rads);
    if (fffo > 0.99999) fffo = 1.0;
    fffo = asin(fffo) + PIo2;
    ha = fffo / PI;
    riset = 12.0 - 12.0 * ha + tzone - longit / 15.0 + equation; // update for version 1.0.156
    settm = 12.0 + 12.0 * ha + tzone - longit / 15.0 + equation; //
    if (riset > 24.0) riset -= 24.0;
    if (settm > 24.0) settm -= 24.0;
    showhrmn(riset);
    xrisehour = hr;
    xriseminute = mn;
    showhrmn(settm);
    xsethour = hr;
    xsetminute = mn;
    sprintf(line3, "Sunrise:   %02d:%02d", xrisehour, xriseminute);
    sprintf(line2, "Sunset:    %02d:%02d", xsethour, xsetminute);
    astrocaseconvert(); // update for version 1.0.212
}
//
// Moon percentage illumination functions
// This is based on Moontool by John Walker.
// An original source can be found here:
// http://tinyurl.com/o8spxte
// The version in this source is stripped down to
// discard extra information provided by the original.
//

static long double phase() { // update for version 1.0.212
    long double pdate;
    long ddc, ddm, ddyl;
    ddyl = uyear;
    ddm = umonth;
    if (ddm > 2) {
        ddm -= 3;
    } else {
        ddm += 9;
        ddyl--;
    }
    ddc = ddyl / 100L;
    ddyl -= 100L * ddc;
    pdate = uday + (ddc * 146097L) / 4 + (ddyl * 1461L) / 4 + (ddm * 153L + 2) / 5 + 1721119L;
    pdate = pdate - 0.5;
    pdate = pdate + (usec + 60L * (long) umin + 3600L * (long) uhour) / 86400.0;
    long double Day, N, M, Ec, Lambdasun, ml, MM, Ev, Ae, A3, MmP,
            mEc, A4, lP, V, lPP, MoonAge, MoonPhase;
    Day = pdate - epoch;
    N = fixangle((360 / 365.2422) * Day);
    M = fixangle(N + elonge - elongp);
    double kepm = M; // update for version 1.0.212
    double kepecc = eccent;
    long double kepe, kepdelta;
    kepe = kepm = torad(kepm);
    do {
        kepdelta = kepe - kepecc * sin(kepe) - kepm;
        kepe -= kepdelta / (1 - kepecc * cos(kepe));
    } while (abs(kepdelta) > EPSILON);
    Ec = kepe;
    Ec = sqrt((1 + eccent) / (1 - eccent)) * tan(Ec / 2);
    Ec = 2 * todeg(atan(Ec));
    Lambdasun = fixangle(Ec + elongp);
    ml = fixangle(13.1763966 * Day + mmlong);
    MM = fixangle(ml - 0.1114041 * Day - mmlongp);
    Ev = 1.2739 * sin(torad(2 * (ml - Lambdasun) - MM));
    Ae = 0.1858 * sin(torad(M));
    A3 = 0.37 * sin(torad(M));
    MmP = MM + Ev - Ae - A3;
    mEc = 6.2886 * sin(torad(MmP));
    A4 = 0.214 * sin(torad(2 * MmP));
    lP = ml + Ev + mEc - Ae + A4;
    V = 0.6583 * sin(torad(2 * (lP - Lambdasun)));
    lPP = lP + V;
    MoonAge = lPP - Lambdasun;
    MoonPhase = (1 - cos(torad(MoonAge))) / 2;
    return (MoonPhase * 100);
}
//
#pragma config FNOSC = FRC      // Clock Switching Enabled and Fail Safe Clock Monitor is disabled
#pragma config FCKSM = CSECMD   // OSC2 Pin Function: OSC2 is Clock Output
#pragma config OSCIOFNC = OFF   //
#pragma config POSCMD = HS      // Primary Oscillator Mode: XT Crystal
#pragma config FWDTEN = OFF     // Watchdog Timer Enabled/disabled by user software
//
void PixelOutput(IMG_PIXEL_XY_RGB_888 *pPix);
//
// start serial interrupt

void Init_UART() {
    IFS1bits.U2RXIF = 0; // reset rx interrupt flag
    IPC7bits.U2RXIP = 2; // set high priority
    U2STAbits.URXISEL = 0; // interrupt after one byte received 
    IEC1bits.U2RXIE = 1; // enable rx interrupt
}
//
// interrupt service routine for gps serial receive
// borrows a section of the idtext array which is not
// used by any demo program that uses the gps module
// also filters gprmc nmea sentences, and sets a flag
// gotgprmc is 2 when data is received, and setting it
// to zero forces another data update to be received

void __attribute__((interrupt, no_auto_psv)) _U2RXInterrupt(void) {
    if (U2STAbits.URXDA != 0) {
        rxbyte = U2RXREG;
    }
    if (gotgprmc < 2) {
        idtext[rxcount + 20] = rxbyte;
        rxcount++;
    }
    if (rxbyte == 0x24 && gotgprmc == 0) {
        idtext[20] = 0x24;
        rxcount = 1;
    }
    if (rxcount == 6) { // test for gprmc (77)
        if (idtext[24] == 0x4D && idtext[25] == 0x43) {
            gotgprmc = 1;
        }
    } // rxcount
    if (rxcount > 77) { // limit number of bytes received (77)
        rxcount = 0;
        if (gotgprmc == 1) {
            gotgprmc = 2;
        }
    } // rxcount
    IFS1bits.U2RXIF = 0; // clear interrupt flag 
}
//
// stop timer interrupt

void stoptimer() { // update for version 1.0.204
    IEC0bits.T1IE = 0;
    T1CONbits.TON = 0; // turn off timer interrupt  
}
//
// start timer interrupt

void Init_Timer1() {
    T1CON = 0; // timer reset
    IFS0bits.T1IF = 0; // reset timer1 interrupt flag
    IPC0bits.T1IP = 6; // low interrupt priority
    IEC0bits.T1IE = 1; // enable timer1 interrupt
    TMR1 = 0x0000; //	
    PR1 = 0xFFFF; // timer1 period register
    T1CONbits.TON = 1; // enable timer1 and start the counter
}
//
// interrupt service routine for timer
// normally used for counting seconds locally
// but also used for random number generation

void __attribute__((interrupt, no_auto_psv)) _T1Interrupt(void) {
    TMR1 = 25538; // reload timer value 100kHz interrupt
    IFS0bits.T1IF = 0;
    T1CONbits.TON = 0;
    tcount++;
    if (tcount == 1000) {
        tcount = 0;
        secondschanged++;
    }
    T1CONbits.TON = 1; // reset timer one interrupt flag
}
//
/******************************************************************************/
//
//

int main(void) {
    RCONbits.SWDTEN = 0; // disable watchdog timer
    PLLFBD = 38; //
    CLKDIVbits.PLLPOST = 0; //
    CLKDIVbits.PLLPRE = 0; //
    __builtin_write_OSCCONH(0x03); // initiate clock switch
    __builtin_write_OSCCONL(0x01); // start clock switching
    while (OSCCONbits.COSC != 0b011); // wait for clock switch
    ADPCFG = 0xFFFF; // set analog ports to digital 
    TRISA = 0x0000; //
    TRISB = 0b0001110001000000; //
    LATA = 0x0000;
    LATB = 0x0000; //
    PORTA = 0x0000;
    PORTB = 0x0000; //
    // configure hardware SPI1 pins
    RPINR20bits.SCK1R = 8; // configure SCKI for input?
    RPINR20bits.SDI1R = 6; // configure SDI for input
    RPOR3bits.RP7R = 7; // configure sdo1 as output
    RPOR4bits.RP8R = 8; // configure sck1 as output
    RPOR4bits.RP9R = 9; // configure ss1 as output
    // configure gps hardware serial port receive pin
    RPINR19 = 0x000B; // assign RP11 pin to U2RX function
    // update for version 1.0.138
    RPOR2 = 0x0005; // assign RP4 pin to U2TX function
    UMODE = 0x8000; // set UARTEN must be done before setting UTXEN
    UBRG = 259; // 9600 baud default for gps module
    //
    _LATB13 = 1; // set vs1003 chip select pins high to deselect
    _LATB14 = 1; //
    SSERIAL = 1; // serial idle state
    GPSPWR_PIN = 1; // start with gps module powered off
    delay_gen(2600); // update for version 1.0.194
    //
    // startup character test updated for version 1.0.63
    rotation = 0;
    trotation = 0; // borrowed variables for character test
    n = 0; //
    while (n < 512) { // copy data to upper and lower frame buffer
        framebuffer[n] = charmap[trotation]; //
        framebuffer[n + 512] = charmap[trotation]; //
        n++;
        trotation++;
        rotation++; //
        if (rotation > 4) {
            rotation = 0;
            n = n + 3;
        } //
    } // n
    trotation = 0; // update for version 1.0.212
    //
    ox = 63.5;
    oy = 31.5; // true screen center
    delay_gen(999); // update for version 1.0.192
    wordrandom = noiseforrandom(); // update for version 1.0.192
    RCFGCALbits.RTCPTR = 1; // update for version 1.0.218
    wordrandom = wordrandom + RTCVAL; // get invalid rtc value
    lcdmoduleinit(); // initialise lcd module
    //
    while (1) {
        drawscreen();
        //
        if (framecount < 9000) { // increment frame counter
            if (framecount < 5) {
                n = 0;
                lcdmoduleinit(); // update for version 1.0.66
                while (n < 511) {
                    framebuffer[n] = framebuffer[n + 1];
                    framebuffer[n + 512] = framebuffer[n + 513];
                    n++;
                } // rotate frame buffer
                framebuffer[1023] = charmap[framecount + 320];
                framebuffer[511] = charmap[framecount + 320];
            }
            if (framecount == 8) {
                reset_register_check();
                hardware_sanity_check();
                if (joyspareb != 0) {
                    sdcardanimation();
                } // card is not inserted
                read_config();
                if (configerror == 1) {
                    nocardiniterror();
                } // card failed first initialise
                software_sanity_check();
                loadintro();
                playvoice();
                if (videointro != 0) {
                    full_motion_video();
                }
                resettomenu();
                loadbitmap();
                demo = -1;
                framecount = 9;
                // set default selected item with all panels ready for first menu transition
                springmenuvars[0] = 999; // set transition to ignore safeguard
                n = 0;
                while (n < 5) { // update for version 1.0.108
                    menuvars[firstmenu] = menupositions[n] + 1; // update for version 1.0.184
                    n++;
                    firstmenu++;
                    if (firstmenu > 4) {
                        firstmenu = 0;
                    }
                } // n
            }
            if (framecount > 18 && framecount < 100) {
                demo = 1;
            }
            if (framecount == 31 && menuvars[5] == 0) {
                menuvars[5] = 1;
            } // no direction, 2 = up , 3 = down
            framecount++;
        }
        //
        //
        //***************************************************************************************************************
        //*****************************************                             *****************************************
        //*****************************************  ANIMATED FIVE OPTION MENU  *****************************************
        //*****************************************                             *****************************************
        //***************************************************************************************************************
        //
        //
        if (demo == 1) {
            clrframebuffer();
            if (framecount < 30) {
                JOYPAR_PIN = 0;
                delay_gen(0);
                if (JOYDAT_PIN == 0 && imagebuffer[0] == 0x42) { // update for version 1.0.133
                    trotation++; // update for version 1.0.212
                    if (trotation > 6) { // joystick to be held left
                        runtimeconfigenable(); // update for version 1.0.190
                    } else { // or too early
                        drawplasma();
                        printcredit();
                    } // trotation
                } else { // joystick was not held left
                    drawplasma();
                    printcredit();
                    trotation = 0;
                }
                JOYPAR_PIN = 1;
                LED_PIN = 1; // backlight stays on for plasma
                backlighttimer = backlightdelay; // set backlight timer
            } else {
                if (framecount == 30) {
                    trotation = 0; // reset the variable just used as a counter just in case
                    if (pitchchanger == 1) {
                        loadpitchchanger();
                    } else {
                        loadspectrum();
                    } // load vs1003b program
                    if (autoplayer == 0) {
                        menutransition(1);
                    } // update for version 1.0.67
                    if (configerror > 2) {
                        lasterror = 7;
                    } // update for version 1.0.199
                    Init_Timer1(); // start interrupt timer for random seed generation
                } // framecount
                framecount = 31; // stay in menu
                //
                int hoff = 0; // update for version 1.0.215
                hoff = hoff - joyright;
                hoff = hoff + joyleft;
                if (hoff == oldhoff && hoff != 0) { // horizontal bump effect
                    thoff++; // delay for bump effect
                    if (thoff > 3) {
                        hoff = 0;
                    } // reset horizontal position
                } else {
                    oldhoff = hoff;
                    thoff = 0;
                } // hoff
                //
                if (menudelay == 0) {
                    menuvars[6] = 10;
                    menuvars[7] = 10;
                } // menudelay
                if (menuvars[5] < 2) {
                    shiftbytein();
                }
                if (joyspareb != 0) {
                    sdcardanimation();
                } // card is not inserted
                //
                if (joyup == 1) {
                    menuvars[6]++;
                    backlighttimer = backlightdelay + backlightdelay; // start backlight timer
                    if (menuvars[6] > 5) { // update for version 1.0.198
                        if (menuvars[5] == 1) {
                            menuvars[5] = 3;
                            playblopsound();
                        }
                        menuvars[6] = 0;
                    }
                } else {
                    menuvars[6] = 0;
                } // joyup

                if (joydown == 1) {
                    menuvars[7]++;
                    backlighttimer = backlightdelay + backlightdelay; // start backlight timer
                    if (menuvars[7] > 5) { // update for version 1.0.198
                        if (menuvars[5] == 1) {
                            menuvars[5] = 2;
                            playblopsound();
                        }
                        menuvars[7] = 0;
                    }
                } else {
                    menuvars[7] = 0;
                } // joydown

                if (joyfire == 0) {
                    lastpressed = 0;
                } else {
                    backlighttimer = backlightdelay; // start backlight timer

                    if (lastpressed == 0) {
                        stoptimer(); // update for version 1.0.204
                        playtinydirect();
                        menutransition(0);

                        if (menuvars[4] == 17) {
                            demo = 6;
                            calgrid();
                        } // table format graphic calendar
                        if (menuvars[3] == 17) { // tetrabrick
                            demo = 5;
                            GPSPWR_PIN = 1;
                            tetransition();
                            tetrastart();
                        }
                        if (menuvars[2] == 17) {
                            framecount = 100;
                            demo = 2;
                        } // vector mapping gps
                        if (menuvars[1] == 17) { // mp3 player
                            framecount = 101;
                            demo = 3; // update for version 1.0.189
                            GPSPWR_PIN = 1; // gps module power off
                            xx = 0;
                            yy = 0; // being used by pitch changer
                            fileextension[4] = 0x33; // change extension to *.mp3
                        }
                        if (menuvars[0] == 17) {
                            framecount = 101;
                            demo = 4;
                            Init_UART();
                            astrotransition(1);
                        } // astronomy clock
                    } // lastpressed
                    lastpressed = 1;
                } // joyfire
                //
                n = 0;
                while (n < 5) {
                    if (menuvars[n] == 17) {
                        drawroundedrectangle(0, springmenuvars[n], 127, 29); // draw button border
                    } else { // draw non selected button border
                        drawroundedrectangle(1 + hoff, springmenuvars[n], 125, 29); // update for version 1.0.215
                    }
                    // print menu items
                    clrfb = 0;
                    while (clrfb < 10) {
                        text[clrfb] = programs[clrfb + (n * 10)];
                        clrfb++;
                    }
                    text[10] = 0;
                    tx = 3;
                    ty = 4 + springmenuvars[n];
                    if (menuvars[n] == 17) {
                        drawtextlargesin();
                    } else {
                        ty = ty + 4;
                        tx = tx + hoff; // update for version 1.0.215
                        drawtextlarge();
                    }
                    n++;
                }

                if (menuvars[5] == 2) {
                    n = 0;
                    while (n < 5) {
                        menuvars[n] = menuvars[n] - 1;
                        if (menuvars[n] < -31) {
                            menuvars[n] = 128;
                        }
                        n++;
                    }
                    menuspringphysics(1); // joy down menu up
                    n = 0;
                    while (n < 5) {
                        if (menuvars[n] == 17) {
                            menuvars[5] = 1;
                            menuspringphysics(0);
                            break;
                        }
                        n++;
                    }
                } // up

                if (menuvars[5] == 3) {
                    n = 0;
                    while (n < 5) {
                        menuvars[n] = menuvars[n] + 1;
                        if (menuvars[n] > 127) {
                            menuvars[n] = -32;
                        }
                        n++;
                    }
                    menuspringphysics(2); // joy up menu down
                    n = 0;
                    while (n < 5) {
                        if (menuvars[n] == 17) {
                            menuvars[5] = 1;
                            menuspringphysics(0);
                            break;
                        }
                        n++;
                    }
                } // down

                if (backlighttimer > 0) { // handle lcd backlight
                    backlighttimer--;
                    LED_PIN = 1;
                } else {
                    plasmasaver();
                }
            } // framecount

            if (autoplayer == 1 && framecount > 29) { // auto start mp3 player
                framecount = 101;
                demo = 3; // update for version 1.0.187
                fileextension[4] = 0x33; // change extension to *.mp3
            } // autoplayer

        } // demo 1
        //
        //
        //***************************************************************************************************************
        //**************************************************            *************************************************
        //************************************************** MP3 PLAYER *************************************************
        //**************************************************            *************************************************
        //***************************************************************************************************************
        //
        // jpgpixcount[0-3] used as previous sample buffers for averaging
        // xx,yy used as pitch and tempo values by the pitch changer program
        //
        if (demo == 3) { // mp3 player demo section
            clrframebuffer(); // clear frame buffer
            if (framecount == 101 && cardstat == 0) {
                if (!sdcardinit()) {
                    cardstat = 0;
                    sdcardanimation();
                } else { // update for version 1.0.118
                    cardstat = 1;
                    sprintf(text, "MUSIC");
                    if (FSchdir(text) != 0) {
                        debugout(5);
                        dirnotfound();
                        framecount = 19;
                    }
                }
            } // cardstat

            // check the card insert switch always
            if (joyspareb == 0) {
                socketstate = 1;
            } else {
                socketstate = 0;
                cardstat = 0;
                sdcardanimation();
            }
            if (cardstat == 1 && framecount == 101) {
                // update for version 1.0.148
                if (serialenabled == 1) {
                    UBRG = 2082;
                } // 1200 baud for info output
                drawcountmpgraphic(8); // draw file count graphic
                file_browserv2();
                clrframebuffer(); // clear file count graphic
                if (filecount > 0) {
                    FSfseek(pointer, 0, SEEK_SET); // replacement for opening the file
                    // seek past id3 tag
                    if (tagsize != 0 && iseyedeethree != 0) {
                        FSfseek(pointer, tagsize, SEEK_SET);
                        fileposition = tagsize;
                    } // tagsize

                    FSfread(receivebuffer, 1, 32, pointer);
                    fileposition = fileposition + 32;
                    bufready = 1;
                    mpfileopen = 1;
                } // filecount
            }

            if (framecount == 104) {
                SD_CS = 1;
                SPISTAT = 0; // deselect sd card
                delay_gen(0);
            }

            if (VSDREQ == 1 && framecount == 110) {
                vscommand(0x03, clockbyte, 0x30); // write clock register
                while (VSDREQ == 0) {
                    delay_gen(0);
                }
                vscommand(0x0B, mpvolumea, mpvolumeb); // write volume
                delay_gen(0);
            }

            if (VSDREQ == 1 && framecount == 125) {
                playing = 1;
            }
            fillaudiobuffer(); // fill vs1003b audio buffer
            if (mpvol != 0) {
                if (mpvol == 1) {
                    if (mpvolume > 4) {
                        mpvolume = mpvolume - 5;
                    }
                } else {
                    if (mpvolume < 56) {
                        mpvolume = mpvolume + 5;
                    }
                } // mpvol
                unsigned short volwork = (mpvolume * 256) + mpvolume;
                mpvolumeb = volwork >> 8; // update for version 1.0.134
                mpvolumea = volwork & 0b0000000011111111; //
                mpvol = 0;
                vscommand(0x0B, mpvolumea, mpvolumeb); // write volume
                delay_gen(1);
            } // mpvol
            //
            if (mpskip != 0) {
                silenceframes(); // fix for clicks with track skip for version 1.0.34
                SPISTAT = 0x8000;
                SD_CS = 0; // select sd card
                FSfclose(pointer);
                bufready = 0;
                mpfileopen = 0;
                fileposition = 0;
                if (mpskip == 1) {
                    fnumber++; // select next mp3 file
                    if (fnumber > filecount - 1) {
                        fnumber = 0;
                    }
                } else {
                    fnumber--; // select previous mp3 file
                    if (fnumber < 0) {
                        fnumber = filecount - 1;
                    }
                } // mpskip
                //
                if (mpshuffle == 0) {
                    int rangestart = getstartofrange();
                    int rangeend = getstartofrange() + 880;
                    if (fnumber < rangestart || fnumber > rangeend) { // update for version 1.0.37
                        drawcountmpgraphic(40);
                        recachefiles(fnumber);
                        rangestart = getstartofrange();
                    } // fnumber
                    retrievefilename(fnumber - rangestart);
                    drawmpscreen(0);
                    rec.entry = fnumber;
                    pointer = FSfopen(rec.filename, "r");
                    filesize = rec.filesize; // update for version 1.0.172
                    FSfclose(pointer);
                } else {
                    get_file();
                } // mpshuffle
                //
                eyedeethree();
                serialeyedeethree();
                eyedeethreepos = 1;
                eyedeethreedel = 90;
                FSfseek(pointer, 0, SEEK_SET); // replacement for opening the file

                // seek past id3 tag
                if (tagsize != 0 && iseyedeethree != 0) {
                    FSfseek(pointer, tagsize, SEEK_SET);
                    fileposition = tagsize;
                } // tagsize

                FSfread(receivebuffer, 1, 32, pointer);
                fileposition = fileposition + 32;
                bufready = 1;
                mpfileopen = 1;

                SD_CS = 1; // deselect sd card
                SPISTAT = 0;
                delay_gen(0);
                fillbufferwithzeros();

                n = 0;
                while (n < 14) {
                    line3[n] = 0;
                    n++;
                }
                vuneedles[0] = 0;
                vuneedles[1] = 0; // update for version 1.0.189
                lrSum[0] = 0;
                lrSum[1] = 0; // reset root square variables
                lrPhase = 0; // reset phase variable
                jpgpixcount[0] = 0;
                jpgpixcount[1] = 0; // update for version 1.0.191
                jpgpixcount[2] = 0;
                jpgpixcount[3] = 0; //
                if (saskin > 6 && saskin < 9) {
                    clearwaterfall();
                } // update for version 1.0.189
                mpresetdecodetime(); // reset decode time for next track
                mpresetdecodetime(); // suggested by vlsi to do this twice
                mpvbr = 0;
                mpvbrcount = 0;
                mpskip = 0;
            } // mpskip

            if (playing == 1 && mpfileopen == 0) { // open next file
                silenceframes();
                SPISTAT = 0x8000;
                SD_CS = 0; // select sd card

                fileposition = 0;
                fnumber++; // select next mp3 file
                if (fnumber > filecount - 1) {
                    fnumber = 0;
                }

                if (mpshuffle == 0) {
                    int rangestart = getstartofrange();
                    int rangeend = getstartofrange() + 880;
                    if (fnumber < rangestart || fnumber > rangeend) { // update for version 1.0.37
                        drawcountmpgraphic(40);
                        recachefiles(fnumber);
                        rangestart = getstartofrange();
                    } // fnumber
                    retrievefilename(fnumber - rangestart);
                    drawmpscreen(0);
                    rec.entry = fnumber;
                    pointer = FSfopen(rec.filename, "r");
                    filesize = rec.filesize; // update for version 1.0.172
                    FSfclose(pointer);
                } else {
                    get_file();
                } // mpshuffle
                //
                eyedeethree();
                serialeyedeethree();
                eyedeethreepos = 1;
                eyedeethreedel = 90;
                FSfseek(pointer, 0, SEEK_SET); // replacement for opening the file

                // seek past id3 tag
                if (tagsize != 0 && iseyedeethree != 0) {
                    FSfseek(pointer, tagsize, SEEK_SET);
                    fileposition = tagsize;
                } // tagsize

                FSfread(receivebuffer, 1, 32, pointer);
                fileposition = fileposition + 32;
                bufready = 1;
                mpfileopen = 1;

                SD_CS = 1; // deselect sd card
                SPISTAT = 0;
                delay_gen(0);
                fillbufferwithzeros();
                n = 0;
                while (n < 14) {
                    line3[n] = 0;
                    n++;
                }
                vuneedles[0] = 0;
                vuneedles[1] = 0; // update for version 1.0.189
                lrSum[0] = 0;
                lrSum[1] = 0; // reset root square variables
                lrPhase = 0; // reset phase variable
                jpgpixcount[0] = 0;
                jpgpixcount[1] = 0; // update for version 1.0.191
                jpgpixcount[2] = 0;
                jpgpixcount[3] = 0; //
                if (saskin > 6 && saskin < 9) {
                    clearwaterfall();
                } // update for version 1.0.189
                mpresetdecodetime(); // reset decode time for next track
                mpresetdecodetime(); // suggested by vlsi to do this twice
                mpvbr = 0;
                mpvbrcount = 0;
            } // mpfileopen
            //
            int dtminutes = 0;
            int dtseconds = 0;
            int rtminutes = 0;
            int rtseconds = 0;
            int ttminutes = 0;
            int ttseconds = 0;
            int trminutes = 0;
            int trseconds = 0;
            int grminutes = 0;
            int grseconds = 0;
            unsigned char mpid = 0;
            unsigned char bitrate = 0;
            int bitratefinal = 0;

            // read spectrum data
            if (playing == 1 && mpfileopen == 1 && VSDREQ == 0) {

                // send spectrum data using hardware serial
                // first part of spectrum data packet
                if (serialenabled == 2 && pitchchanger == 0) {
                    txbyte = 0xFF;
                    hardserialout();
                }

                if (pitchchanger == 0 || saskin < 9) { // update for version 1.0.190
                    unsigned char sval = 0;
                    vscommand(0x07, 0x18, 0x04); // write
                    for (n = 0; n < 14; n++) {
                        VSXCS = 0; // select vs1003 instruction
                        WriteSPIManual(0x03); // read
                        WriteSPIManual(0x06);
                        sval = ReadSPIManual(); // shouldn't need this value
                        sval = ReadSPIManual(); // bits 0-5 needed from this value
                        VSXCS = 1; // deselect vs1003 instruction
                        sval = sval << 2;
                        sval = sval >> 2;
                        line3[n] = sval;
                    } // n
                } // pitchchanger

                // send spectrum data using hardware serial
                // second part of spectrum data packet
                if (serialenabled == 2 && pitchchanger == 0) {
                    fillaudiobuffer(); // fill vs1003b audio buffer
                    n = 2; // start index for the ten spectrum values
                    while (n < 7) { // update for version 1.0.142
                        txbyte = line3[n];
                        hardserialout();
                        n++;
                    } // n
                } // serialenabled

                // get decode time
                unsigned char dtvala = 0;
                unsigned char dtvalb = 0;

                fillaudiobuffer(); // fill vs1003b audio buffer
                if (saskin == 9) { // update for version 1.0.189
                    stereolevelsrs();
                    stereolevelsrs();
                    stereolevelsrs();
                    stereolevelsrs();
                    fillaudiobuffer();
                } // saskin

                while (VSDREQ == 0) {
                    delay_gen(0);
                }
                VSXCS = 0; // select vs1003 instruction
                WriteSPIManual(0x03); // read
                WriteSPIManual(0x04); // decode time
                dtvala = ReadSPIManual(); //
                dtvalb = ReadSPIManual(); //
                VSXCS = 1; // deselect vs1003 instruction

                // elapsed time
                decodetime = dtvala;
                decodetime = decodetime << 8;
                decodetime = decodetime + dtvalb;
                dtminutes = decodetime / 60;
                dtseconds = decodetime - (dtminutes * 60);

                // remaining time
                float secseg = 0.0;
                unsigned short decoderem = 0;
                if (decodetime > 0) {
                    secseg = fileposition / decodetime;
                    decoderem = (filesize - fileposition) / secseg;
                }
                rtminutes = decoderem / 60;
                rtseconds = decoderem - (rtminutes * 60);

                // get ID
                VSXCS = 0; // select vs1003 instruction
                WriteSPIManual(0x03); // read
                WriteSPIManual(0x09); // hdat1
                dtvala = ReadSPIManual(); //
                dtvalb = ReadSPIManual(); //
                VSXCS = 1; // deselect vs1003 instruction
                dtvalb = dtvalb << 3;
                dtvalb = dtvalb >> 6;
                mpid = dtvalb;

                // get bitrate
                long int filesizebits = 0;
                long int decodedrate = 0;
                long int totalduration = 0;
                VSXCS = 0; // select vs1003 instruction
                WriteSPIManual(0x03); // read
                WriteSPIManual(0x08); // hdat0
                dtvala = ReadSPIManual(); //
                dtvalb = ReadSPIManual(); //
                VSXCS = 1; // deselect vs1003 instruction
                bitrate = dtvala >> 4;
                //
                if (pitchchanger != 0) {
                    stereopeaklevels();
                } // update for version 1.0.189
                if (saskin == 9) { // update for version 1.0.190
                    stereolevelsrs();
                    stereolevelsrs();
                    stereolevelsrs();
                    stereolevelsrs();
                } // saskin
                fillaudiobuffer(); // fill vs1003b audio buffer
                //
                // prevent low bitrate values unsupported by this program
                // but supported by the vs1003b accessing out of bounds
                if (bitrate < 8) {
                    bitrate = 8;
                } // update for version 1.0.184
                //
                if (mpid == 3) {
                    decodedrate = stdbitratesa[bitrate - 8] * 2; // update for version 1.0.184
                } else {
                    decodedrate = (bitrate - 8) * 16; // update for version 1.0.184
                    decodedrate = decodedrate + 64; // table b calculated dynamically
                } // mpid
                //
                // another way to find bitrate
                long int filepositionbits = 0;
                long int decodedrateguess = 0;
                long int secondsposition = 0;
                int bitrateerror = 1000;
                int bitrateerrorfinal = 1000;

                if (mpvbr == 1) {
                    n = 0;
                    while (n < 8) {
                        decodedrateguess = bitrates[n] * 2; // update for version 1.0.184
                        decodedrateguess = decodedrateguess * 1024; // convert to bits per second
                        filepositionbits = fileposition - 8192; // accounting for stream buffer in vs1003b
                        filepositionbits = filepositionbits * 8;
                        secondsposition = filepositionbits / decodedrateguess; // position in seconds
                        bitrateerror = 0; // update for version 1.0.174
                        if (secondsposition > decodetime) {
                            bitrateerror = secondsposition - decodetime;
                        }
                        if (secondsposition < decodetime) {
                            bitrateerror = decodetime - secondsposition;
                        }
                        if (bitrateerror < bitrateerrorfinal) { // update for version 1.0.184
                            bitrateerrorfinal = bitrateerror;
                            bitratefinal = bitrates[n]*2;
                            decodedrate = bitrates[n]*2;
                        } // bitrateerror
                        n++;
                    }
                } else { // mpvbr
                    // get bitrate from stream
                    decodedrateguess = decodedrate;
                    decodedrateguess = decodedrateguess * 1024; // convert to bits per second
                    filepositionbits = fileposition - 8192; // accounting for stream buffer in vs1003b
                    filepositionbits = filepositionbits * 8;
                    secondsposition = filepositionbits / decodedrateguess; // position in seconds
                    bitratefinal = decodedrate;
                    //
                    // find vbr
                    if (decodetime > 1) {
                        if (bitratefinal != lastbitrate) {
                            mpvbrcount++;
                        }
                        if (mpvbrcount > 32) {
                            mpvbr = 1;
                        }
                    }
                    lastbitrate = bitratefinal;
                } // mpvbr
                //
                grminutes = secondsposition / 60;
                grseconds = secondsposition - (grminutes * 60);
                //
                decodedrate = decodedrate * 1024; // convert to bits per second
                filesizebits = filesize;
                filesizebits = filesizebits * 8;
                if (decodedrate != 0) {
                    totalduration = filesizebits / decodedrate;
                } // duration in seconds
                ttminutes = totalduration / 60;
                ttseconds = totalduration - (ttminutes * 60);
                unsigned short remainingb = 0;

                if (totalduration > decodetime) {
                    remainingb = totalduration - decodetime;
                } else {
                    remainingb = 0;
                }
                trminutes = remainingb / 60;
                trseconds = remainingb - (trminutes * 60);
                //
            } // playing

            fillaudiobuffer(); // fill vs1003b audio buffer

            if (saskin == 0) { // default screen

                if (mpfileopen == 1) {
                    ty = 30;
                    if (iseyedeethree == 0) {
                        tx = 0;
                        sprintf(text, rec.filename);
                    } else {
                        eyedeemove(12); // update for version 1.0.210
                        idtext[133] = 0; // ensure id text buffer doesn't overflow text buffer
                        sprintf(text, idtext); // update for version 1.0.212
                    }
                    drawtextlarge();
                }
                drawroundedrectangle(0, 28, 127, 17); // draw scroller border

                // print decode time elapsed and remaining
                tx = 1;
                ty = 6;
                if (mppause == 0) {
                    mptimecopy(dtminutes, dtseconds);
                } else {
                    mptimecopy(olddtminutes, olddtseconds);
                } // mppause
                drawtext();
                drawline(32, 9, 35, 9);
                tx = 36;
                if (mppause == 0) {
                    mptimecopy(trminutes, trseconds);
                } else {
                    mptimecopy(oldtrminutes, oldtrseconds);
                } // mppause
                drawtext();

                ty = 15;
                // print total track time and bitrate
                if (mppause == 0) {
                    if (decodetime > 0) {
                        tx = 1;
                        mptimecopy(ttminutes, ttseconds);
                        drawtext();
                        drawline(32, 18, 35, 18);
                        tx = 36;
                        sprintf(text, "%03d", bitratefinal);
                        if (text[3] != 0) {
                            text[0] = 0;
                        } // update for version 1.0.31

                        unsigned char kbsx = 55; // kbps offsets
                        if (mpvbr == 0) { // flash bitrate display for vbr files
                            drawtext();
                        } else { // flash text and draw kbps offscreen
                            if (decodetime << 15 == 0x8000) {
                                drawtext();
                            } else {
                                kbsx = 200;
                            }
                        } // mpvbr

                        // print small "Kbs"
                        x = kbsx;
                        y = 17;
                        n = 0;
                        while (n < 5) {
                            y = 17 + n;
                            setpixel();
                            n++;
                        }
                        x = kbsx + 4;
                        n = 0;
                        while (n < 5) {
                            y = 17 + n;
                            setpixel();
                            n++;
                        }
                        x = kbsx + 1;
                        y = 19;
                        setpixel();
                        x++;
                        y = 17;
                        setpixel();
                        y++;
                        setpixel();
                        pixelworkprivatee();
                        y++;
                        setpixel();
                        x = kbsx + 5;
                        y = 19;
                        setpixel();
                        pixelworkprivatee();
                        x++;
                        y = 20;
                        setpixel();
                        x = x + 2;
                        y = 18;
                        setpixel();
                        y = 21;
                        setpixel();
                        x = kbsx + 9;
                        y = 17;
                        setpixel();
                        pixelworkprivatee();
                        pixelworkprivatee();
                        x++;
                        y = 17;
                        setpixel();
                        y = 20;
                        setpixel();
                        x = kbsx + 6;
                        y = 19;
                        setpixel();
                        pixelworkprivatee();
                        x = x + 2;
                        y = 17;
                        setpixel();
                        pixelworkprivatee();
                        x = x + 2;
                        setpixel();
                        pixelworkprivatee();
                    } // decodetime
                } else { // paused
                    tx = 17;
                    sprintf(text, "PAUSED");
                    if ((framecount & 0b00010000) == 0b00010000) {
                        drawtext();
                        for (n = 0; n < 14; n++) {
                            line3[n] = 0;
                        } // update for version 1.0.191
                    } else {
                        drawtextgrey();
                    } // framecount
                } // mppause
                drawroundedrectangle(0, 0, 67, 26); // draw time border

                // draw graphic spectrum analyser
                unsigned char stretch = 0;
                if (pitchchanger == 0) { // draw graphic spectrum analyser
                    for (n = 0; n < 14; n++) { // update for version 1.0.93
                        clrfb = 0;
                        while (clrfb < 3) {
                            drawline(71 + stretch, 25, 71 + stretch, 25 - line3[n]);
                            stretch++;
                            clrfb++;
                        } // clrfb
                        stretch++;
                    } // n
                } else { // print pitch changer information
                    if (xx == 0 && yy == 0 && playing == 1) { // update for version 1.0.187
                        n = 7;
                        ty = 0; // ty used as stereo channel flag
                        while (n < 21) { // update for version 1.0.204
                            drawline(69, n, 70 + vuneedles[ty], n); // draw stereo bargraph peak meters
                            n++;
                            if (n == 12) {
                                n = 16;
                                ty = 1;
                            } // next channel
                        } // n
                    } else {
                        sprintf(text, "TEMPO: %01d", xx);
                        tx = 71;
                        ty = 16;
                        drawtext();
                        sprintf(text, "PITCH: %01d", yy);
                        tx = 71;
                        ty = 6;
                        drawtext();
                    } // xx-yy
                } // pitchchanger

                fillaudiobuffer();
                drawmpprogress();
                float progresswork = 0.0;
                float progresscalc = 0;
                progresswork = filesize;
                progresswork = 128.0 / progresswork;
                progresscalc = fileposition * progresswork;

                n = 48;
                while (n < 51) {
                    drawline(1, n, progresscalc, n); // draw progress bar
                    n++;
                } // n

                fillaudiobuffer(); // update for version 1.0.74

                // draw eight button borders
                unsigned char buttabindex = 0;
                n = 0;
                while (buttabindex < 8) {
                    drawline(1 + n, 53, (buttab[buttabindex] - 1) + n, 53); // top border line
                    drawline(1 + n, 63, (buttab[buttabindex] - 1) + n, 63); // bottom border line
                    drawline(n, 54, n, 62); // top border line
                    drawline(buttab[buttabindex] + n, 54, buttab[buttabindex] + n, 62); // bottom border line
                    n = n + (buttab[buttabindex] + 2);
                    buttabindex++;
                } // buttabindex

                // draw stop symbol
                n = 0;
                while (n < 5) {
                    drawline(71, 56 + n, 75, 56 + n);
                    n++;
                } // stop symbol lines
                // draw pause or play symbol
                n = 0;
                if (mppause == 0) { // pause
                    while (n < 8) {
                        if (n != 3 && n != 4) {
                            drawline(53 + n, 56, 53 + n, 60); // pause symbol lines
                        }
                        n++;
                    } // n
                } else { // play
                    while (n < 2) {
                        drawline(56 + n, 56, 56 + n, 60); // solid line
                        n++;
                    } // n
                    tx = 54;
                    ty = 55;
                    text[0] = 0x3E; // ">"
                    text[1] = 0;
                    drawtext();
                    tx = 55;
                    drawtext();
                    x = 58;
                    y = 58;
                    setpixel();
                } // mppause

                // draw skip symbols
                tx = 36;
                ty = 55;
                text[0] = 0x3C; // "<"
                text[1] = 0;
                drawtext();
                tx = 35;
                drawtext();
                tx = 37;
                drawtext();
                tx = 86;
                text[0] = 0x3E; // ">"
                drawtext();
                tx = 85;
                drawtext();
                tx = 84;
                drawtext();
                // draw volume symbols
                tx = 98;
                ty = 55;
                text[0] = 0x56; // "V"
                text[1] = 0x2D; // "-"
                text[2] = 0;
                drawtext();
                tx = 114;
                text[1] = 0x2B; // "+"
                drawtext();
                // draw joystick control symbol
                tx = 1;
                if (pitchchanger == 0) {
                    text[0] = 0x4A; // "J"
                } else {
                    text[0] = 0x50; // "P" 
                } // pitchchanger
                text[1] = 0x43; // "C"
                if (joystickcontrol == 0) {
                    drawtext();
                } else {
                    if (decodetime << 15 == 0x8000) {
                        drawtext();
                    }
                }
                // draw bass boost symbol
                tx = 17;
                text[0] = 0x42; // "B"
                text[1] = 0x42; // "B"
                if (basscrank == 0) {
                    drawtext();
                } else {
                    if (decodetime << 15 == 0x8000) {
                        drawtext();
                    }
                } // basscrank
            } // saskin

            // graphic stereo analogue level meter simulation
            if (saskin > 8) { // update for version 1.0.190
                drawanaloguemeters();
                eyedeemove(6); // update for version 1.0.210
            } // saskin

            if (saskin == 1 || saskin > 4) {
                if (saskin < 7) { // update for version 1.0.92
                    // draw full screen graphic spectrum analyser
                    unsigned char stretch = 2;
                    for (n = 0; n < 14; n++) {
                        fillaudiobuffer(); // update for version 1.0.99
                        int segbuffer = 63 - (line3[n] + line3[n] + line3[n]); // update for version 1.0.102
                        if (saskin == 1) { // update for version 1.0.99
                            if (line2[n] > segbuffer) {
                                line2[n] = segbuffer; // write peak
                            } else {
                                if (mx < 32) {
                                    line2[n + 15]++;
                                    if (line2[n + 15] > 34) {
                                        line2[n] = segbuffer; // reset peak
                                        line2[n + 15] = 0;
                                    } // mx
                                } // line2
                            } // line2
                            if (mx > 96) {
                                if ((framecount & 0b00011111) == 0b00011111) {
                                    line2[n] = segbuffer; // reset peak
                                } // framecount
                            } // mx
                            if (mx < 97 && mx > 31) {
                                line2[n] = segbuffer;
                            } // reset peak
                        } // saskin
                        unsigned char salines = 0;
                        while (salines < 7) {
                            if (saskin < 6) {
                                drawline(stretch, 63, stretch, segbuffer);
                                if (saskin == 1) {
                                    x = stretch;
                                    y = line2[n];
                                    setpixel();
                                } // draw peak level
                            } else { // updated for version 1.0.74
                                int dotcalc = segbuffer;
                                // keep spectrum display segments aligned by checking even or odd y value
                                if ((dotcalc & 0b0000000000000001) == 1) {
                                    pff = 0;
                                } else {
                                    pff = 1;
                                }
                                drawline(stretch, 63, stretch, dotcalc); // was drawlinedotted
                                dotcalc = 0; // update for version 1.0.101
                                while (dotcalc < 8) { // a cheat to draw dotted lines
                                    framebuffer[stretch + (dotcalc * 128)] = framebuffer[stretch + (dotcalc * 128)] & 0b01010101;
                                    dotcalc++;
                                } // dotcalc
                            } // saskin < 6
                            stretch++;
                            salines++;
                        } // salines
                        stretch++;
                        stretch++;
                    } // n
                    if (saskin == 5 && mpfileopen == 1) { // masked sin text
                        ty = 30;
                        if (iseyedeethree == 0) {
                            tx = 0;
                            sprintf(text, rec.filename);
                        } else {
                            eyedeemove(12); // update for version 1.0.210
                            idtext[133] = 0; // ensure id text buffer doesn't overflow text buffer
                            sprintf(text, idtext);
                        }
                        sinamp = 10;
                        drawtextlargesinmasked();
                    } // mpfileopen
                }
            } // saskin

            // draw waterfall spectrum analyser
            if (saskin > 6 && saskin < 9) { // update for version 1.0.189
                int mrow = 1976;
                int mbar = 0;
                // load the current spectrum data
                while (mbar < 9) {
                    imagebuffer[mrow + mbar] = line3[mbar]*(saskin - 6);
                    mbar++;
                } // mbar
                // rotate the memory
                mrow = 0;
                while (mrow < 1976) {
                    mbar = 0;
                    while (mbar < 9) {
                        imagebuffer[mrow + mbar] = imagebuffer[mrow + 38 + mbar];
                        mbar++;
                    } // mbar
                    mrow = mrow + 38;
                } // mrow
                // draw all the lines
                mrow = 0;
                int mrowamp = 0;
                while (mrow < 2014) {
                    mbar = 0;
                    while (mbar < 8) {
                        drawline(mbar * 16, (mrow / 38) - imagebuffer[mrow + mbar] + mrowamp, (mbar * 16) + 16, (mrow / 38) - imagebuffer[mrow + mbar + 1] + mrowamp);
                        mbar++;
                    } // mbar
                    mrow = mrow + 38;
                    mrowamp = mrowamp + 1;
                } // mrow
            } // saskin

            // draw 3d spectrum analyser
            if (saskin > 1 && saskin < 5) { // 2, 3 or 4
                int barpointsx[8];
                int barpointsy[8];
                int bar = 6;
                int rearpos = 46;
                int frontpos = 54;
                n = 3;
                while (n < 126) {
                    fillaudiobuffer();
                    barpointsx[0] = 0;
                    barpointsy[0] = rearpos;
                    barpointsx[1] = 16;
                    barpointsy[1] = rearpos;
                    barpointsx[2] = 16;
                    barpointsy[2] = frontpos;
                    barpointsx[3] = 0;
                    barpointsy[3] = frontpos;
                    barpointsx[4] = 0;
                    barpointsy[4] = rearpos - (line3[bar] + line3[bar]);
                    barpointsx[5] = 16;
                    barpointsy[5] = rearpos - (line3[bar] + line3[bar]);
                    barpointsx[6] = 16;
                    barpointsy[6] = frontpos - (line3[bar] + line3[bar]);
                    barpointsx[7] = 0;
                    barpointsy[7] = frontpos - (line3[bar] + line3[bar]);
                    if (saskin == 3) {
                        rotation = line3[bar] * 4;
                    } // saskin
                    oy = frontpos - ((frontpos - rearpos) / 2);
                    ox = 8;
                    x = barpointsx[0];
                    y = barpointsy[0];
                    pixelrotation();
                    barpointsx[0] = x;
                    barpointsy[0] = y;
                    x = barpointsx[1];
                    y = barpointsy[1];
                    multiplerotation();
                    barpointsx[1] = x;
                    barpointsy[1] = y;
                    x = barpointsx[2];
                    y = barpointsy[2];
                    multiplerotation();
                    barpointsx[2] = x;
                    barpointsy[2] = y;
                    x = barpointsx[3];
                    y = barpointsy[3];
                    multiplerotation();
                    barpointsx[3] = x;
                    barpointsy[3] = y;
                    ox = 8;
                    oy = frontpos - (line3[bar] + line3[bar]);
                    oy = oy - ((frontpos - rearpos) / 2);
                    x = barpointsx[4];
                    y = barpointsy[4];
                    pixelrotation();
                    barpointsx[4] = x;
                    barpointsy[4] = y;
                    x = barpointsx[5];
                    y = barpointsy[5];
                    multiplerotation();
                    barpointsx[5] = x;
                    barpointsy[5] = y;
                    x = barpointsx[6];
                    y = barpointsy[6];
                    multiplerotation();
                    barpointsx[6] = x;
                    barpointsy[6] = y;
                    x = barpointsx[7];
                    y = barpointsy[7];
                    multiplerotation();
                    barpointsx[7] = x;
                    barpointsy[7] = y;

                    if (saskin == 4) {
                        x = barpointsx[0] + n;
                        y = barpointsy[0];
                        threedeerotation();
                        barpointsx[0] = x - n;
                        barpointsy[0] = y;
                        int tdn = 1;
                        while (tdn < 8) {
                            x = barpointsx[tdn] + n;
                            y = barpointsy[tdn];
                            threedeemultrotation();
                            barpointsx[tdn] = x - n;
                            barpointsy[tdn] = y;
                            tdn++;
                        } // tdn
                    } else {
                        tdrotation = 0;
                    } // saskin

                    lxa = 0; // lxa & lxb borrowed from gps program to use as counters
                    while (lxa < 24) { // update for version 1.0.59
                        lxb = lpnts[lxa + 1];
                        drawline(barpointsx[lpnts[lxa]] + n, barpointsy[lpnts[lxa]], barpointsx[lxb] + n, barpointsy[lxb]);
                        lxa = lxa + 2;
                    }
                    bar++;
                    n = n + 21; // next bar
                } // n

                if (mx > 63) { // update for version 1.0.100
                    rotation = rotation + 3;
                    if (rotation > 357) {
                        rotation = 0;
                    }
                } else {
                    rotation = rotation - 3;
                    if (rotation < 0) {
                        rotation = 357;
                    }
                } // mx
                if (my > 31) {
                    tdrotation--;
                    if (tdrotation < 0) {
                        tdrotation = 359;
                    }
                } else {
                    tdrotation++;
                    if (tdrotation > 359) {
                        tdrotation = 0;
                    }
                } // my
            } // saskin

            // send spectrum data using hardware serial
            // third part of spectrum data packet
            if (serialenabled == 2 && pitchchanger == 0) {
                fillaudiobuffer(); // fill vs1003b audio buffer
                n = 7; // start index for last spectrum values
                while (n < 12) { // update for version 1.0.142
                    txbyte = line3[n];
                    hardserialout();
                    n++;
                } // n
            } // serialenabled

            fillaudiobuffer(); // fill vs1003b audio buffer
            if (saskin == 9) { // update for version 1.0.190
                jpgpixcount[1] = jpgpixcount[0]; // more averaging for stereo meters
                jpgpixcount[0] = vuneedles[0]; //
                jpgpixcount[3] = jpgpixcount[2]; //
                jpgpixcount[2] = vuneedles[0]; //
            } // saskin
            shiftbytein();
            fillaudiobuffer(); // update for version 1.0.52

            if (joyspareb != 0) {
                sdcardanimation();
            } // card is not inserted
            if (joyup == 1) {
                if (jheld == 1.0 && joystickcontrol == 1) {
                    if (backlighttimer != 0) {
                        if (pitchchanger == 0) {
                            mpvol = 1;
                        } else {
                            pitchtempocontrol();
                        } // volume up
                    } // backlighttimer
                } else {
                    if (joystickcontrol == 0) {
                        mmy = mmy - jheld;
                    }
                }
                backlighttimer = backlightdelay; // start backlight timer
            }

            if (joydown == 1) {
                if (jheld == 1.0 && joystickcontrol == 1) {
                    if (backlighttimer != 0) {
                        if (pitchchanger == 0) {
                            mpvol = -1;
                        } else {
                            pitchtempocontrol();
                        } // volume down
                    } // backlighttimer
                } else {
                    if (joystickcontrol == 0) {
                        mmy = mmy + jheld;
                    }
                }
                backlighttimer = backlightdelay; // start backlight timer
            }

            if (joyleft == 1) {
                if (jheld == 1.0 && joystickcontrol == 1) {
                    if (backlighttimer != 0) {
                        if (pitchchanger == 0) {
                            mpskip = -1;
                        } else {
                            pitchtempocontrol();
                        } // skip previous
                    } // backlighttimer
                } else {
                    if (joystickcontrol == 0) {
                        mmx = mmx - jheld;
                    }
                }
                backlighttimer = backlightdelay; // start backlight timer
            }

            if (joyright == 1) {
                if (jheld == 1.0 && joystickcontrol == 1) {
                    if (backlighttimer != 0) {
                        if (pitchchanger == 0) {
                            mpskip = 1;
                        } else {
                            pitchtempocontrol();
                        } // skip next
                    } // backlighttimer
                } else {
                    if (joystickcontrol == 0) {
                        mmx = mmx + jheld;
                    }
                }
                backlighttimer = backlightdelay; // start backlight timer
            }

            if (joyup + joydown + joyleft + joyright == 0) { // cancel acceleration
                jheld = 1.0;
                msine = 0; // update for version 1.0.119
                if (joytimer > 0) {
                    joytimer--;
                }
            } else {
                joytimer = 60;
                if (msine < 88) { // update for version 1.0.119
                    msine++;
                    jheld = sin_table[msine];
                    jheld = jheld / 1180;
                } // msine
            }

            checkmousebounds();

            if (joyfire == 1) {
                backlighttimer = backlightdelay; // start backlight timer
                if (lastpressed == 0) { // check last state for debounce

                    if (pitchchanger == 0) { // update for version 1.0.186    
                        if (saskin == 0) { // cycle spectrum full screen modes
                            if (mx > 71 && my < 26) {
                                saskin = 1;
                                clrfb = 0; // update for version 1.0.99
                                while (clrfb < 15) {
                                    line2[clrfb] = 64;
                                    line2[clrfb + 15] = 0;
                                    clrfb++;
                                } // clear peak arrays
                            } // mx-my
                        } else {
                            if (saskin > 8) {
                                saskin = 0;
                            } else {
                                saskin++;
                            } // update for version 1.0.191
                            if (saskin == 7 || saskin == 8) {
                                clearwaterfall();
                            } // update for version 1.0.189
                            if (saskin == 9) { // update for version 1.0.191
                                sendclrspectrum(); // send clear spectrum serial data
                                vuneedles[0] = 0;
                                vuneedles[1] = 0; // reset stereo meter positions
                                lrSum[0] = 0;
                                lrSum[1] = 0; // reset root square variables
                                lrPhase = 0; // reset phase variable
                                jpgpixcount[0] = 0;
                                jpgpixcount[1] = 0; // update for version 1.0.191
                                jpgpixcount[2] = 0;
                                jpgpixcount[3] = 0; //
                            } // saskin
                        } // saskin
                    } // pitchchanger

                    if (saskin == 0) { // skip track
                        if (my > 52) {
                            if (mx > 32 && mx < 48) {
                                mpskip = -1;
                            }
                            if (mx > 80 && mx < 96) {
                                mpskip = 1;
                            }
                            if (mx > 111) {
                                mpvol = 1;
                            }
                            if (mx > 95 && mx < 112) {
                                mpvol = -1;
                            }

                            if (mx < 17) {
                                if (joystickcontrol == 0) {
                                    joystickcontrol = 1;
                                } else {
                                    joystickcontrol = 0;
                                }
                            } else {
                                joystickcontrol = 0;
                            } // mx

                            if (mx < 67 && mx > 47) { // pause
                                if (mppause != 0) {
                                    mppause = 0;
                                    playing = 1;
                                } else {
                                    mppause = 1;
                                    playing = 0;
                                    olddtminutes = dtminutes;
                                    olddtseconds = dtseconds;
                                    oldtrminutes = trminutes;
                                    oldtrseconds = trseconds;
                                } // mppause
                            } // mx

                            if (mx > 66 && mx < 79) {
                                framecount = 19;
                            } // cause exit

                            if (mx > 16 && mx < 33) { // bass boost
                                if (basscrank != 0) {
                                    basscrank = 0;
                                } else {
                                    basscrank = 1;
                                }
                                bassboost();
                            } // mx

                        } // my
                    } // saskin

                } // lastpressed
                lastpressed = 1;
            } else {
                lastpressed = 0;
            } // joyfire

            if (joytimer != 0) {
                mx = mmx;
                my = mmy; // convert to integer
                drawpointer();
            } // joytimer

            if (framecount == 19 || lasterror == 11) { // exit
                silenceframes();
                if (pitchchanger == 1) {
                    xx = 0;
                    yy = 0; // reset pitch changer
                    joyup = 0;
                    joydown = 0;
                    joyleft = 0;
                    joyright = 0;
                    pitchtempocontrol(); // to prevent effect on other programs
                } // pitchchanger
                SPISTAT = 0x8000;
                SD_CS = 0; // select sd card
                if (filecount > 0) {
                    FSfclose(pointer);
                }
                // go back to parent directory
                // which should be the root
                FSchdir("..");
                resettomenu();
            }

            if (backlighttimer > 0) { // handle lcd backlight
                if (saskin == 0) {
                    backlighttimer--;
                } // update for version 1.0.92
                LED_PIN = 1;
            } else {
                LED_PIN = 0;
            }
            if (framecount > 3999) {
                framecount = 1000;
            } // update for version 1.0.191
        } // demo 3 (mp3 player)
        //
        //
        //***************************************************************************************************************
        //**************************************************            *************************************************
        //************************************************** VECTOR GPS *************************************************
        //**************************************************            *************************************************
        //***************************************************************************************************************
        //
        //
        // D used as the closest point distance float variable
        // idtext[100]-idtext[124] used as closest point label
        // jpgpixcount[0-3] used by the dynamic clipping box
        // mpvbrcount used as gps simulation point counter
        //
        if (demo == 2) { // gps mapping demo section
            int drewsomething = 0;
            unsigned char labelqualify = 0;

            clrfb = 0;
            if (drawmap == 0) {
                while (clrfb < 1024) { // clear entire frame buffer
                    framebuffer[clrfb] = 0;
                    imagebuffer[clrfb + imageselect] = 0; // clear the image we are about to draw to but not display
                    clrfb++;
                } // clrfb
                drawmap = 1;
                drewsomething = 1;
            }

            if (framecount > 101 && cardstat == 0) { // initialise sd card
                if (!sdcardinit()) {
                    cardstat = 0;
                    sdcardanimation();
                } else { // update for version 1.0.118
                    cardstat = 1;
                    readgpx(); // update for version 1.0.171
                    sprintf(text, "MAPS");
                    if (FSchdir(text) != 0) {
                        debugout(4);
                        dirnotfound();
                        framecount = 19;
                    }
                } // cardstat
            } // framecount

            // check the card insert switch always
            if (joyspareb == 0) {
                socketstate = 1;
            } else {
                socketstate = 0;
                cardstat = 0;
            }

            if (cardstat == 1 && framecount == 103) {
                if (gpslog == 0) { // update for version 1.0.217
                    gpssimulation = 0; // cancel gps simulation configuration
                    drawcountmpgraphic(8); // draw file count graphic
                    file_browserv2();
                } else { // logging must have been activated to append a log file
                    int recsrch = 0; // add extension to file name
                    while (rec.filename[recsrch] != 0) {
                        recsrch++;
                    }
                    rec.filename[recsrch] = 0x2E;
                    recsrch++; // "."
                    rec.filename[recsrch] = 0x4D;
                    recsrch++; // "M"
                    rec.filename[recsrch] = 0x50;
                    recsrch++; // "P"
                    rec.filename[recsrch] = 0x49;
                    recsrch++; // "I"
                    rec.filename[recsrch] = 0x00; // new string termination
                    recsrch = FindFirst(rec.filename, 0x2F, &rec); // find file to get the size
                    if (recsrch == 0) { // file was found
                        filesize = rec.filesize; // copy file size
                        if (gpssimulation == 1) { // update for version 1.0.217
                            gpslog = 0; // turn off track log mode
                            mpvbrcount = 320; // set track log start
                            gpstracklogpoint(mpvbrcount); // read first track log point
                        } // gpssimulation
                        filecount = 1; // satisfy the next condition
                    } else { // map file used to create log file is not present
                        debugout(39); // it could possibly happen
                        framecount = 19; // cause exit
                    } // recsrch
                } // gpslog

                if (filecount > 0) {
                    pointer = FSfopen(rec.filename, "r");
                    drawing = 1;
                    read_map_header(); // read and parse map file header information
                    if (gpslog == 1) { // update for version 1.0.172
                        userlat = gpslat;
                        userlon = gpslon; // set user position to end of track log
                    } // gpslog

                    pointval = screentllat - screenbrlat; // initialise map projection
                    if (pointvaldiv > 3) {
                        pointval = pointval / 128;
                    } // city or topographic map
                    if (pointvaldiv == 1) {
                        pointval = pointval * 2.5;
                    } // continent map
                    if (pointvaldiv == 2) {
                        pointval = pointval * 19.0;
                    } // world map
                    // first zoom
                    pointval = pointval / pointvaldiv; // 20 for east peak view
                    gpszoom();
                    joytimer = 99; // update for version 1.0.143
                } else { // filecount
                    drewsomething = 1; // cause clean exit for no files
                } // filecount
            } // cardstat

            int copys = 0;
            if (imageselect != oldimageselect) {
                while (copys < 1024) {
                    imagebuffer[copys + imageselect] = 0;
                    framebuffer[copys] = 0;
                    copys++;
                } // copys
                oldimageselect = imageselect;
            } // imageselect

            int buffindex = 0;
            while (buffindex < 2 && drewsomething == 0 && drawing > 0) {

                while (drewsomething == 0 && drawing > 0) {
                    if (cardstat == 1 && framecount > 105 && pointer != NULL) {
                        FSfread(receivebuffer, 1, 5, pointer);
                        drawing = drawing + 5;
                    } // drewsomething

                    // responsive joystick exit for gps mode
                    // changed position of this routine for version 1.0.146
                    if (gpsmode == 1) { // update for version 1.0.143
                        JOYPAR_PIN = 0;
                        delay_gen(0);
                        if (JOYDAT_PIN == 0) {
                            JOYPAR_PIN = 1;
                            drewsomething = 1;
                            if (bailedgpsmode == 0) {
                                playtinydirect();
                            }
                            drawing = filesize - polystat; // update for version 1.0.146
                            bailedgpsmode = 1;
                        } // joydat
                    } // gpsmode

                    if (receivebuffer[0] == 0x5B && receivebuffer[1] == 0x50) { // update for version 1.0.178

                        if (receivebuffer[3] == 0x4C) { // found [POL]
                            text[0] = 0;
                            while (receivebuffer[0] != 0x3D) { // find first equals sign "="
                                FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                drawing++;
                                bytes[0] = bytes[1];
                                bytes[1] = bytes[2];
                                bytes[2] = bytes[3];
                                bytes[3] = receivebuffer[0];
                            } // while

                            if (bytes[2] == 0x54) { // find "T" for type
                                FSfread(receivebuffer, 1, 4, pointer); // read in type "0x??"
                                drawing = drawing + 4;

                                labelqualify = 0;
                                if (maptype == 1) {
                                    if (receivebuffer[2] < 0x37 && receivebuffer[2] > 0x33) {
                                        labelqualify = 1;
                                    } // road
                                } else {
                                    if (receivebuffer[2] == 0x32 && receivebuffer[3] == 0x32) {
                                        labelqualify = 1;
                                    } // major contour
                                    if (receivebuffer[2] == 0x31 && receivebuffer[3] == 0x36) {
                                        labelqualify = 2;
                                    } // hiking trail
                                    // fix for world map
                                    if (pointvaldiv < 3 && labelqualify == 2) {
                                        labelqualify = 0;
                                    } // update for version 1.0.76
                                } // maptype

                                if (labelqualify != 0) {
                                    FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                    drawing++;

                                    int txti = 0;
                                    // find second equals sign character
                                    while (receivebuffer[0] != 0x3D && txti < 11) { // "="
                                        FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                        drawing++;
                                        bytes[0] = bytes[1];
                                        bytes[1] = bytes[2];
                                        bytes[2] = bytes[3];
                                        bytes[3] = receivebuffer[0];
                                        txti++;
                                    } // while

                                    if (bytes[2] == 0x4C) { // find "L" for label
                                        txti = 0;
                                        while (receivebuffer[0] != 0x0D) { // read label
                                            FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                            drawing++;
                                            if (receivebuffer[0] == 0x7E) { // tilde "~"
                                                FSfread(receivebuffer, 1, 6, pointer); // read six bytes
                                                drawing = drawing + 6;
                                                text[txti] = 0x20;
                                                txti++;
                                                FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                                drawing++;
                                            } // tilde
                                            text[txti] = receivebuffer[0];
                                            txti++;
                                        }
                                        text[txti - 1] = 0;
                                        // read line label here
                                        if (maptype == 0 && labelqualify == 1) {
                                            convert_meters();
                                        }
                                    } // label
                                }
                            } // type

                            // find open bracket
                            while (receivebuffer[0] != 0x28) { // "("
                                FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                drawing++;
                                maplevel[0] = maplevel[1];
                                maplevel[1] = maplevel[2];
                                maplevel[2] = receivebuffer[0];
                            } // while
                            FSfread(receivebuffer, 1, 8, pointer); // read first coords
                            drawing = drawing + 8;
                            if (maplevel[0] == 0x30) { // for only one map level support
                                gpsconvertfloats();
                                lxa = rounded(screenx);
                                lya = rounded(screeny);
                                firstlinepoint = 1;
                            } // maplevel

                            while (1) { // read the rest of the line points
                                if (mapsniffer != 0) { // update for version 1.0.160
                                    foplat = filelat;
                                    foplon = filelon; // copy the previous point
                                } // mapsniffer
                                FSfread(receivebuffer, 1, 8, pointer); //
                                drawing = drawing + 8;
                                if (receivebuffer[0] == 0x5B && receivebuffer[1] == 0x45) {
                                    break;
                                } // [END]
                                if (maplevel[0] == 0x30) { // for only one map level support
                                    gpsconvertfloats();
                                    lxb = rounded(screenx);
                                    lyb = rounded(screeny);

                                    // find the closest line to current gps position
                                    if (mapsniffer != 0) { // update for version 1.0.160
                                        float fopd = footperpendiculardistance();
                                        if (fopd < fopdistance && text[0] != 0) { // current line is closer
                                            n = 0;
                                            int triggercalc = 0;
                                            while (n < 25) {
                                                foplabel[n] = text[n];
                                                n++;
                                            }
                                            if (maptype == 1 && labelqualify == 1) { // for roads
                                                triggercalc = 1;
                                            } // maptype
                                            if (maptype == 0 && labelqualify == 2) { // for trails
                                                triggercalc = 1;
                                            } // maptype
                                            if (triggercalc != 0) {
                                                fopdistance = fopd; // set a new closest line
                                                msfoplat = foplat;
                                                msfoplon = foplon; // update for version 1.0.161
                                            } else {
                                                foplabel[0] = 0;
                                            } // triggercalc
                                        } // fopb
                                    } // mapsniffer    

                                    int onepointonscreen = 0; // dynamic clipping box for map lines
                                    onepointonscreen = gpsdynamicclippingcheck(); // update for version 1.0.200
                                    lxc = lxb;
                                    lyc = lyb;

                                    if (onepointonscreen == 1) {
                                        if (gpsmode == 1 && gpsfix == 1) {
                                            x = lxa;
                                            y = lya;
                                            multiplerotationf();
                                            lxa = x;
                                            lya = y;
                                            x = lxb;
                                            y = lyb;
                                            multiplerotationf();
                                            lxb = x;
                                            lyb = y;
                                        } // gpsmode

                                        if (labelqualify != 2) { // not a hiking trail
                                            drawlineimage(lxa, lya, lxb, lyb, 0); // solid line
                                        } else { // dotted hiking trail
                                            pff = 1; // keep multi segment line dot pattern consistent
                                            if (((lyb & 0b0000000000000001) + (lya & 0b0000000000000001)) == 1) {
                                                pff = 0;
                                            }
                                            drawlineimage(lxa, lya, lxb, lyb, 1); // dotted line
                                        }
                                        drewsomething = 1;

                                        if (firstlinepoint == 1) {
                                            if (zoomlevel == 1) {
                                                if (text[0] != 0 && labelqualify != 2) { // no labels for trails
                                                    tx = lxa;
                                                    ty = lya;
                                                    txtox = lxa;
                                                    txtoy = lya;
                                                    int brngline = atan2(lyb - lya, lxa - lxb)*57.295791;
                                                    trotation = (360 - brngline) - 180; // update for version 1.0.161
                                                    if (trotation > 336 || trotation < 22) {
                                                        tx = tx + 2;
                                                        ty = ty + 2;
                                                    }
                                                    int trotct = 21;
                                                    n = 0; // update for version 1.0.204
                                                    while (trotct < 292) {
                                                        if (trotation > trotct && trotation < trotct + 46) {
                                                            tx = tx + rtoffs[n];
                                                            ty = ty + rtoffs[n + 7];
                                                        }
                                                        trotct = trotct + 45;
                                                        n++;
                                                    } // trotct
                                                    drawtextimagerotated(); // update for version 1.0.204
                                                    theta = brngpoint; // pre calculation for bearing rotation
                                                    theta = d2r * theta;
                                                } // text
                                            } // zoomlevel
                                            firstlinepoint = 0;
                                        }
                                    }
                                    lya = lyc;
                                    lxa = lxc;
                                } // maplevel
                            } // "("
                        } // found line

                        if (receivebuffer[3] == 0x49) { // found [POI]
                            int poitype = numicons; // initialise as an invalid poi type
                            int pdatlock = 1; //
                            char ptypedata[3] = {0, 0, 0}; // poi type raw data
                            // find first equals sign
                            while (receivebuffer[0] != 0x3D) { // "="
                                FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                drawing++;
                            } // while
                            FSfread(receivebuffer, 1, 1, pointer); // read one byte
                            drawing++;
                            // find second equals sign
                            while (receivebuffer[0] != 0x3D) { // "="
                                FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                drawing++;
                                // buffer the poi type data
                                if (receivebuffer[0] < 0x10) {
                                    pdatlock = 0;
                                }
                                if (pdatlock > 0) {
                                    ptypedata[0] = ptypedata[1]; // update for version 1.0.179
                                    ptypedata[1] = ptypedata[2];
                                    ptypedata[2] = receivebuffer[0];
                                } // pdatlock
                            } // while
                            // qualify the poi types
                            if (ptypedata[0] == 0x64 && ptypedata[1] == 0x30) { // "d0"
                                if (ptypedata[2] < (0x31 + numicons) && ptypedata[2] > 0x30) {
                                    poitype = ptypedata[2] - 0x31;
                                } // icon supported poi 
                            } // ptypedata
                            n = 0;
                            while (receivebuffer[0] != 0x0D) { // return
                                FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                drawing++;
                                filelabel[n] = receivebuffer[0];
                                n++;
                                if (n > 23) {
                                    receivebuffer[0] = 0x0D;
                                }
                            } // while
                            filelabel[n - 1] = 0;
                            // find open bracket
                            while (receivebuffer[0] != 0x28) { // "("
                                FSfread(receivebuffer, 1, 1, pointer); // read one byte
                                drawing++;
                                maplevel[0] = maplevel[1];
                                maplevel[1] = maplevel[2];
                                maplevel[2] = receivebuffer[0];
                            } // while
                            FSfread(receivebuffer, 1, 8, pointer); // read first coords
                            drawing = drawing + 8;
                            if (maplevel[0] == 0x30) { // for only one map level support
                                gpsconvertfloats();

                                // find the closest map point to current gps position
                                if (mapsniffer != 0) { // update for version 1.0.158
                                    float closedistance = getdistance(filelat, filelon);
                                    if (closedistance < D) { // current point was closer
                                        D = closedistance;
                                        n = 0; // set a new closest point
                                        mspoitype = poitype; // set a new closest type
                                        mspointlat = filelat;
                                        mspointlon = filelon; // update for version 1.0.160
                                        while (n < 25) { // update for version 1.0.204
                                            if (filelabel[n] > 0x60 && filelabel[n] < 0x7B) {
                                                filelabel[n] = filelabel[n] - 0x20;
                                            } // convert lower case chars
                                            idtext[100 + n] = filelabel[n];
                                            n++;
                                        } // n
                                    } // closedistance
                                } // mapsniffer

                                x = rounded(screenx);
                                y = rounded(screeny);
                                //drewsomething = 1; // forces progress bar (but slower)

                                // de-clutter for points of interest and their labels
                                int clutterflag = 1; // update for version 1.0.177
                                if (x < 4 || y < 4) {
                                    clutterflag = 0;
                                } // static de-clutter filters
                                if (y > 59) {
                                    clutterflag = 0;
                                } //
                                if (poitype > 1) { // update for version 1.0.181
                                    if (x > 98) {
                                        clutterflag = 0;
                                    } // for most points of interest
                                } else { //
                                    if (x > 124) {
                                        clutterflag = 0;
                                    } // for car parks and restrooms
                                } // poitype
                                if (poitype < 2 && clutterflag == 1) {
                                    clutterflag = 2;
                                } // update for version 1.0.181
                                if (y < limitytop && y > limitybottom) {
                                    clutterflag--;
                                } // dynamic filter
                                if (maxlabels < 3 && clutterflag != 0) { // update for version 1.0.176
                                    limitytop = y + 4;
                                    limitybottom = y - 5; // set exclusion area the next point
                                    if (gpsmode == 1 && gpsfix == 1) {
                                        multiplerotationf();
                                    } // rotate point
                                    if (poitype < numicons) { // update for version 1.0.180
                                        drawpoiicon(poitype); //
                                    } else { //
                                        x--;
                                        setpixelimage();
                                        x++; // update for version 1.0.175
                                        x++;
                                        setpixelimage();
                                        x--; // draw a tiny circle for point
                                        y--;
                                        setpixelimage();
                                        y++; //
                                        y++;
                                        setpixelimage();
                                        y--; //
                                        clearpixelimage(); // update for version 1.0.179
                                    } // poitype
                                    drewsomething = 1; //
                                    if (poitype > 1) { // update for version 1.0.181
                                        n = 0;
                                        while (filelabel[n] != 0 && n < 25) { // update for version 1.0.204
                                            if (filelabel[n] > 0x60 && filelabel[n] < 0x7B) {
                                                filelabel[n] = filelabel[n] - 0x20;
                                            } // convert lower case chars
                                            n++;
                                        } // filelabel
                                        sprintf(text, filelabel); //
                                        int ttx = x + 2; // update for version 1.0.181
                                        int tty = y; // update for version 1.0.183
                                        if (poitype > numicons - 1) {
                                            ttx--;
                                        } // move label left for generic poi
                                        tx = ttx + 2;
                                        ty = tty - 2; // update for version 1.0.179
                                        drawtextimageclear(); // clear the background for the text
                                        tx = ttx + 3; // update for version 1.0.182
                                        drawtextimageclear(); // north-east diagonal direction
                                        tx = ttx + 2;
                                        ty = tty - 4; //
                                        drawtextimageclear(); //
                                        tx = ttx + 3;
                                        ty = tty - 3; //
                                        drawtextimageclear(); //
                                        tx = ttx + 1; //
                                        drawtextimageclear(); //
                                        tx = ttx + 2; // update for version 1.0.175
                                        drawtextimage(); // draw the solid text label
                                        maxlabels++; // increment to label limit
                                    } // poitype
                                } // maxlabels
                            } // maptype...maplevel

                            FSfread(receivebuffer, 1, 8, pointer); // read to skip eight bytes
                            drawing = drawing + 8;
                        } // found poi

                    } // found pol or poi

                    if (forcefirstdraw == 1) {
                        drewsomething = 1;
                    }
                    if (drawmap != 0) {
                        if (forcefirstdraw == 2) {
                            drawing = 0;
                            forcefirstdraw = 0;
                        }
                        if (drawing > (filesize - polystat)) {
                            if (polystat == 1183) {
                                gpsfillpolygon();
                            }
                            forcefirstdraw = 2;
                            drewsomething = 1;
                            buffindex = 2;
                        }
                    } // drawmap
                    framecount++;
                } // drewsomething

                if (forcefirstdraw == 1) {
                    buffindex = 2;
                    forcefirstdraw = 0;
                }
                drewsomething = 0;
                buffindex++;
            } // buffindex

            if (drawing > 0) {
                gpscopygraphics(); // update for version 1.0.163
                int zeroblank = 0;
                if (maptype == 1 && pointvaldiv > 3) { // display speed for street maps only
                    if (gpsmode == 1 && gpsfix == 1) { // update for version 1.0.157
                        sprintf(text, "%03d", gpsunitsph); // three digit speed value
                        tx = 0;
                        ty = 5; // lead zero blanking by sliding left
                        while (text[zeroblank] == 0x30) {
                            tx = tx - 12;
                            zeroblank++;
                        } // update for version 1.0.165
                        drawtextlarge();
                        //**** for testing bearing only ****
                        //tx = 0; ty = 28;
                        //sprintf(text,"%03d",brngdisplay);
                        //drawtextlarge();
                        //**********************************
                    }
                } // maptype

                drawgpsprogress(); // draw progress bar
                float pbar;
                float dbuff;
                pbar = 128.0 / filesize;
                dbuff = drawing * pbar;
                drawline(0, 1, (dbuff), 1); // update for version 1.0.212
                x = dbuff + 1; // update for version 1.0.150
                while (x < 127 && x > 0) {
                    y = 1;
                    clearpixel();
                    x++;
                } // update for version 1.0.212
                if (framecount > 3999) {
                    framecount = 1000;
                }
            } else { // drawing variable  is zero
                gpscopygraphics(); // update for version 1.0.163
                copys = 0;
                maxlabels = 0; // update for version 1.0.157
                limitytop = 32;
                limitybottom = 32; // update for version 1.0.177
                //
                if (gpsmode == 1) { // receive gps data
                    backlighttimer = backlightdelay; // start backlight timer
                    if (gpssimulation == 0) { // update for version 1.0.217
                        Init_UART(); // update for version 1.0.140
                        handlegps(); //
                        IEC1bits.U2RXIE = 0; // disable serial interrupt
                        IFS1bits.U2RXIF = 0; // clear interrupt flag 
                        applytzoffset();
                    } else { // gps simulation mode
                        gpsfix = 1; // pretend we have a position fix always
                        gottime = 1;
                        gotdate = 1; // same for time and date
                        mpvbrcount = mpvbrcount + 8; // increment track log index
                        oldgpslat = gpslat;
                        oldgpslon = gpslon;
                        gpstracklogpoint(mpvbrcount); // read first track log point
                    } // gpssimulation

                    if (gottime != 0 && timewasacquired == 0) {
                        timewasacquired = 1;
                    }
                    if (gpsfix == 0) {
                        clearwindowbackground(); // update for version 1.0.158
                        ty = 37;
                        sprintf(text, "TIME: %02d:%02d:%02d", gpshour, gpsminute, gpssecond);
                        drawtextvarwidthcentered();
                        ty = 19;
                        if (lasterror != 6) {
                            sprintf(text, "NO POSITION FIX"); // good nmea checksum
                        } else { // update for version 1.0.50
                            badcheckstring(); // update for version 1.0.170
                        } // lasterror
                        drawtextvarwidthcentered();
                        ty = 28;
                        sprintf(text, "LAST BEARING:%3d", brngdisplay);
                        drawtextvarwidthcentered();
                        drawdegreessymbol(); // update for version 1.0.158
                        drawroundedrectangle(0, 15, 127, 32); // draw window border
                    } // gpsfix
                    joytimer = 0; // cancel mouse display
                } // gpsmode

                if (gpsmode == 1 && gpsfix == 1 && bailedgpsmode == 0) { // auto refresh
                    FSfseek(pointer, 32, SEEK_SET);
                    forcefirstdraw = 1;
                    userlat = gpslat;
                    userlon = gpslon;
                    gpszoom();
                    jheld = 1.0;
                    drewsomething = 0;
                    drawing = 1;
                    oldimageselect = imageselect;
                    if (imageselect == 0) {
                        imageselect = 1024;
                    } else {
                        imageselect = 0;
                    }
                    brngpoint = getbearing(oldgpslat, oldgpslon);
                    brngdisplay = brngpoint; // update for version 1.0.158
                    if (brngdisplay < 180) { // reverse bearing
                        brngdisplay = brngdisplay + 180;
                    } else {
                        brngdisplay = brngdisplay - 180;
                    } // brngdisplay

                    if (gpslog == 1) {
                        gpstracklogwrite();
                    }
                    ox = 63.5;
                    oy = 31.5; // set origin
                    brngpoint = 360 - brngpoint; // one line fix for the road trip rotation bug
                    theta = brngpoint; // pre calculation for bearing rotation
                    theta = d2r * theta;
                    gpsdynamicclippingset(); // update for version 1.0.200
                    joytimer = 0;
                }

                if (gpsmode == 1) { // cancel gps mode with joystick left
                    JOYPAR_PIN = 0;
                    delay_gen(0); //
                    if (JOYDAT_PIN == 0 || bailedgpsmode == 1) { // update for version 1.0.143
                        gpsmode = 0;
                        Init_Timer1();
                        FSfseek(pointer, 32, SEEK_SET);
                        userlat = gpslat;
                        userlon = gpslon;
                        drewsomething = 0;
                        drawing = 1;
                        imageselect = 0;
                        oldimageselect = 0;
                        forcefirstdraw = 1;
                        gpsskipdraw = 1; // update for version 1.0.158
                        if (gpsfix == 1) {
                            mapsniffer = 800;
                            mapsniffercheckdefault();
                        }
                        if (bailedgpsmode == 0) { // update for version 1.0.143
                            playtinydirect(); // click for exit gps mode
                        } else {
                            joytimer = 99; // set for after next render
                            gpsmodeexitdelay();
                        }
                        bailedgpsmode = 0; // update for version 1.0.143
                        clrgraphics();
                    }
                    JOYPAR_PIN = 1;
                }

                shiftbytein();
                if (gpsmode == 0) { // update for version 1.0.27
                    if (joyspareb != 0) {
                        sdcardanimation();
                    } // card is not inserted
                    joystickaccelerate(); // update for version 1.0.202    
                    if (joyup + joydown + joyleft + joyright == 0) { // cancel acceleration
                        jheld = 1.0;
                        msine = 0; // update for version 1.0.119
                        if (joytimer > 0) {
                            joytimer--;
                        }
                    } else {
                        mapsniffer = 0; // update for version 1.0.161
                        msshowlines = 0; // update for version 1.0.165
                        backlighttimer = backlightdelay; // start backlight timer
                        joytimer = 199; // mouse accelleration
                        if (msine < 88) { // update for version 1.0.119
                            msine++;
                            jheld = sin_table[msine];
                            jheld = jheld / 1993;
                        } // msine
                    }
                    checkmousebounds();
                    mx = mmx;
                    my = mmy; // convert to integer

                    if (gpsmode == 0 && timewasacquired != 0) { // maintain time of day locally
                        if (secondschanged > 0) {
                            gpssecond = gpssecond + secondschanged;
                            if (gpssecond > 59) {
                                gpssecond = 0;
                                gpsminute++;
                            }
                            if (gpsminute > 59) {
                                gpsminute = 0;
                                gpshour++;
                            }
                            if (gpshour > 23) {
                                gpshour = 0;
                            }
                            secondschanged = 0;
                        } // secondschanged
                    } // timewasacquired
                } // gpsmode (fix for version 1.0.27)

                if (joyfire == 0) {
                    if (gpsskipdraw == 0) { // update for version 1.0.158
                        if (msshowlines > 0) { // update for version 1.0.161
                            if (msradius > 2) {
                                msradius--;
                            } else {
                                msradius = 20;
                            }
                            radius = msradius / 4; // set radius for nearest points
                            // draw current position circle at x-63.5, y-31.5
                            x = 63;
                            y = 30;
                            setpixel();
                            x++;
                            setpixel(); // update for version 1.0.174
                            x = x - 2;
                            y++;
                            setpixel();
                            y++;
                            setpixel();
                            x = 65;
                            setpixel();
                            y--;
                            setpixel();
                            x--;
                            y = y + 2;
                            setpixel();
                            x--;
                            setpixel();
                            logflipflop = 0; // update for version 1.0.162
                            if (msradius > 9) {
                                logflipflop = 1;
                            }
                            mapsnifferscrpnt(msfoplat, msfoplon);
                            if (logflipflop == 0) {
                                logflipflop = 1;
                            } else {
                                logflipflop = 0;
                            } // update for version 1.0.162
                            mapsnifferscrpnt(mspointlat, mspointlon);
                            msshowlines--;
                            x = 63;
                            y = 31;
                            clearpixel(); // update for version 1.0.179
                            y++;
                            clearpixel();
                            x++;
                            clearpixel();
                            y--;
                            clearpixel();
                            joytimer = 0; // update for version 1.0.163
                        } else { // msshowlines
                            if (gpslog == 1) {
                                gpstracklogread();
                            } // print track log
                        } // msshowlines

                        if (joytimer != 0) {
                            unsigned char osdmaska = 0x00; // update for version 1.0.94
                            unsigned char osdmaskb = 0x00; //
                            if (joytimer < 24) { // update for version 1.0.146
                                osdmaska = 0xFF >> (joytimer / 3);
                                osdmaskb = 0xFF << (joytimer / 3);
                            } // joytimer

                            unsigned char clearosd = 0;
                            while (clearosd < 11) { // clear on screen display areas
                                int clearcounter = clearbytes[clearosd];
                                if (clearcounter < 128) {
                                    osdmaska = osdmaskb;
                                }
                                while (clearcounter < clearbytes[clearosd + 1]) {
                                    framebuffer[clearcounter] = framebuffer[clearcounter] & osdmaska;
                                    clearcounter++;
                                } // clearcounter
                                clearosd = clearosd + 2;
                                if (clearosd == 10) {
                                    drawpointer();
                                } // draw pointer under top line
                            } // clearosd

                            userlat = screentllat - (pointval * my);
                            userlon = screentllon + (pointval * mx);
                            formatbufa = userlat;
                            formatbufb = userlon;
                            ty = 0;
                            if (joytimer < 24) {
                                ty = ty - (7 - (joytimer / 3));
                            } // update for version 1.0.94
                            if (formatbufa > -10.0 || formatbufb > -100.0) { // update for version 1.0.93
                                sprintf(text, "%f,%f", formatbufa, formatbufb);
                                drawtextcentered();
                            } else { // condensed space text
                                sprintf(text, "%f", formatbufa);
                                tx = -1;
                                drawtext();
                                sprintf(text, "%f", formatbufb);
                                tx = 62;
                                drawtext();
                                text[0] = 0x2C;
                                text[1] = 0;
                                tx = 58;
                                drawtext(); // ","
                            }

                            ty = 8;
                            if (joytimer < 24) {
                                ty = ty - (7 - (joytimer / 3));
                            } // update for version 1.0.94
                            drawline(0, ty, 127, ty);

                            ty = 57; // text part of auto button hide
                            if (joytimer < 24) {
                                ty = ty + (7 - (joytimer / 3));
                            } // update for version 1.0.94
                            tx = 1;
                            text[0] = 0x2B; // "+"
                            text[1] = 0;
                            drawtext();
                            tx = 120;
                            text[0] = 0x2D; // "-"
                            drawtext();
                            tx = 51;
                            sprintf(text, "EXIT");
                            drawtext();
                            tx = 22;
                            sprintf(text, "GPS");
                            drawtext();
                            tx = 86;

                            if (gpslog == 0) {
                                if (gpssimulation == 0) { // update for version 1.0.218
                                    sprintf(text, "LOG");
                                } else {
                                    sprintf(text, "---");
                                } // gpssimulation
                            } else {
                                trackloggauge();
                                sprintf(text, "%2.0f", formatbufa);
                                if (formatbufa < 100) {
                                    text[2] = 0x25; // percent sign
                                } // logguage
                                text[3] = 0; // string termination
                            } // gpslog
                            drawtext();

                            if (gpslog == 1 && distance > 0.0 && joytimer > 24) { // print track odometer
                                if (odometerslide < 46) {
                                    odometerslide++;
                                }
                                copys = 256 - odometerslide;
                                while (copys < 256) {
                                    framebuffer[copys] = 0;
                                    copys++;
                                }
                                if (locale == 0) { // update for version 1.0.211
                                    formatbufa = distance; // leave as kilometers
                                    sprintf(text, "%2.2fkm", formatbufa);
                                } else {
                                    formatbufa = distance * 0.621371; // convert to miles
                                    sprintf(text, "%2.2fmi", formatbufa);
                                } // locale
                                tx = 128 - odometerslide;
                                ty = 8;
                                drawtext();
                                drawline(128 - odometerslide, 16, 127, 16); // distance underline
                                drawline(128 - (odometerslide + 1), 15, 128 - (odometerslide + 1), 8); // distance right line
                                x = 128 - (odometerslide + 1);
                                y = 8;
                                clearpixel(); // for curve
                            } // gpslog

                            if (timewasacquired != 0 && joytimer > 24) { // print local time of day
                                if (timewasacquired < 48) {
                                    timewasacquired++;
                                    joytimer = 199;
                                }
                                copys = 128;
                                while (copys < 129 + timewasacquired) {
                                    framebuffer[copys] = 0;
                                    copys++;
                                }
                                sprintf(text, "%02d:%02d:%02d", gpshour, gpsminute, gpssecond);
                                tx = -48 + timewasacquired;
                                ty = 8;
                                drawtext();
                                drawline(0, 16, timewasacquired, 16); // time underline
                                drawline(timewasacquired + 1, 15, timewasacquired + 1, 8); // time right line
                                x = timewasacquired + 1;
                                y = 8;
                                clearpixel(); // for curve
                                x = timewasacquired;
                                clearpixel(); // for mask
                            } // timewasacquired

                            int blineht = 55; // update for version 1.0.94
                            if (joytimer < 24) {
                                blineht = blineht + (7 - (joytimer / 3));
                            }
                            unsigned char vlindex = 11; // two is subtracted straight away
                            while (vlindex > 2) { // draw horizontal button lines
                                vlindex = vlindex - 2;
                                drawline(vlines[vlindex - 1] + 1, blineht, vlines[vlindex] - 1, blineht);
                            } // vlindex left at value of one
                            while (vlindex < 9) { // draw vertical button lines
                                drawline(vlines[vlindex], blineht + 1, vlines[vlindex], 63);
                                vlindex++;
                            } // vlindex
                            y = blineht;
                            x = 0;
                            setpixel();
                            x = 127;
                            setpixel();
                        } // joytimer
                        //
                        // draw closest point display for map sniffer feature
                        if (mapsniffer != 0) { // update for version 1.0.158
                            if (mapsniffer == 800) {
                                mapsnifferfinish();
                            } // update for version 1.0.160
                            clearwindowbackground(); // update for version 1.0.158
                            sprintf(text, "NEAREST PLACE:");
                            ty = 19;
                            if (mapsniffer < 400) {
                                drawtextvarwidthcentered();
                                if (mapsniffer < 385) {
                                    copys = 0;
                                    while (copys < 25) { // copy label to text string
                                        text[copys] = idtext[100 + copys];
                                        copys++;
                                    } // copys
                                    mapsniffercheckvoid(); // update for version 1.0.181
                                    if (mapsniffercheckfound() == 1) { // update for version 1.0.165
                                        formatbufa = D;
                                        ty = 37;
                                        if (D < 10000.0 && mapsniffer > 200) { // limit the distance display
                                            printdistance();
                                        } else {
                                            printbearing(closebearing);
                                        } // D...mapsniffer
                                    } else { // notfound
                                        mspointlat = 220.0; // used as flag to cancel drawing
                                    } // checkfound
                                } // mapsniffer
                            } else { // mapsniffer
                                if (maptype == 0) { // update for version 1.0.159
                                    text[8] = 0x54;
                                    text[9] = 0x52; // TRAIL
                                    text[11] = 0x49;
                                    text[12] = 0x4C;
                                } else {
                                    text[8] = 0x52;
                                    text[9] = 0x4F; // ROAD:
                                    text[11] = 0x44;
                                    text[12] = 0x3A;
                                    text[13] = 0x00;
                                } // maptype
                                drawtextvarwidthcentered();
                                if (mapsniffer < 785) {
                                    sprintf(text, foplabel); // copy label to text string
                                    mapsniffercheckvoid(); // update for version 1.0.181
                                    ty = 37;
                                    if (mapsniffercheckfound() == 1) { // update for version 1.0.165
                                        if (fopdistancekm < 10000.0 && mapsniffer > 600) {
                                            formatbufa = fopdistancekm;
                                            ty = 37;
                                            printdistance();
                                        } else {
                                            printbearing(fopbearing);
                                        } // fopdistancekm
                                    } else { // notfound
                                        msfoplat = 220.0; // used as flag to cancel drawing
                                    } // checkfound
                                } // mapsniffer
                            } // mapsniffer
                            drawroundedrectangle(0, 15, 127, 32); // draw window border
                            mapsniffer--;
                            mapsniffer--; // decrement display time
                            if (mapsniffer > 25) {
                                joytimer = 25;
                            } // update for version 1.0.163
                            if (mapsniffer == 0) {
                                msshowlines = 440;
                            } // update for version 1.0.161
                        } // mapsniffer
                        //
                    } // gpsskipdraw
                    lastpressed = 0;
                } else {
                    if (lastpressed == 0) {
                        playtinydirect();
                    } // prevent repeated sound for log button
                    // clear find closest point status for fire button
                    mapsniffer = 0; // update for version 1.0.158
                    mspoitype = numicons; // reset nearest poi type to invalid type
                    D = 9999999999.9; // clear closest distance value
                    fopdistance = 9999999999.9; // clear closest foot of perpendicular
                    fopbearing = 0;
                    closebearing = 0; // update for version 1.0.160
                    unsigned char nonecnt = 0; // update for version 1.0.162
                    while (nonecnt < 5) { // set the default map sniffer labels
                        foplabel[nonecnt] = 0x2D; // if labels are changed here they must
                        idtext[100 + nonecnt] = 0x2D; // also be changed in the check function
                        nonecnt++;
                    } // nonecnt
                    idtext[105] = 0;
                    foplabel[5] = 0;
                    msshowlines = 0; // update for version 1.0.161
                    backlighttimer = backlightdelay; // start backlight timer
                    int osdactive = 0; // update for version 1.0.162
                    if (joytimer > 23) {
                        osdactive = 1;
                    }
                    joytimer = 99; // set for after next render
                    zooming = 0;

                    if (my > 56 && osdactive == 1) { // update for version 1.0.162
                        if (mx < 9) {
                            zoomlevel++;
                            zooming = 1;
                        } // check onscreen zoom buttons
                        if (mx > 118) {
                            zoomlevel--;
                            zooming = -1;
                        }

                        if (mx > 21 && mx < 41) { // check gps button
                            if (gpsmode == 0) {
                                gpsmode = 1;
                                joytimer = 0;
                                GPSPWR_PIN = 0; // gps module power on
                                stoptimer(); // update for version 1.0.204
                            } else {
                                gpsmode = 0;
                                Init_Timer1();
                            } // gpsmode
                            gpsbuttonpushed = 1;
                        }

                        if (mx > 85 && mx < 105) { // check track log button
                            if (lastpressed == 0) {
                                if (gpslog == 0) {
                                    if (gpssimulation == 0) { // update for version 1.0.218
                                        gpslog = 1;
                                        odometerslide = 1;
                                    } // gpssimulation
                                } else {
                                    gpslog = 0;
                                    odometerslide = 0;
                                } // gpslog
                            } // lastpressed
                            gpsbuttonpushed = 1;
                        }

                        if (mx > 50 && mx < 76) { // exit to menu button
                            stoptimer(); // update for version 1.0.204
                            framecount = 19; // cause exit
                        }
                    }

                    if (gpsbuttonpushed == 0) {
                        // seek to file start
                        FSfseek(pointer, 32, SEEK_SET); //
                        forcefirstdraw = 1;
                        if (zooming == 0) {
                            if (gpsmode == 1 && gpsfix == 1) {
                                // set gps position
                                userlat = gpslat;
                                userlon = gpslon;
                            } else {
                                // set new position
                                userlat = screentllat - (pointval * my);
                                userlon = screentllon + (pointval * mx);
                            } // gpsmode
                            gpszoom();
                        } else {
                            // set position to screen centre
                            userlat = screentllat - (pointval * 31.5);
                            userlon = screentllon + (pointval * 63.5);
                            //  zoom in or out

                            if (zooming < 0) {
                                if (zoomlevel > -3) {
                                    pointval = pointval * 2; // zoom out
                                } else {
                                    zoomlevel = -2;
                                } // zoomlevel
                            } else {
                                if (zoomlevel < 2) {
                                    pointval = pointval / 2; // zoom in
                                } else {
                                    zoomlevel = 1;
                                } // zoomlevel
                            } // zooming

                            if (gpsmode == 1 && gpsfix == 1) {
                                userlat = gpslat; // set gps position
                                userlon = gpslon; //
                            }
                            gpszoom();
                        } // zooming

                        // reset mouse
                        mmx = 63.5;
                        mmy = 31.5;
                        jheld = 1.0;
                        drewsomething = 0;
                        drawing = 1;

                        // clear image buffer
                        copys = 0;
                        while (copys < 1024) {
                            if (gpsmode == 1) {
                                imagebuffer[copys + imageselect] = 0; // clear the image we are about to draw to, but not display
                            } else {
                                imagebuffer[copys] = 0;
                            } // gpsmode
                            framebuffer[copys] = 0;
                            copys++;
                        } // copys
                    } else {
                        gpsbuttonpushed = 0;
                    } // gpsbuttonpushed
                    lastpressed = 1;
                } // joyfire
            } // drawing
            //
            gpsskipdraw = 0; // update for version 1.0.158
            //
            if (framecount == 19 || lasterror == 11) {
                if (filecount > 0) {
                    FSfclose(pointer);
                }
                // go back to parent directory
                // which should be the root
                FSchdir("..");
                makegpx(); // update for version 1.0.154
                samplenum = 1; // play exit sample
                load_sample();
                playvoice();
                resettomenu();
            } // framecount

            if (drawing > 0) {
                backlighttimer = backlightdelay;
            } // start backlight timer
            if (backlighttimer > 0) { // handle lcd backlight
                backlighttimer--;
                LED_PIN = 1;
            } else {
                LED_PIN = 0;
            } // backlighttimer

        } // demo 2 (gps map)
        //
        //
        //***************************************************************************************************************
        //***************************************************************************************************************
        //**********************************************  ASTRONOMY CLOCK  **********************************************
        //***************************************************************************************************************
        //***************************************************************************************************************
        //
        // idtext[0-5] used as time digit roll flags
        // idtext[10-15] used as old time values
        // idtext[6] used as frame counter for clock
        // idtext[7] used as clock seconds flip flop
        // idtext[17] used as old full moon flag
        // idtext[100-105] used as date digit roll flags
        // idtext[110-115] used as old date values
        // tetx & tety used by the speech visualisation
        // tetdir & oldtetdir used in speech visualisation
        // idtext[130] used as a message scroll counter
        // filelabel & foplabel used as message buffers
        // idtext[131-132] used for message scroller
        // idtext[128-129] used as swap buffers for date
        //
        if (demo == 4) { // astronomy clock
            int waxwaning = 0; // update for version 1.0.200
            //
            if (framecount == 104) {
                startvoice();
                idtext[17] = 0;
            }
            clrframebuffer(); // clear frame buffer

            if (idtext[6] == 0) {
                n = 0;
                while (n < 6) {
                    idtext[n + 10] = tztime[n]; // write old time
                    idtext[n] = 0; // clear digit roll flags
                    idtext[110 + n] = tzdate[n]; // write old date
                    idtext[100 + n] = 0; // clear digit roll flags
                    n++;
                } // n

                if (playing == 0 || vsame == 1) {
                    if (gotgprmc == 2 || firstgpsdata == 0) { // update for version 1.0.141
                        handlegps();

                        if (firstgpsdata == 0) { // update for version 1.0.200
                            if (gpsfix == 1) { // handle first message
                                int gn = 0; // update for version 1.0.199
                                while (gn < 10) { // copy title string
                                    line3[gn] = programs[gn];
                                    if (mixedcase != 0) { // update for version 1.0.212
                                        if (line3[gn] > 0x40 && line3[gn] < 0x5B) {
                                            line3[gn] = line3[gn] + 0x20;
                                        }
                                    } // mixedcase
                                    gn++;
                                } // gn
                                line3[gn] = 0; // terminate title string
                                idtext[132] = 0; // common to both cases
                                if (mixedcase == 0) { // upper case
                                    idtext[131] = 0x53;
                                    idtext[133] = 0x52; // "T" and "S"
                                } else { // mixed case
                                    line3[0] = 0x41;
                                    line3[6] = 0x54; // capitalise first characters again
                                    idtext[131] = 0x73;
                                    idtext[133] = 0x72; // "t" and "s"
                                } // mixedcase
                            } else { // waiting for gps message will appear
                                int hn = 0; // update for version 1.0.213
                                while (hn < 3) {
                                    idtext[131 + hn] = smhistory[hn]; // upper case history values
                                    if (mixedcase != 0) {
                                        idtext[131 + hn] = idtext[131 + hn] + 0x20;
                                    }
                                    hn++;
                                } // hn
                            } // gpsfix
                            firstgpsdata = 1;
                        } // firstgpsdata

                        if (playing == 1) {
                            vfill_audio();
                        } // fill audio buffer if playing
                        applytzoffset(); // apply local time zone offset
                        if (dateformat != 0 && framecount > 102) { // update for version 1.0.211
                            idtext[128] = tzdate[0];
                            idtext[129] = tzdate[1]; // swap day and month
                            tzdate[0] = tzdate[2];
                            tzdate[1] = tzdate[3];
                            tzdate[2] = idtext[128];
                            tzdate[3] = idtext[129];
                        } // dateformat
                    } // gotgprmc
                } // playing
                if (playing == 1) {
                    vfill_audio();
                } // fill audio buffer if playing

                if (tztime[5] != idtext[15]) {
                    idtext[6] = 15; // start digits rolling
                    idtext[5] = 1; // seconds will roll
                    if (tztime[4] != idtext[14]) {
                        idtext[4] = 1;
                        if (idtext[7] != 0) {
                            idtext[7] = 0;
                        } else {
                            idtext[7] = 1;
                        } // flip flop
                    } // tztime
                    n = 0;
                    while (n < 4) {
                        if (tztime[n] != idtext[n + 10]) {
                            idtext[n] = 1;
                        }
                        n++;
                    } // n
                } // tztime

                int dateroll = 0; // update for version 1.0.211
                if (dateformat == 0) { // local
                    if (tzdate[1] != idtext[111]) {
                        dateroll = 1;
                    }
                } else { // usa
                    if (tzdate[3] != idtext[113]) {
                        dateroll = 1;
                    }
                } // dateformat

                if (dateroll == 1) { // update for version 1.0.211
                    n = 0;
                    while (n < 6) { // at least the day lsd will roll
                        if (tzdate[n] != idtext[n + 110]) {
                            idtext[100 + n] = 1;
                        }
                        n++;
                    } // n
                } // tzdate
            } // idtext

            // rolling odometer effect time and date display
            text[1] = 0;
            char posadjust = 16 - idtext[6]; // update for version 1.0.146
            n = 0;
            int tlxindex = 0;
            int tlxcnt = 1;
            while (tlxcnt < 4) { // update for version 1.0.180
                while (n < tlxcnt * 2) { // update for version 1.0.180
                    tx = tlxtable[tlxindex];
                    if (idtext[100 + n] == 0) { // update for version 1.0.186
                        ty = 41;
                    } else {
                        text[0] = idtext[n + 110];
                        ty = 41 + posadjust;
                        drawtextbolddigit();
                        tx = tlxtable[tlxindex];
                        ty = 26 + posadjust;
                    }
                    text[0] = tzdate[n];
                    drawtextbolddigit();
                    tx = tlxtable[tlxindex];
                    if (idtext[n] == 0) {
                        ty = 2;
                    } else {
                        text[0] = idtext[n + 10];
                        ty = 2 + posadjust;
                        drawtextbolddigit();
                        tx = tlxtable[tlxindex];
                        ty = -13 + posadjust;
                    }
                    text[0] = tztime[n];
                    drawtextbolddigit();
                    n++;
                    tlxindex++;
                } // n
                if (tlxcnt < 3) { // draw semicolons
                    if (tlxcnt == 1) {
                        tx = 59;
                        if (tztime[0] == 0x54) {
                            tx = 62;
                        }
                    } else {
                        tx = 91; // update for version 1.0.187
                        if (tztime[0] == 0x54) {
                            tx = 130;
                        }
                    } // tlxcnt
                    ty = 2;
                    text[0] = 0x3A;
                    drawtextlarge();
                } // tlxcnt
                tlxcnt++;
            } // tlxcnt

            y = 4; // update for version 1.0.187
            while (y < 14) { // chop vertical row off the colons
                x = 98;
                clearpixel();
                x = 66;
                clearpixel();
                y++;
            } // y

            // blit the date display area into place
            int blitbit = 0; // update for version 1.0.186
            y = 40;
            while (y < 56) {
                x = 34;
                while (x < 127) {
                    blitbit = getpixel();
                    y = y - 20; // get source bit
                    if (blitbit == 1) {
                        setpixel();
                    } else {
                        clearpixel();
                    }
                    y = y + 20;
                    clearpixel(); // clear source area
                    x++;
                } // x
                y++;
            } // y
            // mask the rest of the scratch area
            n = 930; // update for version 1.0.186
            while (n < 1023) {
                framebuffer[n] = 0;
                x = n - 896; // update for version 1.0.203
                y = 18;
                clearpixel();
                y = 37;
                clearpixel();
                pixelworkprivateg();
                n++;
            } // n

            n = 0; // update for version 1.0.203
            while (n < 4) { // clear leaked pixels
                y = borderys[n];
                x = 124;
                clearpixel();
                x--;
                clearpixel();
                x = x - 87;
                clearpixel();
                x++;
                clearpixel();
                n++;
            } // n

            n = 32;
            while (n > 28) { // update for version 1.0.195
                drawline(63, n, 65, n - 5); // draw backslashes manually
                drawline(95, n, 97, n - 5); // nicer than the scaled font
                n--;
            } // n

            drawroundedrectangle(33, 0, 94, 17); // time rectangle
            drawroundedrectangle(33, 19, 94, 17); // date rectangle
            drawroundedrectangle(0, 38, 127, 25); // astronomy rectangle

            if (framecount > 102) { // prepare the display messages
                if (gpsfix == 1) {
                    if (idtext[130] == 0 && idtext[6] == 15) { // update for version 1.0.199

                        if (tztime[5] == 0x30) { // update for version 1.0.200
                            msgcount++; // increment the message counter
                            if (msgcount > 4) {
                                msgcount = 0;
                            } // limit the message counter
                        } // tztime

                        if (msgcount == 0) { // print day of week name
                            clrfb = 0;
                            n = 0; // update for version 1.0.93
                            while (clrfb < caldow) { // find name in array
                                if (daynames[n] != 0x3A) {
                                    n++;
                                } else {
                                    clrfb++;
                                    n++;
                                }
                            } // clrfb
                            clrfb = 0; // copy day name to text string
                            while (daynames[n] != 0x3A) {
                                line3[clrfb] = daynames[n];
                                n++;
                                clrfb++;
                            }
                            line3[clrfb] = 0; // update for version 1.0.197
                            // place holder message for day of week display line two
                            // unless something better comes up or the memory needed
                            // update for version 1.0.212
                            sprintf(line2, "(Day of the Sun)"); // common for all messages
                            if (caldow == 1) {
                                sprintf(line2 + 12, "Moon)");
                            } // 1
                            if (caldow == 2) {
                                sprintf(line2 + 8, "Mars)");
                            } // 2
                            if (caldow == 3) {
                                sprintf(line2 + 8, "Mercury)");
                            } // 3
                            if (caldow == 4) {
                                sprintf(line2 + 8, "Jupiter)");
                            } // 4
                            if (caldow == 5) {
                                sprintf(line2 + 8, "Venus)");
                            } // 5
                            if (caldow == 6) {
                                sprintf(line2 + 8, "Saturn)");
                            } // 6
                            if (caldow > 6) { // update for version 1.0.204
                                if (lasterror != 31) {
                                    debugout(31);
                                } // out of range error
                            } // caldow
                            astrocaseconvert(); // update for version 1.0.212
                        } // recievebuffer

                        if (msgcount == 1) { // sunrise and set times
                            sunset();
                            // check for sunrise time
                            // update for version 1.0.50
                            unsigned char srmatch = 1;
                            if (tztime[5] != 0x30) { // update for version 1.0.212
                                srmatch = 0; // cancel
                            } else { // or check for match
                                if (tztime[0] != line3[11]) {
                                    srmatch = 0;
                                }
                                if (tztime[1] != line3[12]) {
                                    srmatch = 0;
                                }
                                if (tztime[2] != line3[14]) {
                                    srmatch = 0;
                                }
                                if (tztime[3] != line3[15]) {
                                    srmatch = 0;
                                }
                                if (srmatch == 1 && playing == 0) {
                                    samplenum = 3; // play rooster sample
                                    load_sample();
                                    astrostartsample(); // update for version 1.0.213
                                } // srmatch
                            } // tztime
                        }

                        if (msgcount == 2) {
                            moontimes();
                        } // moonrise and set times

                        if (msgcount == 3) { // moon percentage illumination
                            sprintf(line3, "Moon Percentage");
                            mp = phase(); // update for version 1.0.209
                            sprintf(line2, "Illumination: "); // update for version 1.0.194
                            if (mp < 99.95) { // update for version 1.0.207
                                sprintf(line2 + 14, "%1.1f%%", mp); // update for version 1.0.194
                            } else {
                                sprintf(line2 + 14, "100%%"); // update for version 1.0.175
                            }
                            if (mp < 10.0) {
                                sprintf(line2 + 14, "%1.2f%%", mp); // update for version 1.0.194
                            }
                            astrocaseconvert(); // update for version 1.0.212
                            //
                            // check for full moon
                            unsigned char srmatch = 1; // update for version 1.0.212
                            if (mp < 100.0) {
                                srmatch = 0;
                                idtext[17] = 0;
                            }
                            if (idtext[17] != 0) {
                                srmatch = 0;
                            }
                            if (srmatch == 1 && playing == 0) {
                                idtext[17] = 1; // set wolf howl occurred flag
                                samplenum = 2; // load wolf howl sample
                                load_sample();
                                //
                                // because the wolf howl sample was prepared when the voice array
                                // was larger, it is too long, and has to be trimmed in software.
                                // the beginning of the sample is clipped by adjusting the sample
                                // position in the rom file in the load sample function, and the
                                // sample is clipped even shorter here to allow space for silence
                                // frames to append to the sample to avoid clicking at the end.
                                vlen = 6720;
                                n = 0;
                                while (n < 3) { // update for version 1.0.92
                                    clrfb = 262;
                                    n++;
                                    while (clrfb < 368) {
                                        voice[vlen] = tiny[clrfb];
                                        clrfb++;
                                        vlen++;
                                    }
                                } // n
                                astrostartsample(); // update for version 1.0.213
                            } // srmatch
                        }
                        //
                        // assemble the common moon phase expression
                        if (msgcount == 4) { // update for version 1.0.212
                            formatbufa = mp; // save for next comparison
                            mp = phase();
                            waxwaning = 0; // default invalid status
                            sprintf(text, "Waning ");
                            if (mp < formatbufa) {
                                waxwaning = 7;
                            }
                            if (mp > formatbufa) {
                                text[2] = 0x78;
                                waxwaning = 3;
                            } // "x"
                            sprintf(text + 7, "Quarter");
                            if (waxwaning == 3 || waxwaning == 7) { // update for version 1.0.201
                                if (mp < 49.5) {
                                    sprintf(text + 7, "Crescent");
                                    waxwaning--;
                                } // set to crescent
                                if (mp > 50.5) {
                                    sprintf(text + 7, "Gibbous");
                                    waxwaning++;
                                } // set to gibbous
                            } // mp
                            if (mp < 1.0) {
                                sprintf(text, "New Moon");
                                waxwaning = 1;
                            } // set to new moon
                            // full moon message padded with spaces or it's too close to the last message
                            if (mp > 99.0) {
                                sprintf(text, " Full Moon ");
                                waxwaning = 5;
                            } // update for version 1.0.207
                            if (waxwaning != 0) { // result was valid
                                sprintf(line3, "Moon Phase:");
                                sprintf(line2, "%s", text); // update for version 1.0.212
                                astrocaseconvert(); // update for version 1.0.212
                            } else { // result was invalid
                                msgcount = 0; // skip this message
                                debugout(38); // update for version 1.0.211
                            } // waxwaning
                        } // msgcount
                        //
                    } // idtext
                } else { // no position fix
                    if (lasterror != 6) { // waiting for gps position fix
                        sprintf(line3, "Waiting for GPS"); // update for version 1.0.212
                        n = 0;
                        while (n < 16) {
                            line2[n] = line3[n];
                            n++;
                        } // copy to both lines
                        if (idtext[7] != 0) {
                            line3[0] = 0;
                        } else {
                            line2[0] = 0;
                        }
                    } else { // bad nmea checksum
                        badcheckstring(); // for bad gps nmea checksum
                        sprintf(line3, text); // update for version 1.0.197
                        line2[0] = 0; // cancel line two string
                    } // lasterror
                    astrocaseconvert(); // update for version 1.0.212
                    msgcount = 0; // update for version 1.0.200
                } // gpsfix

                if (idtext[130] == 0) { // update for version 1.0.200
                    if (idtext[131] != line3[1] || idtext[132] != line2[2] || idtext[133] != line3[3]) { // update for version 1.0.198
                        idtext[131] = line3[1];
                        idtext[132] = line2[2];
                        idtext[133] = line3[3]; // update old message buffers
                        idtext[130] = 26; // trigger the scroll
                    } else { // message has not changed
                        n = 0; // buffer the current strings to scroll out when changed
                        while (n < 23) {
                            filelabel[n] = line3[n];
                            foplabel[n] = line2[n];
                            n++;
                        }
                        filelabel[n] = 0;
                        foplabel[n] = 0; // terminate both strings
                    } // idtext
                } // idtext
            } // framecount

            if (idtext[130] == 0) { // update for version 1.0.212
                sprintf(text, "%s", line3);
                ty = 42;
                drawtextvarwidthcentered();
                sprintf(text, "%s", line2);
                ty = 53;
                drawtextvarwidthcentered();
            } else { // message is scrolling
                int scposa = idtext[130];
                int scposb = idtext[130];
                scposa = spmessage[scposa - 1]; // print two sets of messages for the scroll
                scposb = spmessage[scposb];
                sprintf(text, "%s", line3);
                ty = 42;
                drawtextvarwidthcenteredpos(64 + scposa);
                sprintf(text, "%s", filelabel);
                drawtextvarwidthcenteredpos(-64 + scposa);
                sprintf(text, "%s", line2);
                ty = 53;
                drawtextvarwidthcenteredpos(64 + scposb);
                sprintf(text, "%s", foplabel);
                drawtextvarwidthcenteredpos(-64 + scposb);
                idtext[130]--; // decrement the scroll counter
            } // idtext

            int secmsb = 0; // update for version 1.0.157
            int minmsb = 0;
            int hrmsb = 0;
            secmsb = asciitoint(tztime[4], tztime[5]); // update for version 1.0.212
            minmsb = asciitoint(tztime[2], tztime[3]);
            hrmsb = asciitoint(tztime[0], tztime[1]);
            cx = 15;
            cy = 18; // draw the analogue clock face
            radius = 15;
            drawcircle();
            if (framecount > 140) { // update for version 1.0.191
                if (playing == 0 && tetdir == 0) { // update for version 1.0.191
                    radius = 2;
                    drawcircle();
                    ox = 15;
                    oy = 18;
                    rotation = secmsb * 6;
                    y = 3;
                    analogueclockhand();
                    rotation = minmsb * 6;
                    y = 5;
                    analogueclockhand();
                    rotation = hrmsb * 30;
                    y = 8;
                    analogueclockhand();
                    clrfb = 17; // update for version 1.0.94
                    while (clrfb < 20) { // update for version 1.0.180
                        y = clrfb;
                        clrfb++;
                        x = 14;
                        clearpixel();
                        x++;
                        clearpixel(); // update for version 1.0.165
                        x++;
                        clearpixel(); //
                    } // clrfb
                    x--;
                    y = 18;
                    setpixel(); // update for version 1.0.165
                    oldtetdir = 0; // update for version 1.0.196
                    tetx = 0; // update for version 1.0.196
                } else { // playing
                    announcementdisplay(); // update for version 1.0.191
                    tetdir--; // decrement announcement timer
                } // playing
            } // framecount

            if (playing == 0) { // exit with joystick left
                JOYPAR_PIN = 0;
                delay_gen(0); //
                if (JOYDAT_PIN == 0) { // update for version 1.0.133
                    IEC1bits.U2RXIE = 0; // disable serial interrupt
                    IFS1bits.U2RXIF = 0; // clear interrupt flag 
                    gpsfix = 0;
                    framecount = 19;
                    samplenum = 1; // play exit sample
                    load_sample();
                    playvoice();
                    astrotransition(0);
                    resettomenu();
                }
                JOYPAR_PIN = 1;
            } // playing

            // read entire joystick shift register
            if (playing == 0) { // not currently playing
                shiftbytein();
                if (joyright == 1) { // update for version 1.0.213
                    if (gotdate != 0) {
                        vsame = 1;
                        clrfb = 0; // update for version 1.0.147
                        while (clrfb < 4) {
                            if (oldvtime[clrfb] != vtime[clrfb]) {
                                vsame = 0;
                            }
                            clrfb++;
                        } // clrfb
                        if (vsame == 0) { // display time has changed
                            build_announcement();
                            oldvtime[0] = vtime[0];
                            oldvtime[1] = vtime[1];
                            oldvtime[2] = vtime[2];
                            oldvtime[3] = vtime[3];
                        } // vsame
                        tetx = 0; // update for version 1.0.192
                        vbytecount = 0;
                        playing = 1;
                    } else {
                        samplenum = 4;
                        load_sample(); // load sample
                        tetx = 0; // update for version 1.0.213
                        astrostartsample(); // update for version 1.0.213
                    } // gotdate
                } // joyright
            } else { // currently playing
                vfill_audio(); // fill vs1003b buffer
            } // playing

            if (idtext[6] > 0) {
                idtext[6]--;
            } // decrement digit rolling frame counter

            if (framecount > 999) {
                framecount = 990;
            } // update for version 1.0.191
            LED_PIN = 1; // backlight always on for clock
        } // demo 4 (astronomy clock)
        //
    } // loop
    return (0); // keeps the compiler happy
} // end main
//
//
//
// delay of arbitrary duration - not milliseconds

void delay_gen(unsigned int nx) {
    int xd;
    while (nx--) {
        xd = 5000;
        while (xd--) {
            delaycounter = 0;
        } // useless command
    }
}
//
// lcd delay

void delay_lcd() { // update for version 1.0.130
    delaycounter = 0; // useless command
}
//

void setpixel() { // update for version 1.0.62
    if (x > 127 || y > 63) {
        return;
    }
    if (x < 0 || y < 0) {
        return;
    }
    int newx = (y & 0b00111000) << 4;
    int newy = y - (y & 0b00111000);
    newx = newx + x;
    framebuffer[newx] = framebuffer[newx] | (0b00000001 << newy);
}
//

void clearpixel() { // update for version 1.0.62
    if (x > 127 || y > 63) {
        return;
    }
    if (x < 0 || y < 0) {
        return;
    }
    int newx = (y & 0b00111000) << 4;
    int newy = y - (y & 0b00111000);
    newx = newx + x;
    framebuffer[newx] &= ~(0b00000001 << newy);
}
//

void setpixelimage() { // update for version 1.0.62
    if (x > 127 || y > 63) {
        return;
    }
    if (x < 0 || y < 0) {
        return;
    }
    int newx = (y & 0b00111000) << 4;
    int newy = y - (y & 0b00111000);
    newx = newx + x + imageselect;
    imagebuffer[newx] = imagebuffer[newx] | (0b00000001 << newy);
}
//

void clearpixelimage() { // update for version 1.0.75
    if (x > 127 || y > 63) {
        return;
    }
    if (x < 0 || y < 0) {
        return;
    }
    int newx = (y & 0b00111000) << 4;
    int newy = y - (y & 0b00111000);
    newx = newx + x + imageselect;
    imagebuffer[newx] &= ~(0b00000001 << newy);
}
//

void flippixel() { // update for version 1.0.62
    if (x > 127 || y > 63) {
        return;
    }
    if (x < 0 || y < 0) {
        return;
    }
    int newx = (y & 0b00111000) << 4;
    int newy = y - (y & 0b00111000);
    newx = newx + x;
    framebuffer[newx] = framebuffer[newx]^(0b00000001 << newy);
}
//

int getpixel() { // update for version 1.0.78
    if (x > 127 || y > 63) {
        return (2);
    }
    if (x < 0 || y < 0) {
        return (2);
    }
    int pixelvalue = 0;
    int newx = (y & 0b00111000) << 4;
    int newy = y - (y & 0b00111000);
    newx = newx + x;
    pixelvalue = framebuffer[newx] & (0b00000001 << newy);
    if (pixelvalue > 0) {
        pixelvalue = 1;
    }
    return (pixelvalue);
}
//

int getpixelimage() { // update for version 1.0.78
    if (x > 127 || y > 63) {
        return (2);
    }
    if (x < 0 || y < 0) {
        return (2);
    }
    int pixelvalue = 0;
    int newx = (y & 0b00111000) << 4;
    int newy = y - (y & 0b00111000);
    newx = newx + x + imageselect;
    pixelvalue = imagebuffer[newx] & (0b00000001 << newy);
    if (pixelvalue > 0) {
        pixelvalue = 1;
    }
    return (pixelvalue);
}
//
// fast plasma demo screen saver
// not that the screen needs saving

void plasmasaver() { // update for version 1.0.209
    int plassaver = 1;
    int savercount = 149;
    sp1 = 4;
    sp2 = 2;
    sp3 = 4;
    sp4 = 2;
    pl1 = 0;
    pl2 = 0;
    pl3 = 0;
    pl4 = 0;
    pdir = 0;
    GPSPWR_PIN = 1; // gps module power off
    while (plassaver == 1) {
        clrframebuffer();
        drawplasma();
        drawscreen();
        shiftbytein();
        if (joyleft + joyright + joyup + joydown > 0) {
            plassaver = 0;
            backlighttimer = backlightdelay;
        } // joy
        if (savercount > 0) {
            savercount--;
        } else {
            LED_PIN = 0;
        }
    } // plassaver
    menuvars[6] = -5;
    menuvars[7] = -5; // update for version 1.0.125
}
//
// plasma demo sine table lookup

short plasmasin(int val) { // update for version 1.0.208
    short tsinind = 0;
    val %= 360;
    if (val < 0) {
        val = val + 360;
    }
    if (val < 181) {
        if (val < 91) {
            tsinind = val;
        } else {
            tsinind = 180 - val;
        }
        return sin_table[tsinind];
    } else {
        if (val < 271) {
            tsinind = val - 180;
        } else {
            tsinind = 360 - val;
        }
        return -sin_table[tsinind];
    }
}
//
// draw one retro plasma demo frame

void drawplasma() {
    long int plamplitude = 180;
    long zz;
    unsigned char pcount;
    unsigned char z;
    pt[0] = pl1;
    pt[1] = pl2;
    for (y = 0; y < 64; y++) {
        pt[2] = pl3;
        pt[3] = pl4;
        for (x = 0; x < 128; x++) {
            pcount = 0;
            z = 0;
            while (pcount < 4) {
                zz = pt[pcount];
                z = z + (unsigned char) ((plamplitude * (plasmasin((zz * 1440) / 256))) / 16384);
                pcount++;
            } // pcount
            if (z < 252 && z > 100) {
                setpixel();
            }
            pt[2] += 1;
            pt[3] += 2;
        }
        pt[0] += 2;
        pt[1] += 1;
    }
    pl1 += sp1;
    pl2 -= sp2;
    pl3 += sp3;
    pl4 -= sp4;
}
//

void pixelrotation() { // update for version 1.0.216
    theta = rotation;
    theta = d2r * theta; // pre calculation
    multiplerotation();
}
//

void multiplerotation() { // update for version 1.0.144
    int rx;
    int ry;
    xx = x;
    yy = y;
    rx = xx - ox;
    ry = yy - oy;
    xnew = cos(theta) * rx - sin(theta) * ry;
    ynew = sin(theta) * rx + cos(theta) * ry;
    x = xnew + ox;
    y = ynew + oy;
}
//

void textrotation() { // update for version 1.0.144
    int rx;
    int ry;
    xx = x;
    yy = y;
    rx = xx - txtox;
    ry = yy - txtoy;
    xnew = cos(theta) * rx - sin(theta) * ry;
    ynew = sin(theta) * rx + cos(theta) * ry;
    x = xnew + txtox;
    y = ynew + txtoy;
}
//

void drawcircle() { // update for version 1.0.103
    unsigned char glc_x = 0;
    unsigned char glc_y = radius;
    WORD glc_s = 0;
    glc_s = 2 - 2 * radius;
    while (glc_x <= glc_y) {
        x = glc_x + cx;
        y = glc_y + cy;
        setpixel();
        x = cx - glc_x;
        setpixel();
        x = cx + glc_x;
        y = cy - glc_y;
        setpixel();
        x = cx - glc_x;
        setpixel();
        x = glc_y + cx;
        y = glc_x + cy;
        setpixel();
        x = cx - glc_y;
        setpixel();
        x = cx + glc_y;
        y = cy - glc_x;
        setpixel();
        x = cx - glc_y;
        setpixel();
        if (glc_s > 0x7FFF) {
            glc_s = glc_s + (4 * glc_x + 6);
        } else {
            glc_s = glc_s + (4 * (glc_x - glc_y) + 10);
            glc_y--;
        }
        glc_x++;
    }
}
//

void drawcircleimage() { // update for version 1.0.191
    unsigned char glc_x = 0;
    unsigned char glc_y = radius;
    WORD glc_s = 0;
    glc_s = 2 - 2 * radius;
    while (glc_x <= glc_y) {
        x = glc_x + cx;
        y = glc_y + cy;
        setpixelimage();
        x = cx - glc_x;
        setpixelimage();
        x = cx + glc_x;
        y = cy - glc_y;
        setpixelimage();
        x = cx - glc_x;
        setpixelimage();
        x = glc_y + cx;
        y = glc_x + cy;
        setpixelimage();
        x = cx - glc_y;
        setpixelimage();
        x = cx + glc_y;
        y = cy - glc_x;
        setpixelimage();
        x = cx - glc_y;
        setpixelimage();
        if (glc_s > 0x7FFF) {
            glc_s = glc_s + (4 * glc_x + 6);
        } else {
            glc_s = glc_s + (4 * (glc_x - glc_y) + 10);
            glc_y--;
        }
        glc_x++;
    }
}
//

void drawtext() { // updated for version 1.0.191
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    while (text[tchar] != 0) {
        tx++;
        tindex = (text[tchar] - 0x20) * 5;
        tcnt = 0;
        while (tcnt < 5) {
            x = tx;
            int bitcnt = 7;
            while (bitcnt > 0) {
                bitcnt--;
                if ((charmap[tindex + tcnt] & (0b00000001 << bitcnt)) != 0) {
                    y = (ty + bitcnt);
                    setpixel();
                }
            } // bitcnt
            tx++;
            tcnt++;
        } // tcnt
        tchar++;
    }
}
//

void drawtextimage() { // updated for version 1.0.191
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    while (text[tchar] != 0) {
        tx++;
        tindex = (text[tchar] - 0x20) * 5;
        tcnt = 0;
        while (tcnt < 5) {
            x = tx;
            int bitcnt = 7;
            while (bitcnt > 0) {
                bitcnt--;
                if ((charmap[tindex + tcnt] & (0b00000001 << bitcnt)) != 0) {
                    y = (ty + bitcnt);
                    setpixelimage();
                }
            } // bitcnt
            tx++;
            tcnt++;
        } // tcnt
        tchar++;
    }
}
//

void drawtextimageclear() { // updated for version 1.0.191
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    while (text[tchar] != 0) {
        tx++;
        tindex = (text[tchar] - 0x20) * 5;
        tcnt = 0;
        while (tcnt < 5) {
            x = tx;
            int bitcnt = 7;
            while (bitcnt > 0) {
                bitcnt--;
                if ((charmap[tindex + tcnt] & (0b00000001 << bitcnt)) != 0) {
                    y = (ty + bitcnt);
                    clearpixelimage();
                }
            } // bitcnt
            tx++;
            tcnt++;
        } // tcnt
        tchar++;
    }
}
//

void drawtextimagerotated() { // updated for version 1.0.191
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    theta = trotation; // pre calculation
    theta = d2r * theta;
    while (text[tchar] != 0) {
        tx++;
        tindex = (text[tchar] - 0x20) * 5;
        tcnt = 0;
        while (tcnt < 5) {
            int bitcnt = 7;
            while (bitcnt > 0) {
                bitcnt--;
                if ((charmap[tindex + tcnt] & (0b00000001 << bitcnt)) != 0) {
                    x = tx;
                    y = (ty + bitcnt);
                    textrotation();
                    setpixelimage();
                }
            } // bitcnt
            tx++;
            tcnt++;
        } // tcnt
        tchar++;
    }
}
//

void drawtextlarge() { // updated for version 1.0.216
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    unsigned char looptwice;
    while (text[tchar] != 0) {
        tx = tx + 2;
        tindex = (text[tchar] - 0x20) * 5;
        tcnt = 0;
        while (tcnt < 5) {
            looptwice = 0;
            while (looptwice < 2) {
                x = tx;
                int bitcnt = 14;
                while (bitcnt > 0) {
                    bitcnt = bitcnt - 2;
                    if ((charmap[tindex + tcnt] & (0b00000001 << (bitcnt / 2))) != 0) {
                        y = ty + bitcnt;
                        setpixel();
                        y++;
                        setpixel();
                    } // charmap
                } // bitcnt
                tx++;
                looptwice++;
            } // looptwice
            tcnt++;
        } // tcnt
        tchar++;
    }
}
//

void drawtextlargesin() { // updated for version 1.0.216
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    unsigned char looptwice;
    sinindex = sinstart;
    sindir = sinstartdir;
    sinvalue = sin_table[sinindex];
    if (sindir == 0) {
        sinindex = sinindex + sinfreq;
        if (sinindex == 90) {
            sindir = 1;
        }
    } else {
        sinindex = sinindex - sinfreq;
        if (sinindex == 0) {
            sindir = 0;
        }
    } // sindir
    sinvalue = sinvalue >> sinamp; // max 8 for value 11
    sinstart = sinindex;
    sinstartdir = sindir;
    while (text[tchar] != 0) {
        tx = tx + 2;
        tindex = (text[tchar] - 0x20) * 5;
        tcnt = 0;
        while (tcnt < 5) {
            looptwice = 0;
            while (looptwice < 2) {
                sinvalue = sin_table[sinindex];
                if (sindir == 0) {
                    sinindex = sinindex + sinfreq;
                    if (sinindex == 90) {
                        sindir = 1;
                    }
                } else {
                    sinindex = sinindex - sinfreq;
                    if (sinindex == 0) {
                        sindir = 0;
                    }
                } // sindir
                x = tx;
                sinvalue = sinvalue >> sinamp; // max 8
                int tty = 13; // update for version 1.0.128
                unsigned char sbmask = 0b01000000;
                while (tty > 0) {
                    if ((charmap[tindex + tcnt] & sbmask) != 0) {
                        y = sinvalue + (ty + (tty - 1));
                        setpixel();
                        y++;
                        setpixel();
                    }
                    tty = tty - 2;
                    sbmask = sbmask >> 1;
                } // tty
                tx++;
                looptwice++;
            } // looptwice
            tcnt++;
        } // tcnt
        tchar++;
    }
}
//

void drawtextlargesinmasked() { // updated for version 1.0.216
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    int stflipflop = 0;
    sinindex = sinstart;
    sindir = sinstartdir;
    sinvalue = sin_table[sinindex];
    if (sindir == 0) {
        sinindex = sinindex + sinfreq;
        if (sinindex == 90) {
            sindir = 1;
        }
    } else {
        sinindex = sinindex - sinfreq;
        if (sinindex == 0) {
            sindir = 0;
        }
    } // sindir
    sinvalue = sinvalue >> sinamp; // max 8 for value 11
    sinstart = sinindex;
    sinstartdir = sindir;
    while (text[tchar] != 0) {
        tx = tx + 2;
        tindex = (text[tchar] - 0x20) * 5;
        tcnt = 0;
        while (tcnt < 5) {
            stflipflop = 0;
            while (stflipflop < 2) {
                sinvalue = sin_table[sinindex];
                if (sindir == 0) {
                    sinindex = sinindex + sinfreq;
                    if (sinindex == 90) {
                        sindir = 1;
                    }
                } else {
                    sinindex = sinindex - sinfreq;
                    if (sinindex == 0) {
                        sindir = 0;
                    }
                } // sindir
                x = tx;
                sinvalue = sinvalue >> sinamp; // max 8
                int bitcnt = 14;
                while (bitcnt > 0) {
                    bitcnt = bitcnt - 2;
                    if ((charmap[tindex + tcnt] & (0b00000001 << (bitcnt / 2))) != 0) {
                        y = sinvalue + (ty + bitcnt);
                        flippixel();
                        y++;
                        flippixel();
                    } // charmap
                } // bitcnt
                tx++;
                stflipflop++;
            } // stflipflop
            tcnt++;
        } // tcnt
        tchar++;
    }
}
//
// draw text string of up to 20 characters horizontally centered
// on the display at the set y coordinate in variable ty.

void drawtextcentered() { // update for version 1.0.116
    int txlen = 0;
    while (text[txlen] != 0) {
        txlen++;
    }
    txlen = txlen * 3; // convert half length to pixels
    tx = 64 - txlen;
    drawtext();
}
//
// draw variable width text string horizontally centered

void drawtextvarwidthcentered() { // update for version 1.0.212
    int txlen = 0;
    txlen = measuretextvarwidth();
    tx = 64 - (txlen / 2);
    if (tx < 2) {
        tx = 2;
    }
    drawtextvarwidth();
}
//
// draw variable width text string centered at input position

void drawtextvarwidthcenteredpos(int center) { // update for version 1.0.206
    int txlen = 0;
    txlen = measuretextvarwidth();
    tx = center - (txlen / 2);
    drawtextvarwidth();
}
//
// measure a variable width text string in text buffer

int measuretextvarwidth() { // update for version 1.0.210
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    int txstart = tx;
    while (text[tchar] != 0) {
        tindex = (text[tchar] - 0x20) * 7;
        tcnt = 0;
        if (charmapb[tindex] == 0) {
            tcnt++;
            if (charmapb[tindex + 1] == 0) {
                tcnt++;
                if (charmapb[tindex + 2] == 0 && text[tchar] != 0x20) {
                    tcnt++;
                }
            }
        } // charmapb
        while (tcnt < 7) {
            tx++;
            tcnt++;
        }
        if (charmapb[tindex + 6] == 0) {
            tx--;
            if (charmapb[tindex + 5] == 0) {
                tx--;
                if (charmapb[tindex + 4] == 0) {
                    tx--;
                }
            }
        } // charmapb
        tchar++;
        tx++;
    }
    tx--;
    return (tx - txstart); // return string width
}
//
// draw variable width text to framebuffer

void drawtextvarwidth() { // update for version 1.0.210
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    while (text[tchar] != 0) {
        tindex = (text[tchar] - 0x20) * 7;
        tcnt = 0;
        if (charmapb[tindex] == 0) {
            tcnt++;
            if (charmapb[tindex + 1] == 0) {
                tcnt++;
                if (charmapb[tindex + 2] == 0 && text[tchar] != 0x20) {
                    tcnt++;
                }
            }
        } // charmapb
        while (tcnt < 7) {
            x = tx;
            int bitcnt = 8; // support 8 pixel high characters
            while (bitcnt > 0) {
                bitcnt--;
                if ((charmapb[tindex + tcnt] & (0b00000001 << bitcnt)) != 0) {
                    y = ty + bitcnt;
                    setpixel();
                }
            } // bitcnt
            tx++;
            tcnt++;
        } // tcnt
        if (charmapb[tindex + 6] == 0) {
            tx--;
            if (charmapb[tindex + 5] == 0) {
                tx--;
                if (charmapb[tindex + 4] == 0) {
                    tx--;
                }
            }
        } // charmapb
        tchar++;
        tx++;
    }
}
//
// draw one large bold numeric digit to framebuffer

void drawtextbolddigit() { // updated for version 1.0.216
    int tcnt = 0;
    int tindex = 0;
    unsigned char looptwice;
    tx++;
    tindex = (text[0] - 0x20) * 7;
    if (text[0] == 0x54 || text[0] == 0x5A) {
        tindex--;
    } // offset "T" or "Z"
    tcnt = 0;
    while (tcnt < 7) {
        looptwice = 0;
        while (looptwice < 2) {
            x = tx;
            int bitcnt = 14; // support scaled 7 pixel high characters
            while (bitcnt > 0) {
                bitcnt = bitcnt - 2;
                if ((charmapb[tindex + tcnt] & (0b00000001 << (bitcnt / 2))) != 0) {
                    y = ty + bitcnt;
                    setpixel();
                    y++;
                    setpixel();
                } // charmap
            } // bitcnt
            tx++;
            looptwice++;
        } // looptwice
        tcnt++;
    } // tcnt
}
//
// draw small grey text on a dynamic display

void drawtextgrey() { // updated for version 1.0.187
    int tcnt = 0;
    int tindex = 0;
    int tchar = 0;
    int incntt = 0;
    while (text[tchar] != 0) {
        tx++;
        tindex = (text[tchar] - 0x20) * 5;
        tcnt = 0;
        while (tcnt < 5) {
            x = tx;
            incntt = 7;
            while (incntt > 0) {
                incntt--;
                if ((charmap[tindex + tcnt] & (0b00000001 << incntt)) != 0) {
                    y = (ty + incntt);
                    togpixel();
                }
            } // incntt
            tx++;
            tcnt++;
        } // tcnt
        tchar++;
    }
}
//
// private function for grey text function
// sets the  pixel only every second frame

void togpixel() { // update for version 1.0.74
    if (pff == 0) {
        setpixel();
        pff = 1;
    } else {
        pff = 0;
    }
}
//
// private function for the drawlineimage function

void togpixelimage() { // update for version 1.0.184
    if (pff == 0) {
        setpixelimage();
        pff = 1;
    } else {
        pff = 0;
    }
}
//
// Bresenham's line drawing algorithm for framebuffer

void drawline(int bx1, int by1, int bx2, int by2) { // update for version 1.0.173
    int bx, by, bdx, bdy, bdx1, bdy1, bpx, bpy, bxe, bye, bi;
    bdx = bx2 - bx1;
    bdy = by2 - by1;
    bdx1 = fabs(bdx);
    bdy1 = fabs(bdy);
    bpx = 2 * bdy1 - bdx1;
    bpy = 2 * bdx1 - bdy1;
    if (bdy1 <= bdx1) {
        if (bdx >= 0) {
            bx = bx1;
            by = by1;
            bxe = bx2;
        } else {
            bx = bx2;
            by = by2;
            bxe = bx1;
        }
        x = bx;
        y = by;
        setpixel();
        for (bi = 0; bx < bxe; bi++) {
            bx = bx + 1;
            if (bpx < 0) {
                bpx = bpx + 2 * bdy1;
            } else {
                if ((bdx < 0 && bdy < 0) || (bdx > 0 && bdy > 0)) {
                    by++;
                } else {
                    by--;
                }
                bpx = bpx + 2 * (bdy1 - bdx1);
            }
            x = bx;
            y = by;
            setpixel();
        }
    } else {
        if (bdy >= 0) {
            bx = bx1;
            by = by1;
            bye = by2;
        } else {
            bx = bx2;
            by = by2;
            bye = by1;
        }
        x = bx;
        y = by;
        setpixel();
        for (bi = 0; by < bye; bi++) {
            by = by + 1;
            if (bpy <= 0) {
                bpy = bpy + 2 * bdx1;
            } else {
                if ((bdx < 0 && bdy < 0) || (bdx > 0 && bdy > 0)) {
                    bx++;
                } else {
                    bx--;
                }
                bpy = bpy + 2 * (bdx1 - bdy1);
            }
            x = bx;
            y = by;
            setpixel();
        }
    }
}
//
// Bresenham's line drawing algorithm for imagebuffer
// with an attribute input for solid or dotted line

void drawlineimage(int bx1, int by1, int bx2, int by2, int dot) { // update for version 1.0.184
    int bx, by, bdx, bdy, bdx1, bdy1, bpx, bpy, bxe, bye, bi;
    bdx = bx2 - bx1;
    bdy = by2 - by1;
    bdx1 = fabs(bdx);
    bdy1 = fabs(bdy);
    bpx = 2 * bdy1 - bdx1;
    bpy = 2 * bdx1 - bdy1;
    if (bdy1 <= bdx1) {
        if (bdx >= 0) {
            bx = bx1;
            by = by1;
            bxe = bx2;
        } else {
            bx = bx2;
            by = by2;
            bxe = bx1;
        }
        x = bx;
        y = by;
        if (dot == 0) {
            setpixelimage();
        } else {
            togpixelimage();
        } // dot
        for (bi = 0; bx < bxe; bi++) {
            bx = bx + 1;
            if (bpx < 0) {
                bpx = bpx + 2 * bdy1;
            } else {
                if ((bdx < 0 && bdy < 0) || (bdx > 0 && bdy > 0)) {
                    by++;
                } else {
                    by--;
                }
                bpx = bpx + 2 * (bdy1 - bdx1);
            }
            x = bx;
            y = by;
            if (dot == 0) {
                setpixelimage();
            } else {
                togpixelimage();
            } // dot
        }
    } else {
        if (bdy >= 0) {
            bx = bx1;
            by = by1;
            bye = by2;
        } else {
            bx = bx2;
            by = by2;
            bye = by1;
        }
        x = bx;
        y = by;
        if (dot == 0) {
            setpixelimage();
        } else {
            togpixelimage();
        } // dot
        for (bi = 0; by < bye; bi++) {
            by = by + 1;
            if (bpy <= 0) {
                bpy = bpy + 2 * bdx1;
            } else {
                if ((bdx < 0 && bdy < 0) || (bdx > 0 && bdy > 0)) {
                    bx++;
                } else {
                    bx--;
                }
                bpy = bpy + 2 * (bdx1 - bdy1);
            }
            x = bx;
            y = by;
            if (dot == 0) {
                setpixelimage();
            } else {
                togpixelimage();
            } // dot
        }
    }
}
//
// draw the mouse pointer to frame buffer

void drawpointer() { // update for version 1.0.132
    int screeny = 0;
    int pointery = 0;
    while (pointery < 22) {
        unsigned char pmask = 0b10000000;
        unsigned char pyoff = 0;
        unsigned char innercnt = 0;
        y = my + screeny;
        while (innercnt < 11) {
            if ((mpointer[pointery + pyoff] & pmask) != 0) {
                x = mx + innercnt;
                setpixel();
            }
            innercnt++;
            pmask = pmask >> 1;
            if (pmask == 0) {
                pmask = 0b10000000;
                pyoff = 1;
            }
        } // innercnt
        pointery = pointery + 2;
        screeny++;
    } // pointery
}
//

void shiftbit(int srbit) { // update for version 1.0.182
    SRCLK_PIN = 0;
    delay_gen(0); //
    if (srbit == 0) {
        SERDAT_PIN = 0;
    } else {
        SERDAT_PIN = 1;
    } // srbit
    delay_gen(0); //
    SRCLK_PIN = 1;
    delay_gen(0); //
}
//

void shiftbyte() { // update for version 1.0.182
    unsigned char srbuff = 0;
    int srcnt = 0;
    for (srcnt = 0; srcnt < 8; ++srcnt) {
        srbuff = srbyte << srcnt;
        srbuff = srbuff & 0x80;
        shiftbit(srbuff);
    } // srcnt
}
//
// load the shift register data from joystick

void shiftbytein() { // update for version 1.0.133
    SRCLK_PIN = 0;
    delay_gen(1); //
    JOYPAR_PIN = 1;
    delay_gen(1); //
    JOYPAR_PIN = 0;
    delay_gen(1); // load parallel data
    JOYPAR_PIN = 1;
    delay_gen(1); //
    joyleft = JOYDAT_PIN;
    sbprivate();
    joyright = JOYDAT_PIN;
    sbprivate();
    joyup = JOYDAT_PIN;
    sbprivate();
    joydown = JOYDAT_PIN;
    sbprivate();
    joyfire = JOYDAT_PIN;
    sbprivate();
    joysparec = JOYDAT_PIN;
    sbprivate();
    joyspareb = JOYDAT_PIN;
    sbprivate();
    // unused pin
    SRCLK_PIN = 0;
    delay_gen(1); //
    JOYPAR_PIN = 1; //
    if (joyfire == 1) {
        joyfire = 0;
    } else {
        joyfire = 1;
    }
    if (joyup == 1) {
        joyup = 0;
    } else {
        joyup = 1;
    }
    if (joydown == 1) {
        joydown = 0;
    } else {
        joydown = 1;
    }
    if (joyleft == 1) {
        joyleft = 0;
    } else {
        joyleft = 1;
    }
    if (joyright == 1) {
        joyright = 0;
    } else {
        joyright = 1;
    }
}
//
// private function for shift byte in

void sbprivate() { // update for version 1.0.106
    delay_gen(0);
    SRCLK_PIN = 0;
    delay_gen(0);
    SRCLK_PIN = 1;
}
//
// translate joystick input bits to movement

void joystickaccelerate() { // update for version 1.0.202
    if (joyup == 1) {
        mmy = mmy - jheld;
    }
    if (joydown == 1) {
        mmy = mmy + jheld;
    }
    if (joyleft == 1) {
        mmx = mmx - jheld;
    }
    if (joyright == 1) {
        mmx = mmx + jheld;
    }
}
//
// convert a float to four bytes

void floattobytes() {
    unsigned char *pbyte = (unsigned char *) &afloat;
    int findx = 0;
    for (findx = 0; findx<sizeof (float); findx++) {
        bytes[findx] = *pbyte;
        pbyte++;
    } // findx
}
//
// convert four bytes to a float

float Float(char unsigned *const p) {
    float val;
    memcpy(&val, p, sizeof val);
    return val;
}
//
// currently, the SD Card is all hardware spi,
// and vs1003b communication uses software spi.
// vs1003b receives slow spi until it's clock is set.

void WriteSPIManual(unsigned char data_out) { // updated for version 1.0.70
    unsigned char i = data_out;
    int spirotcnt = 0;
    SPICLOCKLAT = 0;
    while (spirotcnt < 8) {
        SPICLOCKLAT = 0;
        if (i & 0x80) {
            SPIOUTLAT = 1;
        } else {
            SPIOUTLAT = 0;
        }
        delay_gen(0);
        SPICLOCKLAT = 1;
        delay_gen(0);
        spirotcnt++;
        i = i << 1;
    } // spirotcnt
    SPICLOCKLAT = 0;
    SPIOUTLAT = 0;
}
//
// see comments in writespimanual function

unsigned char ReadSPIManual() { // updated for version 1.0.70
    unsigned char result = 0x00;
    int wspirotcnt = 0;
    SPICLOCKLAT = 0;
    while (wspirotcnt < 8) {
        result = result << 1;
        SPICLOCKLAT = 0;
        delay_gen(0);
        SPICLOCKLAT = 1;
        delay_gen(0);
        if (SPIINPORT == 1) {
            result |= 0x01;
        }
        wspirotcnt++;
    } // wspirotcnt
    SPICLOCKLAT = 0;
    return result;
}
//

void WriteSPIFast(unsigned char data_out) {
    // currently, the SD Card is all hardware spi,
    // and vs1003b communication uses this faster software spi
    // for mp3 files after the vs1003b clock has been set.
    char i = data_out; // update for version 1.0.77
    SPICLOCKLAT = 0;
    SPIOUTLAT = i >> 7;
    SPICLOCKLAT = 1;
    SPICLOCKLAT = 0;
    SPIOUTLAT = i >> 6;
    SPICLOCKLAT = 1;
    SPICLOCKLAT = 0;
    SPIOUTLAT = i >> 5;
    SPICLOCKLAT = 1;
    SPICLOCKLAT = 0;
    SPIOUTLAT = i >> 4;
    SPICLOCKLAT = 1;
    SPICLOCKLAT = 0;
    SPIOUTLAT = i >> 3;
    SPICLOCKLAT = 1;
    SPICLOCKLAT = 0;
    SPIOUTLAT = i >> 2;
    SPICLOCKLAT = 1;
    SPICLOCKLAT = 0;
    SPIOUTLAT = i >> 1;
    SPICLOCKLAT = 1;
    SPICLOCKLAT = 0;
    SPIOUTLAT = i;
    SPICLOCKLAT = 1;
    SPICLOCKLAT = 0;
    SPIOUTLAT = 0;
}
//
// send the framebuffer memory to the
// display via serial shift register

void drawscreen() {
    // display frame counter or error status
    if (serialenabled > 3) {
        framecounter();
    } // update for version 1.0.199
    int charindex = 0;
    int linecnt = 0; // update for version 1.0.201
    int n = 0; // update for version 1.0.201
    int line = 0xB8; // update for version 1.0.201
    while (n < 512) {
        if (linecnt == 0) {
            LCD_CS2 = 0;
            LCD_CS1 = 1;
            LCD_DI = 1;
            LCD_EN = 0;
            srbyte = line;
            shiftbyte();
            delay_lcd();
            LCD_EN = 1;
            delay_lcd();
        }
        if (linecnt == 64) {
            LCD_CS2 = 1;
            LCD_CS1 = 0;
            LCD_DI = 1;
            LCD_EN = 0;
            srbyte = line;
            shiftbyte();
            delay_lcd();
            LCD_EN = 1;
            delay_lcd();
            line++;
        }
        if (linecnt < 64) {
            LCD_CS2 = 0;
            LCD_CS1 = 1;
        } else {
            LCD_CS2 = 1;
            LCD_CS1 = 0;
        } // linecnt
        LCD_DI = 0;
        LCD_EN = 0;
        srbyte = framebuffer[charindex];
        shiftbyte();
        delay_lcd();
        LCD_EN = 1;
        charindex++;
        n++;
        linecnt++;
        if (linecnt > 127) {
            linecnt = 0;
        }
    }
    charindex = 512;
    linecnt = 0;
    n = 512;
    while (n < 1024) {
        if (linecnt == 0) {
            srbyte = line;
            shiftbyte();
            LCD_CS2 = 0;
            LCD_CS1 = 1;
            LCD_DI = 1;
            LCD_EN = 0;
            delay_lcd();
            LCD_EN = 1;
            delay_lcd();
        }
        if (linecnt == 64) {
            srbyte = line;
            shiftbyte();
            LCD_CS2 = 1;
            LCD_CS1 = 0;
            LCD_DI = 1;
            LCD_EN = 0;
            delay_lcd();
            LCD_EN = 1;
            delay_lcd();
            line++;
        }
        if (linecnt < 64) {
            srbyte = framebuffer[charindex]; //
            shiftbyte(); //
            LCD_CS2 = 0;
            LCD_CS1 = 1;
        } else {
            srbyte = framebuffer[charindex]; //
            shiftbyte(); //
            LCD_CS2 = 1;
            LCD_CS1 = 0;
        } // linecnt
        LCD_DI = 0;
        LCD_EN = 0;
        delay_lcd();
        LCD_EN = 1;
        charindex++;
        n++;
        linecnt++;
        if (linecnt > 127) {
            linecnt = 0;
        }
    }
}
//
// cache the appropriate range of file names

void recachefiles(int filenumber) {
    int xfilecount = 0;
    int resultf = 0;
    int fstart = 0;
    voice[7049] = 0;
    // range already set to zero for range 0-880
    int iteraterange = 1;
    unsigned int newrangeend = 881;
    while (newrangeend < 32500) {
        if (filenumber > newrangeend - 1 && filenumber < newrangeend + 881) {
            voice[7049] = iteraterange;
            fstart = newrangeend;
        }
        newrangeend = newrangeend + 881;
        iteraterange++;
    } // newrangeend
    // should have start position in fstart and range now
    // theoretical limit is over 32000 files
    resultf = FindFirst(fileextension, 0x2F, &rec);
    if (resultf == 0) {
        if (fstart == 0) {
            cachefilename(0);
        }
        xfilecount++;
    } // resultf
    // set up activity graphic variables
    text[121] = 0; // borrowed variable to set direction
    text[120] = 54; // borrowed variable to set position
    text[122] = 0; // borrowed variable to set update frequency
    //
    while (resultf == 0 && xfilecount < fstart + 881) {
        resultf = FindNext(&rec);
        if (resultf == 0) {
            if (xfilecount > fstart - 1 && xfilecount < fstart + 881) {
                cachefilename(xfilecount - fstart);
            }
            xfilecount++;
            text[122]++; // update activity graphic
            if (text[122] > 49) {
                text[122] = 0;
                drawsearchactivity();
            }
        } // resultf
    } // resultf
}
//
// count total files and cache file names

void count_files() {
    filecount = 0;
    int resultf = 0;
    resultf = FindFirst(fileextension, 0x2F, &rec);
    if (resultf == 0) {
        voice[7049] = 0;
        cachefilename(filecount);
        filecount++;
    } else { // no files found
        nofileserror();
        framecount = 19;
        return;
    }
    //
    // set up activity graphic variables
    text[121] = 0; // borrowed variable to set direction
    text[120] = 54; // borrowed variable to set position
    text[122] = 0; // borrowed variable to set update frequency
    //
    while (resultf == 0) {
        resultf = FindNext(&rec);
        if (resultf == 0) {
            cachefilename(filecount);
            filecount++;
        }
        //
        text[122]++; // update activity graphic
        if (text[122] > 49) {
            text[122] = 0;
            drawsearchactivity();
        }
        //
        // maximum files allowed to be counted is 32000
        if (filecount > 32000) {
            filecount = 32000;
            resultf = 1;
        } // update for version 1.0.49
        //
        // no more than 3500 files in shuffle mode for mp3 player
        if (mpshuffle != 0 && demo == 3) {
            if (filecount > 3500) {
                filecount = 3500;
                resultf = 1;
            } // update for version 1.0.53
        } // mpshuffle
        //
    } // resultf
}
//
// get a file number by searching card

void get_file() {
    int resultf = 0;
    int hitcount = 0;
    // change for 1.0.32 shuffle feature
    int retrieved = fnumber;
    if (mpshuffle != 0 && demo == 3) {
        retrieved = retrieveshuffle(fnumber);
        drawmpscreen(retrieved);
    } else { // mpshuffle
        if (demo == 3) {
            drawmpscreen(0);
        }
    }// mpshuffle
    //
    resultf = FindFirst(fileextension, 0x2F, &rec);
    if (hitcount == retrieved) {
        resultf = 1;
    }
    if (resultf == 0) {
        hitcount++;
    }
    while (resultf == 0) {
        resultf = FindNext(&rec);
        if (hitcount == retrieved) {
            resultf = 1;
        }
        if (resultf == 0) {
            hitcount++;
        }
    } // resultf
    filesize = rec.filesize;
}
//
// MDDFS File Browser V2 - Brek Martin 2017
// caches 881 file names at a time and displays from memory
// this is fast but overwrites the large array while in use

void file_browserv2() { // cached browser for version 1.0.37
    unsigned char holdflag = 1;
    unsigned char pressvalid = 0;
    int updatescreen = 1;
    int clrfb = 0;
    int resultf = 0;
    int fline = 1;
    int rangestart = 0;
    int rangeend = 0;
    int ipreview = 0;
    fnumber = 0;
    filecount = -1;
    int fstart = 0;
    //
    count_files(); // count all files in directory that match criteria
    if (filecount == 0) {
        return;
    }
    if (skipbrowser == 1 && demo == 3) { // skip file browser
        if (mpshuffle != 0) {
            mpfileshuffle();
        } // run track shuffle function 
        get_file();
        if (fileextension[4] == 0x33) { // mp3 file extension
            eyedeethree();
            serialeyedeethree();
        } // fileextension
        return;
    } // skip file browser
    //
    clrimagebuffer();
    while (holdflag == 1) { // main screen loop
        clrframebuffer();
        if (updatescreen == 1) { // print range of files
            fline = 1;
            fnumber = fstart - 1;
            while (fline < 56 && fnumber < filecount) { // updated for version 1.0.54
                retrievefilename(fnumber - rangestart);
                fnumber++;
                if (fnumber > fstart) {
                    ty = fline;
                    tx = 85;
                    sprintf(text, "-%04d", fnumber);
                    drawtextimage();
                    sprintf(text, rec.filename);
                    tx = 2;
                    drawtextimage();
                    fline = fline + 9;
                } // fnumber
                updatescreen = 0;
            } // fline
        } // updatescreen

        clrfb = 0;
        while (clrfb < 1024) { // copy image buffer to frame buffer
            framebuffer[clrfb] = imagebuffer[clrfb];
            clrfb++;
        } // clrfb
        drawrectangle(124, 0, 3, 63); // draw border for scroll bar
        if (filecount > 7) {
            float fbpages = 0.0;
            float pixelshigh = 0.0;
            float fbrange = 0.0;
            float fbposition = 0.0;
            fbpages = filecount / 7; // calculate number of screen pages
            pixelshigh = 62 / fbpages; // calculate height of bar sprite
            fbrange = 62 - pixelshigh; // calculate range of sprite motion
            fbposition = fbrange / (filecount - 7); // calculate bar position
            fbposition = fbposition * (fnumber - 7); //
            if (pixelshigh < 1.0) {
                pixelshigh = 1.0;
            } // at least two pixels high
            drawline(125, fbposition, 125, fbposition + pixelshigh); // draw bar
            drawline(126, fbposition, 126, fbposition + pixelshigh); //
            //
            // draw scroll bar preview text box
            // update for version 1.0.49
            if (mmx > 123) { // pointer is on scroll bar
                if (mmy < 0.0) {
                    mmy = 0.0;
                }
                if (mmy > 63.0) {
                    mmy = 63.0;
                }
                fbposition = mmy - 7;
                if (fbposition < 2.0) {
                    fbposition = 2.0;
                }
                if (fbposition > 48.0) {
                    fbposition = 48.0;
                }
                float fpreview = filecount / 63.0;
                fpreview = fpreview * mmy;
                ipreview = fpreview;
                if (ipreview > filecount - 1) {
                    ipreview = filecount - 1;
                }
                if (ipreview + 1 > 9999) { // update for version 1.0.52
                    drawrectanglefilledclear(61, fbposition - 2, 61, 17);
                    sprintf(text, "%05d", ipreview + 1);
                    tx = 61;
                } else {
                    drawrectanglefilledclear(73, fbposition - 2, 49, 17);
                    sprintf(text, "%04d", ipreview + 1);
                    tx = 73;
                } // ipreview
                ty = fbposition;
                drawtextlarge();
                drawline(125, mmy - 1, 126, mmy - 1); // indicator bar
                drawline(125, mmy, 126, mmy);
                drawline(125, mmy + 1, 126, mmy + 1);
            } // mmx
        } // filecount
        drawpointer();
        x = 124;
        y = 0;
        clearpixel(); // mask rounded corners
        y = 63;
        clearpixel(); // of the scroll bar
        x = 127;
        y = 0;
        clearpixel();
        y = 63;
        clearpixel();
        // highlight currently selected file
        unsigned char blockcnt = 63; // update for version 1.0.82
        unsigned char hilightcnt = 0;
        if (mx < 124) { // pointer is not on the scroll bar
            while (blockcnt > my) {
                blockcnt = blockcnt - 9;
            }
            if (blockcnt / 9 < filecount || (my == 63 && fstart == filecount - 7)) { // update for version 1.0.91     
                blockcnt = 0;
                while (my >= blockcnt && blockcnt < 63) {
                    blockcnt = blockcnt + 9;
                }
                if (my == 63 && fstart == filecount - 7) {
                    blockcnt = 63;
                } // update for version 1.0.91
                while (hilightcnt < 9) {
                    x = 0;
                    blockcnt--;
                    hilightcnt++;
                    while (x < 118) {
                        y = blockcnt;
                        flippixel();
                        x++;
                    }
                } // hilightcnt
                x = 0;
                y = blockcnt;
                flippixel(); // mask rounded corners
                y = y + 8;
                flippixel(); // of the highlight box
                x = 117;
                y = blockcnt;
                flippixel(); //
                y = y + 8;
                flippixel(); //
            } // blockcnt
        } // mx

        drawscreen();
        shiftbytein();
        joystickaccelerate(); // update for version 1.0.202
        if (joyup + joydown + joyleft + joyright == 0) {
            jheld = 1.0;
        } else {
            jheld = jheld + 0.25;
        } // acceleration
        if (mmx > 127.0) {
            mmx = 127.0;
            jheld = 1.0;
        } // check mouse boundaries
        if (mmx < 0.0) {
            mmx = 0.0;
            jheld = 1.0;
        }
        //
        if (mmy > 63.0 && mmx < 124) {
            if (jheld > 1.0 && fnumber < filecount) {
                rangestart = getstartofrange();
                rangeend = getstartofrange() + 880;
                if (fnumber > rangeend) {
                    drawcountmpgraphic(40);
                    recachefiles(fnumber);
                    rangestart = getstartofrange();
                    fstart = fstart + 6; // jump new range to top
                } // fnumber
                fstart++;
                clrimagebuffer();
                clickframe();
                updatescreen = 1;
            }
            mmy = 63.0;
            jheld = 1.0;
        }
        //
        if (mmy < 0.0 && mmx < 124) {
            if (jheld > 1.0 && fstart > 0) {
                rangestart = getstartofrange();
                rangeend = getstartofrange() + 880;
                if (fnumber - 8 < rangestart) {
                    drawcountmpgraphic(40);
                    recachefiles(fnumber - 8);
                    rangestart = getstartofrange();
                    fstart = fstart - 6; // jump new range to bottom
                } // fnumber
                fstart--;
                clrimagebuffer();
                clickframe();
                updatescreen = 1;
            }
            mmy = 0.0;
            jheld = 1.0;
        }
        //
        mx = mmx;
        my = mmy; // convert to integer
        //
        if (joyfire == 1) {
            if (lastpressed == 0) { // check last state for debounce
                pressvalid = 0;
                unsigned char fileline = 6;
                unsigned char myfline = 64;
                if (my == 63) {
                    pressvalid = 1;
                    fnumber--;
                } // update for version 1.0.77
                while (myfline > 9) {
                    if (fnumber > fileline) {
                        if (my + 1 < myfline) {
                            fnumber--;
                            pressvalid = 1;
                        }
                    }
                    fileline--;
                    myfline = myfline - 9;
                } // fileline
                //
                // update for version 1.0.49
                if (mmx > 123) { // button press was on scroll bar
                    playtinydirect();
                    fnumber = ipreview;
                    if (fnumber > filecount - 7) {
                        fnumber = filecount - 7;
                    }
                    fstart = fnumber;
                    rangestart = getstartofrange();
                    rangeend = getstartofrange() + 880;
                    if (fnumber > rangeend || fnumber - 8 < rangestart) {
                        drawcountmpgraphic(40);
                        recachefiles(fnumber);
                        rangestart = getstartofrange();
                    } // fnumber
                    clrimagebuffer();
                    updatescreen = 1;
                    pressvalid = 0; // invalidate press for file selector
                } // mmx
                //
                if (pressvalid == 1) {
                    playtinydirect();
                    retrievefilename(fnumber - rangestart); // retrieve file name conventionally
                    tx = 2;
                    ty = blockcnt + 1; // update for version 1.0.97
                    readtextimage(0); // read file name text from image
                    clrfb = 0; // compare the two filenames
                    while (rec.filename[clrfb] != 0) { // they should be identical
                        if (rec.filename[clrfb] != text[clrfb]) {
                            debugout(12);
                        }
                        clrfb++;
                    }
                    clrfb = 0; // currently using the image decoded name
                    while (text[clrfb] != 0) { // copy to file record structure
                        rec.filename[clrfb] = text[clrfb];
                        clrfb++;
                    } // clrfb
                    rec.filename[clrfb] = 0; // terminate file name string
                    fbtransition(blockcnt); // update for version 1.0.75
                    if (demo == 3) {
                        drawmpscreen(0);
                    }
                    rec.entry = fnumber;
                    FindFirst(rec.filename, 0x2F, &rec); // update for version 1.0.176
                    filesize = rec.filesize; // update for version 1.0.172
                    if (fileextension[4] == 0x33) { // mp3 file extension
                        eyedeethree();
                        serialeyedeethree();
                    } // fileextension
                    resultf = 1;
                    holdflag = 0;
                } // pressvalid
            }
            lastpressed = 1;
        } else {
            lastpressed = 0;
        }
    } // resultf
}
//
// ID3 tag extractor for various types of MP3 file sources.
// function should be able to parse at least ID3, MusicMatch Jukebox ID3,
// and iTunes exported MP3 ID tags without the need for ID3 conversion.
// Limited Unicode support tested with MP3 files extracted with iTunes.

void eyedeethree() {
    int index = 0;
    int subindex = 0;
    int textsize = 0;
    char gotinfo = 0;

    pointer = FSfopen(rec.filename, "r"); // open file for reading
    FSfread(receivebuffer, 1, 3, pointer); // check header
    iseyedeethree = 1;
    if (receivebuffer[0] != 0x49) {
        iseyedeethree = 0;
    }
    if (receivebuffer[1] != 0x44) {
        iseyedeethree = 0;
    }
    if (receivebuffer[2] != 0x33) {
        iseyedeethree = 0;
    }
    if (iseyedeethree == 0) { // no modern id3 tag
        eyedeeveeone(); // check for version one id3 tag
        return;
    } // no id3 tag

    FSfread(imagebuffer, 1, 1024, pointer); // read in 1k chunk
    for (index = 0; index < 1024; index++) { // search chunk for artist    
        if (imagebuffer[index + 0] == 0x54) { // "T"
            if (imagebuffer[index + 1] == 0x50) { // "P"
                if (imagebuffer[index + 2] == 0x45) { // "E"
                    if (imagebuffer[index + 3] == 0x31) { // "1"    
                        textsize = imagebuffer[index + 7] - 1;
                        if (textsize > 60) {
                            textsize = 60;
                        } // limit size
                        idlooplimit = 0; // update for version 1.0.112
                        for (subindex = 0; subindex < textsize; subindex++) {
                            subindex = idtextfilter(subindex, index, 0, 11 + subindex);
                            if (idlooplimit > 250) {
                                gotinfo = gotinfo - 2;
                                subindex = textsize;
                                debugout(14);
                            }
                        } // textsize
                        subindex = idtexthyphenator(subindex);
                        index = 1024;
                        gotinfo++;
                    }
                }
            }
        } // imagebuffer
    } // index

    for (index = 0; index < 1024; index++) { // search chunk for title
        if (imagebuffer[index + 0] == 0x54) { // "T"
            if (imagebuffer[index + 1] == 0x49) { // "I"
                if (imagebuffer[index + 2] == 0x54) { // "T"
                    if (imagebuffer[index + 3] == 0x32) { // "2"
                        textsize = imagebuffer[index + 7] - 1;
                        if (textsize > 60) {
                            textsize = 60;
                        } // limit size
                        idlooplimit = 0; // update for version 1.0.112
                        for (n = 0; n < textsize; n++) {
                            subindex = idtextfilter(subindex, index, 1, 11 + n);
                            if (idlooplimit > 250) {
                                gotinfo = gotinfo - 2;
                                n = textsize;
                                debugout(14);
                            }
                        } // textsize
                        index = 1024;
                        gotinfo++;
                    }
                }
            }
        } // imagebuffer
    } // index

    if (gotinfo > 0) {
        eyedeethreelen = subindex;
        eyedeephix();
        idtext[eyedeethreelen] = 0; // terminate string
        iseyedeethree = 1;
        return;
    } // gotinfo

    // fruit company artist non unicode version
    for (index = 0; index < 1024; index++) { // search chunk for artist
        if (imagebuffer[index + 0] == 0x54) { // "T"
            if (imagebuffer[index + 1] == 0x50) { // "P"
                if (imagebuffer[index + 2] == 0x31) { // "1"
                    if (imagebuffer[index + 7] != 0xFF) { // no unicode
                        textsize = imagebuffer[index + 5] - 2;
                        if (textsize > 60) {
                            textsize = 60;
                        } // limit size
                        for (subindex = 0; subindex < textsize; subindex++) {
                            subindex = idtextfilter(subindex, index, 0, 7 + subindex);
                        } // textsize
                        subindex = idtexthyphenator(subindex);
                        index = 1024;
                        gotinfo++;
                    }
                }
            }
        } // imagebuffer
    } // index

    textsize = eyedeeunicodeartist(); // check for fruit company artist unicode field
    if (textsize != 0) {
        gotinfo++;
        subindex = textsize;
    }
    textsize = eyedeeunicodetitle(subindex); // check for fruit company title unicode field
    if (textsize != 0) {
        gotinfo++;
        subindex = subindex + textsize;
    }

    // fruit company title non unicode version
    for (index = 0; index < 1024; index++) { // search chunk for title
        if (imagebuffer[index + 0] == 0x54) { // "T"
            if (imagebuffer[index + 1] == 0x54) { // "T"
                if (imagebuffer[index + 2] == 0x32) { // "2"
                    if (imagebuffer[index + 7] != 0xFF) { // not unicode
                        textsize = imagebuffer[index + 5] - 2;
                        if (textsize > 60) {
                            textsize = 60;
                        } // limit size
                        for (n = 0; n < textsize; n++) {
                            subindex = idtextfilter(subindex, index, 1, 7 + n);
                        }
                        index = 1024;
                        gotinfo++;
                    }
                }
            }
        } // imagebuffer
    } // index

    if (gotinfo > 0) {
        eyedeethreelen = subindex;
        eyedeephix();
        idtext[eyedeethreelen] = 0; // terminate string
        iseyedeethree = 1;
        return;
    } // gotinfo

    // try it all again
    FSfread(imagebuffer, 1, 1024, pointer); // read in 1k chunk
    for (index = 0; index < 1024; index++) { // search chunk for artist
        if (imagebuffer[index + 0] == 0x54) { // "T"
            if (imagebuffer[index + 1] == 0x50) { // "P"
                if (imagebuffer[index + 2] == 0x45) { // "E"
                    if (imagebuffer[index + 3] == 0x31) { // "1"
                        textsize = imagebuffer[index + 7] - 1;
                        if (textsize > 60) {
                            textsize = 60;
                        } // limit size
                        idlooplimit = 0; // update for version 1.0.112
                        for (subindex = 0; subindex < textsize; subindex++) {
                            subindex = idtextfilter(subindex, index, 0, 11 + subindex);
                            if (idlooplimit > 250) {
                                gotinfo = gotinfo - 2;
                                subindex = textsize;
                                debugout(15);
                            }
                        } // textsize
                        subindex = idtexthyphenator(subindex);
                        index = 1024;
                        gotinfo++;
                    }
                }
            }
        } // imagebuffer
    } // index

    for (index = 0; index < 1024; index++) { // search chunk for title
        if (imagebuffer[index + 0] == 0x54) { // "T"
            if (imagebuffer[index + 1] == 0x49) { // "I"
                if (imagebuffer[index + 2] == 0x54) { // "T"
                    if (imagebuffer[index + 3] == 0x32) { // "2"
                        textsize = imagebuffer[index + 7] - 1;
                        if (textsize > 60) {
                            textsize = 60;
                        } // limit size
                        idlooplimit = 0; // update for version 1.0.112
                        for (n = 0; n < textsize; n++) {
                            subindex = idtextfilter(subindex, index, 1, 11 + n);
                            if (idlooplimit > 250) {
                                gotinfo = gotinfo - 2;
                                n = textsize;
                                debugout(15);
                            }
                        } // textsize
                        index = 1024;
                        gotinfo++;
                    }
                }
            }
        } // imagebuffer
    } // index

    if (gotinfo > 0) {
        eyedeethreelen = subindex;
        eyedeephix();
        idtext[eyedeethreelen] = 0; // terminate string
        iseyedeethree = 1;
        return;
    } else {
        if (covershow == 1) {
            clearartwork();
        } // covershow 
        iseyedeethree = 0;
    }// gotinfo
}
//
// private function for id3 text string filter

int idtextfilter(int subindex, int index, int nu, int su) { // update for version 1.0.112
    if (subindex < 120) {
        idtext[subindex] = imagebuffer[index + su];
        if (idtext[subindex] > 0x60 && idtext[subindex] < 0x7B) {
            idtext[subindex] = idtext[subindex] - 0x20;
        } // convert lower case chars
        if (idtext[subindex] < 0x20 || idtext[subindex] > 0x5F) {
            idtext[subindex] = 0x20;
        } // convert unsupported chars to spaces
        if (subindex > 0) { // condense spaces
            if (idtext[subindex] == 0x20 && idtext[subindex - 1] == 0x20) {
                subindex--;
                idlooplimit++;
            }
        } else {
            if (idtext[subindex] == 0x20) {
                subindex--;
                idlooplimit++;
            }
        } // subindex
        if (nu != 0) {
            subindex++;
        }
    }
    return (subindex);
}
//
// private function for id3 parser functions

int idtexthyphenator(int subindex) { // update for version 1.0.173
    if (idtext[subindex - 1] == 0x20) {
        subindex--;
    }
    int hycnt = 0;
    while (hycnt < 3) {
        if (subindex < 120) {
            idtext[subindex] = sphyphen[hycnt];
            subindex++;
        }
        hycnt++;
    } // hycnt
    return (subindex);
}
//
// search for fruit company unicode artist field in exported mp3 files
// converts the unicode artist string to approximate ascii
// unicode support limited to latin range 0xC0 - 0xFF only

int eyedeeunicodeartist() { // update for version 1.0.41
    int index = 0;
    int subindex = 0;
    int textsize = 0;
    unsigned char uflipflop = 0;
    unsigned char counter = 0;
    unsigned char unibuffer = 0;
    // fruit company artist unicode field
    for (index = 0; index < 1024; index++) { // search chunk for artist
        if (imagebuffer[index + 0] == 0x54) { // "T"
            if (imagebuffer[index + 1] == 0x50) { // "P"
                if (imagebuffer[index + 2] == 0x31) { // "1"
                    if (imagebuffer[index + 7] == 0xFF || imagebuffer[index + 8] == 0xFE) { // is unicode
                        textsize = imagebuffer[index + 5] - 5; //
                        if (textsize > 60) {
                            textsize = 60;
                        } // limit size
                        for (subindex = 0; subindex < textsize; subindex++) {
                            if (subindex < 120) {
                                if (uflipflop == 0) {
                                    text[subindex] = imagebuffer[index + 9 + subindex];
                                    uflipflop = 1;
                                } else {
                                    text[subindex] = 0;
                                    uflipflop = 0;
                                }
                                if (text[subindex] > 0x60 && text[subindex] < 0x7B) {
                                    text[subindex] = text[subindex] - 0x20;
                                } // convert lower case chars
                                if (imagebuffer[index + 9 + subindex] > 0xBF) { // convert unicode characters to approximate ascii
                                    unibuffer = imagebuffer[index + 9 + subindex];
                                    text[subindex] = unitrans[unibuffer - 0xC0];
                                } // text
                                if (text[subindex] < 0x20 || text[subindex] > 0x5F) { // convert unsupported chars to question mark
                                    if (text[subindex] > 0x00) {
                                        text[subindex] = 0x3F;
                                    } // ?
                                } // text
                            } // subindex
                        } // textsize
                        counter = 0;
                        subindex = 0;
                        while (counter < textsize) { // remove zero values from string
                            if (text[counter] != 0) {
                                idtext[subindex] = text[counter];
                                subindex++;
                            }
                            counter++;
                        } // textsize
                        subindex = idtexthyphenator(subindex);
                        index = 1024;
                    }
                }
            }
        } // imagebuffer
    } // index
    textsize = subindex;
    return textsize;
}
//
// search for fruit company unicode title field in exported mp3 files
// converts the unicode title string to approximate ascii
// unicode support limited to latin range 0xC0 - 0xFF only

int eyedeeunicodetitle(int idsize) { // update for version 1.0.41
    int textsize = 0;
    int index = 0;
    unsigned char uflipflop = 0;
    unsigned char subindex = 0;
    unsigned char counter = 0;
    unsigned char unibuffer = 0;
    // fruit company title unicode version
    for (index = 0; index < 1024; index++) { // search chunk for title
        if (imagebuffer[index + 0] == 0x54) { // "T"
            if (imagebuffer[index + 1] == 0x54) { // "T"
                if (imagebuffer[index + 2] == 0x32) { // "2"
                    if (imagebuffer[index + 7] == 0xFF || imagebuffer[index + 8] == 0xFE) { // is unicode
                        textsize = imagebuffer[index + 5] - 5;
                        if (textsize > 120) {
                            textsize = 120;
                        } // limit size
                        for (n = 0; n < textsize; n++) {
                            if (n < 120) {
                                if (uflipflop == 0) {
                                    text[n] = imagebuffer[index + 9 + n];
                                    uflipflop = 1;
                                } else {
                                    text[n] = 0;
                                    uflipflop = 0;
                                }
                                if (text[n] > 0x60 && text[n] < 0x7B) {
                                    text[n] = text[n] - 0x20;
                                } // convert lower case chars
                                if (imagebuffer[index + 9 + n] > 0xBF) { // convert unicode characters to approximate ascii
                                    unibuffer = imagebuffer[index + 9 + n];
                                    text[n] = unitrans[unibuffer - 0xC0];
                                } // text
                                if (text[n] < 0x20 || text[n] > 0x5F) { // convert unsupported chars to question mark
                                    if (text[n] > 0x00) {
                                        text[n] = 0x3F;
                                    } // ?
                                } // text
                            } // subindex
                        } // textsize
                        while (counter < textsize) { // remove zero values from string
                            if (text[counter] != 0) {
                                idtext[subindex + idsize] = text[counter];
                                subindex++;
                            }
                            counter++;
                        } // textsize
                        index = 1024;
                    }
                }
            }
        } // imagebuffer
    } // index
    textsize = subindex;
    return textsize;
}
//
// id3 tag version one parser for the oldest mp3 files
// currently untested due to lack of program memory space

void eyedeeveeone() { // update for version 1.0.114
    int eyeonecnt = 3;
    eyedeethreelen = 0;
    FSfseek(pointer, 128, SEEK_END); // seek to position
    FSfread(imagebuffer, 1, 128, pointer); // read last 128 bytes
    iseyedeethree = 1; // check for version one tag header
    if (imagebuffer[0] != 0x54) {
        iseyedeethree = 0;
    }
    if (imagebuffer[1] != 0x41) {
        iseyedeethree = 0;
    }
    if (imagebuffer[2] != 0x47) {
        iseyedeethree = 0;
    }
    if (iseyedeethree == 0) {
        return;
    }
    while (imagebuffer[eyeonecnt] != 0 && eyeonecnt < 33) {
        idtext[eyedeethreelen] = imagebuffer[eyeonecnt];
        if (idtext[eyedeethreelen] > 0x60 && idtext[eyedeethreelen] < 0x7B) { // convert lower case chars
            idtext[eyedeethreelen] = idtext[eyedeethreelen] - 0x20;
        }
        if (idtext[eyedeethreelen] < 0x20 || idtext[eyedeethreelen] > 0x5F) {
            idtext[eyedeethreelen] = 0x20;
        } // convert unsupported chars
        eyeonecnt++;
        eyedeethreelen++;
    } // imagebuffer
    if (eyedeethreelen > 0) {
        while ((idtext[eyedeethreelen - 1] == 0x20 || idtext[eyedeethreelen - 1] == 0x00) && eyedeethreelen > 0) {
            eyedeethreelen--;
        }
        idtext[eyedeethreelen + 0] = 0x20; // " "
        idtext[eyedeethreelen + 1] = 0x2D; // "-"
        idtext[eyedeethreelen + 2] = 0x20; // " "
        eyedeethreelen = eyedeethreelen + 3;
    } // eyedeethreelen
    eyeonecnt = 33;
    while (imagebuffer[eyeonecnt] != 0 && eyeonecnt < 63) {
        idtext[eyedeethreelen] = imagebuffer[eyeonecnt];
        if (idtext[eyedeethreelen] > 0x60 && idtext[eyedeethreelen] < 0x7B) { // convert lower case chars
            idtext[eyedeethreelen] = idtext[eyedeethreelen] - 0x20;
        }
        if (idtext[eyedeethreelen] < 0x20 || idtext[eyedeethreelen] > 0x5F) {
            idtext[eyedeethreelen] = 0x20;
        } // convert unsupported chars
        eyeonecnt++;
        eyedeethreelen++;
    } // imagebuffer
    if (eyeonecnt > 33) {
        while ((idtext[eyedeethreelen - 1] == 0x20 || idtext[eyedeethreelen - 1] == 0x00) && eyedeethreelen > 0) {
            eyedeethreelen--;
        }
    } // eyedeethreelen
    idtext[eyedeethreelen] = 0; // terminate info string
    FSfseek(pointer, 0, SEEK_SET); // seek to start
    tagsize = 0; // set to zero to prevent player skipping bytes at start of file
    filesize = filesize - 128; // subtract tag size from file size for player
}
//
// load VLSI spectrum analyser program
// from sd card to vs1003b decoder chip

void loadspectrum() {
    int file = 0;
    int wordcount = 0;
    int i;
    unsigned char vsdataa; // byte buffers for word data
    unsigned char vsdatab; //
    unsigned char atab = 0; // vs1003 register address byte
    unsigned char csum = 0x3F; // checksum byte
    if (pointer == NULL) {
        return;
    }
    FSfseek(pointer, 333824 + 60416, SEEK_SET);
    // rom position should now be correct
    while (file < 1888) {
        FSfread(receivebuffer, 1, 32, pointer);
        SD_CS = 1; // deselect sd card
        SPISTAT = 0;
        delay_gen(0);
        i = 0;
        while (i < 32) {
            vsdataa = receivebuffer[i + 0];
            vsdatab = receivebuffer[i + 1];
            atab = 0x06;
            int ffkk = 0; // update for version 1.0.171
            while (ffkk < 7) {
                if (wordcount == spaddr[ffkk]) {
                    atab++;
                }
                ffkk++;
            } // ffkk
            while (VSDREQ == 0) {
                delay_gen(0);
            }
            vscommand(atab, vsdataa, vsdatab); // write register
            i++;
            i++;
            wordcount++;
            csum = csum ^ vsdataa; // calculate checksum
            csum = csum ^ vsdatab; //
        } // i
        SPISTAT = 0x8000;
        SD_CS = 0; // select sd card
        delay_gen(0);
        file = file + 32;
    } // file
    SD_CS = 1; // deselect sd card
    SPISTAT = 0;
    delay_gen(0);
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x0A, 0x00, 0x50); // write aiaddr
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
    delay_gen(0);
    // close the rom file
    FSfclose(pointer);
    // go back to parent directory
    // which should be the root
    FSchdir("..");
    // compare with expected checksum for spectrum analyser
    if (vsdatab != 0x24 || csum != 107) {
        debugout(32);
    } // update for version 1.0.196
}
//
// load VLSI pitch changer program
// from sd card to vs1003b decoder chip

void loadpitchchanger() { // update for version 1.0.51
    int file = 0;
    int wordcount = 0;
    int i;
    unsigned char vsdataa; // byte buffers for word data
    unsigned char vsdatab; //
    unsigned char atab = 0; // vs1003 register address byte
    unsigned char chsum = 0x3F; // checksum byte
    if (pointer == NULL) {
        return;
    }
    FSfseek(pointer, 396736, SEEK_SET);
    // rom position should now be correct
    while (file < 2664) {
        FSfread(receivebuffer, 1, 32, pointer);
        SD_CS = 1; // deselect sd card
        SPISTAT = 0;
        delay_gen(0);
        i = 0;
        while (i < 32) {
            vsdataa = receivebuffer[i + 0];
            vsdatab = receivebuffer[i + 1];
            atab = 0x06;
            // update for version 1.0.53
            unsigned char tabindex = 0;
            while (tabindex < 11) {
                if (wordcount == tabtab[tabindex]) {
                    atab++;
                }
                tabindex++;
            } // tabindex
            while (VSDREQ == 0) {
                delay_gen(0);
            }
            vscommand(atab, vsdataa, vsdatab); // write register
            i++;
            i++;
            wordcount++;
            chsum = chsum ^ vsdatab; // calculate checksum
        } // i
        SPISTAT = 0x8000;
        SD_CS = 0; // select sd card
        delay_gen(0);
        file = file + 32;
    } // file
    SD_CS = 1; // deselect sd card
    SPISTAT = 0;
    delay_gen(0);
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x0A, 0x00, 0x30); // write aiaddr
    delay_gen(0);
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    int pccontrol = -16384; // default play 1.0x speed
    vsdataa = pccontrol >> 8; // update for version 1.0.134
    vsdatab = pccontrol & 0b0000000011111111; //
    vscommand(0x0C, vsdataa, vsdatab); // pitch changer settings
    delay_gen(0);
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
    delay_gen(0);
    // close the rom file
    FSfclose(pointer);
    // go back to parent directory
    // which should be the root
    FSchdir("..");
    // compare with expected checksum for pitch changer
    if (chsum != 99) {
        debugout(33);
    } // update for version 1.0.196
}
//
// graphics library function for draw rounded rectangle
// rounded rectangles are not properly tested for what extent
// they may be drawn off screen without breaking
// rounded rectangles also have a minimum size

void drawroundedrectangle(int posx, int posy, int lenx, int leny) {
    int cornx = 0; // update for version 1.0.118
    int corny = 0;
    cornx = posx;
    corny = posy;
    x = cornx + 3;
    y = corny + 1;
    setpixel();
    x++;
    setpixel();
    x = cornx + 2;
    y++;
    setpixel();
    x--;
    y++;
    setpixel();
    y = corny + 4;
    setpixel();
    x = cornx + 1;
    y = corny + 1;
    clearpixel();
    pixelworkprivatea(); // update for version 1.0.165
    y++;
    clearpixel();
    x = cornx;
    y = corny + 1;
    drawrectprivatea();
    cornx = posx + lenx;
    corny = posy;
    x = cornx - 3;
    y = corny + 1;
    setpixel();
    x--;
    setpixel();
    x = cornx - 2;
    y++;
    setpixel();
    x++;
    y++;
    setpixel();
    y = corny + 4;
    setpixel();
    x = cornx - 1;
    y = corny + 1;
    clearpixel();
    pixelworkprivateb(); // update for version 1.0.165
    y++;
    clearpixel();
    x = cornx;
    y = corny + 1;
    drawrectprivatea();
    cornx = posx;
    corny = posy + leny;
    x = cornx + 3;
    y = corny - 1;
    setpixel();
    x++;
    setpixel();
    x = cornx + 2;
    y--;
    setpixel();
    x--;
    y--;
    setpixel();
    y = corny - 4;
    setpixel();
    x = cornx + 1;
    y = corny - 1;
    clearpixel();
    pixelworkprivatea(); // update for version 1.0.165
    y--;
    clearpixel();
    x = cornx;
    y = corny - 1;
    drawrectprivateb();
    cornx = posx + lenx;
    corny = posy + leny;
    x = cornx - 3;
    y = corny - 1;
    setpixel();
    x--;
    setpixel();
    x = cornx - 2;
    y--;
    setpixel();
    x++;
    y--;
    setpixel();
    y = corny - 4;
    setpixel();
    x = cornx - 1;
    y = corny - 1;
    clearpixel();
    pixelworkprivateb(); // update for version 1.0.165
    y--;
    clearpixel();
    x = cornx;
    y = corny - 1;
    drawrectprivateb();
    drawline(posx + 5, posy, (posx + lenx) - 5, posy);
    drawline(posx + 5, posy + leny, (posx + lenx) - 5, posy + leny);
    drawline(posx, posy + 5, posx, (posy + leny) - 5);
    drawline(posx + lenx, posy + 5, posx + lenx, (posy + leny) - 5);
}
//
// private function a for draw rounded rectangle

void drawrectprivatea() { // update for version 1.0.157
    clearpixel();
    y++;
    clearpixel();
    y++;
    clearpixel();
    y++;
    clearpixel();
}
//
// private function b for draw rounded rectangle

void drawrectprivateb() { // update for version 1.0.157
    clearpixel();
    y--;
    clearpixel();
    y--;
    clearpixel();
    y--;
    clearpixel();
}
//
// maintains the vs1003b buffer
// uses the faster software spi
// from version 1.0.51 onwards

void fillaudiobuffer() {
    int an = 0;
    if (playing == 1 && framecount > 145) { // play mp3 file
        while (VSDREQ == 1 && mpfileopen == 1) {
            if (bufready == 0) {
                SPISTAT = 0x8000;
                SD_CS = 0; // select sd card
                if ((filesize - fileposition) < 33) { // check for end of file
                    an = 0;
                    while (an < 32) { // clear buffer 
                        receivebuffer[an] = 0;
                        an++;
                    }
                    FSfread(receivebuffer, 1, filesize - fileposition, pointer);
                    FSfclose(pointer);
                    bufready = 1;
                    mpfileopen = 0;
                } else {
                    FSfread(receivebuffer, 1, 32, pointer);
                    fileposition = fileposition + 32;
                    bufready = 1;
                } // fileposition
                SD_CS = 1; // deselect sd card
                SPISTAT = 0;
            } // bufready
            if (bufready == 1 && VSDREQ == 1) {
                // comment below code while working on vs1003b hardware spi
                VSXDCS = 0; // select vs1003b data
                an = 0;
                if (spispeedconfig == 0) { // update for version 1.0.130
                    while (an < 32) {
                        WriteSPIManual(receivebuffer[an]);
                        an++;
                    }
                } else {
                    while (an < 32) {
                        WriteSPIFast(receivebuffer[an]);
                        an++;
                    }
                }
                VSXDCS = 1; // deselect vs1003b data
                if (mpfileopen == 1) {
                    bufready = 0;
                }
            } // bufready
        } // VSDREQ
    } // playing
}
//

void eyedeephix() {
    int index = 0;
    int indexb = 0;
    unsigned char flag = 1;
    if (idtext[eyedeethreelen - 2] == 0x1F && eyedeethreelen < 100) { // "NO TITLE"
        while (indexb < 8) { // update for version 1.0.157
            idtext[eyedeethreelen + indexb] = defaultinfo[indexb + 12];
            indexb++;
        } // indexb
        eyedeethreelen = eyedeethreelen + 8;
    }

    for (index = 0; index < eyedeethreelen; index++) {
        if (idtext[index] == 0x1F) {
            flag = 0;
            idtext[index] = 0x2D;
        }
    } // index

    if (flag == 1 && eyedeethreelen < 100) { // no artist flag
        for (index = 0; index < eyedeethreelen; index++) {
            idtext[index + 12] = idtext[index];
        } // index

        index = 0; // copy "NO ARTIST - " string part unterminated
        while (index < 12) {
            idtext[index] = defaultinfo[index];
            index++;
        } // update for version 1.0.157
        eyedeethreelen = eyedeethreelen + 12;
    } // flag

    // music match jukebox phix
    int mmindex = 0;
    index = 0;
    while (index < eyedeethreelen) {
        if (idtext[index] > 0x1F && idtext[index] < 0x61) {
            text[mmindex] = idtext[index];
            mmindex++;
        }
        index++;
    }
    index = 0;
    while (index < mmindex) {
        idtext[index] = text[index];
        index++;
    } // mmindex
    idtext[index] = 0;
    eyedeethreelen = mmindex;
    //
    index = 1;
    mmindex = 1;
    while (index < eyedeethreelen) { // check for space every other character
        if (idtext[index] != 0x20) {
            mmindex = 0;
        }
        index++;
        index++;
    } // eyedeethreelen
    //
    if (mmindex == 1) { // is music match jukebox tag
        index = 0;
        mmindex = 0;
        while (index < eyedeethreelen) {
            text[mmindex] = idtext[index];
            index = index + 2;
            mmindex++;
        }
        text[mmindex] = 0;
        index = 0;
        while (index < mmindex) {
            idtext[index] = text[index];
            index++;
        }
        idtext[index] = 0;
        eyedeethreelen = mmindex;
    } // mmindex

    // find jpeg image for fruit company exported files
    eyedeejpgpos = 0;
    long int imagesize = 0;
    tagsize = 0;
    for (index = 0; index < 1024; index++) { // search chunk for image
        if (imagebuffer[index + 0] == 0x00) { // 0
            if (imagebuffer[index + 1] == 0x50) { // "P"
                if (imagebuffer[index + 2] == 0x49) { // "I"
                    if (imagebuffer[index + 3] == 0x43) { // "C" 
                        eyedeejpgpos = index + 4; // set the jpeg image data position
                        long int imgsizework = 0;
                        imgsizework = imagebuffer[index + 4];
                        imgsizework = imgsizework * 65536;
                        imagesize = imgsizework;
                        imgsizework = imagebuffer[index + 5];
                        imgsizework = imgsizework * 256;
                        imagesize = imagesize + imgsizework;
                        imagesize = imagesize + imagebuffer[index + 6];
                        // add the current index to image size
                        // to equal the entire tag size
                        imagesize = imagesize + index + 3;
                        index = 1024;
                        // this is really the entire tag size
                        // assuming the image is the last data
                        tagsize = imagesize;
                    }
                }
            }
        } // imagebuffer
    } // index
    //
    stdimagetag(); // search for image in standard id3 tag
    //
    // a kludge for strings that are missing the no artist
    // part of the info where no artist has been extracted
    mmindex = 9;
    unsigned char compid = 0;
    unsigned char matchid = 1;

    // copy "NO ARTIST - NO TITLE" default terminated string
    while (compid < 21) {
        text[compid] = defaultinfo[compid];
        compid++;
    } // update for version 1.0.157
    compid = 0;

    while (mmindex < eyedeethreelen) {
        if (idtext[compid] != text[mmindex]) {
            matchid = 0;
        }
        mmindex++;
        compid++;
    } // mmindex
    if (matchid == 1) {
        sprintf(idtext, rec.filename);
        debugout(16);
        eyedeethreelen = 0; // update for version 1.0.120
        while (idtext[eyedeethreelen] != 0) {
            eyedeethreelen++;
        }
        idtext[eyedeethreelen] = 0x21;
        eyedeethreelen++;
        idtext[eyedeethreelen] = 0x00;
    } // matchid
    //
    if (covershow == 1) { // check for covershow enabled
        if (eyedeejpgpos != 0) { // image was found
            printjpeg(); // display image
        } else { // image was not found
            clearartwork(); //
        } // eyedeejpgpos
    } // covershow
}
//
// click sound used for file browser and calendar

void clickframe() { // update for version 1.0.121
    unsigned char clickmode = 0x67;
    if (demo == 6) {
        clickmode = 0x6F;
    } // 0x7E max
    if (soundenabled == 0) {
        return;
    }
    SD_CS = 1;
    SPISTAT = 0; // deselect sd card
    while (VSDREQ == 0) {
        SPISTAT = 0;
    }
    vscommand(0x00, 0x08, 0x20); // write tests allowed
    while (VSDREQ == 0) {
        VSXCS = 1;
    }
    VSXDCS = 0; // select vs1003 instruction
    WriteSPIManual(0x53);
    WriteSPIManual(0xEF); // start sine
    WriteSPIManual(0x6E);
    WriteSPIManual(clickfreq); // sine value
    WriteSPIManual(0x00);
    WriteSPIManual(0x00);
    WriteSPIManual(0x00);
    WriteSPIManual(0x00);
    VSXDCS = 1;
    while (VSDREQ == 0) {
        VSXDCS = 1;
    }
    delay_gen(4);
    VSXDCS = 0; // select vs1003 instruction
    WriteSPIManual(0x45);
    WriteSPIManual(0x78); // end sine
    WriteSPIManual(0x69);
    WriteSPIManual(0x74);
    WriteSPIManual(0x00);
    WriteSPIManual(0x00);
    WriteSPIManual(0x00);
    WriteSPIManual(0x00);
    VSXDCS = 1;
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x00, 0x08, 0x00); // restore tests not allowed
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
    if (clickfdir == 0) { // warble effect
        clickfreq++;
        if (clickfreq > clickmode) {
            clickfdir = 1;
        }
    } else { // clickfdir was set
        clickfreq--;
        if (clickfreq < 0x61) {
            clickfdir = 0;
        }
    } // clickfdir
}
//
// send 24 low bitrate silence mp3 frames
// to fix glitches after track skipping

void silenceframes() { // update for version 1.0.173
    int sfcount = 0;
    while (sfcount < 24) {
        while (VSDREQ == 0) {
            delay_gen(0);
        }
        VSXDCS = 0; // select vs1003 data
        int silencecnt = 0;
        while (silencecnt < 9) {
            WriteSPIManual(silencedat[silencecnt]);
            silencecnt++;
        } // silencecnt
        clrfb = 0;
        while (clrfb < 17) {
            WriteSPIManual(0x00);
            clrfb++;
        }
        VSXDCS = 1;
        delay_gen(0);
        sfcount++;
    } // sfcount
}
//
// fill vs1003b buffer with zero values
// recommended by the manufacturer VLSI

void fillbufferwithzeros() {
    int bytecount = 0;
    while (bytecount < 2048) {
        if (VSDREQ == 1) {
            VSXDCS = 0; // select vs1003 data
            n = 0;
            while (n < 32) {
                WriteSPIManual(0);
                n++;
            }
            bytecount = bytecount + 32;
            VSXDCS = 1;
        } // dreq
    } // bytecount
}
//
// move the id3 tag track information in scroller windows

void eyedeemove(int width) { // update for version 1.0.210
    if (eyedeethreepos == 1) {
        eyedeethreedel--;
    }
    if (eyedeethreedel == 0) {
        if (eyedeethreepos < 2) {
            if (eyedeethreepos > 0 - (eyedeethreelen * width)) {
                eyedeethreepos--;
            } else {
                eyedeethreepos = 132;
            }
        }
        if (eyedeethreepos > 1) {
            eyedeethreepos--;
            if (eyedeethreepos == 1) {
                eyedeethreedel = 96;
            }
        }
    }
    tx = eyedeethreepos;
}
//
// reset programs ready to start from menu
// all borrowed global variables must be
// reset or set to default in this function

void resettomenu() {
    demo = 1;
    bufready = 0;
    mpfileopen = 0;
    fileposition = 0;
    playing = 0;
    mpvbr = 0;
    clickfreq = 0x60;
    clickfdir = 0;
    lastpressed = 1;
    cardstat = 0;
    drawmap = 0;
    zooming = 0;
    zoomlevel = 0;
    gpsfix = 0;
    forcefirstdraw = 1;
    imageselect = 0;
    oldimageselect = 0;
    screentllat = -28.10;
    screentllon = 152.50;
    screenbrlat = -28.40;
    screenbrlon = 152.80;
    oldlogmins = 99;
    oldlogsecs = 99; // update for version 1.0.174
    limitytop = 32;
    limitybottom = 32; // update for version 1.0.177
    maxlabels = 0; // update for version 1.0.176
    menuvars[5] = 0;
    tetdir = 0;
    oldtetdir = 0; // update for version 1.0.187
    backlighttimer = backlightdelay; // start backlight timer
    oldvtime[0] = 0x99; // set old announcement time to invalid value
    //GPSPWR_PIN = 1; // optional gps module power off
    UBRG = 259; // default 9600 baud
    n = 0;
    while (n < 33) {
        idtext[n] = 0;
        receivebuffer[n] = 0;
        text[n] = 0;
        idtext[n + 100] = 0; // update for version 1.0.186
        if (n < 30) {
            line2[n] = 0;
        }
        if (n < 22) {
            line3[n] = 0;
        } // update for version 1.0.177
        if (n < 6) {
            tztime[n] = 0x30;
            tzdate[n] = 0x2D;
        }
        n++;
    } // n
    startvoice();
    clrgraphics();
    gpslog = 0;
    logindex = 320; // start of track log area
    distance = 0.0; // reset track odometer
    timewasacquired = 0;
    odometerslide = 0;
    sinamp = 11;
    if (framecount != 8 && autoplayer == 0) {
        menutransition(1);
    }
    sprintf(fileextension, "*.mpi"); // gets changed to *.mp3
    framecount = 30;
    msine = 0;
    jheld = 1.0;
    // update for version 1.0.158
    mapsniffer = 0; // find closest point flag in gps program
    brngpoint = 0;
    brngdisplay = 0; //
    lxa = 0;
    lxb = 0;
    lyc = 0; // update for version 1.0.190
    idtext[133] = 0; // update for version 1.0.198
    msgcount = 0; // update for version 1.0.200
    formatbufa = 200.0; // update for version 1.0.200
    mpvbrcount = 0; // update for version 1.0.217
    lasterror = 0; // reset last error for main menu
}
//

void threedeerotation() { // update for version 1.0.216
    tdtheta = d2r * tdrotation; // pre calculation
    threedeemultrotation();
}
//

void threedeemultrotation() {
    int tdxx;
    int tdyy;
    int tdrx;
    int tdry;
    int tdxnew;
    int tdynew;
    tdxx = x;
    tdyy = y;
    tdrx = tdxx - 64;
    tdry = tdyy - 32;
    tdxnew = cos(tdtheta) * tdrx - sin(tdtheta) * tdry;
    tdynew = sin(tdtheta) * tdrx + cos(tdtheta) * tdry;
    tdxnew = tdxnew + 64;
    tdynew = tdynew + 32;
    x = tdxnew;
    y = tdynew;
}
//
// GPS function for U-Blox NEO-6M module NMEA parsing.
// This is a blocking function if waiting for new data,
// or non blocking if the program can accept the last
// received gps data which may not be the most current. 

void handlegps() { // update for version 1.0.140
    int rmccnt = 0; // loop counter
    receivebuffer[79] = 0x2A; // asterisk string terminator
    U2STAbits.OERR = 0; // clear any rx overflow error
    if (demo != 4) {
        gotgprmc = 0;
    } // force the next gps update
    while (gotgprmc != 2) { // update for version 1.0.145
        // responsive joystick exit for gps mode with no position fix
        JOYPAR_PIN = 0;
        delay_gen(0);
        if (demo == 2 && gpsfix == 0 && gpsmode == 1) {
            if (JOYDAT_PIN == 0) {
                if (bailedgpsmode == 0) {
                    playtinydirect();
                }
                gpsmodeexitdelay();
                bailedgpsmode = 1;
                JOYPAR_PIN = 1;
            } // joydat
        } // demo
        JOYPAR_PIN = 1;
    } // gotgprmc

    while (idtext[rmccnt + 20] != 0x2A) { // copy data to the receive buffer
        receivebuffer[rmccnt] = idtext[rmccnt + 20];
        rmccnt++;
    } // rmccnt
    receivebuffer[rmccnt] = 0x2A;
    rmccnt++; // append with sentence terminator
    receivebuffer[rmccnt] = idtext[rmccnt + 20];
    rmccnt++; // append with checksum
    receivebuffer[rmccnt] = idtext[rmccnt + 20]; //
    gotgprmc = 0; // reset the received status for new data
    // end of the updated section for interrupt driven serial
    //
    // formatted utc string
    text[0] = receivebuffer[7];
    text[1] = receivebuffer[8];
    text[2] = 0x3A; // ":"
    text[3] = receivebuffer[9];
    text[4] = receivebuffer[10];
    text[5] = 0x3A; // ":"
    text[6] = receivebuffer[11];
    text[7] = receivebuffer[12];
    text[8] = 0x00;
    //
    // verify the nmea checksum for gprmc sentence
    int gpsn = 1; // update for version 1.0.49
    unsigned char check[2];
    unsigned char checksum = 0; // declare checksum
    while (receivebuffer[gpsn] != 0x2A && gpsn < 79) { // prevents array overflow
        checksum = checksum ^ receivebuffer[gpsn]; // calculate checksum
        gpsn++; // increment index
    }
    //
    check[0] = (checksum / 16) + 0x30; // update for version 1.0.182
    check[1] = (checksum % 16) + 0x30;
    if (check[0] > 0x39) {
        check[0] = check[0] + 7;
    }
    if (check[1] > 0x39) {
        check[1] = check[1] + 7;
    }
    checksum = 1; // reset checksum
    if (check[0] != receivebuffer[gpsn + 1]) {
        checksum = 0;
    }
    if (check[1] != receivebuffer[gpsn + 2]) {
        checksum = 0;
    }
    if (checksum != 1) { // update for version 1.0.144
        debugout(6); // set error for bad checksum
        receivebuffer[17] = 0; // cancel gps fix status
    } // checksum
    //
    if (receivebuffer[17] != 0x41) {
        gpsfix = 0;
    } else {
        gpsfix = 1;
        oldgpslat = gpslat;
        oldgpslon = gpslon;
        float xlat = 0.1;
        float xlon = 0.1;
        int latminutes = 0;
        int lonminutes = 0;
        int lathemisphere = 0;
        int lonhemisphere = 0;
        // convert ascii to float values
        latminutes = (receivebuffer[19] - 0x30) * 10;
        latminutes = latminutes + (receivebuffer[20] - 0x30);
        gpslat = (receivebuffer[21] - 0x30) * 10;
        gpslat = gpslat + (receivebuffer[22] - 0x30);
        //
        lathemisphere = 24; // update for version 1.0.66
        while (lathemisphere < 29) { // some variables borrowed
            gpslat = gpslat + (receivebuffer[lathemisphere] - 0x30) * xlat;
            lathemisphere++;
            xlat = xlat / 10;
        } // lathemishpere
        //
        if (receivebuffer[30] == 0x4E) { // N
            lathemisphere = 1; // north (positive)
        } else {
            lathemisphere = 0; // south (negative)
        } // 
        //
        lonminutes = (receivebuffer[32] - 0x30) * 100;
        lonminutes = lonminutes + (receivebuffer[33] - 0x30) * 10;
        lonminutes = lonminutes + (receivebuffer[34] - 0x30);
        gpslon = (receivebuffer[35] - 0x30) * 10;
        gpslon = gpslon + (receivebuffer[36] - 0x30);
        //
        lonhemisphere = 38; // update for version 1.0.66
        while (lonhemisphere < 43) { // some variables borrowed
            gpslon = gpslon + (receivebuffer[lonhemisphere] - 0x30) * xlon;
            lonhemisphere++;
            xlon = xlon / 10;
        } // lonhemishpere
        //
        if (receivebuffer[44] == 0x45) { // E
            lonhemisphere = 1; // east (positive)
        } else {
            lonhemisphere = 0; // west (negative)
        } //
        //
        xlat = (gpslat / 60) + latminutes; // convert to decimal degrees
        xlon = (gpslon / 60) + lonminutes;
        //
        if (lathemisphere == 0) { // convert negative numbers
            xlat = 0.0 - xlat;
        }
        if (lonhemisphere == 0) { // convert negative numbers
            xlon = 0.0 - xlon;
        }
        gpslat = xlat;
        gpslon = xlon;
        //
        formatbufa = gpslat;
        formatbufb = gpslon;
        sprintf(text, "%f,%f", formatbufa, formatbufb);
        //
        float gpsspeedknots = 0.0; // update for version 1.0.156
        float spmultiplier = 0.1;
        if (receivebuffer[46] != 0x2C) { // update for version 1.0.157
            int spcounter = 46; // first digit index
            while (spcounter < 51) {
                if (receivebuffer[spcounter] != 0x2E) {
                    spmultiplier = spmultiplier * 10;
                } else {
                    spcounter = 51;
                }
                spcounter++;
            } // spcounter
            spcounter = 46; // first digit index
            while (spcounter < 51) {
                if (receivebuffer[spcounter] != 0x2E) {
                    gpsspeedknots = gpsspeedknots + (receivebuffer[spcounter] - 0x30) * spmultiplier;
                    spmultiplier = spmultiplier / 10.0;
                } // receivebuffer
                spcounter++;
            } // spcounter
            if (locale == 0) { // update for version 1.0.211
                gpsspeedknots = gpsspeedknots * 1.852; // convert knots to kilometers per hour
            } else { // configuration option set for miles per hour
                gpsspeedknots = gpsspeedknots * 1.15078; // convert knots to miles per hour
            } // locale
        } else { // comma was found where first speed digit should have been
            gpsspeedknots = 0.0;
            debugout(22); // reset speed if the value was not present
        } // receivebuffer
        gpsunitsph = 0; // set to zero
        if (gpsspeedknots > 4.99) { // update for version 1.0.158
            gpsunitsph = gpsspeedknots; // copy to integer
        } // gpsspeedknots
    } // gpsfix
}
//
// $GPRMC sentence format in receivebuffer buffer when gps is position fixed
//
// la,la,la,dt,la,la,la,la,la,cm,he,cm,lo,lo,lo,lo,lo,dt,lo,lo,lo,lo,lo,cm,he,cm,sp,dt,sp,sp,sp,
// 20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,
//
// cm,cs,cs,dt,cs,cs,cm,dd,dd,mm,mm,yy,yy,cm,cm,cm,md,as,cs,cs,CR,LF.
// 51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79.
//
//
// distance and bearing calculations are
// based on information from these sites:
// http://www.movable-type.co.uk/scripts/latlong.html
// http://www.geomidpoint.com/destination/calculation.html
// information from both sites has been ported to
// C for this implementation by Brek Martin 2013.
// calculate bearing from a coordinate pair

int getbearing(float inlat, float inlon) { // update for version 1.0.158
    int bearing = 0;
    double brng = 0.0;
    double lat2 = gpslat;
    double long2 = gpslon;
    double lat1 = inlat;
    double long1 = inlon;
    double x, y;
    double dLon;
    dLon = (long2 - long1) * d2r;
    lat1 = lat1 * d2r;
    lat2 = lat2 * d2r;
    y = sin(dLon) * cos(lat2);
    x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
    brng = atan2(y, x);
    brng = brng * 57.2957795;
    bearing = rounded(brng); // update for version 1.0.162
    bearing = (bearing + 360) % 360;
    if (bearing == 360) {
        bearing = 0;
    }
    return (bearing);
}
//
// calculate haversine distance in Km

float getdistance(float inlat, float inlon) { // update for version 1.0.158
    double lat2 = gpslat;
    double long2 = gpslon;
    double lat1 = inlat;
    double long1 = inlon;
    double dlong = (long2 - long1) * d2r;
    double dlat = (lat2 - lat1) * d2r;
    double a = pow(sin(dlat / 2.0), 2) + cos(lat1 * d2r) * cos(lat2 * d2r) * pow(sin(dlong / 2.0), 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double d = 6367 * c;
    float distf = d;
    return (distf);
}
//

void multiplerotationf() { // update for version 1.0.144
    float rxf;
    float ryf;
    float xnewf;
    float ynewf;
    float xxf;
    float yyf;
    xxf = x;
    yyf = y;
    rxf = xxf - 63.5;
    ryf = yyf - 31.5;
    xnewf = cos(theta) * rxf - sin(theta) * ryf;
    ynewf = sin(theta) * rxf + cos(theta) * ryf;
    x = xnewf + 63.5;
    y = ynewf + 31.5;
}
//

void convert_meters() { // update for version 1.0.85
    int feet = 0;
    int meters = 0;
    float mtr = 0.0;
    int cntdig = 0;
    while (cntdig < 4) {
        if (text[cntdig] > 0x2F && text[cntdig] < 0x3A) {
            feet = (feet * 10) + (text[cntdig] - 0x30);
        } else {
            text[cntdig + 1] = 0;
        } // text
        cntdig++;
    } // cntdig
    mtr = feet * 0.304811;
    meters = mtr;
    mtr = mtr - meters;
    if (mtr > 0.5) {
        meters++;
    }
    sprintf(text, "%d", meters);
}
//
// Likeness of the classic Amiga "Hand Holding Disk" screen
// Original Amiga graphic believed to be by Sheryl Knowles.
// Long live the Commodore Amiga!!!

void sdcardanimation() {
    sdx = 76;
    sdy = 68; //dynamic offsets
    int clrsd = 0;
    unsigned char sdflasher = 0;
    WORD spirestore = 0;
    unsigned char selectrestore = 0;
    //
    selectrestore = SD_CS;
    //SD_CS = 1;		// deselect sd card
    spirestore = SPISTAT;
    //SPISTAT = 0;
    //SD_CS = 0;		// turn sd card pins off
    //SPIOUTLAT = 0;	//
    //
    while (sdy != 0 || joyspareb != 0) {
        if (sdy != 0) {
            sdy--;
        }
        clrframebuffer();
        // sd card outline
        drawline(12 + sdx, sdy, 39 + sdx, sdy); // top of sd card
        drawline(12 + sdx, sdy, 12 + sdx, 30 + sdy); // left of sd card
        drawline(44 + sdx, 5 + sdy, 44 + sdx, 40 + sdy); // right of sd card
        drawline(21 + sdx, 40 + sdy, 44 + sdx, 40 + sdy); // bottom of sd card
        // sd card cut corner
        clrsd = 1;
        while (clrsd < 5) {
            x = clrsd + 39;
            y = clrsd;
            sdpixel();
            clrsd++;
        } // clrsd
        // left side of thumb over sd card
        x = 13;
        y = 29;
        sdpixel();
        x = 14;
        sdpixelx();
        y = 28;
        sdpixely();
        x = 15;
        sdpixelx();
        x = 16;
        sdpixelx();
        y = 27;
        sdpixely();
        x = 17;
        sdpixelx();
        y = 26;
        sdpixely();
        x = 18;
        sdpixelx();
        drawline(18 + sdx, 25 + sdy, 21 + sdx, 25 + sdy);
        drawline(20 + sdx, 24 + sdy, 20 + sdx, 22 + sdy);
        x = 21;
        y = 21;
        sdpixel();
        drawline(21 + sdx, 20 + sdy, 26 + sdx, 20 + sdy);
        x = 25;
        y = 21;
        sdpixel();
        x = 27;
        sdpixelx();
        drawline(28 + sdx, 21 + sdy, 28 + sdx, 27 + sdy);
        drawline(26 + sdx, 22 + sdy, 26 + sdx, 25 + sdy);
        drawline(22 + sdx, 26 + sdy, 25 + sdx, 26 + sdy);
        drawline(20 + sdx, 39 + sdy, 20 + sdx, 34 + sdy);
        x = 7;
        y = 33;
        sdpixel();
        x = 21;
        sdpixelx();
        x = 22;
        y = 32;
        sdpixel();
        x = 23;
        sdpixelx();
        x = 24;
        y = 31;
        sdpixel();
        x = 25;
        y = 30;
        sdpixel();
        x = 7;
        y = 29;
        sdpixel();
        x = 26;
        sdpixelx();
        x = 27;
        y = 28;
        sdpixel();
        // left line of hand
        drawline(0 + sdx, 33 + sdy, 0 + sdx, 63 + sdy);
        // hand parts to the left of sd card
        x = 10;
        y = 30;
        sdpixel();
        x = 11;
        sdpixelx();
        x = 1;
        y = 32;
        sdpixel();
        y = 31;
        sdpixely();
        x = 2;
        y = 30;
        sdpixel();
        x = 3;
        y = 29;
        sdpixel();
        x = 4;
        sdpixelx();
        x = 5;
        y = 28;
        sdpixel();
        y = 27;
        sdpixely();
        x = 6;
        y = 26;
        sdpixel();
        y = 25;
        sdpixely();
        x = 7;
        y = 24;
        sdpixel();
        y = 23;
        sdpixely();
        x = 8;
        y = 22;
        sdpixel();
        x = 9;
        y = 21;
        sdpixel();
        x = 10;
        sdpixelx();
        x = 11;
        y = 20;
        sdpixel();
        x = 8;
        y = 30;
        sdpixel();
        while (x < 11) {
            x++;
            sdpixelx();
        }
        x = 9;
        y = 31;
        sdpixel(); // try omitted
        x = 10;
        sdpixelx();
        x = 8;
        y = 32;
        sdpixel();
        x = 9;
        sdpixelx();
        x = 5;
        y = 34;
        sdpixel();
        x = 6;
        sdpixelx();
        x = 4;
        y = 35;
        sdpixel();
        x = 5;
        sdpixelx();
        // line and curves of thumb under sd card
        drawline(21 + sdx, 41 + sdy, 21 + sdx, 50 + sdy);
        x = 20;
        y = 51;
        sdpixel();
        drawline(19 + sdx, 52 + sdy, 15 + sdx, 52 + sdy);
        drawline(15 + sdx, 53 + sdy, 15 + sdx, 62 + sdy);
        // bottom of hand
        drawline(1 + sdx, 63 + sdy, 24 + sdx, 63 + sdy);
        // bottom right line of hand
        drawline(25 + sdx, 63 + sdy, 25 + sdx, 57 + sdy);
        x = 26;
        y = 57;
        sdpixel();
        x = 27;
        y = 56;
        sdpixel();
        x = 28;
        sdpixelx();
        x = 29;
        y = 55;
        sdpixel();
        x = 30;
        y = 54;
        sdpixel();
        y = 53;
        sdpixely();
        x = 31;
        y = 52;
        sdpixel();
        x = 32;
        y = 51;
        sdpixel();
        // fingernails of hand
        x = 26;
        y = 43;
        sdpixel();
        x = 22;
        sdpixelx();
        y = 44;
        sdpixely();
        y = 45;
        sdpixely();
        x = 23;
        y = 41;
        sdpixel();
        y = 42;
        sdpixely();
        y = 43;
        sdpixely();
        x = 24;
        y = 42;
        sdpixel();
        x = 25;
        sdpixelx();
        x = 27;
        y = 44;
        sdpixel();
        y = 45;
        sdpixely();
        x = 28;
        sdpixelx();
        drawline(23 + sdx, 46 + sdy, 32 + sdx, 46 + sdy);
        drawline(29 + sdx, 44 + sdy, 29 + sdx, 48 + sdy);
        x = 30;
        y = 43;
        sdpixel();
        y = 44;
        sdpixely();
        x = 31;
        y = 42;
        sdpixel();
        y = 43;
        sdpixely();
        x = 32;
        sdpixelx();
        y = 42;
        sdpixely();
        x = 28;
        y = 47;
        sdpixel();
        x = 29;
        sdpixelx();
        x = 35;
        y = 48;
        sdpixel();
        x = 28;
        sdpixelx();
        x = 29;
        sdpixelx();
        x = 30;
        y = 49;
        sdpixel();
        y = 50;
        sdpixely();
        x = 31;
        y = 50;
        sdpixel();
        x = 32;
        sdpixelx();
        x = 34;
        y = 49;
        sdpixel();
        x = 33;
        sdpixelx();
        y = 48;
        sdpixely();
        y = 47;
        sdpixely();
        x = 36;
        y = 46;
        sdpixel();
        y = 47;
        sdpixely();
        x = 37;
        y = 44;
        sdpixel();
        y = 45;
        sdpixely();
        x = 38;
        y = 41;
        sdpixel();
        y = 42;
        sdpixely();
        y = 43;
        sdpixely();
        // pattern filled sd card graphic
        clrsd = 0; // update for version 1.0.86
        while (clrsd < 1024) { // clear frame buffer
            imagebuffer[clrsd + imageselect] = framebuffer[clrsd];
            clrsd++;
        } // clrsd
        floodfill(110, sdy + 1, 1);
        floodfill(115, sdy + 39, 1);
        while (clrsd > 0) {
            clrsd--;
            framebuffer[clrsd] = imagebuffer[clrsd + imageselect];
        } // clrsd
        //
        if (sdy == 0) {
            tx = 15;
            ty = 24; // update for version 1.0.205
            sprintf(text, "INSERT");
            if (sdflasher < 25) {
                drawtextvarwidth();
            }
            tx = 12;
            ty = 34;
            sprintf(text, "SD CARD");
            if (sdflasher < 25) {
                drawtextvarwidth();
            }
        } // sdy zero
        if (sdy < 1) {
            drawroundedrectangle(8, 20, 56, 24); // draw window border
        }
        drawscreen();
        if (sdy == 0) {
            shiftbytein();
            sdflasher++;
            if (sdflasher > 49) {
                sdflasher = 0;
            }
        }
        if (joyspareb == 0) {
            sdy--;
        }
        if (sdy < -20) {
            sdy = 0;
        }
    } // sdy
    //
    sdy = 0;
    while (sdy < 20) {
        clrframebuffer();
        drawroundedrectangle(8 + sdy, 20, 56 - (sdy * 2), 24); // draw window border
        drawscreen();
        sdy++;
    }
    SPISTAT = spirestore;
    SD_CS = selectrestore;
}
//
// please do not remove credit messages from distribution.

void printcredit() { // update for version 1.0.192
    ty = 40; // set credit string vertical position
    sprintf(text, "BREK MARTIN 2018"); // buffer credit string
    int copycred = 653;
    while (copycred < 755) { // clear rectangle of frame buffer
        framebuffer[copycred] = 0;
        copycred++;
    } // copycred
    drawline(14, 38, 113, 38); // draw message border
    drawline(14, 48, 113, 48);
    y = 39;
    copycred = 0;
    while (copycred < 18) {
        x = xpixels[copycred];
        setpixel();
        x = xpixels[copycred + 1];
        setpixel();
        y++;
        copycred = copycred + 2;
    } // copycred
    copycred = 14;
    while (copycred < 114) { // mask unwanted plasma with clear pixels
        y = 39;
        x = copycred;
        clearpixel();
        copycred++;
    } // copycred
    y = 40;
    creditprivateb(); // update for version 1.0.164
    copycred = 41; // update for version 1.0.163
    while (copycred < 46) {
        y = copycred;
        creditprivatea();
        copycred++;
    } // copycred
    y = 46;
    creditprivateb(); // update for version 1.0.164
    x = 14;
    y = 47;
    clearpixel();
    x = 113;
    clearpixel();
    tx = 16; // set credit string horizontal position
    drawtext(); // print the credit string
    delay_gen(0); // potentially delay the intro
}
//
// private function for duplicated pixel code

void creditprivatea() { // update for version 1.0.163
    x = 12;
    clearpixel();
    x = 115;
    clearpixel();
    creditprivateb();
}
//
// private function for duplicated pixel code

void creditprivateb() { // update for version 1.0.166
    x = 13;
    clearpixel();
    x++;
    clearpixel();
    x = 114;
    clearpixel();
    x--;
    clearpixel();
}
//

void serialeyedeethree() {
    if (serialenabled != 1) {
        return;
    } // update for version 1.0.139
    unsigned char idindex = 0;
    if (iseyedeethree != 0) {
        while (idtext[idindex] != 0) { // send title and artist info
            txbyte = idtext[idindex];
            hardserialout();
            idindex++;
        }
    } else {
        while (rec.filename[idindex] != 0) { // or fall back to filename
            txbyte = rec.filename[idindex];
            hardserialout();
            idindex++;
        }
    } // iseyedeethree
    txbyte = 0x0D;
    hardserialout();
    txbyte = 0x0A;
    hardserialout();
}
//
// hardware serial transmit 9600 baud

void hardserialout() { // update for version 1.0.139
    while (U2STAbits.UTXBF == 1) {
        delay_gen(0);
    }
    U2TXREG = txbyte;
}
//
// apply local time zone offset to gps time

void applytzoffset() {
    int nmcnt = 0; // counter
    int gpswork = 0;
    int adjust = 0;
    gpsyear = 0;
    gpsmonth = 0;
    gpsday = 0;
    gpsdow = 0;
    gpshour = 0;
    gpsminute = 0;
    gpssecond = 0;
    gotdate = 0;
    gottime = 1;

    nmcnt = 7; // update for version 1.0.182
    while (nmcnt < 12) { // time of day sanity check
        if (receivebuffer[nmcnt] > 0x35 || receivebuffer[nmcnt + 1] < 0x30) {
            gottime = 0;
        }
        nmcnt = nmcnt + 2;
    } // nmcnt
    if (receivebuffer[7] > 0x32) {
        gottime = 0;
    }

    if (gottime == 1) {
        gpshour = asciitoint(receivebuffer[7], receivebuffer[8]); // update for version 1.0.157
        gpsminute = asciitoint(receivebuffer[9], receivebuffer[10]);
        gpssecond = asciitoint(receivebuffer[11], receivebuffer[12]);
    } // gottime

    nmcnt = 0;
    while (receivebuffer[nmcnt] != 0x2A) { // "*"
        nmcnt++;
        if (nmcnt > 99) {
            break;
        }
    }
    nmcnt++;
    while (receivebuffer[nmcnt] != 0x2A) { // "*"
        nmcnt--;
        if (nmcnt < 10) {
            break;
        }
    }

    n = 0;
    while (n < 3) { // update for version 1.0.117
        nmcnt--;
        while (receivebuffer[nmcnt] != 0x2C) { // ","
            nmcnt--;
            if (nmcnt < 10) {
                break;
            }
        }
        n++;
    } // n

    gotdate = 1;
    if (receivebuffer[nmcnt - 2] == 0x2C || receivebuffer[nmcnt - 1] == 0x2C) {
        gotdate = 0;
    }
    if (receivebuffer[nmcnt - 4] < 0x30 || receivebuffer[nmcnt - 4] > 0x31) {
        gotdate = 0;
    }
    if (receivebuffer[nmcnt - 6] < 0x30 || receivebuffer[nmcnt - 6] > 0x33) {
        gotdate = 0;
    }
    if (gotdate == 0) {
        n = 6; // write six zero digits to buffer
        while (n > 0) {
            receivebuffer[nmcnt - n] = 0x30;
            n--;
        } // update for version 1.0.62
    } // gotdate

    if (gotdate == 1) { // copy date to buffers as integers
        gpsyear = asciitoint(receivebuffer[nmcnt - 2], receivebuffer[nmcnt - 1]); // update for version 1.0.157
        gpsyear = gpsyear + 2000;
        gpsmonth = asciitoint(receivebuffer[nmcnt - 4], receivebuffer[nmcnt - 3]); // update for version 1.0.157
        gpsday = asciitoint(receivebuffer[nmcnt - 6], receivebuffer[nmcnt - 5]); // update for version 1.0.157
    } // gotdate

    if (gottime == 1) {
        // save original gps time prior to applying offset
        uhour = gpshour;
        umin = gpsminute;
        usec = gpssecond;
        gpshour = gpshour + timezone; // convert hour
        if (gpshour > 23) { // jumped ahead one day
            gpshour = gpshour - 24; // set hour in correct range
            adjust = 1; // set flag to adjust calendar
        } // gpshour
        if (gpshour < 0) { // jumped behind one day
            gpshour = 24 + gpshour; // set hour in correct range
            adjust = -1; // set flag to adjust calendar
        } // gpshour
        sprintf(tztime, "%02d%02d%02d", gpshour, gpsminute, gpssecond);
        n = 0;
        while (n < 4) {
            vtime[n] = tztime[n] - 0x30;
            n++;
        } // update for version 1.0.165
    } else {
        n = 0;
        while (n < 6) {
            tztime[n] = 0x30;
            n++;
        } // update for version 1.0.174
    } // gottime

    if (gotdate == 1) {
        calyear = gpsyear; // copy date to calendar buffers
        calmonth = gpsmonth; //
        calday = gpsday; //
        setcal(); // set calendar to current gps date

        // save original gps date prior to applying offset
        uday = gpsday;
        umonth = gpsmonth;
        uyear = gpsyear;

        if (adjust > 0) { // move day forward
            gpsday++;
            if (gpsday > caldim[gpsmonth - 1]) {
                gpsday = 1;
                gpsmonth++;
                if (gpsmonth > 12) {
                    gpsmonth = 1;
                    gpsyear++;
                }
            }
            calyear = gpsyear; // copy date to calendar buffers
            calmonth = gpsmonth; //
            calday = gpsday; //
            setcal(); // set calendar to adjusted gps date
        } // adjust = 1

        if (adjust < 0) { // move day backward
            gpsday--;
            gpswork = 0;
            if (gpsday < 1) {
                gpsmonth--;
                gpswork = -1;
                gpsday = 1;
                if (gpsmonth < 1) {
                    gpsmonth = 12;
                    gpsyear--;
                }
            }
            calyear = gpsyear; // copy date to calendar buffers
            calmonth = gpsmonth; //
            calday = gpsday; //
            setcal(); // set calendar to adjusted gps date
            if (gpswork < 0) {
                gpsday = caldim[gpsmonth - 1];
                calyear = gpsyear; // copy date to calendar buffers
                calmonth = gpsmonth; //
                calday = gpsday; //
                setcal(); // set calendar to adjusted gps date
            }
        } // adjust = -1

        gpswork = gpsyear - 2000;
        sprintf(tzdate, "%02d%02d%02d", gpsday, gpsmonth, gpswork);
    } else { // no date
        calbadentry = 10; // update for version 1.0.66
    } // gotdate

    if (calbadentry != 0) {
        sprintf(tzdate, "------");
    } // calbadentry

    if (framecount < 103) { // show timezone
        int tzdisplay = 0;
        if (timezone > 0 || timezone == 0) {
            tzdisplay = timezone;
        } else {
            tzdisplay = -1 * timezone;
        }
        sprintf(tztime, "TZ  %02d", tzdisplay);
        if (configsign != 0) {
            tztime[3] = 0x2B;
        } else {
            tztime[3] = 0x2D;
        }
        sprintf(tzdate, "      ");
    } // framecount

}
//

void sdpixely() {
    y = y + sdy;
    setpixel();
}
//

void sdpixelx() {
    x = x + sdx;
    setpixel();
}
//

void sdpixel() {
    y = y + sdy;
    x = x + sdx;
    setpixel();
}
//

void startvoice() {
    unsigned char vola = 0x1E;
    unsigned char volb = 0x1E;
    if (soundenabled == 0) {
        return;
    }
    SD_CS = 1;
    SPISTAT = 0; // deselect sd card
    delay_gen(0);
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x03, 0x00, 0x00); // write clock register zero
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    if (demo > 3) {
        vola = mpvolumea;
        volb = mpvolumeb;
    }
    if (demo == 6) {
        vola = 0x3E;
        volb = 0x3E;
    } // calendar volume
    vscommand(0x0B, vola, volb); // write volume
    delay_gen(0);
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    basscrank = 0; // reset bass boost in mp3 player
    vscommand(0x02, 0x00, 0x00); // reset bass enhance
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
    delay_gen(0);
}
//
// play tiny sample directly from constant array so
// the sample doesn't overwrite data in voice array

void playtinydirect() {
    if (soundenabled == 0) {
        return;
    }
    int bytecount = 0;
    SD_CS = 1; // deselect sd card
    SPISTAT = 0;
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    while (bytecount < 368) {
        if (VSDREQ == 1) {
            VSXDCS = 0; // select vs1003 data
            n = 0;
            while (n < 32) {
                WriteSPIManual(tiny[bytecount + n]);
                n++;
            }
            bytecount = bytecount + 32;
            VSXDCS = 1;
        } // dreq
    } // bytecount
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
    delay_gen(0);
    vlen = 0; // reset announcement length
}
//
// play lock sample directly from constant array so
// the sample doesn't overwrite data in voice array

void playlockdirect() { // update for version 1.0.214
    if (soundenabled == 0) {
        return;
    }
    int bytecount = 0;
    SD_CS = 1; // deselect sd card
    SPISTAT = 0;
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    while (bytecount < 160) {
        if (VSDREQ == 1) {
            VSXDCS = 0; // select vs1003 data
            n = 0;
            while (n < 32) {
                WriteSPIManual(lock[bytecount + n]);
                n++;
            }
            bytecount = bytecount + 32;
            VSXDCS = 1;
        } // dreq
    } // bytecount
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
    delay_gen(0);
    vlen = 0; // reset announcement length
}
//

void playvoice() {
    if (soundenabled == 0) {
        return;
    }
    int bytecount = 0;
    SD_CS = 1; // deselect sd card
    SPISTAT = 0;
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    while (bytecount < vlen) {
        if (VSDREQ == 1) {
            VSXDCS = 0; // select vs1003 data
            n = 0;
            while (n < 32) {
                WriteSPIManual(voice[bytecount + n]);
                n++;
            }
            bytecount = bytecount + 32;
            VSXDCS = 1;
        } // dreq
    } // bytecount
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
    delay_gen(0);
    vlen = 0; // reset announcement length
}
//

void vfill_audio() {
    if (soundenabled == 0) {
        playing = 0;
        return;
    }
    SD_CS = 1; // deselect sd card
    SPISTAT = 0;
    delay_gen(0);
    while (VSDREQ == 1 && playing == 1) {
        if (vbytecount < vlen) {
            VSXDCS = 0; // select vs1003 data
            n = 0;
            while (n < 32) {
                WriteSPIManual(voice[vbytecount + n]);
                n++;
            }
            vbytecount = vbytecount + 32;
            VSXDCS = 1;
            delay_gen(0);
        } else {
            playing = 0;
        } // bytecount
    } // dreq
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
    delay_gen(0);
}
//

void load_sample() {
    if (soundenabled == 0) {
        return;
    }
    if (!sdcardinit()) {
        cardstat = 0;
        vlen = 1850;
        return;
    } else { // update for version 1.0.118
        cardstat = 1;
    }
    if (FSchdir("ROM") != 0) {
        vlen = 1850;
        return;
    }
    openromfile();
    if (pointer != NULL) { // file opened
        if (samplenum != 0) { // update for version 1.0.118
            vlen = 0; // reset announcement length
            samlen = samlens[samplenum - 1];
            readvoice(samadds[samplenum - 1]);
            samplenum = 0; // reset sample select value
        } // samplenum
        FSfclose(pointer); // close rom file
    } // file exists and was opened
    FSchdir(".."); // go back to root directory
}
//
// build a macro assembled time of day announcement

void build_announcement() {
    if (soundenabled == 0) {
        return;
    }
    unsigned int rompos = 0;
    unsigned char vcount = 4;
    vlen = 0; // reset announcement length
    if (!sdcardinit()) {
        cardstat = 0;
        vlen = 1850;
        return;
    } else { // update for version 1.0.118
        cardstat = 1;
    }
    if (FSchdir("ROM") != 0) {
        vlen = 1850;
        return;
    }
    openromfile();
    if (vtime[0] != 0) { // first digit
        if (vtime[0] == 1) { // first digit was one
            samlen = vtimelensb[vtime[1]];
            rompos = vtimeposb[vtime[1]]; // update for version 1.0.174
            readvoice(rompos);
            vcount = 2;
        } // first digit was one
        if (vtime[0] == 2) { // first digit was two
            samlen = 1216;
            readvoice(33050); // say twenty
            if (vtime[1] > 0) {
                rompos = vtimeposa[vtime[1] - 1]; // update for version 1.0.117
                samlen = 1306;
                readvoice(rompos); // 1,2, or 3
            }
            vcount = 2;
        } // first digit was two
    } else { // first digit was zero
        // say nothing
        if (vcount != 2) {
            vcount = 3;
        } // fix for teens
        if (vtime[1] == 0) {
            samlen = 1472;
            readvoice(0);
            vcount = 2;
        } //say zero
    }
    if (vcount == 3) { // second digit if first digit was zero
        if (vtime[1] > 0) { // update for version 1.0.174
            samlen = vtimelensa[vtime[1] - 1];
            rompos = vtimeposa[vtime[1] - 1];
        } // vtime
        readvoice(rompos);
        vcount = 2;
    }
    // up to the minutes digits now
    if (vcount == 2) {
        if (vtime[2] != 0) { // first digit was not zero
            if (vtime[2] > 1) { // update for version 1.0.174
                samlen = samlens[vtime[2] + 4];
                readvoice(samadds[vtime[2] + 4]);
                if (vtime[3] == 0) {
                    vcount = 0;
                } else {
                    vcount = 1;
                }
            }
            if (vtime[2] == 1) { //say something from ten to nineteen
                samlen = vtimelensb[vtime[3]];
                rompos = vtimeposb[vtime[3]]; // update for version 1.0.174
                readvoice(rompos);
                vcount = 0;
            } // first digit was one
        } // first digit was not zero
    } // vcount = 2
    if (vtime[2] == 0 && vtime[3] == 0) {
        samlen = 1088;
        readvoice(39086);
        vcount = 0;
    } //say hundred and finish
    if (vtime[2] == 0 && vtime[3] != 0) {
        samlen = 1472;
        readvoice(0);
        vcount = 1;
    } // say zero
    if (vcount == 1) {
        if (vtime[3] > 0) { // update for version 1.0.174
            samlen = vtimelensa[vtime[3] - 1];
            rompos = vtimeposa[vtime[3] - 1];
        } // vtime
        readvoice(rompos);
        vcount = 0;
    } // vcount = 1
    FSfclose(pointer); // close rom file
    FSchdir(".."); // go back to root directory
}
//

void readvoice(unsigned int pos) {
    unsigned char vtbuff[33]; // buffer
    int bytesin = 0;
    int vbytes = 0;
    // file should already be open
    FSfseek(pointer, pos, SEEK_SET);
    delay_gen(0);
    while (bytesin < samlen) {
        FSfread(vtbuff, 1, 32, pointer);
        delay_gen(0);
        bytesin = bytesin + 32;
        vbytes = 0;
        // pad end of the chunk if it overran sample length
        if (bytesin > samlen) { // update for version 1.0.47
            unsigned char padcnt = bytesin - samlen;
            while (padcnt > 0) {
                vtbuff[32 - padcnt] = 0;
                padcnt--;
            } // padcnt
        } // bytesin
        //
        while (vbytes < 32) {
            voice[vlen] = vtbuff[vbytes];
            vlen++;
            vbytes++;
        } // vbytes
    } // while bytesin
    delay_gen(0);
}
//
// 500 year calendar calculator - Brek Martin 2015 - 2017.
// outputs a day of week for a given day, month, and year.
// originally intended and tested range of years 1800-2300.

void setcal() { // update for version 1.0.113
    calleap = 0; // clear leap year status
    if (calyear % 4 == 0) {
        calleap = 1; // set leap year status
        if (calyear % 100 == 0) {
            calleap = 0; // clear leap year status
            if (calyear % 400 == 0) {
                calleap = 1; // set leap year status
            }
        }
    }
    caldim[1] = 28 + calleap; // adjust day of month table for current year
    calbadentry = 0; // check for invalid date input
    xwork = 2310;
    cacnt = 12; // find best epoch year to iterate from
    while (xwork >= calyear) {
        if (cacnt < 13) {
            xwork = epochtable[cacnt];
        } else {
            debugout(3);
        }
        cacnt--;
    }
    cwork = 0; // working variable for days    
    cacnt = xwork; // iterate years from epoch year
    while (cacnt < calyear) { // update for version 1.0.107
        int calleapx = 0; // clear leap year status
        if (cacnt % 4 == 0) {
            calleapx = 1; // set leap year status
            if (cacnt % 100 == 0) {
                calleapx = 0; // clear leap year status
                if (cacnt % 400 == 0) {
                    calleapx = 1; // set leap year status
                }
            }
        }
        cwork = (cwork + 365 + calleapx) % 7; // add days over the years from epoch
        cacnt++;
    } // cacnt
    //
    caldim[1] = 28 + calleap; // adjust day of month table for current year
    cacnt = calmonth - 1; // add days for elapsed months of current year
    while (cacnt > 0) { //
        xwork = caldim[cacnt - 1]; //
        cwork = (cwork + xwork) % 7; //
        cacnt = cacnt - 1; //
    } // wend						//
    //
    cacnt = calday - 1; // add days elapsed of current month
    xwork = 0; //
    while (cacnt > 0) { //
        xwork = xwork + 1; //
        cacnt = cacnt - 1; //
    } // wend
    cwork = (cwork + xwork) % 7; //
    caldow = cwork + 1; // shift the range
    if (caldow == 7) {
        caldow = 0;
    } //
    if (calbadentry == 1) {
        debugout(2);
    }
} // setcal
//
// load 64x64 monochrome bitmap created with MS Paint.
// should have it's own array if not disposed of quickly.

void loadbitmap() {
    if (pointer == NULL) {
        return;
    }
    FSfseek(pointer, 335744 + 60416, SEEK_SET);
    FSfread(imagebuffer, 1, 574, pointer);
}
//
// loads the intro sample to play once
// soon to be erased by other data

void loadintro() {
    if (pointer == NULL) {
        return;
    }
    FSfseek(pointer, 60416, SEEK_SET);
    FSfread(voice, 1, 1880, pointer);
    // leave the rom file open
}
//

void read_config() {
    configerror = 0;
    filesize = 0;
    configsign = 99;
    if (!sdcardinit()) {
        cardstat = 0;
        configerror = 1;
        return;
    } else { // update for version 1.0.118
        cardstat = 1;
    }
    if (FSchdir("CONFIG") != 0) {
        configerror = 2;
        return;
    }
    // config directory was present
    int resultc = FindFirst("config.txt", 0x2F, &rec); // update for version 1.0.172
    if (resultc == 0) { // file was found
        pointer = FSfopen(rec.filename, "r"); // open for reading
    } else { // file was not found
        configerror = 2;
        return;
    } // resultc
    filesize = rec.filesize;
    if (filesize == 0) {
        configerror = 3;
        return;
    }
    // read file chunk to ram
    configreadram(2047, 4094); // update for version 1.0.163
    search_config(); // search up to 2047 bytes
    if (configerror == 4) { // config file was 2047 bytes or greater
        configerror = 0;
        FSfseek(pointer, 2027, SEEK_SET);
        configreadram(4077, filesize); // update for version 1.0.163
        filesize = filesize - 2046;
        search_config();
    } // configerror
    FSfclose(pointer); // close the config file
    // go back to parent directory
    // which should be the root
    FSchdir("..");
}
//
// read chunk of config file to ram buffer

void configreadram(int ia, int ib) { // update for version 1.0.185
    if (filesize < ia) {
        ib = ib - 2047;
    } else {
        ib = 2047;
        configerror = 4; // file was still too large
    } // filesize
    FSfread(imagebuffer, 1, ib, pointer);
}
//
// search for configuration data that has
// already been loaded to a byte array

void search_config() { // update for version 1.0.163
    configindex = 0;
    while (configindex < filesize) {

        if (imagebuffer[configindex] == 0x2F && imagebuffer[configindex + 1] == 0x2F) { // search for comments
            configindex = configtrima(configindex);
        } // done comments
        unsigned char configmatch = 0;

        configmatch = ctrmc(15, 0); // search for timezone offset
        if (configmatch == 1) {
            configindex = configtrimb(configindex);
            if (imagebuffer[configindex] == 0x2B) {
                configsign = 1;
            } // sign was plus
            if (imagebuffer[configindex] == 0x2D) {
                configsign = 0;
            } // sign was minus
            if (configsign != 99) {
                configindex++;
            } // skip past sign if present
            if (imagebuffer[configindex] > 0x2F && imagebuffer[configindex] < 0x3A) { // check for a digit value
                timezone = imagebuffer[configindex] - 0x30; // write msd
                if (imagebuffer[configindex + 1] > 0x2F && imagebuffer[configindex + 1] < 0x3A) { // check for a digit value
                    timezone = timezone * 10;
                    configindex++; // multiply by ten for msd
                    timezone = timezone + (imagebuffer[configindex] - 0x30); // write lsd
                } //
                if (configsign == 0) {
                    timezone = 0 - timezone;
                } // apply negative sign
            } //
            configindex = configtrima(configindex);
        } // configmatch

        configmatch = ctrmc(13, 15); // search for serial enable
        if (configmatch == 1) {
            configindex = configtrimb(configindex);
            if (imagebuffer[configindex] > 0x2F && imagebuffer[configindex] < 0x36) { // check for a digit value
                serialenabled = imagebuffer[configindex] - 0x30; // write value
            } //
            configindex = configtrima(configindex);
        } // configmatch

        configmatch = ctrmc(12, 28); // search for sound enable
        if (configmatch == 1) {
            configindex = configtrimb(configindex);
            if (imagebuffer[configindex] > 0x2F && imagebuffer[configindex] < 0x32) { // check for a digit value
                soundenabled = imagebuffer[configindex] - 0x30; // write value
            } //
            configindex = configtrima(configindex);
        } // configmatch

        configmatch = ctrmc(11, 40); // search for clock value
        if (configmatch == 1) {
            configindex = configtrimb(configindex);
            if (imagebuffer[configindex] > 0x2F && imagebuffer[configindex] < 0x35) { // check for a digit value
                clockvalue = imagebuffer[configindex] - 0x30; // write clock value
                if (clockvalue < 2) { // update for version 1.0.137
                    clockbyte = 0x84; //
                } else { //
                    if (clockvalue < 5) {
                        clockbyte = 0x84 + ((clockvalue - 1) * 0x20);
                    } //
                } //
            } //
            configindex = configtrima(configindex);
        } // configmatch

        // update for version 1.0.163
        configmatch = ctrmc(10, 51); // search for first menu
        if (configmatch == 1) {
            firstmenu = configtrimbig();
        } // mp3 player or vector gps
        // update for version 1.0.162
        configmatch = ctrmc(12, 61); // search for skip browser
        if (configmatch == 1) {
            skipbrowser = configtrimbig();
        } //
        configmatch = ctrmb(11, 0); // search for auto player
        if (configmatch == 1) {
            autoplayer = configtrimbig();
        } //
        configmatch = ctrmb(10, 11); // search for menu delay
        if (configmatch == 1) {
            menudelay = configtrimbig();
        } //
        configmatch = ctrmb(12, 21); // search for log interval
        if (configmatch == 1) {
            loginterval = configtrimbig();
        } //
        configmatch = ctrmb(9, 33); // search for cover show
        if (configmatch == 1) {
            covershow = configtrimbig();
        } //
        configmatch = ctrmb(7, 42); // search for mp3 shuffle
        if (configmatch == 1) {
            mpshuffle = configtrimbig();
        } //
        configmatch = ctrmb(9, 49); // search for greyscale
        if (configmatch == 1) {
            csgreyscale = configtrimbig();
        } //
        // update for version 1.0.211
        configmatch = ctrma(10, 25); // search for speed unit
        if (configmatch == 1) {
            locale = configtrimbig();
        } //
        configmatch = ctrma(11, 35); // search for date format
        if (configmatch == 1) {
            dateformat = configtrimbig();
        } //
        // update for version 1.0.212
        configmatch = ctrma(10, 46); // search for mixed case
        if (configmatch == 1) {
            mixedcase = configtrimbig();
        } //
        // update for version 1.0.215
        configmatch = ctrma(13, 56); // search for pitch shifter
        if (configmatch == 1) {
            pitchchanger = configtrimbig();
        } //
        // update for version 1.0.217
        configmatch = ctrmb(14, 58); // search for gps simulation
        if (configmatch == 1) {
            gpssimulation = configtrimbig();
        } //
        //
        configmatch = ctrma(11, 0); // search for video intro
        if (configmatch == 1) {
            configindex = configtrimb(configindex);
            if (imagebuffer[configindex] > 0x2F && imagebuffer[configindex] < 0x33) { // check for a value 0-2
                videointro = imagebuffer[configindex] - 0x30; // write value
            } //
            configindex = configtrima(configindex);
        } // checkbool

        configmatch = ctrma(9, 11); // search for lcd delay
        if (configmatch == 1) { // software spi speed is either 0 or 1
            configindex = configtrimb(configindex); // but a two digit value is read as an
            if (imagebuffer[configindex] > 0x2F && imagebuffer[configindex] < 0x3A) { // artifact of lcd hardware delay config
                spispeedconfig = imagebuffer[configindex] - 0x30; //
                if (imagebuffer[configindex + 1] > 0x2F && imagebuffer[configindex + 1] < 0x3A) { // check for second digit
                    spispeedconfig = spispeedconfig * 10; //
                    spispeedconfig = spispeedconfig + (imagebuffer[configindex + 1] - 0x30); // update for version 1.0.130
                }
            } //
            configindex = configtrima(configindex);
        } // checkbool

        configmatch = ctrma(10, 20); // search for sd card speed
        if (configmatch == 1) {
            configindex = configtrimb(configindex);
            if (imagebuffer[configindex] > 0x2F && imagebuffer[configindex] < 0x32) { // check for a value 0-1
                sdspeed = 5 + (imagebuffer[configindex] - 0x30); // write value
            } // update for version 1.0.76
            if (imagebuffer[configindex] == 0x2D) { // check for values -0 to -2
                if (imagebuffer[configindex + 1] > 0x2F && imagebuffer[configindex + 1] < 0x33) { //
                    sdspeed = 5 - (imagebuffer[configindex + 1] - 0x30); // write value
                }
            } // update for version 1.0.77
            configindex = configtrima(configindex);
        } // checkbool
        configindex++;
    } // configindex
    //
    // the file browser and music shuffle function can not work together
    // they use the same memory and will definitely break each other
    // they could both be enabled by the user in the configuration file
    // so a check is done to disable one based on the setting of the other
    // if the browser is enabled the shuffle function will be turned off
    // could be changed so if shuffle is enabled the browser is turned off
    if (skipbrowser == 0) {
        mpshuffle = 0;
    } // override shuffle setting if file browser is active for mp3 player
    //if (mpshuffle == 1) {skipbrowser = 1;} // override file browser setting if shuffle is active for mp3 player
    if (pitchchanger == 1) { // pitch changer will be active
        if (clockvalue < 3) {
            clockvalue = 3;
            clockbyte = 0xC4;
        } // update for version 1.0.215
    } // set to at least the default clock value for vs1003b
    if (serialenabled == 3) {
        serialenabled = 5;
    } // update for version 1.0.203
    if (serialenabled > 0 && serialenabled < 4) {
        U2STAbits.UTXEN = 1;
    } // set hardware serial transmit enable
}
//

int configtrima(int cinx) { // update for version 1.0.61
    while (imagebuffer[cinx] != 0x0A && cinx < filesize) {
        cinx++;
    } // search to end of line
    while (imagebuffer[cinx] == 0x0A && cinx < filesize) {
        cinx++;
    } // skip any extra new lines
    return cinx;
}
//

int configtrimb(int cinx) { // update for version 1.0.61
    while (imagebuffer[cinx] != 0x3D) {
        cinx++;
    } // search for equals sign
    cinx++; // skip past equals sign
    while (imagebuffer[cinx] == 0x20) {
        cinx++;
    } // skip any spaces
    return cinx;
}
//

unsigned char ctrmc(int a, int b) { // update for version 1.0.158
    unsigned char matchflag = 1;
    n = 0;
    while (n < a) {
        if (configsa[n + b] != imagebuffer[configindex + n]) {
            matchflag = 0;
        }
        n++;
    } // n
    return (matchflag);
}
//
//

unsigned char ctrmb(int a, int b) { // update for version 1.0.158
    unsigned char matchflag = 1;
    n = 0;
    while (n < a) {
        if (configsb[n + b] != imagebuffer[configindex + n]) {
            matchflag = 0;
        }
        n++;
    } // n
    return (matchflag);
}
//

unsigned char ctrma(int a, int b) { // update for version 1.0.158
    unsigned char matchflag = 1;
    n = 0;
    while (n < a) {
        if (configsc[n + b] != imagebuffer[configindex + n]) {
            matchflag = 0;
        }
        n++;
    } // n
    return (matchflag);
}
//

unsigned char configtrimbig() { // update for version 1.0.162
    unsigned char configvalue = 0;
    configindex = configtrimb(configindex);
    if (imagebuffer[configindex] == 0x30 || imagebuffer[configindex] == 0x31) { // check for a binary value
        configvalue = imagebuffer[configindex] - 0x30; // write value
    } //
    configindex = configtrima(configindex);
    return (configvalue);
}
//
// function to set or reset bass boost for mp3 player
// basscrank value is status for bass boost on or off

void bassboost() {
    SD_CS = 1; // deselect sd card
    SPISTAT = 0;
    delay_gen(0);
    while (VSDREQ == 0) {
        delay_gen(0);
    } // wait for chip ready
    VSXCS = 0; // select vs1003 instruction
    WriteSPIManual(0x02); // write
    WriteSPIManual(0x02); // bass enhance register
    WriteSPIManual(0x00); // 15db enhancement below 60Hz
    if (basscrank != 0) {
        WriteSPIManual(0xF6); //
    } else {
        WriteSPIManual(0x00); //
    } // basscrank
    VSXCS = 1;
    while (VSDREQ == 1) {
        delay_gen(0);
    }
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
    delay_gen(0);
}
//
// clear frame buffer and both image buffers

void clrgraphics() {
    clrfb = 0;
    while (clrfb < 1024) { // clear entire frame buffer
        framebuffer[clrfb] = 0;
        imagebuffer[clrfb] = 0; // clear first image
        imagebuffer[clrfb + 1024] = 0; // clear second image
        clrfb++;
    } // clrfb
}
//
// print graphic error message screen for
// maps or music media directory not found

void dirnotfound() { // update for version 1.0.205
    clrgraphics();
    clrfb = 0;
    while (clrfb < 7) {
        text[clrfb + lasterror] = notfound[clrfb];
        clrfb++;
    }
    text[clrfb + lasterror] = 0;
    ty = 29;
    drawtextvarwidthcentered();
    sprintf(text, "NOT FOUND");
    ty = 37;
    drawtextvarwidthcentered();
    pdir = 0;
    while (pdir < 90) {
        drawroundedrectangle(3, 26, 121, 20); // draw window border
        draw_folder(60, 4); // 7x7 folder graphic
        drawroundedrectangle(57, 1, 13, 13); //  draw folder border
        drawscreen();
        delay_gen(40);
        pdir++;
    }
    debugout(11); // set error status to reset to menu
}
//
// print 64x64 monochrome bitmap created with MS Paint

void printbitmap() { // update for version 1.0.182
    int bx = 0;
    int by = 0;
    int bindex = 573;
    unsigned char xxxxx;
    sdx = 46;
    sdy = 6; // offsets could be function arguments
    while (by < 64) {
        while (bx < 64) {
            xxxxx = 63;
            while (xxxxx > 55) {
                if ((imagebuffer[bindex] & 0b00000001 << (63 - xxxxx)) == 0) {
                    x = xxxxx - bx;
                    y = by;
                    sdpixel();
                }
                xxxxx--;
            } // xxxxx
            bindex--;
            bx = bx + 8;
        }
        bx = 0;
        by++;
    }
}
//
// print 64x64 monochrome bitmap created with MS Paint
// flipped horizontally to the display.

void printbitmapflippedhoriz() { // update for version 1.0.182
    int bx = 64;
    int by = 0;
    int bindex = 573;
    unsigned char xxxxx;
    sdx = 26;
    sdy = 6; // offsets could be function arguments
    while (by < 64) {
        while (bx > 0) {
            xxxxx = 63;
            while (xxxxx > 55) {
                if ((imagebuffer[bindex] & 0b10000000 >> (63 - xxxxx)) == 0) {
                    x = xxxxx - bx;
                    y = by;
                    sdpixel();
                }
                xxxxx--;
            } // xxxxx
            bindex--;
            bx = bx - 8;
        }
        bx = 64;
        by++;
    }
}
//
// runtime configuration enable timer screen

void runtimeconfigenable() { // update for version 1.0.190
    int pbfd = 0;
    int rconftrig = 0;
    while (pbfd < 8 && JOYDAT_PIN == 0) {
        clrframebuffer();
        if (framecount > 5) { // update for version 1.0.192
            drawcubedisplay();
            clrbmpchunk(); // update for version 1.0.191
            printbitmap();
        } // framecount
        drawscreen();
        pbfd++;
        if (framecount > 28) {
            rconftrig = 1;
            pbfd = 8;
        }
    } // pbfd
    if (rconftrig == 1) { // runtime configuration enabled
        int pbfd = 0;
        while (pbfd < 132) {
            clrframebuffer();
            drawcubedisplay();
            clrbmpchunk();
            if (pbfd < 18) {
                printbitmapflippedhoriz();
            } else {
                if (pbfd < 130) {
                    sprintf(text, "EXTRA");
                    ty = 18;
                    drawtextvarwidthcentered();
                    sprintf(text, "MUSIC");
                    ty = 29;
                    drawtextvarwidthcentered();
                    sprintf(text, "MENU!");
                    ty = 40;
                    drawtextvarwidthcentered();
                } // pbfd
            } // pbfd
            drawscreen();
            pbfd++;
        } // pbfd
        JOYPAR_PIN = 1;
        rtconfigtransitionout(); // update for version 1.0.191
        runtime_config();
        rtconfigtransitionoutb(); // update for version 1.0.191
    } // rconftrig
}
//
// clear background area for bitmap image

void clrbmpchunk() { // update for version 1.0.191
    int fbfd = 42; // clear bitmap background
    while (fbfd < 85) { // update for version 1.0.191
        framebuffer[fbfd + 128] = 0;
        framebuffer[fbfd + 256] = 0;
        framebuffer[fbfd + 384] = 0;
        framebuffer[fbfd + 512] = 0;
        framebuffer[fbfd + 640] = 0;
        framebuffer[fbfd + 768] = 0;
        fbfd++;
    } // fbfd
    y = 8;
    while (y > 4) {
        x = 43;
        while (x < 85) {
            clearpixel();
            x++;
        }
        y--;
    } // y
    y = 56;
    while (y < 60) {
        x = 43;
        while (x < 85) {
            clearpixel();
            x++;
        }
        y++;
    } // y
    drawroundedrectangle(42, 4, 44, 55);
}
//
// graphic transition out from runtime config enable screen

void rtconfigtransitionout() { // update for version 1.0.191
    int rectylf = 4;
    int rectyrt = 55;
    while (rectyrt < 64) { // expand border
        clrframebuffer(); // clear framebuffer   
        drawroundedrectangle(42, rectylf, 44, rectyrt);
        rectyrt = rectyrt + 2;
        rectylf--;
        delay_gen(18);
        drawscreen();
    } // rectylf
    rectylf = 42;
    rectyrt = 45;
    while (rectyrt < 127) { // expand border
        clrframebuffer(); // clear framebuffer   
        drawroundedrectangle(rectylf, 0, rectyrt, 63);
        rectyrt = rectyrt + 2;
        rectylf--;
        delay_gen(spmessage[rectylf / 5] / 2); // update for version 1.0.202
        drawscreen();
    } // rectylf
    while (joyfire == 1) {
        shiftbytein();
    } // wait for release
}
//
// graphic transition out from runtime config screen

void rtconfigtransitionoutb() { // update for version 1.0.191
    int rectytp = 0;
    int rectybt = 63;
    while (rectybt > 29) { // shrink border
        clrframebuffer(); // clear framebuffer   
        drawroundedrectangle(0, rectytp, 127, rectybt);
        rectytp++;
        rectybt = rectybt - 2;
        delay_gen(mpspringtable[rectybt] / 2); // update for version 1.0.202
        drawscreen();
    } // rectylf
    rectybt = 8;
    while (rectybt > 0) {
        clrframebuffer(); // clear framebuffer 
        drawroundedrectangle(0, 17, 127, 29); // draw button border
        delay_gen(8);
        drawscreen();
        rectybt--;
    } // rectybt
    while (joyfire == 1) {
        shiftbytein();
    } // wait for release
}
//
// parse map file header that identifies topographic or street type,
// zoom value, and default start screen center position for a map.
// displays the map description including basic graphic transition.

void read_map_header() {
    // byte 0 - map type
    // 0 = topographic map
    // 1 = street map
    //
    // byte 1 - polygons status
    // 0x00 = map has no filled polygons
    // 0xFF = map does have filled polygons
    //
    // byte 2 - set zoom
    // pointvaldiv = 01 for continent maps (0x01)
    // pointvaldiv = 02 for world maps (0x02)
    // pointvaldiv = 20 for most topographic maps (0x14)
    // pointvaldiv = 40 for most street maps (0x28)
    //
    FSfread(receivebuffer, 1, 32, pointer);
    maptype = receivebuffer[0];
    polystat = receivebuffer[1];
    pointvaldiv = receivebuffer[2];
    //
    // set file end offset the program will use
    if (polystat == 0) {
        polystat = 100;
    }
    if (polystat != 100) {
        polystat = 1183;
    }
    //
    // bytes 3,4,5,6. - default latitude
    // bytes 7,8,9,10. -  default longitude
    //
    // Example:
    // userlat = -28.280567  (9A 3E E2 C1) - Mt Barney East Peak
    // userlon = +152.698638 (DA B2 18 43)
    bytes[0] = receivebuffer[3];
    bytes[1] = receivebuffer[4];
    bytes[2] = receivebuffer[5];
    bytes[3] = receivebuffer[6];
    userlat = Float(bytes);
    bytes[0] = receivebuffer[7];
    bytes[1] = receivebuffer[8];
    bytes[2] = receivebuffer[9];
    bytes[3] = receivebuffer[10];
    userlon = Float(bytes);
    //
    if (maptype > 1 || receivebuffer[31] != 0) { // update for version 1.0.91
        lasterror = 200;
        drawmap = 0; // error value used as a counter first
        while (lasterror > 11) { // ends up set to the correct error value
            drawroundedrectangle(10, 20, 107, 20); // update for version 1.0.205
            sprintf(text, "UNSUPPORTED");
            ty = 24;
            drawtextvarwidthcentered();
            sprintf(text, "MAP FILE");
            ty = 32;
            drawtextvarwidthcentered();
            drawscreen();
            lasterror--;
        } // lasterror
        debugout(11); // update for version 1.0.203
    } else { // if file type was recognised
        if (receivebuffer[11] == 0) { // map description string was not found
            clrgraphics();
            drawgpsprogress(); // update for version 1.0.212
            drawscreen();
            debugout(37); // set error for no description
        } else { // map description string was found
            // read map file description for display
            clrfb = 11; // copy up to 20 character string
            while (clrfb < 32 && receivebuffer[clrfb] != 0) {
                text[clrfb - 11] = receivebuffer[clrfb];
                clrfb++;
                if (text[clrfb - 11] > 0x60 && text[clrfb - 11] < 0x7B) {
                    text[clrfb - 11] = text[clrfb - 11] - 0x20;
                }
            } // clrfb
            text[clrfb - 11] = 0; // terminate string
            clrgraphics();
            ty = blockpos + 1;
            drawroundedrectangle(1, blockpos - 1, 125, 10); // draw border
            drawtextcentered();
            clrfb = 0;
            while (clrfb < 1024) {
                imagebuffer[clrfb] = framebuffer[clrfb];
                clrfb++;
            }
            floodfill(63, blockpos - 11, 0);
            floodfill(63, blockpos + 11, 0);
            floodfill(0, blockpos + 2, 0);
            floodfill(127, blockpos + 2, 0);
            clrfb = 0;
            while (clrfb < 1024) {
                framebuffer[clrfb] = imagebuffer[clrfb] ^ 0xFF;
                clrfb++;
            }
            drawscreen(); // update for version 1.0.204
            int upordown = 0; // default move down
            if (blockpos > 29) {
                upordown = 1;
            } // up
            while (blockpos != 29) {
                clrimagebuffer();
                y = blockpos;
                while (y < blockpos + 10) {
                    x = 0;
                    while (x < 127) {
                        if (getpixel() == 1) {
                            clearpixel();
                            if (upordown == 1) {
                                y--;
                            } else {
                                y++;
                            }
                            setpixelimage();
                            if (upordown == 1) {
                                y++;
                            } else {
                                y--;
                            }
                        } // getpixel
                        x++;
                    } // x
                    y++;
                } // y
                clrfb = 0;
                while (clrfb < 1024) {
                    framebuffer[clrfb] = imagebuffer[clrfb];
                    clrfb++;
                }
                if (upordown == 1) {
                    blockpos--;
                } else {
                    blockpos++;
                }
                drawscreen();
            } // blockpos
            drawgpsprogress();
            if (rec.filesize > 1048575) {
                sprintf(text, "RENDERING MAP");
                ty = 13;
                drawtextvarwidthcentered();
                drawscreen();
            } else {
                drawscreen();
                delay_gen(1900); // title delay for small map
            } // filesize
            clrgraphics();
        } // text
    } // maptype
}
//
// draw entire gps track log to framebuffer

void gpstracklogread() { // update for version 1.0.218
    if (gpsmode == 0) {
        int readindex = 320;
        unsigned char firstpoint = 1;
        while (readindex < logindex) {
            bytes[0] = voice[readindex + 0];
            bytes[1] = voice[readindex + 1];
            bytes[2] = voice[readindex + 2];
            bytes[3] = voice[readindex + 3];
            filelat = Float(bytes);
            bytes[0] = voice[readindex + 4];
            bytes[1] = voice[readindex + 5];
            bytes[2] = voice[readindex + 6];
            bytes[3] = voice[readindex + 7];
            filelon = Float(bytes);
            screenx = (filelon - screentllon) / pointval;
            screeny = (screentllat - filelat) / pointval;
            lxa = lxb;
            lya = lyb;
            //lxb = screenx; lyb = screeny;
            lxb = rounded(screenx);
            lyb = rounded(screeny);
            readindex = readindex + 8; // increment log index
            if (firstpoint == 0) { // draw the line segment
                unsigned char onelogpointonscreen = 1; // test clipping area
                if (lxa < 0 && lxb < 0) {
                    onelogpointonscreen = 0;
                }
                if (lxa > 127 && lxb > 127) {
                    onelogpointonscreen = 0;
                }
                if (lya < 0 && lyb < 0) {
                    onelogpointonscreen = 0;
                }
                if (lya > 63 && lyb > 63) {
                    onelogpointonscreen = 0;
                }
                if (onelogpointonscreen == 1) {
                    drawline(lxa, lya, lxb, lyb); // solid line option
                } // onelogpointonscreen
            } // firstpoint
            firstpoint = 0;
        } // readindex
    } // gpsmode
}
//
// record gps track log to memory

void gpstracklogwrite() {
    int logint = 0;
    if (maptype == 0 || loginterval == 1) { // update for version 1.0.177
        if (gpsminute != oldlogmins) {
            logint = 1;
        }
    } else { // road map
        if (gpssecond != oldlogsecs) {
            logint = 1;
        }
    } // maptype
    if (logint == 1) {
        if (logindex < 7536) { // update for version 1.0.214
            oldlogmins = gpsminute;
            oldlogsecs = gpssecond;
            if (logindex > 320) {
                distance = distance + getdistance(lastloglat, lastloglon);
            }
            afloat = gpslat;
            floattobytes();
            voice[logindex + 0] = bytes[0];
            voice[logindex + 1] = bytes[1];
            voice[logindex + 2] = bytes[2];
            voice[logindex + 3] = bytes[3];
            afloat = gpslon;
            floattobytes();
            voice[logindex + 4] = bytes[0];
            voice[logindex + 5] = bytes[1];
            voice[logindex + 6] = bytes[2];
            voice[logindex + 7] = bytes[3];
            logindex = logindex + 8;
            lastloglat = gpslat;
            lastloglon = gpslon;
        } // logindex
    } // gpsminute
}
//
// read a single track log point for gps simulation

void gpstracklogpoint(int readindex) { // update for version 1.0.217
    if (readindex > logindex - 8) { // check for log end
        gpsmode = 0;
        gpssimulation = 0; // end simulation
        logindex = 320;
        simwasactive = 1; // update for version 1.0.219
        return;
    } // readindex
    bytes[0] = voice[readindex + 0];
    bytes[1] = voice[readindex + 1];
    bytes[2] = voice[readindex + 2];
    bytes[3] = voice[readindex + 3];
    gpslat = Float(bytes);
    bytes[0] = voice[readindex + 4];
    bytes[1] = voice[readindex + 5];
    bytes[2] = voice[readindex + 6];
    bytes[3] = voice[readindex + 7];
    gpslon = Float(bytes);
}
//
// round float up or down to integer value.
// replacement for standard round function.

int rounded(float unrounded) {
    int integer = unrounded;
    if (unrounded > 0.0) {
        if ((unrounded - integer) > 0.5) {
            integer++;
        }
    } else {
        if ((unrounded - integer) > -0.5) {
            integer++;
        }
    } // unrounded
    return integer;
}
//
// read, decode, and print either what the program thinks
// is the best monochrome decoded jpeg image file,
// or print in greyscale by cycling multiple bitplanes.

void printjpeg() {
    int clrgfx = 0;
    unsigned char bitplane = 0;
    jpgdecstatus = 0;
    jpgpixcount[0] = 0;
    jpgpixcount[1] = 0;
    jpgpixcount[2] = 0;
    jpgpixcount[3] = 0;
    while (clrgfx < 2048) {
        imagebuffer[clrgfx] = 0;
        clrgfx++;
    } // clear image buffers
    ImageDecoderInit();
    FSfseek(pointer, eyedeejpgpos, SEEK_SET);
    ImageDecode(pointer, IMG_JPEG, 0, 0, 64, 64, (IMG_ALIGN_CENTER | IMG_DOWN_SCALE), NULL, PixelOutput);
    if (jpgdecstatus != 0) {
        clrframebuffer();
        drawscreen();
    } // jpgdecstatus
    // figure out the best of the four mono decoded images
    clrgfx = 0;
    while (clrgfx < 4) {
        jpgpixcount[clrgfx] = abs(jpgpixcount[clrgfx] - 1300);
        clrgfx++;
    }
    // the lowest value should now be the best image
    int foo = jpgpixcount[0];
    jpgbestmono = 0;
    n = 1;
    while (n < 4) { // update for version 1.0.118
        if (jpgpixcount[n] < foo) {
            foo = jpgpixcount[n];
            jpgbestmono = n;
        }
        n++;
    } // n
    // done finding best mono decoded image
    unsigned char jpegtime = 100;
    unsigned char positionjpeg = 0;
    while (jpegtime > 0 && jpgdecstatus != 0) {
        clrgfx = 0;
        char animframe = 0;
        if (jpegtime > 96 || jpegtime < 3) {
            animframe = 0;
        } // 100-97,2,1
        // show image in greyscale
        if (csgreyscale == 1) {
            if (jpegtime > 2 && jpegtime < 97) { // 3 - 96
                animframe = bitplane;
            } // jpegtime
        } else { // or fade into best mono decoded image
            if (jpegtime > 92 && jpegtime < 97) {
                animframe = 1;
            } // 93-96
            if (jpegtime > 88 && jpegtime < 93) { // 89-92
                if (jpgbestmono > 1) {
                    animframe = 2;
                } else {
                    animframe = 1;
                } // update for version 1.0.144
            }
            if (jpegtime > 6 && jpegtime < 89) { // 7 - 88
                if (jpgbestmono < 4) {
                    animframe = jpgbestmono;
                } else {
                    animframe = 1;
                } // update for version 1.0.144
            }
            if (jpegtime > 4 && jpegtime < 7) { // 5,6
                if (jpgbestmono > 1) {
                    animframe = 2;
                } else {
                    animframe = 1;
                } // update for version 1.0.144
            }
            if (jpegtime > 2 && jpegtime < 5) {
                animframe = 1;
            } // 3,4
        } // csgreyscale
        //
        // updated for version 1.0.63
        foo = 0; // animframe = 3
        if (animframe == 0) {
            foo = 1088;
        }
        if (animframe == 1) {
            foo = 1024;
        }
        if (animframe == 2) {
            foo = 64;
        }
        while (clrgfx < 1024) {
            framebuffer[clrgfx + 32] = imagebuffer[clrgfx + foo];
            clrgfx++;
            positionjpeg++;
            if (positionjpeg > 63) {
                positionjpeg = 0;
                clrgfx = clrgfx + 64;
            }
        } // clrgfx
        //
        drawscreen();
        bitplane++;
        if (bitplane > 3) {
            bitplane = 1;
        }
        jpegtime--;
        LED_PIN = 1;
    }
    backlighttimer = backlighttimer + 280; // extend backlight timer
    clrframebuffer();
    drawscreen();
    if (jpgdecstatus == 0) {
        clearartwork();
    } // jpgdecstatus
}
//

void PixelOutput(IMG_PIXEL_XY_RGB_888 *pPix) {
    if (pPix->X <= 63 && pPix->Y <= 63) { // mono decoded images
        imageselect = 0; // update for version 1.0.215
        x = pPix->X;
        y = pPix->Y; // decoded mono image 3
        if (pPix->R < 162) {
            setpixelimage();
            jpgpixcount[3]++;
            jpgdecstatus = 1;
        }
        x = pPix->X + 64; // decoded mono image 2
        if (pPix->R < 82) {
            setpixelimage();
            jpgpixcount[2]++;
        }
        imageselect = 1024;
        x = pPix->X; // decoded mono image 1
        if (pPix->R < 62) {
            setpixelimage();
            jpgpixcount[1]++;
        }
        x = pPix->X + 64; // decoded mono image 0
        if (pPix->R < 32) {
            setpixelimage();
            jpgpixcount[0]++;
        }
    }
    imageselect = 0;
    if (pPix->X == 31) { // draw decode progress animation
        clrframebuffer();
        // draw preview border square
        drawrectangle(32, 0, 63, 63); // update for version 1.0.168
        unsigned char jpegline = 0;
        unsigned char currentcircle = 0;
        while (jpegline < pPix->Y) {
            currentcircle++;
            if (currentcircle > 7) {
                currentcircle = 0;
            }
            jpegline++;
        } // jpegline
        //
        rotation = 0;
        while (rotation < 8) {
            if (rotation == currentcircle) {
                radius = 5;
            } else {
                radius = 4;
            }
            cx = xrotp[rotation] + 4;
            cy = yrotp[rotation] + 4;
            drawcircle();
            if (rotation == currentcircle) {
                radius = 6;
            } else {
                radius = 5;
            }
            drawcircle();
            y = cy - 4;
            x = cx + 2;
            setpixel(); // update for version 1.0.202
            x = x - 4;
            setpixel(); // fill some missing pixels
            y = y + 8;
            setpixel();
            x = x + 4;
            setpixel();
            x = x + 2;
            y = y - 2;
            setpixel();
            y = y - 4;
            setpixel();
            x = x - 8;
            setpixel();
            pixelworkprivateh();
            rotation++;
        } // rotation
        drawscreen();
    } // pPix->X
}
//
// clear the previous album artwork
// for when new file has no artwork

void clearartwork() {
    int clrgfx = 0;
    while (clrgfx < 2048) {
        imagebuffer[clrgfx] = 0;
        clrgfx++;
    }
    jpgbestmono = 3;
}
//
// copy a time to text buffer for player display

void mptimecopy(int at, int bt) { // update for version 1.0.175
    sprintf(text, "%02d:%02d", at, bt);
}
//
// mp3 player shuffle feature
// shuffles up to 3525 file indexes like a deck of cards
// ensures all files are included and none are repeated

void mpfileshuffle() {
    unsigned int randa = 0;
    unsigned int randb = 0;
    sp2 = 0;
    WORD wordcount = 0;
    unsigned long int nextr; // set random seed
    if (tcount < 90) {
        nextr = sin_table[tcount] + wordrandom; // update for version 1.0.192
    } else {
        nextr = (0xABCD ^ TMR1) + wordrandom; // update for version 1.0.192
    } // count
    nextr = nextr + (sinindex + (secondschanged * 5));
    nextr = nextr + TMR1;
    nextr = nextr + 0xBFE001 + my + mx;
    // clear screen
    clrframebuffer();
    drawscreen();
    drawshufflegraphic();
    while (sp2 < (filecount * 2) && sp2 < 7050) { // populate shuffle array
        voice[sp2 + 0] = (wordcount & 0b1111111100000000) >> 8;
        voice[sp2 + 1] = wordcount & 0b0000000011111111;
        sp2 = sp2 + 2;
        wordcount++;
    } // sp2
    // voice array should now be populated with incremented values
    // sp2 should contain the number of files multiplied by two 0-7050
    //
    sp3 = 0; // shuffle the array
    if (mpshuffle != 0) {
        while (sp3 < wordcount) {
            randa = nextr % wordcount;
            randb = sp3;
            pl1 = voice[(randa * 2) + 0]; // swap a pair of 16 bit elements
            pl2 = voice[(randa * 2) + 1];
            voice[(randa * 2) + 0] = voice[(randb * 2) + 0];
            voice[(randa * 2) + 1] = voice[(randb * 2) + 1];
            voice[(randb * 2) + 0] = pl1;
            voice[(randb * 2) + 1] = pl2;
            sp3++;
            nextr = nextr * 1103515245 + 12345;
        }
    } // mpshuffle
    clrframebuffer();
    drawscreen();
}
//

int retrieveshuffle(int value) {
    int result = 0;
    WORD wbuff = 0;
    wbuff = voice[(value * 2) + 0];
    wbuff = wbuff << 8;
    wbuff = wbuff + voice[(value * 2) + 1];
    result = wbuff;
    if (result > filecount - 1) {
        result = 0;
    } // error checking
    if (result < 0) {
        result = filecount - 1;
    } //
    return result;
}
//
// static screen to display shuffling graphic

void drawshufflegraphic() { // update for version 1.0.164
    int clrgfx = 0;
    int cpointa = 0;
    unsigned char cubex = 33;
    unsigned char cubey = 2;
    clrframebuffer();
    drawroundedrectangle(32, 0, 63, 63); // draw icon border
    x = 37 + cubex;
    y = 19 + cubey;
    setpixel(); // touch ups
    y = 40 + cubey;
    setpixel();
    x++;
    y = 20 + cubey;
    setpixel();
    y = 39 + cubey;
    setpixel();
    x = 24 + cubex;
    y = 19 + cubey;
    setpixel();
    y = 40 + cubey;
    setpixel();
    x--;
    y = 20 + cubey;
    setpixel();
    y = 39 + cubey;
    setpixel();
    cpointa = 52 + cubex;
    drawline(cpointa, 37 + cubey, cpointa + 2, 39 + cubey);
    drawline(cpointa, 44 + cubey, cpointa + 2, 42 + cubey);
    drawline(cpointa, 15 + cubey, cpointa + 2, 17 + cubey);
    drawline(cpointa, 22 + cubey, cpointa + 2, 20 + cubey);
    while (clrgfx < 4) {
        if (clrgfx == 1) {
            cubex++;
        }
        if (clrgfx == 2) {
            cubey++;
        }
        if (clrgfx == 3) {
            cubex--;
        }
        drawline(6 + cubex, 18 + cubey, 22 + cubex, 18 + cubey);
        drawline(6 + cubex, 40 + cubey, 22 + cubex, 40 + cubey);
        drawline(38 + cubex, 18 + cubey, 54 + cubex, 18 + cubey);
        drawline(38 + cubex, 40 + cubey, 54 + cubex, 40 + cubey);
        drawline(24 + cubex, 20 + cubey, 28 + cubex, 24 + cubey);
        drawline(24 + cubex, 38 + cubey, 28 + cubex, 34 + cubey);
        drawline(36 + cubex, 20 + cubey, 32 + cubex, 24 + cubey);
        drawline(36 + cubex, 38 + cubey, 32 + cubex, 34 + cubey);
        x = 28 + cubex;
        y = 26 + cubey;
        setpixel();
        y = 32 + cubey;
        setpixel();
        x = 32 + cubex;
        y = 26 + cubey;
        setpixel();
        y = 32 + cubey;
        setpixel();
        x = x - 2;
        y = 28 + cubey;
        setpixel();
        pixelworkprivatee();
        x = 50 + cubex;
        y = 14 + cubey;
        setpixel();
        pixelworkprivatee();
        pixelworkprivateh();
        pixelworkprivatee();
        y = 36 + cubey;
        setpixel();
        pixelworkprivatee();
        pixelworkprivateh();
        pixelworkprivatee();
        x = 52 + cubex;
        y = 16 + cubey;
        setpixel();
        y = 20 + cubey;
        setpixel();
        y = 38 + cubey;
        setpixel();
        pixelworkprivateh();
        clrgfx++;
    } // clrgfx
    drawscreen();
    delay_gen(800);
}
//
// counting files, caching, and re-caching files animation
// should be called with argument 8 for files stack up, or
// 40 for files already stacked and just the magnifier moves.

void drawcountmpgraphic(int gfiles) {
    int clrgfx = 0;
    unsigned char totalfiles = gfiles;
    unsigned char files = 0;
    unsigned char fcount = 0;
    unsigned char cubex = 0;
    unsigned char cubey = 0;
    unsigned char magx = 0;
    int magy = 0;
    int gcnta = 0;
    int gcntb = 0;
    if (gfiles == 40) {
        fcount = 23;
    } // non blocking for re-cache
    while (fcount < 24) {
        clrframebuffer();
        drawroundedrectangle(32, 0, 63, 63); // draw icon border
        clrgfx = 0;
        cubex = 34;
        cubey = 3; // 2
        while (clrgfx < 4) {
            if (clrgfx == 1) {
                cubex++;
            }
            if (clrgfx == 2) {
                cubey++;
            }
            if (clrgfx == 3) {
                cubex--;
            }
            cubey = cubey + (40 - totalfiles);
            drawline(6 + cubex, 4 + cubey, 30 + cubex, 4 + cubey); // draw top file
            drawline(30 + cubex, 4 + cubey, 40 + cubex, 14 + cubey);
            drawline(6 + cubex, 4 + cubey, 16 + cubex, 14 + cubey);
            drawline(16 + cubex, 14 + cubey, 40 + cubex, 14 + cubey);
            files = 4; // increment to 4x the number of files underneath the top file
            while (files < totalfiles) {
                drawline(6 + cubex, 4 + cubey + files, 16 + cubex, 14 + cubey + files);
                drawline(16 + cubex, 14 + cubey + files, 40 + cubex, 14 + cubey + files);
                drawline(6 + cubex, 4 + cubey + files, 10 + cubex, 4 + cubey + files);
                drawline(40 + cubex, 14 + cubey + files, 36 + cubex, 10 + cubey + files);
                files = files + 4;
            } // files
            cubey = cubey - (40 - totalfiles);
            clrgfx++;
        } // clrgfx
        if (totalfiles < 40) {
            totalfiles++;
        }
        clrgfx = 0;
        magx = 32;
        magy = fcount - 7;
        drawline(56 + magx, 43 + magy, 50 + magx, 37 + magy);
        drawline(55 + magx, 44 + magy, 49 + magx, 38 + magy);
        while (clrgfx < 4) {
            if (clrgfx == 1) {
                magx = magx + 1;
            }
            if (clrgfx == 2) {
                magy = magy + 1;
            }
            if (clrgfx == 3) {
                magx = magx - 1;
            }
            gcnta = 56 + magx;
            gcntb = 44 + magy; // update for version 1.0.163
            while (gcnta > (47 + magx)) { // magnifier handle 
                x = gcnta;
                y = gcntb;
                setpixel();
                gcnta = gcnta - 2;
                gcntb = gcntb - 2;
            } // gcnta
            radius = 8;
            cx = 42 + magx;
            cy = 30 + magy;
            drawcircle();
            x = 42 + magx;
            y = 30 + magy;
            clearpixel();
            pixelworkprivatef();
            pixelworkprivatef();
            pixelworkprivatef();
            y = 32 + magy;
            clearpixel();
            pixelworkprivateg();
            pixelworkprivateg();
            x = 40 + magx;
            y = 30 + magy;
            clearpixel();
            pixelworkprivatef();
            pixelworkprivatei();
            y = 26 + magy;
            clearpixel();
            y = 34 + magy;
            clearpixel();
            y = y - 10;
            clearpixel();
            y = 36 + magy;
            clearpixel();
            x = 38 + magx;
            y = 30 + magy;
            clearpixel();
            pixelworkprivatef();
            pixelworkprivatei();
            y = 26 + magy;
            clearpixel();
            y = 34 + magy;
            clearpixel();
            x = 36 + magx;
            y = y - 4;
            clearpixel(); // update for version 1.0.219
            pixelworkprivatef();
            pixelworkprivatei();
            x = 38 + magx;
            pixelworkprivateg();
            x++;
            y++;
            clearpixel();
            clrgfx++;
        } // clrgfx
        x = 39 + magx;
        y = 24 + magy;
        clearpixel();
        x = x - 2;
        y = y + 9;
        clearpixel(); // update for version 1.0.219
        x = 57 + magx;
        y = 44 + magy;
        clearpixel();
        drawscreen();
        fcount++;
    } // fcount
}
//

void drawmpscreen(int shuffnum) {
    int clrgfx = 0;
    sendclrspectrum();
    sendclrspectrum();
    sendclrspectrum();
    sendclrspectrum();
    clrframebuffer();
    drawroundedrectangle(0, 0, 67, 26); // draw time border
    // draw a flat dummy spectrum analyser
    unsigned char stretch = 0;
    for (clrgfx = 0; clrgfx < 14; clrgfx++) {
        y = 25;
        x = 71 + stretch;
        setpixel();
        stretch++;
        x = 71 + stretch;
        setpixel();
        stretch++;
        x = 71 + stretch;
        setpixel();
        stretch = stretch + 2;
    } // clrgfx
    drawmpprogress();
    //
    ty = 30;
    if (mpshuffle != 0) {
        unsigned char syoff = 1; // shuffle symbol offset
        while (syoff < 3) { // update for version 1.0.186
            drawline(54, syoff + 30, 60, syoff + 30); // left lines
            drawline(54, syoff + 41, 60, syoff + 41); //
            drawline(66, syoff + 30, 72, syoff + 30); // right lines
            drawline(66, syoff + 41, 72, syoff + 41); //
            drawline(60, syoff + 30, 66, syoff + 41); // cross part
            drawline(60, syoff + 41, 66, syoff + 30); //
            syoff++;
        } // syoff
        //
        tx = 1;
        sprintf(text, "%04d", fnumber + 1);
        drawtextlarge();
        tx = 77;
        sprintf(text, "%04d", shuffnum + 1);
        drawtextlarge();
    } else { // mpshuffle
        sprintf(text, "<< %04d <<", fnumber + 1);
        unsigned char mbyte = 0x3E; // update for version 1.0.165
        if (mpskip == 0) {
            mbyte = 0x20;
        }
        if (mpskip == 0 || mpskip == 1) {
            text[0] = mbyte;
            text[1] = mbyte;
            text[8] = mbyte;
            text[9] = mbyte;
        } // mpskip
        tx = 3;
        drawtextlarge();
    } // mpshuffle
    //
    drawroundedrectangle(0, 28, 127, 17); // draw scroller border
    drawscreen();
    if (fnumber < 1001) { // update for version 1.0.76
        delay_gen(1100 - fnumber); // delay for files that are quick to find
    } // mpshuffle
}
//

void cachefilename(int index) { // update for version 1.0.36
    if (demo == 3 && mpshuffle == 1) {
        return;
    } // shuffle needs this memory
    if (index > 880) {
        return;
    } // check array bounds
    int fncharacters = 0; //
    while (fncharacters < 8) { // write eight character record
        voice[(index * 8) + fncharacters] = rec.filename[fncharacters];
        fncharacters++;
    } // fncharacters
}
//

void retrievefilename(int index) { // update for version 1.0.36
    int fncharacters = 0;
    while (fncharacters < 8) { // retrieve eight character record
        rec.filename[fncharacters] = voice[(index * 8) + fncharacters];
        fncharacters++;
    } // fncharacters
    fncharacters = 0; // restore the file extension
    while (fncharacters < 8 && rec.filename[fncharacters] != 0x2E) {
        fncharacters++;
    }
    rec.filename[fncharacters] = 0x2E;
    fncharacters++; // "."
    rec.filename[fncharacters] = 0x4D;
    fncharacters++; // "M"
    rec.filename[fncharacters] = 0x50;
    fncharacters++; // "P"
    if (demo == 3) {
        rec.filename[fncharacters] = 0x33; // "3"
    } else { // to support mpi files
        rec.filename[fncharacters] = 0x49; // "I"
    } // demo
    fncharacters++;
    rec.filename[fncharacters] = 0; //
}
//

int getstartofrange() {
    int multiplier = voice[7049];
    int fstart = 881 * multiplier;
    return fstart;
}
//
// find jpeg cover image in standard mp3 tags

void stdimagetag() { // update for version 1.0.39
    int mmindex = 0;
    int index = 0;
    unsigned char imgmatch = 0;
    if (eyedeejpgpos == 0) {
        for (index = 0; index < 1024; index++) { // search chunk for image
            if (imagebuffer[index + 0] == 0x50) { // "P"
                if (imagebuffer[index + 1] == 0x49 && imagebuffer[index + 2] == 0x43) { // "IC"
                    mmindex = index + 8;
                    while (mmindex < index + 30) {
                        imgmatch = 1;
                        unsigned char jfifcnt = 0; // update for version 1.0.161
                        while (jfifcnt < 4) {
                            if (imagebuffer[mmindex + jfifcnt] != jfif[jfifcnt]) {
                                imgmatch = 0;
                            }
                            jfifcnt++;
                        } // jfifcnt
                        if (imgmatch == 1) {
                            eyedeejpgpos = index + 8;
                            mmindex = index + 30;
                        }
                        mmindex++;
                    } // mmindex
                    if (imgmatch == 1) {
                        index = 1024;
                    } // exit loop
                }
            } // imagebuffer
        } // index
    } // eyedeejpgpos
}
//
// check if the vs1003b decoder chip exists and is working
// first by timing how long it's dreq pin is high if at all
// second by checking the value returned from mode register
// check for impossible sd card socket condition of apparent
// card socket empty but write protect set and for impossible
// joystick combinations such as left and right both set
// backlight led will flash if any problems are found and
// execution will not be allowed to continue any further

void hardware_sanity_check() { // update for version 1.0.161
    unsigned char error = 0;
    unsigned char busytime = 0xFF;
    unsigned char readretries = 0x03;
    unsigned char regista = 0;
    unsigned char registb;
    shiftbytein();
    if (joysparec == 0 && joyspareb == 1) {
        error++;
    } // check card
    if (joyleft + joyright == 2) {
        error++;
    }
    SD_CS = 1; // deselect sd card
    SPISTAT = 0;
    delay_gen(0);
    while (VSDREQ == 0 && busytime > 0) {
        delay_gen(10);
        busytime--;
    } // test dreq pin
    if (busytime > 0) { // read mode register only if passed the first test
        while (readretries > 0 && regista != 0x08) { // exit loop on success
            while (VSDREQ == 0) {
                delay_gen(0);
            }
            VSXCS = 0; // select vs1003 instruction
            WriteSPIManual(0x03); // read
            WriteSPIManual(0x00); // mode
            regista = ReadSPIManual();
            registb = ReadSPIManual();
            VSXCS = 1; // deselect vs1003 instruction
            delay_gen(0);
            SPISTAT = 0x8000;
            SD_CS = 0; // select sd card
            delay_gen(5);
            readretries--;
        } // readretries
    } // busytime
    if (error > 0) {
        lasterror = 1;
    } // joystick shift register error
    if (busytime == 0) {
        lasterror = 23;
        error++;
    } // vs1003b not ready
    if (regista != 0x08) {
        lasterror = 24;
        error++;
    } // vs1003b not detected
    if (error != 0) { // at least one error occurred
        while (1) { // prevent execution
            sanityprivatea(); // print fatal error string
            if (lasterror > 22) { // update for version 1.0.164
                sprintf(text, "MP3 DECODER CHIP");
            } else { // joystick or card switch problem
                sprintf(text, "JOY/CARD SWITCHES");
            } // lasterror
            drawtextvarwidthcentered();
            sanityprivateb(); // print session halted string
            drawscreen();
            delay_gen(lasterror * 100); // update for version 1.0.163
        } // error
    } // error
}
//
// check for presence of consolidated rom file
// and a basic check it is the correct rom version
// this is where the rom file is first opened
// also checks the integrity of some program data

void software_sanity_check() { // update for version 1.0.161 
    int clrgfx = 1024;
    int resultf = 0;
    unsigned char romerror = 0;
    if (!sdcardinit()) {
        romerror++;
    }
    if (FSchdir("ROM") != 0) {
        romerror++;
    }
    // rom directory was present
    resultf = FindFirst("FREEDOM.ROM", 0x2F, &rec);
    if (resultf != 0) { // no rom file
        romerror++;
    } else { // update for version 1.0.154
        if (rec.filesize != 400480) {
            romerror++;
        } // size check
        pointer = FSfopen(rec.filename, "r");
    } // resultf
    if (pointer == NULL) {
        romerror++;
    } // no rom file
    if (romerror > 0) {
        lasterror = 35; // update for version 1.0.197
        while (clrgfx == 1024) { // show message and prevent execution
            sanityprivatea(); // print fatal error string
            sprintf(text, "FREEDOM.ROM");
            drawtextvarwidthcentered();
            ty = 37;
            sprintf(text, "FILE NOT FOUND");
            drawtextvarwidthcentered();
            sanityprivateb();
            drawscreen();
            delay_gen(20);
        } // clrgfx
    } // romerror
    clrgfx = 0;
    romerror = 0x3F; // used as checksum
    while (clrgfx < 90) { // update for version 1.0.200
        romerror = romerror ^ charmap[clrgfx];
        romerror = romerror ^ poiicons[clrgfx];
        romerror = romerror ^ blop[clrgfx];
        romerror = romerror ^ cube[clrgfx];
        clrgfx++;
    } // clrgfx
    if (romerror != 197) { // update for version 1.0.200
        debugout(36); // bad program data checksum
    } // romerror
    while (romerror != 197) { // update for version 1.0.201
        sanityprivatea(); // print fatal error string
        sprintf(text, "MICROCONTROLLER");
        drawtextvarwidthcentered();
        ty = 37;
        sprintf(text, "DATA CORRUPT");
        drawtextvarwidthcentered();
        sanityprivateb(); // print session halted string
        drawscreen();
    } // romerror
}
//
// private function for sanity check functions
// solely to save program memory storing twice

void sanityprivatea() { // update for version 1.0.109
    drawroundedborder(); // draw border
    ty = 21;
    sprintf(text, "FATAL ERROR!"); // update for version 1.0.204
    drawtextvarwidthcentered();
    ty = 29;
}
//
// private function for sanity check functions
// solely to save program memory storing twice

void sanityprivateb() {
    ty = 45;
    sprintf(text, "SESSION HALTED");
    drawtextvarwidthcentered();
}
//
// full motion video player for intro

void full_motion_video() { // update for version 1.0.43
    int clrgfx = 0;
    int videoframes = 0;
    if (pointer != NULL) { // rom file should already be open
        FSfseek(pointer, 2048 + 60416, SEEK_SET);
        // rom file position should now be correct
        clrframebuffer();
        JOYPAR_PIN = 0;
        drawscreen();
        delay_gen(20); // delay on clear screen
        // 1196 frames for alternate test spinning globe video
        while (videoframes < 324 / videointro) { // play number of frames
            // skip intro video for joystick left - update for version 1.0.69
            if (JOYDAT_PIN == 0 || videoframes == 323) {
                JOYPAR_PIN = 1;
                videoframes = 324;
            } // update for version 1.0.133
            FSfread(imagebuffer, 1, 1024, pointer);
            if (videointro != 1) {
                FSfseek(pointer, 1024, SEEK_CUR);
            } // skip frame
            // frame should be in buffer
            clrgfx = 0; // invert frame
            while (clrgfx < 1024) {
                framebuffer[clrgfx] = imagebuffer[clrgfx] ^ 0xFF;
                clrgfx++;
            }
            drawroundedrectangle(0, 0, 127, 63); // draw screen border
            drawscreen(); // draw frame
            videoframes++;
        } // videoframes
        // leave the rom file open
    } // pointer
    drawscreen(); // update for version 1.0.219
}
//
// graphic main menu transition animation
// transitions from menu to a program and back
// this is a blocking function for fifteen frames
// argument 0 is to transition from menu to program
// argument 1 is to transition from program to menu

void menutransition(int dir) { // update for version 1.0.105
    int n = 0;
    int trcount = 0;
    if (dir == 0) {// sanity check menu variables are in valid states
        while (n < 5) {
            springmenuvars[n] = menuvars[n];
            n++;
        } // update for version 1.0.120
        n = 0;
        while (menuvars[n] != 17) {
            n++;
        } // find selected item
        while (trcount < 5) { // update for version 1.0.108
            menuvars[n] = lpositions[trcount];
            trcount++;
            n++;
            if (n > 4) {
                n = 0;
            }
        } // trcount
        trcount = 0;
    } // dir
    while (trcount < 15) {
        clrframebuffer();
        n = 0;
        while (n < 5) { // move upper and lower buttons up or down
            if (dir == 1) {
                if (menuvars[n] > 17) {
                    menuvars[n]--;
                } // in
                if (menuvars[n] < 17) {
                    menuvars[n]++;
                } //
            } else {
                if (menuvars[n] < 17) {
                    menuvars[n]--;
                } // out
                if (menuvars[n] > 17) {
                    menuvars[n]++;
                } //
            } // dir
            n++;
        } // n
        n = 0;
        while (n < 5) {
            if (menuvars[n] == 17) {
                drawroundedrectangle(0, menuvars[n], 127, 29); // draw button border
            } else {
                drawroundedrectangle(1, menuvars[n], 125, 29); // draw button border
            }
            clrfb = 0;
            while (clrfb < 10) {
                text[clrfb] = programs[clrfb + (n * 10)];
                clrfb++;
            }
            text[10] = 0; // terminate string
            tx = 3;
            ty = 4 + menuvars[n];
            if (menuvars[n] == 17) {
                if (dir != 0) { // in
                    if (trcount > 0) {
                        drawtextlargesin();
                    } else {
                        ty = ty + 4;
                        drawtextlarge();
                    }
                } else { // out
                    if (trcount > 13) {
                        ty = ty + 4;
                        drawtextlarge();
                    } else {
                        drawtextlargesin();
                    }
                } // dir
            } else {
                ty = ty + 4;
                drawtextlarge();
            }
            n++;
        }
        drawscreen();
        trcount++;
    } // trcount
    if (dir == 1 && springmenuvars[0] != 999) { // update for version 1.0.120
        n = 0;
        while (n < 5) {
            menuvars[n] = springmenuvars[n];
            n++;
        } // restore the values
    } // dir
    if (dir == 1) {
        menuspringphysics(0);
    } // update for version 1.0.193
}
//
// draw hollow rectangle using four lines
// graphics library update for version 1.0.48

void drawrectangle(int posx, int posy, int lenx, int leny) {
    drawline(posx, posy, posx + lenx, posy); // top line
    drawline(posx, posy + leny, posx + lenx, posy + leny); // bottom line
    drawline(posx, posy, posx, posy + leny); // left line
    drawline(posx + lenx, posy, posx + lenx, posy + leny); // right line
}
//
// draw filled circle using horizontal lines
// this is slow and intended for static display

void drawfilledcircle(int quality) { // update for version 1.0.192
    int lcxa = 0;
    int lcya = 0;
    int crot = 1; // zero to draw a single top pixel
    ox = cx;
    oy = cy; // set origin
    while (crot < 180) { // 181 to draw a single bottom pixel
        rotation = crot;
        x = cx;
        y = cy - radius;
        pixelrotation();
        lcxa = x;
        lcya = y;
        rotation = 360 - crot;
        x = cx;
        y = cy - radius;
        pixelrotation();
        drawline(lcxa, lcya, x, y);
        // the lower this number the better the fill
        // and the longer it takes to draw the circle
        // larger circles need the number to be small
        // crot = crot + 13; // minumum for radius 6
        // crot = crot + 4; // minumum for radius 14
        if (quality == 0) { // take a chance
            crot = crot + radius + radius + 1; // auto
        } else { // set quality as input argument
            crot = crot + quality; // minumum for radius
        } // quality
    } // crot
}
//
// draw a rectangle outline filled with clear
// rectangle outline is drawn with four lines
// similar to the draw hollow rectangle function
// except pixels inside the rectangle are erased
// graphics library update for version 1.0.49

void drawrectanglefilledclear(int posx, int posy, int lenx, int leny) {
    drawline(posx, posy, posx + lenx, posy); // top line
    drawline(posx, posy + leny, posx + lenx, posy + leny); // bottom line
    drawline(posx, posy, posx, posy + leny); // left line
    drawline(posx + lenx, posy, posx + lenx, posy + leny); // right line
    y = posy + 1;
    while (y < posy + leny) { // raster fill with clear
        x = posx + 1; // reset start for new line
        while (x < posx + lenx) {
            clearpixel();
            x++;
        } // line
        y++;
    } // y
}
//
// draw folder graphic for folder not found error
// and also for possible expansion of file browser
// to support sub directories in music

void draw_folder(int fx, int fy) { // update for version 1.0.86
    drawrectangle(fx, fy + 2, 7, 5); // main part
    drawrectangle(fx, fy, 5, 2); // tab part
    x = fx + 5;
    y = fy;
    clearpixel();
}
//
// activity bar for file counting or caching
// just to show the program has not frozen
// this borrows some text string variables

void drawsearchactivity() { // update for version 1.0.216
    int dlarg = 2;
    while (dlarg < 6) {
        drawline(54, dlarg, 74, dlarg); // upper and lower borders
        dlarg = dlarg + 3;
    } // dlarg
    drawline(53, 3, 53, 4); // left side
    drawline(75, 3, 75, 4); // right side
    x = text[120]; // erase old position
    y = 3;
    clearpixel();
    y++;
    clearpixel();
    x++;
    y = 3;
    clearpixel();
    y++;
    clearpixel();
    if (text[121] == 0) {
        text[120]++;
        if (text[120] > 72) {
            text[121] = 1;
        }
    } else {
        text[120]--;
        if (text[120] < 55) {
            text[121] = 0;
        }
    } // text[121]
    x = text[120]; // draw new position
    y = 3;
    setpixel();
    y++;
    setpixel();
    x++;
    y = 3;
    setpixel();
    y++;
    setpixel();
    drawscreen();
}
//
// graphic transition into or out of astro time program

void astrotransition(int dir) { // update for version 1.0.205
    unsigned char transcount = 0;
    unsigned char tframecount = 0;
    unsigned char transpring = 0;
    if (dir == 0) {
        transcount = 40;
    }
    cx = 15;
    cy = 18;
    while (tframecount < 41) {
        clrframebuffer();
        if (transcount > 7) {
            transpring = mpspringtable[transcount - 8] + 8;
            int tworects = 0; // update for version 1.0.216
            while (tworects < 20) { // time and date rectangles
                drawroundedrectangle(73 - transpring, tworects, 14 + transpring + transpring, 17);
                tworects = tworects + 19;
            } // tworects
            drawroundedrectangle(40 - transpring, 38, 47 + transpring + transpring, 25); // astronomy rectangle
            radius = transpring / 2; // analogue clock face
            if (radius > 15) {
                radius = 15;
            } // enforce clock size limit
            drawcircle();
        } // transcount
        drawscreen();
        if (dir != 0) {
            transcount++;
            GPSPWR_PIN = 0;
        } else {
            transcount--;
        }
        tframecount++;
    } // tframecount
    if (dir != 0) { // for transition from menu to program
        clrframebuffer(); // create solid circle image sprites
        int circlecnt = 0; // update for version 1.0.216
        int cirsizes = 14; // solid circles
        int twocircles = 0;
        while (twocircles < 2) {
            circlecnt = 15;
            while (circlecnt < 106) {
                cx = circlecnt;
                cy = 50;
                radius = cirsizes;
                drawfilledcircle(4);
                circlecnt = circlecnt + 30;
                cirsizes--;
            } // circlecnt
            twocircles++;
        } // twocircles
        y = 2; // create checkerboard pattern
        while (y < 63) {
            x = 0;
            while (x < 127) {
                clearpixel();
                x = x + 2;
            }
            y++;
            x = 1;
            while (x < 127) {
                clearpixel();
                x = x + 2;
            }
            y++;
        } // y
        twocircles = 18; // update for version 1.0.216
        while (twocircles < 51) { // circle outlines
            circlecnt = 15;
            while (circlecnt < 106) {
                cx = circlecnt;
                cy = twocircles;
                radius = 15;
                drawcircle();
                circlecnt = circlecnt + 30;
            } // circlecnt
            twocircles = twocircles + 32;
        } // twocircles
        circlecnt = 0; // use image buffer as sprite table
        while (circlecnt < 1024) { // copy to image buffer
            imagebuffer[circlecnt] = framebuffer[circlecnt];
            circlecnt++;
        } // circlecnt
    } // dir
    firstgpsdata = 0;
}
//
// read registers to display reason for unexpected reset
// delays program execution to allow display to be read
// includes spring physics graphics transition effects

void reset_register_check() { // update for version 1.0.211
    unsigned int regvalueb = 0;
    int regerror = 0;
    unsigned char balldir = 0;
    int reframes = 0;
    msine = 0;
    radius = 6;
    regvalueb = RCON; // read reset register
    RCON = 0; // clear rcon after read
    if (regvalueb != 3) {
        regerror++;
    }
    if (lasterror != 0) {
        regerror++;
    } // update for version 1.0.144
    if (regerror > 0) { // found at least one unexpected value
        samplenum = 6;
        load_sample();
        playvoice(); // play sample
        vlen = 1850; // reset intro sample length
        regerror = -64;
        while (reframes < 1200) { // show first message and delay execution
            clrframebuffer();
            drawroundedrectangle(0, 0 - regerror, 127, 63); // moving screen border
            sprintf(text, "CRASH RESET");
            ty = 21 - regerror;
            drawtextvarwidthcentered();
            sprintf(text, "DEBUG: %02d", lasterror); // update for version 1.0.204
            if (regvalueb == 0x02) { // check for brown out reset
                sprintf(text, "BROWN OUT"); // updat for version 1.0.204
            } else { // not a brown out reset
                ty = 29 - regerror;
                drawtextvarwidthcentered();
                sprintf(text, "RESET: %04X", regvalueb);
            } // rcon
            ty = 37 - regerror;
            drawtextvarwidthcentered();
            if (balldir == 0) { // update for version 1.0.119
                msine = msine + 5;
                if (msine > 84) {
                    balldir = 1;
                }
            } else {
                msine = msine - 5;
                if (msine < 5) {
                    balldir = 0;
                }
            } // balldir
            if (regerror == 0) {
                cx = 12;
                debugprivate(msine);
                regerror = 87 - msine;
                cx = 116;
                debugprivate(regerror);
            } // regerror
            drawscreen();
            reframes++;
            regerror = 0;
            if (reframes > 1168) {
                regerror = (mpspringtable[reframes - 1169]*2);
            }
            if (reframes < 33) {
                regerror = 0 - (mpspringtable[32 - reframes]*2);
            }
        } // clrgfx
        if (reframes > 1199) { // check for reset
            reframes = 0; // second reset screen counter
            regerror = 64; // vertical position
            while (reframes < 33) { // update for version 1.0.211
                clrframebuffer();
                drawroundedrectangle(0, 0 + regerror, 127, 63); // moving screen border
                drawscreen();
                reframes++;
                regerror = 0;
                regerror = (mpspringtable[32 - reframes]*2); // spring into position
            } // reframes
        } // reframes
    } // regerror
}
//
// private function for reset register check function
// calculation for bouncing ball positions

void debugprivate(unsigned char p) { // update for version 1.0.162
    jheld = sin_table[p];
    jheld = jheld + (msine / 13);
    jheld = jheld / 300;
    cy = jheld;
    cy = 61 - cy;
    drawfilledcircle(0);
}
//
// run time configuration interface
// overrides configuration settings

void runtime_config() { // update for version 1.0.51
    unsigned char playswitch = 0;
    while (joyfire == 0) { // show configuration screen
        drawroundedborder(); // draw screen border
        playswitch = 0; // reset sound flag
        radius = 6; // set circle radius
        drawroundedrectangle(101, 5, 24, 12); // draw switch outline
        tx = 4;
        ty = 8;
        if (my > 4 && my < 4 + 12) {
            if (joyleft == 1 && mpshuffle != 0) {
                mpshuffle = 0;
                playswitch = 1;
                tx--;
            }
            if (joyright == 1 && mpshuffle == 0) {
                mpshuffle = 1;
                skipbrowser = 1;
                playswitch = 1;
                tx++;
            }
        } // shuffle
        sprintf(text, "MUSIC SHUFFLE");
        drawtextvarwidth();
        if (mpshuffle == 0) {
            cx = 107;
        } else {
            cx = 119;
        }
        cy = 5 + 6;
        drawfilledcircle(0); // draw switch
        drawroundedrectangle(101, 19, 24, 12); // draw switch outline
        tx = 4;
        ty = 22;
        if (my > 18 && my < 18 + 12) {
            if (joyright == 1 && skipbrowser != 0) {
                skipbrowser = 0;
                mpshuffle = 0;
                playswitch = 1;
                tx++;
            }
            if (joyleft == 1 && skipbrowser == 0) {
                skipbrowser = 1;
                playswitch = 1;
                tx--;
            }
        } // browser
        sprintf(text + 6, "BROWSER"); // update for version 1.0.97
        drawtextvarwidth();
        if (skipbrowser == 1) {
            cx = 107;
        } else {
            cx = 119;
        }
        cy = 19 + 6;
        drawfilledcircle(0); // draw switch
        drawroundedrectangle(101, 33, 24, 12); // draw switch outline
        tx = 4;
        ty = 36;
        if (my > 32 && my < 32 + 12) {
            if (joyleft == 1 && covershow != 0) {
                covershow = 0;
                playswitch = 1;
                tx--;
            }
            if (joyright == 1 && covershow == 0) {
                covershow = 1;
                playswitch = 1;
                tx++;
            }
        } // covershow
        sprintf(text, "JPG COVER ART"); // update for version 1.0.97
        drawtextvarwidth();
        if (covershow == 0) {
            cx = 107;
        } else {
            cx = 119;
        }
        cy = 33 + 6;
        drawfilledcircle(0); // draw switch
        drawroundedrectangle(101, 47, 24, 12); // draw switch outline
        tx = 4;
        ty = 50;
        if (my > 46 && my < 46 + 12) {
            if (joyleft == 1 && pitchchanger != 0) {
                pitchchanger = 0;
                playswitch = 1;
                tx--;
            }
            if (joyright == 1 && pitchchanger == 0) {
                pitchchanger = 1;
                playswitch = 1;
                tx++;
            }
        } // pitch shifter
        sprintf(text, "PITCH SHIFTER"); // update for version 1.0.97
        drawtextvarwidth();
        if (pitchchanger == 0) {
            cx = 107;
        } else {
            cx = 119;
        }
        cy = 47 + 6;
        drawfilledcircle(0); // draw switch
        if (playswitch != 0) { // play switch sound for any switch change
            playswitch = 0;
            playswitchsound(); // update for version 1.0.192
        } // playswitch
        drawpointer();
        drawscreen();
        shiftbytein();
        joystickaccelerate(); // update for version 1.0.202
        if (joyup + joydown + joyleft + joyright == 0) {
            jheld = 1.0;
        } else {
            jheld = jheld + 0.25;
        }// cancel acceleration
        checkmousebounds();
        mx = mmx;
        my = mmy; // convert to integer
    } // joyfire
    playtinydirect();
    if (pitchchanger == 1) { // pitch changer will be active
        if (clockvalue < 3) {
            clockvalue = 3;
            clockbyte = 0xC4;
        }
    } // set to at least the default clock value for vs1003b
    delay_gen(20);
}
//
// pitch and tempo control via joystick
// borrows xx and yy integer variables

void pitchtempocontrol() { // update for version 1.0.51
    unsigned char vsdataa = 0;
    unsigned char vsdatab = 0;
    SD_CS = 1; // deselect sd card
    SPISTAT = 0;
    int pcontrol = -16384; // default play 1.0x speed
    if (joyright == 1) {
        xx++;
        yy = 0;
    } // set tempo display value
    if (joyleft == 1) {
        xx--;
        yy = 0;
    }
    if (joyup == 1) {
        yy++;
        xx = 0;
    } // set pitch display value
    if (joydown == 1) {
        yy--;
        xx = 0;
    }
    if (xx > 4) {
        xx = 4;
    } // check tempo limits
    if (xx < -3) {
        xx = -3;
    }
    if (yy > 5) {
        yy = 5;
    } // check pitch limits
    if (yy < -6) {
        yy = -6;
    }
    if (xx != 0) {
        pcontrol = 0 - tempos[xx + 3];
    } // set tempo chip value
    if (yy != 0) {
        pcontrol = pitches[yy + 6];
    } // set pitch chip value
    vsdataa = pcontrol >> 8; // update for version 1.0.134
    vsdatab = pcontrol & 0b0000000011111111; //
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x0C, vsdataa, vsdatab); // settings for pitch changer
    delay_gen(0);
    SPISTAT = 0x8000;
    SD_CS = 0; // select sd card
}
//
// draw mp3 player program progress bar outline

void drawmpprogress() { // update for version 1.0.74
    drawroundedrectangle(69, 0, 58, 26); // draw spectrum border
    drawline(1, 47, 126, 47); // draw border for progress bar
    drawline(1, 51, 126, 51); //
    drawline(0, 29 + 19, 0, 31 + 19); //
    drawline(127, 29 + 19, 127, 31 + 19); //
}
//
// draw gps program progress bar outline

void drawgpsprogress() { // update for version 1.0.212
    drawline(1, 0, 126, 0);
    drawline(1, 2, 126, 2);
    x = 0;
    y = 1;
    setpixel();
    x = 127;
    setpixel();
    y--;
    clearpixel();
    y = 2;
    clearpixel();
    x = 0;
    clearpixel();
    y = 0;
    clearpixel();
}
//
// copy previous map render to framebuffer

void gpscopygraphics() { // update for version 1.0.163
    int gclr = 0; // display image we are not drawing to
    while (gclr < 1024) { // copy image to frame buffer
        if (imageselect == 1024 || gpsmode == 0) {
            framebuffer[gclr] = imagebuffer[gclr];
        } else {
            framebuffer[gclr] = imagebuffer[gclr + 1024]; //
        } // imageselect
        gclr++;
    } // gclr
}
//
// keep the mouse pointer on the screen

void checkmousebounds() {
    if (mmx > 127.0) {
        mmx = 127.0;
        jheld = 1.0;
        msine = 0;
    } // check mouse boundaries
    if (mmy > 63.0) {
        mmy = 63.0;
        jheld = 1.0;
        msine = 0;
    }
    if (mmx < 0.0) {
        mmx = 0.0;
        jheld = 1.0;
        msine = 0;
    }
    if (mmy < 0.0) {
        mmy = 0.0;
        jheld = 1.0;
        msine = 0;
    }
}
//
// error message for cases where a correct media folder exists
// but there are no files with expected extension in the folder

void nofileserror() { // update for version 1.0.61
    clrframebuffer();
    drawroundedrectangle(16, 22, 88, 22);
    text[10] = text[1]; // move directory name in string
    text[11] = text[2]; //
    text[12] = text[3]; //
    text[13] = text[4]; //
    text[14] = 0; // terminate new string
    sprintf(text, "FOUND IN "); // gps error message
    text[9] = 0x4D; // M for either folder
    tx = 18;
    ty = 34;
    drawtext(); // bottom line
    sprintf(text, "NO MPI FILES"); // gps error message
    tx = 21; //
    if (demo == 3) { //
        text[5] = 0x33; // or change to MP3
        tx = tx + 3; // adjust centering
    } // demo                       //
    ty = 25;
    drawtext(); // top line
    drawrectangle(124, 0, 3, 63); // draw dummy scroll bar border
    x = 124;
    nofilesprivate(); // mask rounded corners
    x = 127;
    nofilesprivate(); //
    drawscreen();
    delay_gen(8000); //
    debugout(8); // set error status
}
//
// private function for nofileserror function

void nofilesprivate() { // update for version 1.0.218
    y = 0;
    clearpixel(); //
    y = 63;
    clearpixel(); //
}
//
// reset the decode time for vs1003b chip
// assumes spi is already set for the chip

void mpresetdecodetime() { // update for version 1.0.67
    delay_gen(0); // reset decode time for next track
    vscommand(0x04, 0x00, 0x00); // reset decode time
}
//
// convert bytes from file to screen coordinates

void gpsconvertfloats() { // update for version 1.0.76
    bytes[0] = receivebuffer[0];
    bytes[1] = receivebuffer[1];
    bytes[2] = receivebuffer[2];
    bytes[3] = receivebuffer[3];
    filelat = Float(bytes);
    bytes[0] = receivebuffer[4];
    bytes[1] = receivebuffer[5];
    bytes[2] = receivebuffer[6];
    bytes[3] = receivebuffer[7];
    filelon = Float(bytes);
    screenx = (filelon - screentllon) / pointval;
    screeny = (screentllat - filelat) / pointval;
}
//
// graphic transition screen out of file browser
// most of the graphic effect has been disabled
// in favour of a simplified and faster effect

void fbtransition(int block) { // update for version 1.0.197
    blockpos = block; // save position for map description
    tx = 85;
    ty = block + 1; // set coordinates to read text from image
    readtextimage(1); // read file number text from image
    y = 9 + block; // update for version 1.0.78
    while (y > block) { // clear the printed file name from image
        x = 0;
        while (x < 116) {
            clearpixelimage();
            x++;
        }
        y--;
    } // y
    tx = 85;
    ty = block + 1;
    drawtextimage(); // print file number
    sprintf(text, rec.filename); // print file name
    tx = 2;
    drawtextimage(); //
    clrfb = 0; // update for version 1.0.194
    while (clrfb < 1024) { // copy image buffer to frame buffer
        framebuffer[clrfb] = imagebuffer[clrfb];
        clrfb++;
    } // clrfb
    if (demo != 2) {
        drawscreen();
    } // update for version 1.0.203
}
//
// initialise the 128x64 KS0108 based graphic lcd module
// Freedom Graphics library by Brek Martin 2016-2018.

void lcdmoduleinithalf() { // update for version 1.0.216
    int initcnt = 0;
    LCD_DI = 1; // set instruction mode
    LCD_EN = 0; // set enable pin high
    srbyte = 0x00; // clear lcd data bus
    shiftbyte(); //
    delay_gen(10); // delay for reset
    while (initcnt < 3) {
        LCD_EN = 0; // set enable pin high
        srbyte = initbytes[initcnt]; //
        shiftbyte(); //
        delay_gen(5); //
        LCD_EN = 1; // set enable pin low
        delay_gen(5); //  
        initcnt++; //
    } // initcnt
    LCD_EN = 0; // set enable pin high
    delay_gen(5); //
}
//

void lcdmoduleinit() { // update for version 1.0.134
    delay_gen(12); //
    LCD_CS1 = 0; //
    LCD_CS2 = 1; //
    lcdmoduleinithalf();
    LCD_CS1 = 1; //
    LCD_CS2 = 0; //
    lcdmoduleinithalf();
    LED_PIN = 1; // 
}
//
// initialise the sd card filesystem and apply
// the configured spi frequency setting

int sdcardinit() { // update for version 1.0.117
    int success = 0; // variable for return status
    if (FSInit()) { // initialise file system
        success = 1; // set status for success
        SD_CS = 1; // deselect sd card
        SPISTAT = 0;
        delay_gen(0);
        SPICON1bits.PPRE = 3; // 3 fastest 1:1
        SPICON1bits.SPRE = sdspeed; // 5 fastest for prototype 
        delay_gen(0);
        SPISTAT = 0x8000;
        SD_CS = 0; // select sd card
    } // fsinit
    return (success);
}
//
// clear the data used for waterfall spectrum display
// without taking time to clear the entire image buffer

void clearwaterfall() { // update for version 1.0.92
    clrfb = 0;
    while (clrfb < 2014) {
        n = 0;
        while (n < 9) {
            imagebuffer[clrfb + n] = 0;
            n++;
        }
        clrfb = clrfb + 38;
    } // clrfb  
}
//
// fifo queue based on functions by Adam Davis
// required for forest fire floodfill algorithm

int queueput(unsigned char new) {
    if (queuein == ((queueout + 318) % 319)) {
        debugout(10);
        return -1;
    } // queue full
    voice[queuein] = new;
    queuein = (queuein + 1) % 319;
    return 0; // success
}
//

int queueget(unsigned char *old) {
    if (queuein == queueout) {
        return -1;
    } // queue empty
    *old = voice[queueout];
    queueout = (queueout + 1) % 319;
    return 0; // success
}
//
// graphics forest fire flood fill - Brek Martin 2017.
// This will fill any bordered area of clear pixels
// with either solid black, a checkered pixel pattern,
// or a pattern of either horizontal or vertical lines
// draws to an image buffer and destroys previous data
// that was in the global voice array used as a queue
// pattern filling options destroy the framebuffer data
// the shape does not have to be entirely in the image area
// this function should not crash the micro so is preferred
// to use instead of the stack version flood fill function.

void floodfill(int ffx, int ffy, int pattern) { // update for version 1.0.79
    int xeast = 0;
    int xwest = 0;
    int ynode = 0;
    unsigned char gotvalue = 0;
    queuein = 0;
    queueout = 0;
    if (pattern != 0) { // update for version 1.0.81
        clrfb = 0; // copy imagebuffer to framebuffer
        while (clrfb < 1024) {
            framebuffer[clrfb] = imagebuffer[clrfb + imageselect];
            clrfb++;
        }
    } // pattern
    x = ffx;
    y = ffy; //
    if (getpixelimage() != 0) {
        return;
    } // return if node is set or out of bounds
    queueput(y);
    queueput(x); // add the start node to the queue
    while (queuein != queueout) { // while the queue is not empty
        queueget(&gotvalue);
        y = gotvalue; // get the next node x coordinate
        queueget(&gotvalue);
        x = gotvalue; // get the next node y coordinate
        ynode = y; //
        while (getpixelimage() == 0) {
            x--;
        } // search for east border
        x++;
        xeast = x;
        x = gotvalue; //
        while (getpixelimage() == 0) {
            x++;
        } // search for west border
        x--;
        xwest = x; //
        while (xeast < xwest + 1) { // for each node between east and west
            x = xeast; //
            setpixelimage(); // set the pixels from east to west
            if (pattern == 1) { // checkered pixel pattern
                if ((x & 0b0000000000000001) == 1) {
                    if ((y & 0b0000000000000001) == 1) {
                        setpixel();
                    }
                } else {
                    if ((y & 0b0000000000000001) != 1) {
                        setpixel();
                    }
                } // x
            } // pattern
            if (pattern == 2) { // horizontal lines pattern
                if ((y & 0b0000000000000001) == 1) {
                    setpixel();
                }
            } // pattern
            if (pattern == 3) { // vertical lines pattern
                if ((x & 0b0000000000000001) == 1) {
                    setpixel();
                }
            } // pattern
            y = ynode - 1; // look north
            if (getpixelimage() == 0) { //
                queueput(y);
                queueput(x); // add north node to the queue
            } //
            y = ynode + 1; // look south
            if (getpixelimage() == 0) { //
                queueput(y);
                queueput(x); // add south node to the queue
            } //
            y = ynode; // restore current y coordinate
            xeast++; // continue west on the current line
        }
    } // queue not empty
    if (pattern != 0) {
        clrfb = 0; // write framebuffer back to image buffer
        while (clrfb < 1024) {
            imagebuffer[clrfb + imageselect] = framebuffer[clrfb];
            clrfb++;
        }
    } // pattern
}
//
// seed map polygons for forest fire flood fill
// various seeds are needed because part of the
// map could be off screen, and the floodfill
// will ignore seed points that are off screen

void gpsfillpolygon() { // update for version 1.0.82
    int fillpattern = 0;
    FSfread(receivebuffer, 1, 4, pointer);
    if (receivebuffer[0] == 0x5B && receivebuffer[2] == 0x5D) {
        while (receivebuffer[0] != 0x5D) {
            FSfread(receivebuffer, 1, 9, pointer);
            if (receivebuffer[0] != 0x5D) {
                gpsconvertfloats();
                fillpattern = receivebuffer[8];
                x = rounded(screenx);
                y = rounded(screeny);
                if (gpsmode == 1 && gpsfix == 1) {
                    multiplerotationf();
                }
                floodfill(x, y, fillpattern); // floodfill with checkered pattern
            }
        }
    } // receivebuffer
}
//
// read printed text from an image buffer into text string
// tx and ty must be set to the same coordinates that were
// used to draw the text with the drawtextimage function
// characters are written to the text string until either:
// the text area is polluted with other image data, a space
// character is encountered with dmode set, the function
// tries to decode any characters out of display bounds,
// or the end of the printed string on display is reached.

int readtextimage(int dmode) { // update for version 1.0.192
    int tcnt = 0;
    int ycnt = 0;
    int stx = 0;
    int tindex = 0;
    int charscan = 0;
    int charcnt = 0;
    unsigned char notmatch = 0;
    unsigned char pixflag = 0;
    unsigned char gotchar = 1;
    unsigned char dshift = 0;
    tx++;
    while (gotchar == 1) {
        charscan = dmode;
        tindex = charscan * 5;
        gotchar = 0; // update for version 1.0.97
        while (charscan < 95) { //
            tcnt = 0;
            notmatch = 0;
            stx = tx;
            while (tcnt < 5) {
                dshift = 0b00000001;
                x = stx;
                ycnt = 0;
                while (ycnt < 7) { // update for version 1.0.97
                    y = (ty + ycnt);
                    pixflag = 0;
                    if ((charmap[tindex + tcnt] & dshift) != 0) {
                        pixflag = 1;
                    }
                    if (getpixelimage() != pixflag) {
                        notmatch = 1;
                    }
                    dshift = dshift << 1;
                    ycnt++;
                } // ycnt 
                tcnt++;
                stx++;
            } // tcnt
            if (notmatch == 0) {
                text[charcnt] = charscan + 0x20;
                charcnt++;
                charscan = 95;
                tx = tx + 6;
                gotchar = 1;
            } else {
                charscan++;
                tindex = charscan * 5;
            } // notmatch
        } // charscan
    } // gotchar
    if (dmode == 0) { // update for version 1.0.97
        while (text[charcnt - 1] == 0x20 && charcnt > 0) {
            charcnt--;
        } // truncate trailing spaces
    } // dmode
    text[charcnt] = 0; // terminate string
    return (charcnt - 1); // return the number of characters that were decoded
}
//

void playblopsound() { // update for version 1.0.102
    vlen = 0;
    while (vlen < 240) {
        voice[vlen] = blop[vlen];
        vlen++;
    }
    playvoice();
}
//

void playswitchsound() { // update for version 1.0.204
    vlen = 0;
    while (vlen < 166) {
        voice[vlen] = lswitch[vlen];
        vlen++;
    }
    while (vlen < 224) {
        voice[vlen] = 0;
        vlen++;
    }
    playvoice();
}
//

void drawroundedborder() { // update for version 1.0.160
    clrframebuffer();
    drawroundedrectangle(0, 0, 127, 63); // draw screen border
}
//

void clrframebuffer() { // update for version 1.0.102
    int clfbuf = 0;
    while (clfbuf < 1024) {
        framebuffer[clfbuf] = 0;
        clfbuf++;
    }
}
//

void clrimagebuffer() { // update for version 1.0.108
    int clibuf = 0;
    while (clibuf < 1024) {
        imagebuffer[clibuf] = 0;
        clibuf++;
    }
}
//
// apply zoom to gps map projection

void gpszoom() { // update for version 1.0.115
    float zoomwork;
    zoomwork = pointval * 63.5;
    screentllon = userlon - zoomwork;
    screenbrlon = userlon + zoomwork;
    zoomwork = pointval * 31.5;
    screentllat = userlat + zoomwork;
    screenbrlat = userlat - zoomwork;
}
//
/*
// static clipping box for rotating vector gps
// uses the minimum safe clipping box values.
int gpsstaticclipping() { // update for version 1.0.214
int lineonscreen = 1; // return flag
if (gpsmode == 1 && gpsfix == 1) { // for track up mode
if (lxa < -7 && lxb < -7) {lineonscreen = 0;}
if (lxa > 133 && lxb > 133) {lineonscreen = 0;}
if (lya < -39 && lyb < -39) {lineonscreen = 0;}
if (lya > 101 && lyb > 101) {lineonscreen = 0;}
} else { // for north up mode
if (lxa < 0 && lxb < 0) {lineonscreen = 0;}
if (lxa > 127 && lxb > 127) {lineonscreen = 0;}
if (lya < 0 && lyb < 0) {lineonscreen = 0;}
if (lya > 63 && lyb > 63) {lineonscreen = 0;}
} // gpsmode
return(lineonscreen);
}
 */
//
// sets bounds of the dynamic clipping box for an angle.
// this is called once for every render, and must rotate
// four corner points to save rotating many more points.

void gpsdynamicclippingset() { // update for version 1.0.201
    int exbuf = 0; // coordinate buffers
    int wybuf = 0; //
    int cnrcnt = 0; // corner point counter
    jpgpixcount[0] = 32; // minxbuf
    jpgpixcount[1] = 0; // minybuf
    jpgpixcount[2] = 0; // maxxbuf
    jpgpixcount[3] = 0; // maxybuf
    while (cnrcnt < 4) { // cycle screen corners
        x = 0;
        y = 0; // corner zero defaults
        if (cnrcnt == 1 || cnrcnt == 2) {
            x = 127;
        } // corrections
        if (cnrcnt > 1) {
            y = 63;
        }
        multiplerotationf();
        exbuf = x;
        wybuf = y;
        if (exbuf > jpgpixcount[2]) {
            jpgpixcount[2] = exbuf;
        }
        if (exbuf < jpgpixcount[0]) {
            jpgpixcount[0] = exbuf;
        }
        if (wybuf > jpgpixcount[3]) {
            jpgpixcount[3] = wybuf;
        }
        if (wybuf < jpgpixcount[1]) {
            jpgpixcount[1] = wybuf;
        }
        cnrcnt++;
    } // cnrcnt
    jpgpixcount[0]++; // minxbuf
    jpgpixcount[1]++; // minybuf
    jpgpixcount[2]--; // maxxbuf
    jpgpixcount[3]--; // maxybuf
}
//
// dynamic clipping box for rotating vector maps.
// this checks clipping bounds for every point.
// to pay for itself, this function has to render
// rotating maps faster than if the maximum sized
// clipping box constant values were always used.

int gpsdynamicclippingcheck() { // update for version 1.0.200
    int lineonscreen = 1; // return flag
    if (gpsmode == 1 && gpsfix == 1) { // for track up mode
        if (lxa < jpgpixcount[0] && lxb < jpgpixcount[0]) {
            lineonscreen = 0;
        }
        if (lxa > jpgpixcount[2] && lxb > jpgpixcount[2]) {
            lineonscreen = 0;
        }
        if (lya < jpgpixcount[1] && lyb < jpgpixcount[1]) {
            lineonscreen = 0;
        }
        if (lya > jpgpixcount[3] && lyb > jpgpixcount[3]) {
            lineonscreen = 0;
        }
    } else { // for north up mode
        if (lxa < 0 && lxb < 0) {
            lineonscreen = 0;
        }
        if (lxa > 127 && lxb > 127) {
            lineonscreen = 0;
        }
        if (lya < 0 && lyb < 0) {
            lineonscreen = 0;
        }
        if (lya > 63 && lyb > 63) {
            lineonscreen = 0;
        }
    } // gpsmode
    return (lineonscreen);
}
//
// send a command to the vs1003b decoder chip

void vscommand(unsigned char bbb, unsigned char ccc, unsigned char ddd) { // update for version 1.0.121
    VSXCS = 0; // select vs1003 instruction
    WriteSPIManual(0x02);
    WriteSPIManual(bbb); //
    WriteSPIManual(ccc);
    WriteSPIManual(ddd); //
    VSXCS = 1;
}
//
// error message for failing the first card initialisation

void nocardiniterror() { // update for version 1.0.204
    int errortrap = 1;
    sdcardanimation();
    while (errortrap == 1) {
        sanityprivatea();
        sprintf(text, "SD CARD FAILED");
        drawtextvarwidthcentered();
        sanityprivateb();
        drawscreen();
    } // errortrap
}
//
// Tetrabrick - Brek Martin 2010-2018.
// This is based on a previous tetrominoes version for 16F877A,
// but has been completely rewritten in C for this new version.
// This version inherently supports a colour display platform.
// Every well array element is numbered according to the shape
// each brick came from. These values represent brick colours.
// The same source has been successfully implemented in colour.

void tetrastart() { // update for version 1.0.102
    int sscreen = 0; //
    startvoice(); // set audio volume
    shiftbytein(); // read joystick to test for game
    while (joyfire == 0) { // exit for joystick fire
        Init_Timer1(); // start interrupt timer for random generation
        clrgraphics(); // erases data in current shape for clean startup
        tetraload(); // load the shape data to ram buffer
        tetrafloor(); // set the solid well floor
        tetgameover = 0;
        tetrascore = 0; // reset end game and score flags
        tetratimer = 100;
        tetdelay = 50; // set initial game speed and delay values
        tetrottimer = 0;
        tetrepeat = 0; // reset repeat and rotation timers
        tetx = 3;
        tety = -1; // update for version 1.0.192
        tetdir = 0;
        oldtetdir = 0; //
        tetrahistory[0] = 6; // reset shape history
        tetrahistory[1] = 4; //
        tetrahistory[2] = 3; //
        tetrahistory[3] = 6; //
        wordrandom = tetrarandom(); // get a shape into next buffer
        playblopsound(); // new game sound
        tetradraw(); //
        sscreen = 0; // update for version 1.0.199
        while (sscreen < 150 && joyfire == 0) { // update for version 1.0.215
            shiftbytein(); //
            sscreen++; //
            drawscreen(); // update for version 1.0.199
        } // sscreen
        tetrashape = tetraspawn(); // spawn a new shape
        tetrotate(tetrashape, tetrarot); // load first shape into buffer
        tetrabrick(); // run game loop
    } // joyfire
    samplenum = 1;
    load_sample(); // load exit sample
    playvoice(); // play exit sample
    tetransition(); // do graphic transition
    stoptimer(); // update for version 1.0.204
    resettomenu(); // reset to menu
}
//
// the main game function

void tetrabrick() { // update for version 1.0.100
    int gameloop = 1; // keep game loop running
    while (gameloop == 1) { // game loop
        if (joyfire == 1) {
            vlen = 0;
            playing = 0;
            gameloop = 0;
        } // exit for joystick fire
        tetrotate(tetrashape, tetrarot);
        tetradraw(); // draw the display
        shiftbytein(); // read joystick
        tetdir = 0; // reset direction
        if (joyspareb != 0) {
            sdcardanimation();
        } // card is not inserted
        if (joyup == 1) {
            tetdir = 1;
        } // rotate shape
        if (joydown == 1) {
            tetdir = 2;
        } // set shape in place
        if (joyleft == 1) {
            tetdir = 3;
        } // move shape
        if (joyright == 1) {
            tetdir = 4;
        } // move shape
        if (tetdir == 0) { // handle direction auto repeat
            tetrepeat = 0;
        } else {
            wordrandom = wordrandom + tetdir;
            if (tetrepeat > 12) {
                oldtetdir = 0;
                tetrepeat = 8;
            } else {
                tetrepeat++;
            }
        } // tetdir
        if (tetdir != oldtetdir) { // do the input actions
            if (tetdir == 1 && tetrottimer == 0) { // rotate shape
                tetrarot++; // increment the rotation
                tetrottimer = 6; // restart rotation debounce timer
                if (tetrarot > 3) {
                    tetrarot = 0;
                }
                tetrotate(tetrashape, tetrarot);
                tetcollide = tetcollision();
                if (tetcollide == 1) {
                    tetrarot--; // decrement for collision
                    if (tetrarot < 0) {
                        tetrarot = 3;
                    }
                    tetrotate(tetrashape, tetrarot);
                } // tetcollide
            } // direction up
            if (tetdir == 3) { // move left
                tetx--;
                tetcollide = tetcollision();
                if (tetcollide == 1) {
                    tetx++;
                }
            } // direction left
            if (tetdir == 4) { // move right
                tetx++;
                tetcollide = tetcollision();
                if (tetcollide == 1) {
                    tetx--;
                }
            } // direction right
        } // tetdir
        oldtetdir = tetdir;
        if (tetdir == 2) {
            tetrottimer = 4; // restart rotation debounce timer
            tetratimer = 1;
        } // direction down
        framecount++; // used to help random generator
        if (framecount > 899) {
            framecount = 500;
        }
        tetratimer--; // decrement the drop down timer
        if (tetratimer < 1) {
            tetratimer = tetdelay;
            tety++;
            tetcollide = tetcollision();
            if (tetcollide == 1) {
                tety--;
                playlockdirect(); // update for version 1.0.214
                tetralock();
                tetrascore = tetrascore + 10;
                tetx = 3;
                tety = -1;
                tetrarot = 0;
                tetrashape = tetraspawn();
                tetgameover = tetcollision();
                if (tetgameover != 0) { // game is over
                    playblopsound(); // game over sound
                    tetratimer = 127; // used for scroll position
                    sinamp = 16; // turn off sin amplitude
                    while (tetratimer > -48) { // draw grey text
                        sprintf(text, "GAME");
                        ty = 20;
                        tx = tetratimer;
                        drawtextlargesinmasked();
                        sprintf(text, "OVER");
                        ty = 36;
                        tx = tetratimer;
                        drawtextlargesinmasked();
                        drawscreen();
                        tetratimer--;
                        while (playing != 0) {
                            vfill_audio();
                        } // empty sample buffer
                    } // tetratimer
                    tetrascore = 0; // update for version 1.0.151
                    tetgameover = 2; // update for version 1.0.151
                    tetratimer = 44; // update for version 1.0.140
                    while (tetratimer > 0) { // rotate cells off the screen
                        tetradraw();
                        drawscreen();
                        tetratimer--;
                        tetrarotatewell(190);
                    } // tetratimer
                    vlen = 0; // reset announcement length
                    delay_gen(1000);
                    gameloop = 0; // exit
                } // tetragameover
            } // tetcollide
        } // tetratimer
        if (tetrottimer > 0) {
            tetrottimer--;
        } // decrement rotation timer
        if (playing != 0) {
            vfill_audio();
        } // fill audio buffer
    } // gameloop
}
//
// load the tetromino definition and audio sample data
// or day of month names array and days of week bitmap

void tetraload() { // update for version 1.0.108
    if (!sdcardinit()) {
        return;
    }
    if (FSchdir("ROM") != 0) {
        return;
    }
    openromfile();
    if (pointer != NULL) { // file exists and was opened  
        if (demo == 5) { // load tetrabrick data
            FSfseek(pointer, 399432, SEEK_SET);
            FSfread(imagebuffer + 1024, 1, 448, pointer);
            // first 240 bytes will be overwritten by blop sample
            // total 6338 bytes encoded audio data loaded to ram
            samlen = 240;
            readvoice(1232); // dummy data
            clrfb = 0;
            while (clrfb < 4) { // iterate numeric samples 1-4
                samlen = vtimelensa[clrfb];
                readvoice(vtimeposa[clrfb]);
                clrfb++;
            } // clrfb
            samlen = 926;
            readvoice(54448); // game over
        } else { // load month names for calendar
            FSfseek(pointer, 399880, SEEK_SET);
            FSfread(imagebuffer, 1, 146 + 128, pointer);
        } // demo
        FSfclose(pointer); // close rom file
    } else { // file does not exist or was not opened
        debugout(13); // update for version 1.0.104
    } // file pointer
    FSchdir(".."); // go back to root directory
}
//
// write the solid well bottom

void tetrafloor() { // update for version 1.0.140
    int tramcnt = 200;
    while (tramcnt < 210) {
        imagebuffer[tramcnt] = 0xFF; // solid bottom
        tramcnt++;
    } // tramcnt
}
//
// draw the current game frame

void tetradraw() { // update for version 1.0.100
    int squarecnt = tetx;
    int linecnt = tety;
    clrframebuffer();
    if (tetgameover == 0) { // update for version 1.0.140
        tetdrawshape(); // draw current falling shape
    } // tetragameover
    tetrotate(tetranext, 0); // update for version 1.0.102
    tetx = 17;
    tety = 1; // write shape preview coordinates
    if (tetgameover != 2) { // update for version 1.0.151
        tetdrawshape(); // draw next shape preview
    } // tetgameover
    tetx = squarecnt;
    tety = linecnt; // restore current coordinates
    tetrotate(tetrashape, tetrarot); // reload current shape
    linecnt = 0;
    while (linecnt < 200) { // draw the squares
        squarecnt = 0;
        while (squarecnt < 10) {
            if (imagebuffer[squarecnt + linecnt] != 0) {
                y = ((linecnt / 10)*3) + 2;
                x = (squarecnt * 3) + 49;
                tetdrawcell(x, y);
            } // imagebuffer
            squarecnt++;
        } // squarecnt
        linecnt = linecnt + 10;
    } // linecnt
    drawline(48, 3, 48, 63);
    drawline(79, 3, 79, 63); // well sides
    drawline(47, 2, 47, 62);
    drawline(80, 2, 80, 62); //
    drawline(48, 63, 79, 63);
    drawline(48, 62, 79, 62); // well bottom
    sprintf(text, "%05d", tetrascore); // update for version 1.0.194
    tx = 6;
    ty = 4;
    drawtextvarwidth(); // print game score
    drawrectangle(97, 2, 17, 17); // preview border square
    drawrectangle(96, 1, 19, 19); //
    x = 96;
    y = 1;
    clearpixel(); // update for version 1.0.151
    pixelworkprivatea(); //
    pixelworkprivatec(); //
    x = 115;
    clearpixel(); // round off outside corners
    pixelworkprivateb(); //
    pixelworkprivatec(); //
    y = 20;
    clearpixel(); //
    pixelworkprivateb(); //
    pixelworkprivated(); // update for version 1.0.164
    x = 96;
    clearpixel(); //
    pixelworkprivatea(); //
    pixelworkprivated(); // update for version 1.0.164
    x = 98;
    y = 3;
    setpixel(); // fill inside corners
    x = 113;
    setpixel(); //
    y = 18;
    setpixel(); //
    x = 98;
    setpixel(); //
    drawrectangle(4, 2, 37, 10); // game score border
    drawrectangle(3, 1, 39, 12); // update for version 1.0.194
    x = 3;
    y = 1;
    clearpixel(); // update for version 1.0.151
    pixelworkprivatea(); //
    pixelworkprivatec(); //
    x = 42;
    clearpixel(); // round off outside corners
    pixelworkprivateb(); //
    pixelworkprivatec(); //
    y = 13;
    clearpixel(); //
    pixelworkprivateb(); //
    pixelworkprivated(); // update for version 1.0.164
    x = 3;
    clearpixel(); //
    pixelworkprivatea(); //
    pixelworkprivated(); // update for version 1.0.164
    x = 5;
    y = 3;
    setpixel(); // fill inside corners
    x = 40;
    setpixel(); // update for version 1.0.194
    y = 11;
    setpixel(); //
    x = 5;
    setpixel(); //
    drawscreen(); //
}
//
// draw a single tetrabrick cell

void tetdrawcell(int px, int py) { // update for version 1.0.164
    int rcount = 0;
    while (rcount < 3) {
        drawline(px, py + rcount, px + 2, py + rcount);
        rcount++;
    } // rcount
}
//
// check for full rows to clear

void tetracheckrows() { // update for version 1.0.100
    int squarecnt = 0;
    int linecnt = 190;
    int bonus = 0;
    int tetraclr = 0;
    while (linecnt > -1) {
        squarecnt = 0;
        tetraclr = 0;
        while (squarecnt < 10) {
            if (imagebuffer[squarecnt + linecnt] == 0) {
                tetraclr = 1;
            }
            squarecnt++;
        } // squarecnt
        if (tetraclr == 0) { // row is full of squares
            bonus++;
            tetrascore = tetrascore + (bonus * 100);
            if (tetdelay > 10) {
                tetdelay--;
            } // increase game speed
            playblopsound();
            tetrarotatewell(linecnt);
        } else {
            linecnt = linecnt - 10; // next row up
        } // tetraclr
    } // linecnt
    if (playing == 0 && bonus > 0) { // trigger voice sample to play
        vbytecount = tetraspeech[bonus - 1]; // update for version 1.0.164
        vlen = tetraspeech[bonus]; //
        playing = 1;
    } // playing
}
//
// clear a full row of squares

void tetrarotatewell(int linecnt) { // update for version 1.0.00
    int currentrow = linecnt;
    int squarecnt = 0;
    while (currentrow > 9) {
        squarecnt = 0;
        while (squarecnt < 10) {
            imagebuffer[squarecnt + currentrow] = imagebuffer[squarecnt + (currentrow - 10)];
            squarecnt++;
        } // squarecnt
        currentrow = currentrow - 10;
    } // currentrow
    squarecnt = 0;
    while (squarecnt < 10) { // clear top row
        imagebuffer[squarecnt] = 0;
        squarecnt++;
    } // squarecnt
    tetdelayrows(); // update for version 1.0.151
}
//
// get random value 0 to 6 for next shape

int tetrarandom() { // update for version 1.0.106
    unsigned char tetracurrent = tetranext;
    unsigned char tetramask = 0b00000111;
    int tismatch = 0; // history match flag
    int retries = 0; // retry counter
    while (retries < 6) { // up to six retries
        wordrandom = wordrandom + TMR1 + framecount;
        tetranext = wordrandom & tetramask;
        if (tetranext > 6) {
            wordrandom = wordrandom + (tetrascore / 10) + mx;
            tetranext = wordrandom & tetramask;
        } // tetranext
        if (tetranext > 6) { // update for version 1.0.192
            tetranext = (((masterlongrandom * 1103515245) + 12345 + tetrascore / 10) & 0xDEADBEEF);
            tetranext = tetranext & tetramask;
        } // tetranext
        if (tetranext > 6) {
            tetranext = (framecount + wordrandom) % 7;
        } // last invalid shape trap
        tismatch = tetcheckhis();
        if (tismatch == 0) {
            retries = 6;
        } // end trying
        retries++;
    } // retries
    tethistory(); // enter new shape into history
    return (tetracurrent);
}
//
// get a random shape into the buffer

int tetraspawn() { // update for version 1.0.103
    unsigned char tetrnd = 0;
    tetrnd = tetrarandom();
    tetrotate(tetrnd, 0);
    return (tetrnd);
}
//
// retrieve and optionally rotate shape

void tetrotate(int shape, int rot) { // update for version 1.0.101
    int tetrahorz = 0;
    int lla = 0;
    int llb = 0;
    while (tetrahorz < 4) {
        lla = 500;
        llb = 1024;
        while (lla < 513) {
            imagebuffer[lla + tetrahorz] = imagebuffer[llb + (shape * 64) + tetrahorz + (rot * 4)];
            lla = lla + 4;
            llb = llb + 16;
        } // lla
        tetrahorz++;
    } // tetrahorz
}
//
// check shape collision

int tetcollision() { // update for version 1.0.101
    int collided = 0;
    int tetrahorz = 0;
    int lla = 0;
    int llb = 0;
    while (tetrahorz < 4) { // check for brick collision
        lla = 500;
        llb = 0;
        while (lla < 513) {
            if (imagebuffer[lla + tetrahorz] != 0) {
                if (imagebuffer[tetrahorz + tetx + llb + (tety * 10)] != 0) {
                    collided = 1;
                }
            }
            lla = lla + 4;
            llb = llb + 10;
        } // lla
        tetrahorz++;
    } // tetrahorz
    tetrahorz = 0;
    while (tetrahorz < 4) { // check for well walls collision
        if ((tetx + tetrahorz < 0) || (tetx + tetrahorz > 9)) {
            lla = 500;
            while (lla < 513) {
                if (imagebuffer[lla + tetrahorz] != 0) {
                    collided = 1;
                }
                lla = lla + 4;
            } // lla
        } // tetx
        tetrahorz++;
    } // tetrahorz
    return (collided);
}
//
// draw the current falling shape

void tetdrawshape() {
    int tetravert = 0;
    int tetrahorz = 0;
    while (tetravert < 4) {
        tetrahorz = 0;
        while (tetrahorz < 4) {
            if (imagebuffer[500 + tetrahorz + (tetravert * 4)] != 0) {
                y = ((tety + tetravert)*3) + 2;
                x = ((tetx + tetrahorz)*3) + 49;
                tetdrawcell(x, y);
            }
            tetrahorz++;
        } // tetrahorz
        tetravert++;
    } // tetravert
}
//
// lock current shape into the well

void tetralock() { // update for version 1.0.101
    int tetrahorz = 0;
    int lla = 0;
    int llb = 0;
    while (tetrahorz < 4) {
        lla = 500;
        llb = 0;
        while (lla < 513) {
            if (imagebuffer[lla + tetrahorz] != 0) {
                imagebuffer[tetrahorz + tetx + llb + (tety * 10)] = tetrashape + 1;
            }
            lla = lla + 4;
            llb = llb + 10;
        } // lla
        tetrahorz++;
    } // tetrahorz
    tetracheckrows();
}
//
// graphic transition to game

void tetransition() { // update for version 1.0.105
    int ttranscnt = 0;
    tetrashape = 7; // invalid shape for blank display
    while (ttranscnt < 60) {
        clrgraphics();
        tetradraw();
        ttranscnt++;
    }
}
//
// updates history of the last four shapes and
// rotates history discarding the oldest shape
// for consistency with popular tetromino algorithms
// that help prevent shape floods and droughts.

void tethistory() { // update for version 1.0.105
    int historycnt = 1;
    while (historycnt < 4) { // iterate history
        tetrahistory[historycnt] = tetrahistory[historycnt - 1]; // rotate history
        historycnt++;
    } // hisorycnt    
    tetrahistory[0] = tetranext; // load newest shape
}
//
// checks history of the last four shapes
// and returns true if a proposed new shape
// matches any of the last four in history

int tetcheckhis() { // update for version 1.0.106
    int historycnt = 0;
    int historymatch = 0;
    while (historycnt < 4) { // iterate history
        if (tetrahistory[historycnt] == tetranext) {
            historymatch = 1;
        }
        historycnt++;
    } // hisorycnt    
    return (historymatch);
}
//
// delay completed tetrabrick cell rows

void tetdelayrows() { // update for version 1.0.151
    int tflashcnt = 4;
    if (tetgameover != 0) {
        return;
    }
    tetgameover = 1; // set game over status
    while (tflashcnt > 0) {
        tetradraw();
        delay_gen(64);
        tflashcnt--;
    } // tflashcnt
    tetgameover = 0; // restore game over status
}
//
// draw graphic table format monthly calendar
// provides preview and rewind with joystick
// leap year safe - by Brek Martin 2017

void calgrid() { // update for version 1.0.132
    int calrows = 0;
    int calcolumns = 0;
    int printdays = 0;
    int calcalc = 0;
    int caldaybuf = 0; // for current day highlight
    int cjoytimer = 0;
    int calloop = 1;
    startvoice();
    clrframebuffer(); // clear frame buffer
    tetraload(); // load month names array
    if (gotdate != 0) { // update for version 1.0.182
        calyear = gpsyear;
        calmonth = gpsmonth;
        caldaybuf = gpsday;
    } else {
        calmonth = 1;
        calyear = 2018; // set default month
    } // gotdate
    while (calloop == 1) { // main calendar loop
        clrframebuffer(); // clear framebuffer 
        calday = 1;
        setcal(); // get day of week
        printdays = caldim[calmonth - 1];
        clrfb = 0; // copy days of week bitmap
        while (clrfb < 128) {
            framebuffer[clrfb + 128] = imagebuffer[clrfb + 146];
            clrfb++;
        }
        clrfb = 0;
        n = 0; // update for version 1.0.115
        while (clrfb < calmonth - 1) { // find month name in array
            if (imagebuffer[n] != 0x3A) {
                n++;
            } else {
                clrfb++;
                n++;
            }
        } // clrfb
        clrfb = 0; // copy month name to text string and print
        while (imagebuffer[n] != 0x58) {
            text[clrfb] = imagebuffer[n];
            if (clrfb != 0 && mixedcase != 0) { // update for version 1.0.213
                if (text[clrfb] > 0x40 && text[clrfb] < 0x5B) {
                    text[clrfb] = text[clrfb] + 0x20; // convert to mixed case
                }
            } // clrfb
            n++;
            clrfb++;
        } // imagebuffer
        sprintf(text + clrfb, "%04d", calyear); // draw table title
        ty = 1;
        drawtextvarwidthcentered(); // update for version 1.0.194
        int bannoffset = 8 + mixedcase; // update for version 1.0.213
        while (ty < bannoffset) { // invert title
            x = 0;
            while (x < 128) {
                y = ty;
                flippixel();
                x++;
            }
            ty++;
        } // ty
        x = 0; // add title text effect
        while (x < 128) { // update for version 1.0.116
            y = 1;
            if (getpixel() == 0) {
                y = 0;
                setpixel();
            }
            y = bannoffset - 1; // update for version 1.0.213
            if (getpixel() == 0) {
                y = bannoffset;
                setpixel();
            } // update for version 1.0.213
            x++;
        } // x
        calcolumns = 0;
        calrows = caldow;
        while (calcolumns < 8) {
            while (calrows < 7 && printdays > 0) {
                calcalc = (caldim[calmonth - 1] - printdays) + 1;
                sprintf(text, "%02d", calcalc);
                tx = (calrows * 18) + 4;
                ty = (calcolumns * 8) + 16;
                if (text[0] == 0x30) { // zero blanking
                    text[0] = 0x20; // blank the leading zero
                    tx = tx + 4; // update for version 1.0.213
                } // text
                drawtextvarwidth(); // update for version 1.0.195
                if (calcalc == caldaybuf) { // check for current day
                    if (calyear == gpsyear && calmonth == gpsmonth && gotdate != 0) {
                        if ((framecount & 0b01000000) == 0b01000000) { // update for version 1.0.112
                            y = ty - 1; // update for version 1.0.117
                            while (y < ty + 8) { // flashing inverted current date
                                x = tx - 16; // update for version 1.0.213
                                while (x < tx + 1) {
                                    flippixel();
                                    x++;
                                } // update for version 1.0.213
                                y++;
                            } // y
                            y--;
                            x--;
                            clearpixel(); // rounded corners
                            x = tx - 16;
                            clearpixel(); // update for version 1.0.213
                            y = ty - 1;
                            clearpixel(); //
                            x = tx;
                            clearpixel(); //
                        } // framecount
                    }
                } // calcalc
                printdays--;
                calrows++;
            } // calrows
            calrows = 0;
            calcolumns++;
        } // calcolumns
        drawscreen();
        //
        shiftbytein(); // read joystick
        if (joyleft == 1) {
            clickframe();
            if (cjoytimer < 36) { // update for version 1.0.109
                calmonth--;
                cjoytimer++;
                if (calmonth < 1) {
                    calmonth = 12;
                    if (calyear > 1800) {
                        calyear--;
                    }
                } // calmonth
                delay_gen(360);
            } else {
                if (calyear > 1800) {
                    calyear--;
                }
            } // cjoytimer
        } // joyleft
        if (joyright == 1) {
            clickframe();
            if (cjoytimer < 36) { // update for version 1.0.109
                calmonth++;
                cjoytimer++;
                if (calmonth > 12) {
                    calmonth = 1;
                    calyear++;
                }
                delay_gen(360);
            } else {
                calyear++;
            } // cjoytimer
        } else { // joyright is clear
            if (joyleft == 0) {
                cjoytimer = 0;
            } // reset joystick timer
        } // joyright
        if (joyfire == 1) {
            calloop = 0;
        } // exit calendar
        framecount++;
    } // calloop
    // exit to menu
    samplenum = 1;
    load_sample();
    playvoice(); // play exit sample
    resettomenu();
}
//
// send data to clear an external spectrum display

void sendclrspectrum() { // update for version 1.0.139
    int bytecnt = 0;
    if (serialenabled == 2) {
        txbyte = 0xFF;
        hardserialout();
        while (bytecnt < 10) { // update for version 1.0.140
            txbyte = 0;
            hardserialout();
            bytecnt++;
        } // bytecnt
    } // serialenabled
}
//
// calculation and drawing for analogue clock hands

void analogueclockhand() { // update for version 1.0.156
    x = 15;
    pixelrotation();
    drawline(15, 18, x, y);
}
//
// draw the degrees symbol for bearing display

void drawdegreessymbol() { // update for version 1.0.158
    x = tx + 1;
    y = ty + 1;
    setpixel();
    x++;
    y--;
    setpixel();
    pixelworkprivatee();
    x++;
    y--;
    setpixel();
}
//
// clear the large display window background

void clearwindowbackground() { // update for version 1.0.158
    int clrd = 768;
    while (clrd < 895) { // clear display window background
        framebuffer[clrd - 128] = 0;
        framebuffer[clrd - 256] = 0;
        framebuffer[clrd - 384] = 0;
        framebuffer[clrd - 512] = 0;
        clrd++;
    } // clrd
}
//
// program memory optimisation pixel drawing functions

void pixelworkprivatea() { // update for version 1.0.157
    x++;
    clearpixel();
    x--; //
}
//

void pixelworkprivateb() { // update for version 1.0.157
    x--;
    clearpixel();
    x++; //
}
//

void pixelworkprivatec() { // update for version 1.0.157
    y++;
    clearpixel();
    y--; //
}
//

void pixelworkprivated() { // update for version 1.0.164
    y--;
    clearpixel();
    y++; //
}
//

void pixelworkprivatee() { // update for version 1.0.173
    y = y + 2;
    setpixel(); //
}
//

void pixelworkprivatef() { // update for version 1.0.173
    y = y - 2;
    clearpixel(); //
}
//

void pixelworkprivateg() { // update for version 1.0.205
    y = y + 2;
    clearpixel(); //
}
//

void pixelworkprivateh() { // update for version 1.0.213
    y = y + 4;
    setpixel(); //
}
//

void pixelworkprivatei() { // update for version 1.0.213
    y = y + 4;
    clearpixel(); //
}
//
// copy the string for bad nmea checksum message

void badcheckstring() { // update for version 1.0.212
    sprintf(text, "GPS COMMS ERROR!"); // bad nmea checksum
    lasterror = 9; // clear error six for programs
}
//
// delay for exiting gps mode with joystick left
// an attempt to prevent joystick left for exiting
// gps mode also immediately canceling map sniffer

void gpsmodeexitdelay() { // update for version 1.0.165
    while (JOYDAT_PIN == 0) {
        delay_gen(2);
    }
    delay_gen(18);
    while (JOYDAT_PIN == 0) {
        delay_gen(2);
    }
    delay_gen(18);
}
//
// convert a pair of ascii bytes to integer value

int asciitoint(char msb, char lsb) { // update for version 1.0.157
    int output = 0;
    msb = msb - 0x30;
    lsb = lsb - 0x30;
    output = (msb * 10) + lsb;
    return (output);
}
//
// convert astro time mixed case messages to upper case

void astrocaseconvert() { // update for version 1.0.212
    int ncs = 0;
    if (mixedcase != 0) {
        return;
    }
    while (ncs < 21) { // convert message strings to upper case
        if (line2[ncs] > 0x60 && line2[ncs] < 0x7B) {
            line2[ncs] = line2[ncs] - 0x20;
        }
        if (line3[ncs] > 0x60 && line3[ncs] < 0x7B) {
            line3[ncs] = line3[ncs] - 0x20;
        }
        ncs++;
    } // ncs
}
//
// start non-blocking sample playing

void astrostartsample() { // update for version 1.0.213
    oldvtime[0] = 99; // invalidate announcement repeat
    vbytecount = 0;
    playing = 1; // start sample playing
}
//
// hardware serial debug output artifact
// removed in favour of display version

void debugout(int derror) { // update for version 1.0.203
    lasterror = derror; // set the error in memory
}
//
// open the sd card rom file for reading

void openromfile() { // update for version 1.0.154
    pointer = FSfopen("FREEDOM.ROM", "r");
}
//
// print the gpx file save message dialogue

void gpxmessagescreen(int gmsg) { // update for version 1.0.156
    int crot = 5;
    drawroundedborder(); // draw screen border
    ty = 23;
    text[0] = 0; // clear string
    if (gmsg != 0) { // saving track log
        sprintf(text, "SAVING TRACK LOG");
    } // gmsg
    if (oldlogsecs == 100) { // update for version 1.0.172
        if (gpssimulation == 0) { // update for version 1.0.217
            sprintf(text, "APPEND");
        } else { // gps simulation mode
            sprintf(text, "REPLAY");
        } // gpssimulation
        text[6] = 0x20;
    } // oldlogsecs
    if (gmsg == 2) { // saved track log
        text[3] = 0x45; // E
        text[4] = 0x44; // D
        while (crot < 16) {
            text[crot] = text[crot + 1];
            crot++;
        } // crot
    } // gmsg
    if (gmsg > 2) { // file error!
        sprintf(text, "FILE ERROR!");
    } // gmsg
    drawtextvarwidthcentered();
    if (oldlogsecs == 100 && gmsg == 1) { // update for version 1.0.172
        ty = 33; // print the time of day from the log file name
        text[0] = filelabel[16];
        text[1] = filelabel[17]; // hours
        text[2] = 0x3A; // ":"
        text[3] = filelabel[18];
        text[4] = filelabel[19]; // minutes
        text[5] = 0x20; // space
        text[8] = 0x2F; // "/"
        if (dateformat == 0) { // day first
            text[6] = filelabel[20];
            text[7] = filelabel[21]; // day
            text[9] = filelabel[22];
            text[10] = filelabel[23]; // month
        } else { // month first
            text[6] = filelabel[22];
            text[7] = filelabel[23]; // month
            text[9] = filelabel[20];
            text[10] = filelabel[21]; // day
        } // date format
        text[11] = 0;
        drawtextvarwidthcentered();
        if (gpssimulation == 0) { // update for version 1.0.218
            trackloggauge();
            formatbufa = 100.0 - formatbufa;
            sprintf(text, "%2.0f  FREE", formatbufa); // free track log memory gauge
            ty = 0; // used as a counter here
            while (text[ty] != 0x20) {
                ty++;
            } // find first space
            text[ty] = 0x25; // add the percent sign
            tx = ty + 4; // borrow as a counter
            while (text[tx] != 0) {
                tx++;
            } // update for version 1.0.217
        } else { // gps simulation mode
            tx = 0; // copy filename to start of string
        } // gps simulation
        sprintf(text + tx, " - %s", rec.filename); // update for version 1.0.217
        if (gpssimulation != 0) { // update for version 1.0.218
            text[1] = 0x20; // fix map file name string
            while (text[tx] != 0) {
                tx++;
            } // find the end of string
            text[tx] = 0x20;
            tx++; // add spaces for centering
            text[tx] = 0x20;
            tx++;
            text[tx] = 0x20;
            tx++;
            text[tx] = 0x00; // terminate new string
        } // gpssimulation
        ty = 43; // set vertical position
        drawtextvarwidthcentered();
        ty = 13;
        sprintf(text, "THIS SESSION WILL");
        drawtextvarwidthcentered();
        if (pff < 30) { // alternate the options displayed
            tx = 4;
            sprintf(text, "< CANCEL");
            drawroundedrectangle(0, 53, 57, 10); // cancel window
        } else {
            tx = 99;
            sprintf(text, "OK >");
            drawroundedrectangle(127 - 33, 53, 33, 10); // confirm window
        } // pff
        drawroundedrectangle(0, 0, 127, 63); // redraw screen border
        pff++;
        if (pff > 59) {
            pff = 0;
        } // update for version 1.0.174
        ty = 55;
        drawtext();
        oldlogsecs = 100; // update for version 1.0.174
    } // oldlogsecs
    drawscreen();
    if (gmsg == 4) {
        delay_gen(1600);
    }
}
//
// create a basic gpx track log file and save to card

void makegpx() { // update for version 1.0.154
    int wretries = 0;
    int gpxindex = 320; // start of track log area
    float glat = 0.0;
    float glon = 0.0;
    double gpxlat = 0.0;
    double gpxlon = 0.0;
    if (simwasactive != 0) { // update for version 1.0.219
        simwasactive = 0;
        return;
    } // simwasactive
    if (distance == 0.0) {
        return;
    } // check track log has more than one point
    shiftbytein(); // update for version 1.0.155
    if (joysparec != 0) {
        debugout(19);
        return;
    } // check write protect status
    GPSPWR_PIN = 1; // turn gps module power off
    delay_gen(24); // safety delay
    if (FSchdir("ROM") != 0) {
        gpxmessagescreen(4);
        debugout(18);
        return;
    } // load gpx file header data from file
    openromfile();
    if (pointer == NULL) {
        FSchdir("..");
        gpxmessagescreen(4);
        debugout(18);
        return;
    } // check rom file exists
    FSfseek(pointer, 400154, SEEK_SET);
    FSfread(imagebuffer, 1, 326, pointer);
    FSfclose(pointer); // close sd card rom file
    FSchdir(".."); // go back to root directory
    while (wretries < 8 && rec.filename[wretries] != 0x2E) { // update for version 1.0.215
        imagebuffer[0x126 + wretries] = rec.filename[wretries]; // write map file name to gpx header info
        wretries++;
    } // wretries
    while (wretries < 9) {
        imagebuffer[wretries + 0x126] = 0x23; // suffix with "#" characters
        wretries++;
    } // wretries
    wretries = 0; // reset the borrowed counter
    delay_gen(10); // safety delay
    if (FSchdir("GPX") != 0) {
        debugout(17);
        return;
    } // write header data to gpx file
    gpxmessagescreen(0); // update for version 1.0.158
    delay_gen(24); // safety delay
    copygpxfilename(); // update for version 1.0.170
    gpxmessagescreen(1); // draw saving message screen
    delay_gen(2200); // delay to display message
    while (wretries < 3) { // up to three potential write attempts
        pointer = FSfopen(filelabel, "w");
        if (pointer == NULL) {
            FSchdir("..");
            gpxmessagescreen(4);
            debugout(21);
            return;
        }
        FSfwrite(imagebuffer, 1, 326, pointer);
        while (gpxindex < logindex) { // write track points to file
            int gn = 0;
            while (gn < 4) {
                bytes[gn] = voice[gpxindex + gn];
                gn++;
            }
            glat = Float(bytes); // convert float to four bytes
            gpxlat = glat;
            int gb = 0;
            while (gb < 4) {
                bytes[gb] = voice[gpxindex + gn];
                gb++;
                gn++;
            }
            glon = Float(bytes); // convert float to four bytes
            gpxlon = glon;
            sprintf(text, "      <trkpt lat=\"%.6f\" lon=\"%.6f\"></trkpt>", gpxlat, gpxlon);
            int flen = 0; // get length of string
            while (text[flen] != 0) {
                flen++;
            } // flen
            text[flen] = 0x0D;
            flen++; // add return and new line
            text[flen] = 0x0A;
            FSfwrite(text, 1, flen, pointer); // write position string to gpx file
            gpxindex = gpxindex + 8; // increment file index by a coordinate pair
        } // gpxindex
        FSfwrite(gpxfooter, 1, 33, pointer); // write footer data
        delay_gen(16); // safety delay
        FSfclose(pointer); // close the gpx file after writing
        delay_gen(16); // safety delay
        pointer = FSfopen(filelabel, "r"); // open file to check it was written
        FSfread(bytes, 1, 4, pointer); // read the first four bytes
        FSfclose(pointer); // close the gpx file
        if (bytes[0] == 0x3C && bytes[3] == 0x6D) { // check some bytes
            wretries = 8;
            gpxmessagescreen(2); // draw saved message screen
            delay_gen(2222); // delay to display message
        } // bytes
        wretries++;
    } // wretries
    FSchdir(".."); // go back to root directory
    if (wretries < 9) { // file write failed
        debugout(20);
        gpxmessagescreen(3); // write error screen
    } else {
        gpxmessagescreen(0); // blank message screen 
    } // wretries
    delay_gen(200);
}
//
// read in an existing gpx file for the same day
// must have already acquired gps time and date

void readgpx() { // update for version 1.0.200
    int resultg = 0; // file flags
    int resultp = 0; //
    int gpxcnt = 0; // file position counter
    char gpxchar[1] = {0}; // receive buffer for file reading
    char gpsignflag = 0;
    float gplon = gpslon; // coordinate buffers
    float gplat = gpslat; // for save and restore
    if (gotdate == 0) {
        debugout(26);
        return;
    } // date was not acquired
    if (FSchdir("GPX") != 0) {
        debugout(17);
        return;
    }
    copygpxfilename(); // copy file name to buffer
    while (gpxcnt < 4) {
        filelabel[gpxcnt] = 0x3F;
        gpxcnt++;
    } // "????"
    resultg = FindFirst(filelabel, 0x2F, &rec); // find file for the current date
    while (resultp == 0) {
        resultp = FindNext(&rec);
    } // update for version 1.0.173
    if (resultg != 0) { // no file was found for current date
        int gpday = gpsday - 1; // so try the previous date
        int gpmonth = gpsmonth; //
        if (gpday < 1) { //
            gpmonth = gpmonth - 1; //
            if (gpmonth < 1) {
                gpmonth = 12;
            }
            gpday = caldim[gpmonth - 1];
        } // gpday
        filelabel[4] = gpday / 10;
        filelabel[5] = gpday - (filelabel[4] * 10);
        filelabel[4] = filelabel[4] + 0x30;
        filelabel[5] = filelabel[5] + 0x30;
        filelabel[6] = gpmonth / 10;
        filelabel[7] = gpmonth - (filelabel[6] * 10);
        filelabel[6] = filelabel[6] + 0x30;
        filelabel[7] = filelabel[7] + 0x30;
        resultg = 0;
        resultp = 0;
        resultg = FindFirst(filelabel, 0x2F, &rec);
        while (resultp == 0) {
            resultp = FindNext(&rec);
        }
    } // resultg
    gpxcnt = 0;
    while (gpxcnt < 8) {
        filelabel[gpxcnt + 16] = rec.filename[gpxcnt];
        gpxcnt++;
    } // copy time for display
    if (resultg == 0) { // file was found
        gpslog = 1;
        logindex = 320; // start of track log area
        distance = 0.0; // reset track odometer 
        pointer = FSfopen(rec.filename, "r"); // open for reading
        FSfseek(pointer, 326, SEEK_SET); // seek before first track point
        // read gpx file in to ram track log here
        gpxcnt = 356; // reset file position counter
        while (gpxcnt < rec.filesize) { // until end of file
            imagebuffer[0] = 0x00; // reset field buffer
            configindex = 2; // reset field search index
            int gpflag = 0; // field found flag
            while (gpflag == 0 && gpxcnt < rec.filesize) {
                FSfread(gpxchar, 1, 1, pointer); // read file one byte at a time
                imagebuffer[configindex] = gpxchar[0];
                configindex++;
                gpxcnt++;
                if (imagebuffer[configindex - 1] == 0x3C && imagebuffer[configindex - 2] == 0x3E) {
                    gpflag = 1;
                } // find "><"
            } // gpflag
            if (gpflag == 1) { // if search did not end at the end of the file
                gpsignflag = 0; // reset value sign flag
                configindex = configindex - 4; // reverse to the last lon value character
                gpslon = parsegpxprivatea();
                if (imagebuffer[configindex] == 0x2D) {
                    gpsignflag = 1;
                } // value is negative
                if (imagebuffer[configindex] != 0x22 && imagebuffer[configindex] != 0x2D) { // "" or -"
                    gpslon = gpslon + (100.0 * (imagebuffer[configindex] - 0x30));
                    configindex--;
                } // imagebuffer
                if (imagebuffer[configindex] == 0x2D) {
                    gpsignflag = 1;
                } // value is negative
                if (gpsignflag == 1) {
                    gpslon = 0.0 - gpslon;
                } // reverse the sign for negative value
                parsegpxprivateb();
                parsegpxprivateb();
                gpsignflag = 0; // reset value sign flag
                gpslat = parsegpxprivatea();
                if (imagebuffer[configindex] == 0x2D) {
                    gpsignflag = 1;
                } // value is negative
                if (gpsignflag == 1) {
                    gpslat = 0.0 - gpslat;
                } // reverse the sign for negative value
                // write data to track log memory
                oldlogmins = 99;
                oldlogsecs = 99; // bypass log interval settings
                gpstracklogwrite(); // populate track log memory
            } // gpxcnt
        } // gpflag
        // read filename of the map the gpx file is associated with
        FSfseek(pointer, 0x126, SEEK_SET); // update for version 1.0.216
        FSfread(rec.filename, 1, 8, pointer); // read map name
        int fnamecnt = 0;
        while (rec.filename[fnamecnt] != 0x23) {
            fnamecnt++;
        } // find hash character
        rec.filename[fnamecnt] = 0x00; // terminate string
        FSfclose(pointer); // close gpx file
        int messtimer = 1420; // message display timer
        while (messtimer > 1) { // update for version 1.0.174
            oldlogsecs = 100;
            gpxmessagescreen(1);
            delay_gen(10); // message screen frame delay
            shiftbytein();
            if (joyleft == 1) { // cancel append track log
                logindex = 320;
                gpslog = 0;
                messtimer = 0;
                gpslon = gplon;
                gpslat = gplat; // update for version 1.0.217
            } // cancel
            if (joyright == 1) { // confirm append track log
                messtimer = 0;
            } // joyright
            messtimer--;
        } // messtimer
        pff = 0; // update for version 1.0.174
    } else { // file was not found
        debugout(25);
    } // resultg
    FSchdir(".."); // go back to root directory
}
//
// private function a for the readgpx function

float parsegpxprivatea() { // update for version 1.0.172
    float outfl = 0.0;
    float gpmultiplier = 0.000001; // reset float multiplier
    int gpmultcnt = 0;
    while (gpmultcnt < 6) {
        outfl = outfl + (gpmultiplier * (imagebuffer[configindex] - 0x30));
        configindex--; // parse six decimal place digits
        gpmultiplier = gpmultiplier * 10.0;
        gpmultcnt++;
    } // gpmultcnt
    configindex--; // skip decimal point
    outfl = outfl + (1.0 * (imagebuffer[configindex] - 0x30));
    configindex--; // read in least significant integer digit
    if (imagebuffer[configindex] != 0x22 && imagebuffer[configindex] != 0x2D) { // "" or -"
        outfl = outfl + (10.0 * (imagebuffer[configindex] - 0x30));
        configindex--;
    } // imagebuffer
    return (outfl);
}
//
// private function b for the readgpx function

void parsegpxprivateb() { // update for version 1.0.172
    while (imagebuffer[configindex] != 0x22) {
        configindex--;
    }
    configindex--;
}
//
// copy gpx file name to text string buffer

void copygpxfilename() { // update for version 1.0.171
    sprintf(filelabel, "%02d%02d%02d%02d.GPX", gpshour, gpsminute, gpsday, gpsmonth);
}
//
// percentage track log capacity gauge calculation

void trackloggauge() { // update for version 1.0.214
    formatbufa = logindex - 320;
    formatbufa = (formatbufa / 7216) * 100;
}
//
// finish the map sniffer calculations that
// that can wait until after the map render

void mapsnifferfinish() { // update for version 1.0.160
    fopbearing = getbearing(msfoplat, msfoplon); // bearing for line fop
    if (fopbearing < 180) { // reverse bearing
        fopbearing = fopbearing + 180;
    } else {
        fopbearing = fopbearing - 180;
    } // fopbearing
    fopdistancekm = getdistance(msfoplat, msfoplon); // km distance to fop
    closebearing = getbearing(mspointlat, mspointlon); // bearing for point
    if (closebearing < 180) { // reverse bearing
        closebearing = closebearing + 180;
    } else {
        closebearing = closebearing - 180;
    } // closebearing
}
//
// calculate screen coordinates for map sniffer
// bearing lines feature, print the lines,
// and draw circles over the nearest points

void mapsnifferscrpnt(float inlat, float inlon) { // update for version 1.0.161
    if (inlat < 200.0) { // update for version 1.0.165
        screenx = (inlon - screentllon) / pointval; // translate to screen coordinates
        screeny = (screentllat - inlat) / pointval; //
        if (screenx < 128 && screeny < 64) { // only if on screen
            if (screenx > -1 && screeny > -1) { //
                cx = screenx;
                cy = screeny;
                drawcircle(); // draw circle
            }
        } // screenx-screeny
        if (logflipflop == 0) { // optionally flash the lines
            int bx = 64; // update for version 1.0.163
            if (screenx < 64) {
                bx--;
            } // manual interpolation for current position
            int by = 31; // screen coordinates are really x63.5 y31.5.
            if (screeny > 31) {
                by++;
            } //
            drawline(bx, by, screenx, screeny); // draw bearing line
        } // logflipflop
    } // inlat
}
//
// print map sniffer bearing string to framebuffer

void printbearing(int pbearing) { // update for version 1.0.204
    sprintf(text, "BEARING: %3d", pbearing);
    drawtextvarwidthcentered();
    drawdegreessymbol();
}
//
// print map sniffer distance string to framebuffer

void printdistance() { // update for version 1.0.211
    int chardcnt = 11;
    sprintf(text, "DISTANCE: ");
    if (locale != 0) { // convert to miles
        formatbufa = formatbufa * 0.621371;
    } // locale
    sprintf(text + 10, "%4.2f", formatbufa);
    while (text[chardcnt] != 0x2E) {
        chardcnt++;
    } // find decimal point
    if (formatbufa < 10.0) {
        chardcnt++;
    }
    if (formatbufa < 100.0) {
        chardcnt = chardcnt + 2;
    }
    text[chardcnt] = 0x20;
    chardcnt++; // space character
    if (locale == 0) { // for kilometers
        text[chardcnt] = 0x6B;
        text[chardcnt + 1] = 0x6D; // km
    } else { // or miles
        text[chardcnt] = 0x6D;
        text[chardcnt + 1] = 0x69; // mi
    } // locale
    text[chardcnt + 2] = 0x00; // terminate string
    drawtextvarwidthcentered(); // print string
}
//
// check if a nearest point or line was found

int mapsniffercheckfound() { // update for version 1.0.165
    int wasfound = 0;
    int wascnt = 0;
    while (wascnt < 5) { // check for default label
        if (text[wascnt] != 0x2D) {
            wasfound = 1;
        }
        wascnt++;
    } // wascnt
    return (wasfound);
}
//
// check for null string, filter undefined characters
// for the bold font and print the string

void mapsniffercheckvoid() { // update for version 1.0.204
    text[18] = 0; // limit size of existing display label
    if (text[0] == 0) { // check for null string
        sprintf(text, "NO LABEL");
    } // text
    ty = 28;
    drawtextvarwidthcentered();
}
//
// check for the default map sniffer label
// and reset bearings for display if void

void mapsniffercheckdefault() { // update for version 1.0.181
    int chkcnt = 0;
    int fdefault = 1;
    int ldefault = 1;
    while (chkcnt < 5) {
        if (foplabel[chkcnt] != 0x2D) {
            fdefault = 0;
        }
        if (idtext[chkcnt + 100] != 0x2D) {
            ldefault = 0;
        }
        chkcnt++;
    } // chkcnt
    if (fdefault != 0) {
        fopbearing = 0;
        debugout(27);
    }
    if (ldefault != 0) {
        closebearing = 0;
        debugout(28);
    }
}
//
// draw graphic point of interest icon over a gps map
// this is a solid graphic that draws both the set and
// clear pixels over whatever is underneath the icon.

void drawpoiicon(int poitype) { // update for version 1.0.180
    int tcnt = 0;
    int tindex = 0;
    int poiy = y - 4; // update for version 1.0.183
    int poix = x;
    x = x - 3;
    tindex = poitype * 7;
    while (tcnt < 7) {
        int bitcnt = 0;
        while (bitcnt < 8) {
            bitcnt++;
            y = poiy + bitcnt;
            if ((poiicons[tindex + tcnt] & (0b10000000 >> bitcnt)) != 0) {
                setpixelimage();
            } else {
                clearpixelimage();
            }
        } // bitcnt
        tcnt++;
        x++;
    } // tcnt
    x = poix;
    y = poiy + 4; // restore coordinates
}
//
// display frame counter function for performance testing
// can also be used to display the last error status

void framecounter() { // update for version 1.0.185
    int fbclrcnt = 57;
    while (fbclrcnt < 71) { // clear background area
        framebuffer[fbclrcnt] = 0;
        fbclrcnt++;
    } // fbclrcnt
    char btext[3] = {text[0], text[1], text[2]}; // back up text string
    if (serialenabled == 4) { // update for version 1.0.199
        sprintf(text, "%02d", dframecount); // frame counter
    } else { // display error output
        sprintf(text, "%02d", lasterror); // last error
    } // serial enabled
    fbclrcnt = ty; // backup ty variable
    ty = 0;
    drawtextcentered();
    dframecount++;
    if (dframecount > 99) {
        dframecount = 0;
    }
    text[0] = btext[0]; // restore text string
    text[1] = btext[1]; //
    text[2] = btext[2]; //
    ty = fbclrcnt; // restore ty variable
}
//
// This function used to calculate the foot of the perpendicular
// (shortest distance between a point and a line segment)
// was initially found here: http://blog.csharphelper.com/2010/03/26/
// find-the-shortest-distance-between-a-point-and-a-line-segment-in-c.aspx
// This was a C# function that has been ported to C, and size optimised
// for this microcontroller implementation by Brek Martin 2017.
// calculate the foot of the perpendicular for polyline segments
// calculate the minimum distance between the point and line segment.

float footperpendiculardistance() { // update for version 1.0.161
    float p1X = filelon; // current line segment point
    float p1Y = filelat; //
    float p2X = foplon; // previous line segment point
    float p2Y = foplat; // will be written to as output
    float ptX = gpslon; // current position point
    float ptY = gpslat; //
    float pcX, pcY; // closest point on line output
    float dx = p2X - p1X; //
    float dy = p2Y - p1Y; //
    float t = ((ptX - p1X) * dx + (ptY - p1Y) * dy) / (dx * dx + dy * dy);
    if (t < 0 || (dx == 0 && dy == 0)) {
        pcX = p1X;
        pcY = p1Y;
        dx = ptX - p1X;
        dy = ptY - p1Y;
    } else if (t > 1) {
        pcX = p2X;
        pcY = p2Y;
        dx = ptX - p2X;
        dy = ptY - p2Y;
    } else {
        pcX = p1X + t * dx;
        pcY = p1Y + t * dy;
        dx = ptX - pcX;
        dy = ptY - pcY;
    }
    foplat = pcY;
    foplon = pcX; // fop coordinates
    return (fabs(dx * dx + dy * dy)); // fop distance
}
//
// fast rotating cube using pre-calculated points
// cube is fixed size and can be drawn at offsets
// code size optimisation for this implementation
// rotating integer cube by Brek Martin 2013-2018.

void drawcube(int intx, int inty) { // update for version 1.0.190
    int ix = 0; // counter
    int xcoords[9]; // cube point arrays
    int ycoords[9]; //
    unsigned char addr = 0; // array index
    int xata = 0; // array buffer
    int xy1 = 0; // line coordinates
    int xy0 = 0; //
    int x1 = 0; //
    int x0 = 0; //
    for (ix = 1; ix < 9; ++ix) {
        addr = lxa + (ix - 1);
        xata = cube[addr] + 1;
        xcoords[ix] = xata + intx;
        addr = lxa + (ix + 7);
        xata = cube[addr] + 3;
        ycoords[ix] = xata + inty;
    } // ix
    ix = 0;
    while (ix < 12) { // draw the lines
        x0 = xcoords[cubexpoint[ix]];
        xy0 = ycoords[cubexpoint[ix]];
        x1 = xcoords[cubeypoint[ix]];
        xy1 = ycoords[cubeypoint[ix]];
        drawline(x0, xy0, x1, xy1);
        ix++;
    } // ix
}
//
// draw a dynamic demo display of rotating cubes
// some variables should be reset prior to using

void drawcubedisplay() { // update for version 1.0.191
    int cubedy = 0;
    while (cubedy < 64) {
        drawcube(-1, cubedy + lyc); // draw cube a
        drawcube(41, cubedy + lyc); // draw cube c
        lxb++;
        if (lxb > 15) { // slow down cube rotation
            lxa = lxa + 16; // rotate cube
            if (lxa > 239) {
                lxa = 0;
            } // check array limit
            lxb = 0;
        } // lxb
        drawcube(83, cubedy + lyc); // draw cube e
        cubedy = cubedy + 20; // next vertical position
    } // cubedy
    cubedy = -40;
    while (cubedy < 24) {
        drawcube(20, cubedy + (20 - lyc)); // draw cube b
        lxb++;
        if (lxb > 15) { // slow down cube rotation
            lxa = lxa + 16; // rotate cube
            if (lxa > 239) {
                lxa = 0;
            } // check array limit
            lxb = 0;
        } // lxb
        drawcube(104, cubedy + (20 - lyc)); // draw cube f
        drawcube(62, cubedy + (20 - lyc)); // draw cube d
        cubedy = cubedy + 20; // next vertical position
    } // cubedy
    if (lxb == 0 || lxb == 7) {
        lyc--; // move cubes vertically for next frame
        if (lyc < -19) {
            lyc = 0;
        }
    } // lxb
}
//
// stereo peak level for graphic bargraph meters
// adjusted for peaks to use less ram and time

void stereopeaklevels() { // update for version 1.0.189
    unsigned int vulevel;
    unsigned int ivu;
    unsigned int vubuffer;
    unsigned char vsvala;
    unsigned char vsvalb;
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x07, 0xC0, 0x15); // update for version 1.0.193
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    for (ivu = 0; ivu < 2; ivu++) { // for each channel
        VSXCS = 0; // select vs1003 instruction    
        WriteSPIManual(0x03); // read
        WriteSPIManual(0x06); //
        vsvala = ReadSPIManual(); // read dac value ram
        vsvalb = ReadSPIManual(); //
        VSXCS = 1; // deselect vs1003 instruction
        vubuffer = vsvala;
        vubuffer = vubuffer << 8;
        vubuffer = vubuffer + vsvalb;
        vulevel = abs((int) (vubuffer));
        vulevel = vulevel / 256; // scale the value here
        if (vulevel > vuneedles[ivu]) {
            vuneedles[ivu] = vulevel;
        } else {
            if (vuneedles[ivu] > 5) { // spring constant value
                vuneedles[ivu] = vuneedles[ivu] - 5; // spring constant value
            } else {
                if (vuneedles[ivu] > 0) {
                    vuneedles[ivu]--;
                }
            } // vuneedles
        } // vulevel
    } // ivu
    while (VSDREQ == 0) {
        delay_gen(0);
    }
}
//
// stereo root square level for graphic analogue meters
// this method is by Henrik of VLSI Solutions

void stereolevelsrs() { // update for version 1.0.191
    int vulevel;
    unsigned int ivu;
    unsigned char vsvala;
    unsigned char vsvalb;
    while (VSDREQ == 0) {
        delay_gen(0);
    } // dac output address
    vscommand(0x07, 0xC0, 0x15); // update for version 1.0.193
    lrPhase = (lrPhase + 1) & 7;
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    for (ivu = 0; ivu < 2; ivu++) { // for each channel
        VSXCS = 0; // select vs1003 instruction    
        WriteSPIManual(0x03); // read
        WriteSPIManual(0x06); //
        vsvala = ReadSPIManual(); // read dac value ram
        vsvalb = ReadSPIManual(); //
        VSXCS = 1; // deselect vs1003 instruction
        vulevel = vsvala;
        vulevel = vulevel << 8;
        vulevel = vulevel + vsvalb;
        vulevel = abs(vulevel);
        lrSum[ivu] += ((unsigned long) vulevel * vulevel) >> 4;
        if (!lrPhase) {
            vulevel = (unsigned int) sqrt(lrSum[ivu]);
            vulevel = vulevel / 43; // scale adjustment
            vuneedles[ivu] = vulevel;
            lrSum[ivu] = 0;
        } // lrPhase
    } // ivu
    while (VSDREQ == 0) {
        delay_gen(0);
    }
}
//
// draw a pair of simulated analogue level meters
// and the artist and title info text in window

void drawanaloguemeters() { // update for version 1.0.191
    int chancnt = 0; // channel counter
    cy = 39;
    oy = 39;
    radius = 5; // common for both spindles
    int maxdeflection = 135; // needle maximum deflection
    int stereosample[2] = {0, 0}; // stereo sample buffer
    stereosample[0] = (jpgpixcount[0] + jpgpixcount[1] + vuneedles[0] + vuneedles[0]) / 4;
    stereosample[1] = (jpgpixcount[2] + jpgpixcount[3] + vuneedles[1] + vuneedles[1]) / 4;
    while (chancnt < 2) { // for each stereo channel
        stereosample[chancnt] = (stereosample[chancnt] + 90) / 2; // scale for meter
        rotation = stereosample[chancnt]; // set needle angle
        if (rotation > maxdeflection) {
            rotation = maxdeflection;
        }
        ox = meterpos[chancnt]; // set origin for meter
        x = meterpos[chancnt + 2];
        y = 39; // needle point to be rotated
        pixelrotation(); // rotate the point
        drawline(meterpos[chancnt], 39, x, y); // draw the needle
        cx = meterpos[chancnt]; //
        drawcircle(); // draw left meter spindle
        drawroundedrectangle(1 + (chancnt * 64), 0, 61, 50); // draw meter border
        chancnt++; //
    } // chancnt
    chancnt = 0; //
    while (chancnt < 2) { // for each stereo cnannel
        x = meterpos[chancnt] + radius; // bottom part of needle
        y = 39;
        rotation = stereosample[chancnt]; // set left needle angle
        if (rotation > maxdeflection) {
            rotation = maxdeflection;
        }
        ox = meterpos[chancnt]; // set the origin
        pixelrotation(); // rotate the point
        drawline(meterpos[chancnt], 39, x, y); // finish the needle
        chancnt++; // next channel
    } // chancnt
    tx = eyedeethreepos + 2;
    ty = 54; // text coordinates
    idtext[133] = 0; // limit size for text buffer
    sprintf(text, idtext); // copy string to text buffer
    drawtext(); // draw the text
    drawroundedrectangle(0, 52, 127, 10); // draw text border
}
//
// request mono sample for time announcement display
// stereo channels are read and combined for mono
// this scales the mono value for the voice samples

void monolevelsample() { // update for version 1.0.191
    unsigned int vulevel;
    unsigned int ivu;
    unsigned int vubuffer;
    unsigned char vsvala;
    unsigned char vsvalb;
    int vuhistory[2];
    while (VSDREQ == 0) {
        delay_gen(0);
    } // dac output address
    vscommand(0x07, 0xC0, 0x15); // update for version 1.0.193
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    for (ivu = 0; ivu < 2; ivu++) { // for each channel
        VSXCS = 0; // select vs1003 instruction    
        WriteSPIManual(0x03); // read
        WriteSPIManual(0x06); //
        vsvala = ReadSPIManual(); // read dac value ram
        vsvalb = ReadSPIManual(); //
        VSXCS = 1; // deselect vs1003 instruction
        vubuffer = vsvala;
        vubuffer = vubuffer << 8;
        vubuffer = vubuffer + vsvalb;
        vulevel = abs((int) (vubuffer));
        vulevel = vulevel / 792; // scale for display
        vuhistory[ivu] = vulevel; // live reading
    } // ivu
    tety = tetx; // update for version 1.0.192
    tetx = (vuhistory[0] + vuhistory[1]) / 2; // combine for mono
    tetx = (tetx + tety + tetx + tetx + tety) / 4; // update for version 1.0.192
    while (VSDREQ == 0) {
        delay_gen(0);
    }
}
//
// graphic display for time of day announcement

void announcementdisplay() { // update for version 1.0.196
    unsigned int ampbuf = 0; // mono amplitude buffer
    int blitcnt = 0; // blit memory counter
    int circleworkx = 0; // x work buffer
    int circleend = 0; // work buffer
    SD_CS = 1; // deselect sd card
    SPISTAT = 0; //
    monolevelsample(); // get dac sample value
    SPISTAT = 0x8000; // select sd card
    SD_CS = 0; //
    delay_gen(0); // safety delay
    cx = 15;
    cy = 18; // same coordinates as clock face
    ampbuf = tetx; // copy mono amplitude
    if (ampbuf < 15) { // scale the circle for display
        radius = ampbuf; // copy the scaled radius
        if (ampbuf < oldtetdir) { // update for version 1.0.195
            oldtetdir = ampbuf; //
            oldtetdir--;
            ampbuf = oldtetdir; //
        } // ampbuf
    } else { // limit the circle size
        radius = 14; // maximum radius
    } // ampbuf
    if (ampbuf > 6) { // lower circle size limit
        blitcnt = 0; // blit sprite from image buffer
        int radbuf = 7; // update for version 1.0.205
        int blitoff = 512;
        if (radius > 10) {
            radbuf = 11;
            blitoff = 0;
        }
        circleworkx = 90 - ((radius - radbuf) * 30); // x start point 
        circleend = circleworkx + 31;
        while (circleworkx < circleend) {
            framebuffer[blitcnt] = imagebuffer[circleworkx + blitoff];
            framebuffer[blitcnt + 128] = imagebuffer[circleworkx + 128 + blitoff];
            framebuffer[blitcnt + 256] = imagebuffer[circleworkx + 256 + blitoff];
            framebuffer[blitcnt + 384] = imagebuffer[circleworkx + 384 + blitoff];
            circleworkx++;
            blitcnt++;
        } // circleworkx
        drawcircle(); // draw extra inside circle
    } // ampbuf
    x = 0;
    y = 0; // clear graphics artifacts
    while (x < 28) {
        clearpixel();
        x++;
    }
    x = 0;
    y++;
    while (x < 28) {
        clearpixel();
        x++;
    }
    if (playing != 0) {
        tetdir = 62;
    } // timer to extend display
}
//
// sample audio background noise for random number seeds
// sets the vs1003b chip to recording mode and retrieves
// input sample data from the audio line input whether
// anything is connected to the audio line input or not
// this function has to software reset the vs1003b chip
// when done, and potentially makes up to five attempts
// this is not tested for normal use in audio sampling

unsigned int noiseforrandom() { // update for version 1.0.192
    unsigned char vsvala = 0;
    unsigned char vsvalb = 0;
    unsigned int sambuff = 0;
    unsigned int ranbuff = 0;
    unsigned int numword = 0;
    unsigned int exetime = 0;
    unsigned int vsreset = 1024;
    int resetretries = 5;
    SD_CS = 1; // deselect sd card
    SPISTAT = 0; //
    delay_gen(0); // update for version 1.0.196
    while (VSDREQ == 0 && vsreset > 0) {
        delay_gen(10);
        vsreset--;
    } // test for ready
    if (vsreset == 0) { // vs1003b chip was not ready
        lasterror = 34;
        debugflash();
        SPISTAT = 0x8000;
        SD_CS = 0; // select sd card
        return (0xDEAD); // return with fake random value
    } // vsreset
    vscommand(0x0B, 0xFE, 0xFE); // write lowest output volume
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x0C, 0x00, 0x00); // write default divider value
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x0D, 0x04, 0x00); // set gain to one
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x00, 0x58, 0x04); // enable input sampling
    // get sample noise floor data
    while (exetime < 4) { // stay in loop timer
        numword = 0; // reset number of words ready
        while (numword == 0) { // wait for data ready
            while (VSDREQ == 0) {
                delay_gen(0);
            }
            VSXCS = 0;
            WriteSPIManual(0x03); // read
            WriteSPIManual(0x09); // hdat1
            vsvala = ReadSPIManual(); // read ready status
            vsvalb = ReadSPIManual(); //
            VSXCS = 1;
            numword = vsvala;
            numword = numword << 8;
            numword = numword + vsvalb;
        } // numword
        while (numword > 0) { // read sample data
            while (VSDREQ == 0) {
                delay_gen(0);
            }
            VSXCS = 0;
            WriteSPIManual(0x03); // read
            WriteSPIManual(0x08); // hdat0
            vsvala = ReadSPIManual(); // read sample data
            vsvalb = ReadSPIManual(); //
            VSXCS = 1;
            sambuff = vsvala;
            sambuff = sambuff << 8;
            sambuff = sambuff + vsvalb;
            ranbuff = ranbuff + sambuff; // add to random
            numword--;
        } // numword
        masterlongrandom = masterlongrandom + sambuff;
        exetime++; // increment loop counter
    } // exetime
    // restore registers and software reset
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    vscommand(0x0D, 0x00, 0x00); // reset control register
    // verify the record mode was set
    vsreadmode(); // update for version 1.0.199
    vsvala = ReadSPIManual(); // to verify
    vsvalb = ReadSPIManual(); // setting
    VSXCS = 1;
    if (vsvala != 0x58) {
        lasterror = 30;
    } // record mode was never written
    // attempt software reset up to five times
    while (resetretries > 0) { // possible retries
        while (VSDREQ == 0) {
            delay_gen(10);
        }
        vscommand(0x00, 0x08, 0x04); // software reset
        while (VSDREQ == 0) {
            delay_gen(10);
        }
        vscommand(0x00, 0x08, 0x00); // set mode for normal operation
        // verify the reset occurred
        vsreadmode(); // update for version 1.0.199
        vsvala = ReadSPIManual(); // to verify reset
        vsvalb = ReadSPIManual(); //
        VSXCS = 1;
        sambuff = vsvala;
        sambuff = sambuff << 8;
        sambuff = sambuff + vsvalb;
        if (sambuff == 0x0800) { // break retry loop for success
            resetretries = 1; // will decrement to zero and break
        } else { // need to retry the reset
            lasterror = 29;
            debugflash(); // error for each failed attempt   
        } // sambuff
        resetretries--; // decrement the retry counter
    } // resetretries
    while (VSDREQ == 0) {
        delay_gen(8);
    }
    vscommand(0x0B, 0x0A, 0x0A); // write intro output volume
    while (VSDREQ == 0) {
        delay_gen(0);
    }
    SPISTAT = 0x8000; // select sd card
    SD_CS = 0; //
    delay_gen(2); // safety delay
    return (ranbuff); // return value
}
//
// program memory optimisation function

void vsreadmode() { // update for version 1.0.199
    while (VSDREQ == 0) {
        delay_gen(10);
    }
    VSXCS = 0;
    WriteSPIManual(0x03); // read
    WriteSPIManual(0x00); // mode
}
//
// main menu panel movement spring physics
// 0 = reset, 1 = menu up, 2 = menu down.

void menuspringphysics(int mendir) { // update for version 1.0.193
    int mencnt = 0; // local loop counter
    tetx++; // used as a menu movement counter
    if (mendir == 1) { // joystick down - menu panels up (-)
        while (mencnt < 5) {
            springmenuvars[mencnt] = (menuvars[mencnt] + tetx) - mpspringtable[tetx];
            mencnt++;
        } // mencnt
    } // mendir
    if (mendir == 2) { // joystick up - menu panels down (+)
        while (mencnt < 5) {
            springmenuvars[mencnt] = (menuvars[mencnt] - tetx) + mpspringtable[tetx];
            mencnt++;
        } // mencnt
    } // mendir
    if (mendir == 0) { // re-cache menu positions and reset movement count
        while (mencnt < 5) {
            springmenuvars[mencnt] = menuvars[mencnt];
            mencnt++;
        }
        tetx = 0;
    } // mendir
}
//
// flash the led backlight for general testing
// calls can be inserted temporarily for debug.
// if called by the audio noise floor function,
// the vs1003b chip might be held in reset for
// too long by the hardware RC reset circuit,
// and need the capacitor or resistor adjusted.

void debugflash() { // update for version 1.0.196
    int dbflash = 0;
    while (dbflash < 20) {
        delay_gen(30);
        LED_PIN = 0; // backlight off
        delay_gen(30);
        LED_PIN = 1; // backlight on
        dbflash++;
    } // dbflash
}

